package rps.com.evgeny.rps;

import org.junit.Test;
import rps.com.evgeny.rps.services.CarPlateManager;
import rps.com.evgeny.rps.utils.StringUtils;

import static org.junit.Assert.assertTrue;

public class CarPlateManagerTest {
    CarPlateManager carPlateManager;

    @Test
    public void isNumberPlaceTest() {
        carPlateManager = CarPlateManager.getInstance();
        String plateNumber = "A123bb19";
        assertTrue(carPlateManager.isNumberPlace(plateNumber));
    }

    @Test
    public void getNumberPlaceTest() {
        carPlateManager = CarPlateManager.getInstance();
        String plateNumber = "E102CH 102\nB170MM 161";
        carPlateManager.isNumberPlace(plateNumber);
        boolean flag = "Е102СН".equals(carPlateManager.getNumberPlate());
        assertTrue(flag);
    }

    @Test
    public void numberCorrectionTest() {
        carPlateManager = CarPlateManager.getInstance();
        String plateNumber = "0123bb19";
        carPlateManager.isNumberPlace(plateNumber);
        boolean flag = "O123BB".equals(carPlateManager.getNumberPlate());
        assertTrue(flag);
    }

    @Test
    public void getRegionTest() {
        carPlateManager = CarPlateManager.getInstance();
        String plateNumber = "P123PP";
        for (int i=0; i<999; i++) {
            String region = "";
            if (i<10) {
                region += "0";
            }
            region+=i;

            carPlateManager.isNumberPlace(plateNumber+region);
            boolean flag = region.equals(carPlateManager.getRegionPlate());
            assertTrue(flag);
        }
    }

    @Test
    public void getWrongRegionTest() {
        carPlateManager = CarPlateManager.getInstance();
        String plateNumber = "P123PP";
        String rightRegion = "";
        for (int i=1; i<999; i++) {
            String region = "";
            if (i<10) {
                region += "0";
            }
            region+=i;
            rightRegion = region;
            region+= StringUtils.getRandomString();

            carPlateManager.isNumberPlace(plateNumber+region);
            boolean flag = rightRegion.equals(carPlateManager.getRegionPlate());
            if (!flag) {
                flag = Integer.valueOf(carPlateManager.getRegionPlate())>99;
            }
            assertTrue(region + " " + rightRegion + " " + carPlateManager.getRegionPlate(), flag);
        }
    }
}
