package rps.com.evgeny.rps;

import com.google.gson.Gson;

import org.junit.Test;

import rps.com.evgeny.rps.models.network.AlarmWebSocketModel;
import rps.com.evgeny.rps.utils.TimeUtils;

import static junit.framework.Assert.assertTrue;

/**
 * Created by Evgeny on 05.10.2017.
 */

public class TimeHelperTest {
    @Test
    public void testGetToday() {
        assertTrue(TimeUtils.getToday().equals("05.10.2017"));
    }

    @Test
    public void testGetYesterday() {
        assertTrue(TimeUtils.getYesterday().equals("04.10.2017"));
    }

    @Test
    public void testGetWeek() {
        assertTrue(TimeUtils.getWeek().equals("29.09.2017+-+05.10.2017"));
    }

    @Test
    public void testGetMonth() {
        assertTrue(TimeUtils.getMonth().equals("05.09.2017+-+05.10.2017"));
    }

    @Test
    public void testGetQuart() {
        assertTrue(TimeUtils.getQuart().equals("05.07.2017+-+05.10.2017"));
    }

    @Test
    public void testIsNewerTime() {
        assertTrue(TimeUtils.isNewerTime("2017-11-23 23:55:22", "2017-11-23 23:55:16"));
    }

    @Test
    public void testParsingAlarm() {
        String text = "42[\"new alarm\",\n" +
                "{\"Id\":\n" +
                "[\"C1F31946-E7BC-4B61-B7B7-000F41901013\",\"6AEB578E-500B-4A37-8B18-759AD4052AA9\"],\n" +
                "\"DeviceId\":\"6AEB578E-500B-4A37-8B18-759AD4052AA9\",\n" +
                "\"Begin\":\"2018-08-14T14:13:36.983Z\",\n" +
                "\"End\":null,\n" +
                "\"Value\":\"мало карт\",\n" +
                "\"ValueEnd\":\"\",\n" +
                "\"_IsDeleted\":[false,null],\n" +
                "\"Sync\":[\"2018-08-14T15:27:13.533Z\",\"2018-04-17T14:16:16.203Z\"],\n" +
                "\"AlarmColorId\":3,\n" +
                "\"TypeId\":240,\n" +
                "\"Type\":2,\n" +
                "\"Name\":\"Касса Light офис\",\n" +
                "\"Host\":\"192.168.50.27\",\n" +
                "\"User\":\"sa\",\n" +
                "\"Password\":\"02093B34B178C99AA6860B6F943D1800\",\n" +
                "\"Advanced\":null,\n" +
                "\"DataBase\":\"rpsMDBG\",\n" +
                "\"SlaveCode\":\"375731363839FFFF10FF0A\",\n" +
                "\"action\":\"I\",\n" +
                "\"id\":\"c1f31946-e7bc-4b61-b7b7-000f41901013\"}]";
        int start = text.indexOf("{");
        int end = text.lastIndexOf("}")+1;
        String json = text.substring(start, end);
        AlarmWebSocketModel alarm = (new Gson()).fromJson(json, AlarmWebSocketModel.class);
        assertTrue(alarm.id.equals("c1f31946-e7bc-4b61-b7b7-000f41901013"));
    }
}
