package rps.com.evgeny.rps;

import org.junit.Test;

import java.nio.ByteBuffer;

import rps.com.evgeny.rps.nfc.CardData;
import rps.com.evgeny.rps.nfc.CardReader;
import rps.com.evgeny.rps.nfc.CardReaderHelper;
import rps.com.evgeny.rps.nfc.CardReaderResponse;

import static org.junit.Assert.assertTrue;

/**
 * Created by Evgeny on 05.09.2017.
 */

public class CardReaderTest {
    @Test
    public void testHexToString() {
        byte[] array = CardReader.HexStringToByteArray("93B91BFA");
        String str2 = CardReader.ByteArrayToHexString(array);
        assertTrue(str2.equals("93B91BFA".toLowerCase()));
    }

    @Test
    public void testIDClient() {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(1518952262L);
        byte[] array = buffer.array();
        assertTrue(array.length == 8);
    }

    @Test
    public void testReadCard() {
        String card = "8859311f000000000102000100000000000000000000000000000000000000000000000000000000000000000bf83621";
        byte[] rBlock = CardReader.HexStringToByteArray(card);
        CardData cardData = CardReaderHelper.rBlockToData(card);
        cardData.CardId = Long.parseLong("93b91bfa", 16)+"";
        CardReaderResponse response = CardReaderHelper.GetResult(cardData);

        CardData data = CardReaderHelper.GetParam(response);
        byte[] array = CardReaderHelper.dataToWBlock(data);
        String strData = CardReader.ByteArrayToHexString(array);

        int a=0;
        for (int i=0; i<rBlock.length; i++) {
            if (rBlock[i]!=array[i]) {
                a++;
            }
        }
        if (a>0)
            assertTrue("byte[] not equals", false);
        assertTrue("bad write", strData.equals(card));
    }
}
