package rps.com.evgeny.rps.fragments.zone;

import android.databinding.DataBindingUtil;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.activities.AdminMainActivity;
import rps.com.evgeny.rps.databinding.FragmentZoneDevicesBinding;
import rps.com.evgeny.rps.fragments.zone.details.ZoneDetailsFragment;
import rps.com.evgeny.rps.models.event_bus.BackFromControlFragment;
import rps.com.evgeny.rps.models.network.DeviceZoneModel;
import rps.com.evgeny.rps.views.FadeInUpAnimator;

/**
 * Created by Evgeny on 30.05.2018.
 */

public class ZoneDevicesFragment extends DaggerFragment implements ZoneDevicesContract.View {
    FragmentZoneDevicesBinding binding;

    @Inject
    ZoneDevicesContract.Presenter presenter;

    @Inject
    public ZoneDevicesFragment() {}

    private static ZoneDevicesFragment instance;

    public static ZoneDevicesFragment getInstance() {
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_zone_devices, container, false);
        initViews();
        return binding.getRoot();
    }

    void initViews() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        binding.recyclerView.setLayoutManager(layoutManager);

        LinearLayoutManager layoutManager2 = new LinearLayoutManager(getActivity());
        layoutManager2.setReverseLayout(true);
        layoutManager2.setStackFromEnd(false);
        binding.recyclerViewBottom.setLayoutManager(layoutManager2);
        binding.recyclerViewBottom.setItemAnimator(new FadeInUpAnimator());
        binding.recyclerViewBottom.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    MotionEvent e = MotionEvent.obtain(event);
                    binding.recyclerView.dispatchTouchEvent(e);
                    e.recycle();
                } catch (Exception e) {
                    Crashlytics.logException(e);
                }
                return false;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.takeView(this);
        presenter.setPhoneListener();
        FragmentManager fm = getChildFragmentManager();
        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.dropView();
    }

    @Override
    public void setAdapter(RecyclerView.Adapter adapter) {
        binding.recyclerView.setAdapter(adapter);
    }

    @Override
    public void setAdapterBottom(RecyclerView.Adapter adapter) {
        binding.recyclerViewBottom.setAdapter(adapter);
    }

    @Override
    public LayoutInflater getInflater() {
        return LayoutInflater.from(getContext());
    }

    @Override
    public void openMoreDetails(DeviceZoneModel model) {
        ZoneDetailsFragment fragment = new ZoneDetailsFragment();
        fragment.setModel(model);

        addFragment(fragment);
    }

    @Override
    public void showProgressBar(final boolean show) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                binding.progressBar.setVisibility(show?View.VISIBLE:View.GONE);
            }
        });
    }

    @Override
    public void showToast(final int text) {
        if (getActivity()==null || text == R.string.error_call_stop)
            return;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    Toast.makeText(getActivity(), text, Toast.LENGTH_LONG).show();
                } catch (Exception e) {}
            }
        });
    }

    public void addFragment(Fragment fragment) {
        FragmentManager manager = getChildFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.zoneContainer, fragment, "Control");
        transaction.addToBackStack("Control");
        transaction.commit();
    }

    @Override
    public void shadowCallingScreen(boolean show) {
        ((AdminMainActivity)getActivity()).showShadowTalking(show);
    }
}
