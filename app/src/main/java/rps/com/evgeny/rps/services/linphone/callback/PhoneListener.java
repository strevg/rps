package rps.com.evgeny.rps.services.linphone.callback;

import org.linphone.core.LinphoneCall;

/**
 * Created by Evgeny on 07.08.2018.
 */

public interface PhoneListener {
    void onCallStart(String id);
    void onCallConnect(String id);
    void onCallFinish(String id);
    void onIncomingCall(String id);
    void onError(String message);
}
