package rps.com.evgeny.rps.models.person;

import java.util.UUID;

/**
 * Created by Evgeny on 23.09.2017.
 */

public class TransactionModel {
    public UUID Id;
    public UUID DeviceId;
    public String Time;
    public String TimeEntry;
    public String TimeExit;
    public String ZoneAfterId;
    public String ZoneBeforeId;
    public Integer _IsDeleted;
    public String Sync;
    public Integer DeviceTypeID;
    public String Cardnumber;
    public String ClientID;
    public String TarifPlanId;
    public String TariffScheduleId;
    public String Nulltime1;
    public String Nulltime2;
    public String Nulltime3;
    public String TVP;
    public Integer TKVP;
    public String SumOnCardBefore;
    public String Amount;
    public String Balance;
    public String Paid;
    public String SumOnCardAfter;
    public String SumAccepted;
    public UUID ClientGroupId;
    public Integer ClientTypidFC;
    public UUID ZoneId;
    public String LastRecountTime;
    public String TimeOplat;
    public Integer PassageTransactionType;
    public String PlateNumberEntrance;
    public String PlateNumberExit;
    public String PaymentTransactionTypeId;
    public String ChangeIssued;
    public String CachDispenerUp;
    public String CachDispenerDown;
    public String Hopper1;
    public String Hopper2;
    public String BanknotesAccepted;
    public String CoinsAccepted;
    public String BankCardAccepted;
    public String Tariff;
    public String SheduleSwitchTariff;
    public String SheduleReturnTariff;
    public String PoradNumber;
    public String Type;
    public String PaymentType;
    public String DeviceName;
    public Boolean BlackList;
    public String DeviceType;
    public Integer Transit;
    public String ParkingTime;
    public Integer index;
}
