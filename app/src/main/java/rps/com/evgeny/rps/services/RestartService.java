package rps.com.evgeny.rps.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import java.util.ArrayList;

import rps.com.evgeny.rps.models.KeyWarehouse;
import rps.com.evgeny.rps.models.SecurityUser;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.Settings;

public class RestartService extends BroadcastReceiver {
    KeyWarehouse keyStore;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(Consts.TAG, "RestartService");

        keyStore = new KeyWarehouse(context);
        ArrayList<SecurityUser> tokens = keyStore.findAccountsForService(context.getPackageName());

        if (Settings.adminAccount && Settings.isAutoServiceStart && tokens.size()>0) {
            context.startService(new Intent(context, BackgroundControlService.class));
        }
    }
}