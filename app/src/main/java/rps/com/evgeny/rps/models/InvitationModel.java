package rps.com.evgeny.rps.models;

/**
 * Created by Evgeny on 22.09.2017.
 */

public class InvitationModel {
    private String nameAccount;
    private String carNumber;
    private String time;
    private String timeComeIn;
    private String timeComeOut;

    public InvitationModel() {
        this.nameAccount = "Кузнецов Е.В.";
        this.carNumber = "P 123 PP 199";
        this.time = "21.03.2017 20:54";
        this.timeComeIn = "22.03.2017 10:54";
        this.timeComeOut = "23.03.2017 19:54";
    }

    public String getNameAccount() {
        return nameAccount;
    }
    public String getCarNumber() {
        return carNumber;
    }
    public String getTime() {
        return time;
    }
    public String getTimeComeIn() {
        return timeComeIn;
    }
    public String getTimeComeOut() {
        return timeComeOut;
    }
}
