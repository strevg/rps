package rps.com.evgeny.rps.fragments.main;

import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.databinding.FragmentMainAdminBinding;
import rps.com.evgeny.rps.fragments.alarms.AlarmsFragment;
import rps.com.evgeny.rps.fragments.reading.NotReadingFragment;
import rps.com.evgeny.rps.fragments.reading.ReadingFragment;
import rps.com.evgeny.rps.fragments.settings.MenuFragment;
import rps.com.evgeny.rps.fragments.zone.ZoneDevicesFragment;
import rps.com.evgeny.rps.services.linphone.LinphoneManager;
import rps.com.evgeny.rps.utils.ViewUtils;

/**
 * Created by Evgeny on 09.06.2018.
 */

public class MainAdminFragment extends DaggerFragment implements MainAdminContract.View {
    FragmentMainAdminBinding binding;

    @Inject
    MainAdminContract.Presenter presenter;

    AlarmsFragment alarmsFragment;
    ZoneDevicesFragment zoneDevicesFragment;
    ReadingFragment readingFragment;

    @Inject
    public MainAdminFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        alarmsFragment = new AlarmsFragment();
        zoneDevicesFragment = new ZoneDevicesFragment();
        readingFragment = new ReadingFragment();

        LinphoneManager.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main_admin, container, false);
        initViews();
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.bottomNavigation.setCurrentItem(1);
    }

    void initViews() {
        initBottomNavigation();
    }

    void initBottomNavigation() {
        initBottomNavigationItems();

        int activeSize = ViewUtils.convertSpToPixels(10, getActivity());
        int inactiveSize = ViewUtils.convertSpToPixels(10, getActivity());
        binding.bottomNavigation.setTitleTextSize(activeSize, inactiveSize);
        binding.bottomNavigation.setColored(true);
        binding.bottomNavigation.setAccentColor(getResources().getColor(R.color.buttonYellow));

        binding.bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                if (!wasSelected) {
                    switch (position) {
                        case 0:
                            replaceFragment(alarmsFragment);
                            break;
                        case 1:
                            replaceFragment(zoneDevicesFragment);
                            break;
                        case 2:
                            if (!getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_NFC)) {
                                replaceFragment(new NotReadingFragment());
                            } else {
                                replaceFragment(readingFragment);
                            }
                            break;
                        case 3:
                            replaceFragment(new MenuFragment());
                            break;
                    }
                }
                return true;
            }
        });
    }

    void initBottomNavigationItems() {
        AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.menu_tab1, R.drawable.ic_notification, R.color.colorNavigationBackground);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.menu_tab2, R.drawable.ic_apps, R.color.colorNavigationBackground);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem(R.string.menu_tab3, R.drawable.ic_credit_card, R.color.colorNavigationBackground);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem(R.string.menu_tab4, R.drawable.baseline_menu_black_24, R.color.colorNavigationBackground);

        binding.bottomNavigation.addItem(item1);
        binding.bottomNavigation.addItem(item2);
        binding.bottomNavigation.addItem(item3);
        binding.bottomNavigation.addItem(item4);

        binding.bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.takeView(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.dropView();
    }

    void replaceFragment(Fragment fragment) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
