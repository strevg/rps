package rps.com.evgeny.rps.fragments.main;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import rps.com.evgeny.rps.di.ActivityScoped;
import rps.com.evgeny.rps.di.FragmentScoped;

/**
 * Created by Evgeny on 09.06.2018.
 */
@Module
public abstract class MainAdminModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract MainAdminFragment mainAdminFragment();

    @ActivityScoped
    @Binds
    abstract MainAdminContract.Presenter mainAdminPresenter(MainAdminPresenter presenter);
}
