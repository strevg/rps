package rps.com.evgeny.rps.nfc;

import android.content.Context;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.TagLostException;
import android.nfc.tech.MifareClassic;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import java.io.IOException;
import java.lang.ref.WeakReference;

import rps.com.evgeny.rps.MainApplication;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.Settings;

/**
 * Created by Evgeny on 02.09.2017.
 */

public class CardWriter implements NfcAdapter.ReaderCallback {
    private static final String TAG = "CardReader";
    // AID for our loyalty card service.
    private static String SAMPLE_LOYALTY_CARD_AID = "F222222222";
    // ISO-DEP command HEADER for selecting an AID.
    // Format: [Class | Instruction | Parameter 1 | Parameter 2]
    private static String SELECT_APDU_HEADER = "00A40400";
    // "OK" status word sent in response to SELECT AID command (0x9000)
    private static byte[] SELECT_OK_SW = { (byte)0x90, (byte)0x00 };

    private String mifareKeyType = "60";
    private String keyIndex = "1";
    private MifareClassic mifare;
    private String cardKey = "FFFFFFFFFFFF";
    private int cardSector = 1;

    // Weak reference to prevent retain loop. mCardCallback is responsible for exiting
    // foreground mode before it becomes invalid (e.g. during onPause() or onStop()).
    private WeakReference<CardCallback> mCardCallback;
    private static byte[] respBuffer;

    MainApplication app;

    public CardWriter(WeakReference<CardCallback> cardCallback, Context context) {
        app = (MainApplication)context.getApplicationContext();
        mCardCallback = cardCallback;
        cardKey = Settings.model.CardKey;
        cardSector = Integer.valueOf(Settings.model.SectorNumber);
    }

    @Override
    public void onTagDiscovered(Tag tag) {
        mifare =  MifareClassic.get(tag);
        app.showProgressDialog("Запись...");
        if (mifare != null) {
            try  {
                // Connect to the remote NFC device
                String id = GetId();
                String data = ReadData();
                CardData cdata =  CardReaderHelper.rBlockToData(data);
                cdata.CardId = (Long.parseLong(id, 16) + "");

                CardReaderResponse response = CardReaderHelper.GetResult(cdata);

                if (mCardCallback.get() != null)  {
                    mCardCallback.get().onCardReceived(response);
                }
            }  catch (Exception e) {
                Log.e(Consts.TAG, e.getMessage());
                Crashlytics.logException(e);
                app.dismissProgressDialog();
            }
        }
    }

    /**
     * Build APDU for SELECT AID command. This command indicates which service a reader is
     * interested in communicating with. See ISO 7816-4.
     *
     * @param aid Application ID (AID) to select
     * @return APDU for SELECT AID command
     */
    public static byte[] BuildSelectApdu(String aid) {
        // Format: [CLASS | INSTRUCTION | PARAMETER 1 | PARAMETER 2 | LENGTH | DATA]
        return HexStringToByteArray(SELECT_APDU_HEADER + (Integer.parseInt(aid.length() / 2+"", 16)) + aid);
    }

    /**
     * Utility class to convert a byte array to a hexadecimal string.
     *
     * @param bytes Bytes to convert
     * @return String, containing hexadecimal representation.
     */

    public static String ByteArrayToHexString(byte[] bytes) {
        String s = "";
        for (int i = 0; i < bytes.length; i++) {
            s += String.format("%02X", bytes[i]);
        }
        return s;
    }

    /**
     * Utility class to convert a hexadecimal string to a byte string.
     *
     * <p>Behavior with input strings containing non-hexadecimal characters is undefined.
     *
     * @param s String containing hexadecimal characters to convert
     * @return Byte array generated from input
     */
    public static byte[] HexStringToByteArray(String s) {
        int len = s.length();
        if (len % 2 == 1) {
            throw new IllegalArgumentException();
        }
        byte[] data = new byte[len / 2]; //Allocate 1 byte per 2 hex characters
        for (int i = 0; i < len; i += 2) {
            int val, val2;
            // Convert each chatacter into an unsigned integer (base-16)
            try  {
                val = Integer.valueOf(s.toCharArray()[i] + "0", 16);
                val2 = Integer.valueOf("0" + s.toCharArray()[i + 1]+"", 16);
            } catch (Exception e) {
                continue;
            }

            data[i / 2] = (byte)(val + val2);
        }
        return data;
    }


    public String GetId() {
        return ByteArrayToHexString(mifare.getTag().getId());
    }


    public String ReadData() {
        String res="";

        int block = 4 * cardSector;
        try {
            mifare.connect();
            if (mifare.isConnected()) {
                Log.i(Consts.TAG, "card is " + (mifare.isConnected() ? "" : " NOT ") + " connected!");
            }

            if (Authenticate()) {
                Log.i(Consts.TAG, "auth success");

                byte[] readResult = mifare.readBlock(block);
                res += ByteArrayToHexString(readResult);
                Log.i(Consts.TAG, "block №" + block + " read...");

                block++;
                readResult = mifare.readBlock(block);
                res += ByteArrayToHexString(readResult);
                Log.i(Consts.TAG, "block №" + block + " read...");

                block++;
                readResult = mifare.readBlock(block);
                res += ByteArrayToHexString(readResult);
                Log.i(Consts.TAG, "block №" + block + " read...");
                return res;
            }
        } catch (TagLostException e) {
            Crashlytics.logException(e);
            Log.i(Consts.TAG, e.getMessage());
        } catch (IOException e) {
            Crashlytics.logException(e);
            Log.i(Consts.TAG, e.getMessage());
        } catch (Exception e) {
            Crashlytics.logException(e);
            Log.i(Consts.TAG, e.getMessage());
        } finally {
            if (mifare != null) {
                try {
                    mifare.close();
                } catch (IOException e) {
                    Crashlytics.logException(e);
                    Log.i(Consts.TAG, e.getMessage());
                }
            }
        }
        return null;
    }

    public void WriteData(String data) {
        int block = 4 * cardSector;

        try {
            mifare.connect();
            if (Authenticate()) {
                for (int i = 0; i <= 2; i++) {
                    byte[] wblock = HexStringToByteArray(data.substring(i * 32, 32));
                    mifare.writeBlock(block+i, wblock);
                }
            }
        } catch (IOException e) {
            Crashlytics.logException(e);
            Log.i(Consts.TAG, e.getMessage());
        } finally {
            if (mifare != null) {
                try {
                    mifare.close();
                } catch (IOException e) {
                    Crashlytics.logException(e);
                    Log.i(Consts.TAG, e.getMessage());
                }
            }
        }
    }

    public String Send(String apdu) {
        try {
            byte[] payload = mifare.transceive(HexStringToByteArray(apdu));
            return ByteArrayToHexString(payload);
        } catch (IOException e) {
            Crashlytics.logException(e);
            Log.i(Consts.TAG, e.getMessage());
        }
        return null;
    }

    public boolean Authenticate() {
        try  {
            return mifare.authenticateSectorWithKeyA(cardSector, HexStringToByteArray(cardKey));
        } catch (Exception e) {
            Crashlytics.logException(e);
            Log.e(Consts.TAG, e.getMessage());
            return false;
        }
    }

    public void OnTagRemoved() {
        Log.i(Consts.TAG, "removed");
    }

    public interface CardCallback {
        void onCardReceived(CardReaderResponse account);
    }
}

