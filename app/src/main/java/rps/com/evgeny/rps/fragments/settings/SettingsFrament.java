package rps.com.evgeny.rps.fragments.settings;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.databinding.FragmentSettingsBinding;
import rps.com.evgeny.rps.utils.Settings;

public class SettingsFrament extends Fragment {
    FragmentSettingsBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false);
        initView();
        return binding.getRoot();
    }

    void initView() {
        ((AppCompatActivity)getActivity()).setSupportActionBar(binding.toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
        binding.toolbar.setTitle(R.string.menu_settings);

        changePlateRecognition(Settings.isEnabledPlateRecognition);
        binding.switchRecognition.setChecked(Settings.isEnabledPlateRecognition);
        binding.switchRecognition.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Settings.isEnabledPlateRecognition = isChecked;
                Settings.saveBoolean(getActivity(), "isEnabledPlateRecognition", isChecked);
                changePlateRecognition(isChecked);
            }
        });
    }

    void changePlateRecognition(boolean flag) {
        binding.switchRecognition.setText(flag?R.string.switch_on:R.string.switch_off);
        int color = getActivity().getResources().getColor(flag?R.color.colorGreen:R.color.colorGrey);
        binding.switchRecognition.setTextColor(color);
    }
}
