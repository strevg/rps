package rps.com.evgeny.rps.models;

/**
 * Created by Evgeny on 08.09.2017.
 */

public class CardModel {
    public String CardId;
    public String ParkingEnterTime;
    public String LastRecountTime;
    public String TSidFC;
    public String TPidFC;
    public String ZoneidFC;
    public String ClientGroupidFC;
    public String SumOnCard;
    public String LastPaymentTime;
    public String Nulltime1;
    public String Nulltime2;
    public String Nulltime3;
    public String TVP;
    public String TKVP;
    public String ClientTypidFC;
    public String DateSaveCard;
    public String Blocked;
    public String _IsDeleted;
    public String Sync;
    public String ClientId;
    public String CompanyId;
    public String LastPlate;
    public String id;
    public boolean editable;
    public boolean editable_fields;
    public String Name;
    public String CompanyName;
}
