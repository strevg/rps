package rps.com.evgeny.rps.utils;

import android.content.Context;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rps.com.evgeny.rps.MainApplication;
import rps.com.evgeny.rps.models.list.ClientModel;
import rps.com.evgeny.rps.models.list.GroupModel;
import rps.com.evgeny.rps.models.list.TariffModel;
import rps.com.evgeny.rps.models.list.TariffSchedule;
import rps.com.evgeny.rps.models.list.ZoneModel;
import rps.com.evgeny.rps.network.ApiFactory;
import rps.com.evgeny.rps.network.RPSApi;

/**
 * Created by Evgeny on 09.10.2017.
 */

public class ListsUtils {
    private static ListsUtils instance;

    private List<GroupModel> groups;
    private List<ZoneModel> zones;
    private List<TariffModel> tariffs;
    private List<TariffSchedule> tariffSchedules;
    private List<ClientModel> clients;

    private List<onDownloadLists> subsribers;

    MainApplication app;
    RPSApi rpsApi;

    private int countLoadedLists = 0;
    private int countTime = 0;

    private ListsUtils(MainApplication app) {
        this.app = app;
        rpsApi = ApiFactory.getRpsApi(Settings.siteName+"/");
        subsribers = new ArrayList<>();

        loadAllList();
    }

    public void stopProgressDialog(int type, boolean loaded) {
        if (countTime==4) {
            app.dismissProgressDialog();
        } else {
            countTime++;
        }
        if (loaded) {
            notifyCompleted(type);
            countLoadedLists++;
        }
    }

    public boolean hasData() {
        return countLoadedLists==5;
    }

    void loadAllList() {
        app.showProgressDialog("Загрузка с сервера...");
        countLoadedLists = 5;
        countTime = 5;
        if (getGroups().size()==0) {
            loadGroups();
            degrease();
        }
        if (getZones().size()==0) {
            loadZones();
            degrease();
        }
        if (getTariffs().size()==0) {
            loadTariffs();
            degrease();
        }
        if (getTariffSchedules().size()==0) {
            loadTariffSchedules();
            degrease();
        }
        if (getClients().size()==0) {
            loadClient();
            degrease();
        }
    }

    void degrease() {
        countLoadedLists--;
        countTime--;
    }

    public static ListsUtils getInstance(Context context) {
        if (instance == null) {
            instance = new ListsUtils((MainApplication)context);
        } else if (instance.countLoadedLists < 4) {
            instance.loadAllList();
        }
        return instance;
    }

    public int getPosition(int type, Integer value) {
        if (value==null)
            return -1;
        switch (type) {
            case 0: // group
                if (groups==null)
                    return -1;
                for (int i=0; i<groups.size(); i++) {
                    if (groups.get(i).idFC==value)
                        return i;
                }
                break;
            case 1: // zone
                if (zones==null)
                    return -1;
                for (int i=0; i<zones.size(); i++) {
                    if (zones.get(i).IdFC==value)
                        return i;
                }
                break;
            case 2: // tariff
                if (tariffs==null)
                    return -1;
                for (int i=0; i<tariffs.size(); i++) {
                    if (tariffs.get(i).IdFC==value)
                        return i;
                }
                break;
            case 3: // tariff schedule
                if (tariffSchedules==null)
                    return -1;
                for (int i=0; i<tariffSchedules.size(); i++) {
                    if (tariffSchedules.get(i).IdFC==value)
                        return i;
                }
                break;
            case 4: // client
                if (clients==null)
                    return -1;
                for (int i=0; i<clients.size(); i++) {
                    if (clients.get(i).index==value)
                        return i;
                }
                break;
        }
        return -1;
    }

    public List<GroupModel> getGroups() {
        if (groups==null)
            return new ArrayList<>();
        return groups;
    }

    public List<ZoneModel> getZones() {
        if (zones==null)
            return new ArrayList<>();
        return zones;
    }

    public List<TariffModel> getTariffs() {
        if (tariffs==null)
            return new ArrayList<>();
        return tariffs;
    }

    public List<TariffSchedule> getTariffSchedules() {
        if (tariffSchedules==null)
            return new ArrayList<>();
        return tariffSchedules;
    }

    public List<ClientModel> getClients() {
        clients = new ArrayList<>();
        clients.add(new ClientModel("Разовый", 0));
        clients.add(new ClientModel("Постоянный", 1));
        clients.add(new ClientModel("Вездеход", 3));

        return clients;
    }

    public void onAddListener(onDownloadLists listener) {
        subsribers.add(listener);
    }

    public void onRemoveListener(onDownloadLists listener) {
        subsribers.remove(listener);
    }

    void notifyCompleted(int type) {
        for (onDownloadLists listener : subsribers)
            listener.onCompleted(type);
    }
    //------------- network ------------
    void loadGroups() {
        rpsApi.getGroups(ApiFactory.getHeaders()).enqueue(new Callback<List<GroupModel>>() {
            @Override
            public void onResponse(Call<List<GroupModel>> call, Response<List<GroupModel>> response) {
                if (response.isSuccessful())
                    groups = response.body();
                stopProgressDialog(0, true);
            }

            @Override
            public void onFailure(Call<List<GroupModel>> call, Throwable t) {
                Log.e(Consts.TAG, "loadGroup " + t.toString());
                Crashlytics.logException(t);
                stopProgressDialog(0, false);
            }
        });
    }

    void loadZones() {
        rpsApi.getZones(ApiFactory.getHeaders()).enqueue(new Callback<List<ZoneModel>>() {
            @Override
            public void onResponse(Call<List<ZoneModel>> call, Response<List<ZoneModel>> response) {
                if (response.isSuccessful())
                    zones = response.body();
                stopProgressDialog(1, true);
            }

            @Override
            public void onFailure(Call<List<ZoneModel>> call, Throwable t) {
                Log.e(Consts.TAG, "loadZones " + t.toString());
                Crashlytics.logException(t);
                stopProgressDialog(1, false);
            }
        });
    }

    void loadTariffs() {
        rpsApi.getTariffs(ApiFactory.getHeaders()).enqueue(new Callback<List<TariffModel>>() {
            @Override
            public void onResponse(Call<List<TariffModel>> call, Response<List<TariffModel>> response) {
                if (response.isSuccessful())
                    tariffs = response.body();
                stopProgressDialog(2, true);
            }

            @Override
            public void onFailure(Call<List<TariffModel>> call, Throwable t) {
                Log.e(Consts.TAG, "loadTariffs " + t.toString());
                Crashlytics.logException(t);
                stopProgressDialog(2, false);
            }
        });
    }

    void loadTariffSchedules() {
        rpsApi.getTariffsSchedule(ApiFactory.getHeaders()).enqueue(new Callback<List<TariffSchedule>>() {
            @Override
            public void onResponse(Call<List<TariffSchedule>> call, Response<List<TariffSchedule>> response) {
                if (response.isSuccessful())
                    tariffSchedules = response.body();
                stopProgressDialog(3, true);
            }

            @Override
            public void onFailure(Call<List<TariffSchedule>> call, Throwable t) {
                Log.e(Consts.TAG, "loadTariffSchedules " + t.toString());
                Crashlytics.logException(t);
                stopProgressDialog(3, false);
            }
        });
    }

    void loadClient() {
        rpsApi.getClient(ApiFactory.getHeaders()).enqueue(new Callback<List<ClientModel>>() {
            @Override
            public void onResponse(Call<List<ClientModel>> call, Response<List<ClientModel>> response) {
                stopProgressDialog(4, true);
            }

            @Override
            public void onFailure(Call<List<ClientModel>> call, Throwable t) {
                Log.e(Consts.TAG, "loadTariffSchedules " + t.toString());
                Crashlytics.logException(t);
                stopProgressDialog(4, false);
            }
        });
    }

    public interface onDownloadLists {
        void onCompleted(int type);
    }
}
