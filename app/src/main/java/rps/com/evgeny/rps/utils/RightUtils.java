package rps.com.evgeny.rps.utils;

import java.util.UUID;

import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.models.person.Role;

/**
 * Created by Evgeny on 21.10.2017.
 */

public class RightUtils {
    public static boolean hasRights(int id) {
        switch (id) {
            case R.id.timeComeIn:
                return checkRight(UUID.fromString("1E1DB7FB-91B7-48CC-ADD8-7A7B15765765"));
            case R.id.timeRecalculation:
                return checkRight(UUID.fromString("ED0C2CCA-AF8B-4494-8C1A-8BF0B2EC2F05"));
            case R.id.tvp:
                return checkRight(UUID.fromString("61BB4395-72C3-417B-A4B8-6635F9EC56A3"));
            case R.id.tkvp:
                return checkRight(UUID.fromString("01847E21-F107-4414-9CCC-568703AE912A"));
            case R.id.nullVP:
                return checkRight(UUID.fromString("3CDC1E28-9598-4DBB-A801-B09E304C2F89"));
            case R.id.nullKVP:
                return checkRight(UUID.fromString("A1ADFD2E-E5F2-484C-9416-5C671D15E645"));
            case R.id.nullAbonement:
                return checkRight(UUID.fromString("6B132D01-4E4A-4732-A8F2-AA0C7582D8B6"));
            case R.id.sumCard:
                return checkRight(UUID.fromString("C43AC4B6-E98B-48A2-B66C-8D005FF2A1A0"));
            case R.id.zoneSpinner:
                return checkRight(UUID.fromString("6FAD3F37-0C45-40B8-8B49-CFCD8CF088F3"));
            case R.id.groupSpinner:
                return checkRight(UUID.fromString("4A851EE1-CF9D-4049-9B8E-3A097D2BA71C"));
            case R.id.clientSpinner:
                return checkRight(UUID.fromString("8B08D89B-BBE0-4DED-AFE4-5EF042773946"));
            case R.id.tariffScheduleSpinner:
                return checkRight(UUID.fromString("3AA80A84-2B0D-4641-ACDA-EBA1E2074FD3"));
            case R.id.tariffSpinner:
                return checkRight(UUID.fromString("6A065C35-9FBB-47F2-86A8-5C5C51ABBF20"));
            case 111: //Блокировка карты
                return checkRight(UUID.fromString("A9DC4617-DB5A-4AE3-B285-A29CB3AA75B2"));
            case 222: //Привязать клиента/компанию
                return checkRight(UUID.fromString("DC59C9F0-E5A2-4C9A-ADEA-5D55E6371F4C"));
            case 333: //Просмотр транзакций по карте
                return checkRight(UUID.fromString("0DEC53E9-9CA3-4B74-BBEC-9DBB086030F1"));
        }
        return false;
    }

    public static boolean checkRight(UUID uuid) {
        if (Settings.currentUser==null || Settings.currentUser.role==null)
            return false;
        for (Role.Right right : Settings.currentUser.role.ActiveRights) {
            if (right.RightId.equals(uuid) && right.Active == 1)
                return true;
        }
        return false;
    }
}
