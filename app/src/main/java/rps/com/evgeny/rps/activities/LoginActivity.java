package rps.com.evgeny.rps.activities;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rps.com.evgeny.rps.MainApplication;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.databinding.LoginActivityBinding;
import rps.com.evgeny.rps.models.KeyWarehouse;
import rps.com.evgeny.rps.models.SecurityUser;
import rps.com.evgeny.rps.models.User;
import rps.com.evgeny.rps.models.network.WebSocketModel;
import rps.com.evgeny.rps.models.person.UserModel;
import rps.com.evgeny.rps.network.ApiFactory;
import rps.com.evgeny.rps.network.RPSApi;
import rps.com.evgeny.rps.services.BackgroundControlService;
import rps.com.evgeny.rps.services.RpsWebSocket;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.Settings;
import rps.com.evgeny.rps.utils.StringUtils;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    public static final int PERMISSIONS_REQUEST_RECORD_AUDIO = 100;
    MainApplication app;
    LoginActivityBinding binding;
    KeyWarehouse keyStore;
    RPSApi rpsApi;
    String lastChar = " ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.login_activity);

        initViews();

        if (Settings.sessionId!=null)
            autoLogin.start();

        requestPermission();

        //initTest();
    }

    void initViews() {
        app = (MainApplication)getApplication();
        binding.loginButton.setOnClickListener(this);
        binding.domenButton.setOnClickListener(this);
        /*
        binding.username.setText("1234567890");
        binding.password.setText("1111");
        binding.site.setText("office.r-p-s.ru");
        */
        /*binding.username.setText("1111111111");
        binding.password.setText("1111");
        binding.site.setText("empire.r-p-s.ru");*/

        binding.username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                int digits = binding.username.getText().toString().length();
                if (digits > 1)
                    lastChar = binding.username.getText().toString().substring(digits-1);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int digits = binding.username.getText().toString().length();
                if (!lastChar.equals(" ")) {
                    if (digits == 3 || digits == 7 || digits == 10) {
                        binding.username.append(" ");
                    }
                }
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        binding.username.setFilters(new InputFilter[]{new InputFilter.LengthFilter(13)});
        binding.domenButton.setText(Settings.getDomainServer());

        keyStore = new KeyWarehouse(app);
    }

    void requestPermission() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && ((checkSelfPermission(Manifest.permission.RECORD_AUDIO)!= PackageManager.PERMISSION_GRANTED))){
            requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO},
                    PERMISSIONS_REQUEST_RECORD_AUDIO);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_RECORD_AUDIO) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, R.string.hint_record_audio_granted, Toast.LENGTH_SHORT).show();
            } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                Toast.makeText(this, R.string.hint_record_audio_denied, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.domenButton) {
            showDialog();
        } else {
            if (isValidValues()) {
                login();
            }
        }
    }

    void login() {
        Map<String, String> map = new HashMap<>();
        map.put("Accept", "application/json");
        map.put("RestAction", "Auth");

        try {
            rpsApi = ApiFactory.getRpsApi(Settings.siteName + "/");
        } catch (Exception e) {
            Toast.makeText(app, "Не удается подключиться!", Toast.LENGTH_SHORT).show();
            Log.e(Consts.TAG, "OnResponse " + e.toString());
            app.dismissProgressDialog();
            Crashlytics.logException(e);
            return;
        }
        rpsApi.login(getLogin(), getPassword(), map).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    User login = response.body();
                    if (areCredentialsCorrect(login)) {
                        keyStore.save(login.token, login, Settings.domain, app.getPackageName());
                        Settings.isUserLoggedIn = true;
                        Settings.userId = login.UserId.toString();
                        Settings.model = login;
                        Settings.objectId = login.ObjectId.toString();
                        Settings.token = login.token;
                        Settings.sessionId = login.SessionId;
                        Settings.userRole = login.RoleName;
                        Settings.userName = login.UserName;
                        Settings.siteName = Settings.getProtocole() + Settings.domain;
                        saveSession(response.headers());

                        loadUserModel(true);
                    }
                } else {
                    Log.i(Consts.TAG, "Неправильный логин или пароль!");
                    Toast.makeText(app, "Неправильный логин или пароль!", Toast.LENGTH_SHORT).show();
                }
                app.dismissProgressDialog();
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(app, "Не удается подключиться!", Toast.LENGTH_SHORT).show();
                Log.e(Consts.TAG, "OnResponse " + t.toString());
                Crashlytics.logException(t);
                app.dismissProgressDialog();
            }
        });
    }

    public void loadUserModel(final boolean stop) {
        rpsApi.getUser(Settings.userId, ApiFactory.getHeaders()).enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                if (response.isSuccessful()) {
                    Settings.currentUser = response.body();
                }
                if (Settings.currentUser!=null) {
                    Settings.adminAccount = true;
                    Settings.saveBoolean(LoginActivity.this, "adminAccount", true);
                    startActivity(new Intent(app, AdminMainActivity.class));
                    startService(stop);
                } else {
                    Settings.adminAccount = false;
                    Settings.saveBoolean(LoginActivity.this, "adminAccount", false);
                    startActivity(new Intent(app, MainActivity.class));
                }

                finish();
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
                Settings.adminAccount = false;
                Settings.saveBoolean(LoginActivity.this, "adminAccount", false);
                startActivity(new Intent(app, MainActivity.class));
                finish();

                Log.e(Consts.TAG, "loadUserModel " + t.toString());
                Crashlytics.logException(t);
            }
        });
    }

    void saveSession(Headers headerList) {
        try {
            Settings.save(app);
            List<String> values = headerList.values("Set-Cookie");
            for (String cookie : values) {
                if (cookie.startsWith("LoginTime")) {
                    Settings.loginTime = (cookie.split(";")[0]).split("=")[1];
                }
            }
            Settings.someNameId = (values.get(2).split(";")[0]).split("=")[0];
            Settings.someValueId = (values.get(3).split(";")[0]).split("=")[1];
            if (Settings.someValueId.equals("deleted"))
                Settings.someValueId = (values.get(4).split(";")[0]).split("=")[1];
        } catch (Exception e) {
            //Log.e(Consts.TAG, e.getMessage());
        }
    }

    // ------------ Network ---------------
    Thread autoLogin = new Thread(new Runnable() {
        @Override
        public void run() {
            ArrayList<SecurityUser> tokens = keyStore.findAccountsForService(app.getPackageName());
            if (tokens.size()>0) {
                app.showProgressDialog("Авторизация....");
                final SecurityUser secUser = tokens.get(0);
                Settings.domain = secUser.domain;

                rpsApi = ApiFactory.getRpsApi(Settings.getProtocole()+ secUser.domain+"/");
                rpsApi.autoLogin(secUser.token).enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response.isSuccessful()){
                            Settings.isUserLoggedIn = true;
                            Settings.siteName = Settings.getProtocole() + secUser.domain;
                            Settings.domain = secUser.domain;
                            Settings.userId = secUser.user.Id.toString();
                            Settings.model = secUser.user;
                            Settings.objectId = secUser.user.ObjectId.toString();
                            Settings.token = secUser.token;

                            loadUserModel(false);
                        }
                        app.dismissProgressDialog();
                        Log.i(Consts.TAG, "OnResponse " + response.toString());
                    }
                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Log.e(Consts.TAG, "OnResponse " + t.toString());
                        Crashlytics.logException(t);
                        app.dismissProgressDialog();
                    }
                });
            }
        }
    });

    void startService(final boolean stop) {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                BackgroundControlService mSensorService = new BackgroundControlService();
                Intent mServiceIntent = new Intent(getApplicationContext(), mSensorService.getClass());
                if (stop)
                    stopService(mServiceIntent);
                if (Settings.adminAccount && !isMyServiceRunning(mSensorService.getClass())) {
                    startService(mServiceIntent);
                }

            }
        });
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            Log.i(Consts.TAG, "Running services " + service.service.getClassName());
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i (Consts.TAG, true+" isMyServiceRunning");
                return true;
            }
        }
        Log.i (Consts.TAG, false+" isMyServiceRunning");
        return false;
    }

    boolean isValidValues() {
        app.showProgressDialog("");
        boolean valid = true;

        if (!getLogin().matches("\\+?\\d{10}$")) {
            setError("Телефон должен быть в формате:\n495-600-4040");
            valid = false;
        }

        if (StringUtils.isNullOrEmpty(getPassword())) {
            setError("Пароль не должен быть пустым");
            valid = false;
        }

        Settings.siteName = Settings.getProtocole() + binding.site.getText().toString()+Settings.getDomainServer();
        Settings.domain = binding.site.getText().toString()+Settings.getDomainServer();

        if (!valid) {
            app.dismissProgressDialog();
        }
        return valid;
    }

    boolean areCredentialsCorrect(User user) {
        return user.UserId instanceof UUID;
    }

    String getPassword() {
        return binding.password.getText().toString();
    }

    String getLogin() {
        return binding.username.getText().toString().replace(" ", "");
    }

    private void setError(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    void showDialog() {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                        .theme(Theme.LIGHT)
                        .title(R.string.titleDomen)
                        .customView(R.layout.dialog_site, true)
                        .positiveText(android.R.string.ok)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                saveSettings(dialog);
                            }
                        }).build();

        EditText editText = (EditText)dialog.getCustomView().findViewById(R.id.domenName);
        RadioButton radioButton = dialog.getCustomView().findViewById(R.id.radioButton2);
        radioButton.setChecked(!Settings.isHttp);
        editText.setText(Settings.domainServer);

        dialog.show();
    }

    void saveSettings(MaterialDialog dialog) {
        RadioButton radioButton = dialog.getCustomView().findViewById(R.id.radioButton1);
        Settings.isHttp = radioButton.isChecked();
        Settings.saveBoolean(this, "isHttp", Settings.isHttp);

        EditText editText = (EditText)dialog.getCustomView().findViewById(R.id.domenName);
        String text = editText.getText().toString();
        if (!text.isEmpty()) {
            Settings.domainServer = text;
            Settings.saveString(this, "domainServer", text);
            binding.domenButton.setText(Settings.getDomainServer());
        }
    }


}
