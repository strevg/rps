package rps.com.evgeny.rps.fragments.person;

import android.util.Log;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rps.com.evgeny.rps.MainApplication;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.models.CardModel;
import rps.com.evgeny.rps.models.network.BalanceModel;
import rps.com.evgeny.rps.models.person.CardFullModel;
import rps.com.evgeny.rps.models.person.UserModel;
import rps.com.evgeny.rps.network.ApiFactory;
import rps.com.evgeny.rps.network.RPSApi;
import rps.com.evgeny.rps.nfc.CardData;
import rps.com.evgeny.rps.nfc.CardReader;
import rps.com.evgeny.rps.nfc.CardReaderHelper;
import rps.com.evgeny.rps.nfc.CardReaderResponse;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.ListsUtils;
import rps.com.evgeny.rps.utils.Settings;
import rps.com.evgeny.rps.utils.StringUtils;

/**
 * Created by Evgeny on 13.10.2017.
 */

public class InformationModel {
    InformationPresenter presenter;
    MainApplication app;
    RPSApi rpsApi;

    ListsUtils listsUtils;

    public InformationModel(InformationPresenter presenter) {
        this.presenter = presenter;
        this.rpsApi = presenter.rpsApi;
        this.app = presenter.app;
        this.listsUtils = ListsUtils.getInstance(app);

        loadCardInformation();

        if (!StringUtils.isNullOrEmpty(Settings.userRole) && Settings.currentUser == null)
            loadUserModel();
    }

    public void changeSpinnerData(int id, int value) {
        if (value!=-1)
            changeData(getSpinnerParam(id, value));
    }

    public void changeData(int id, String value) {
        changeData(getParam(id, value));
    }

    public void loadUserModel() {
        rpsApi.getUser(Settings.userId, ApiFactory.getHeaders()).enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                if (response.isSuccessful()) {
                    Settings.currentUser = response.body();
                }
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
                Log.e(Consts.TAG, "loadUserModel " + t.toString());
                Crashlytics.logException(t);
            }
        });
    }

    void loadCardInformation() {
        app.showProgressDialog("Загрузка с сервера...");
        rpsApi.getCardFull(Settings.card.CardId, ApiFactory.getHeaders()).enqueue(new Callback<CardFullModel>() {
            @Override
            public void onResponse(Call<CardFullModel> call, Response<CardFullModel> response) {
                if (Settings.fragmentType == Consts.FragmentType.PERSON) {
                    if (response.isSuccessful()) {
                        presenter.setCardFullModel(response.body().getCard());
                        PersonFragment.getInstance().setTitle(response.body().Name, response.body().CardId);
                        getBalance(response.body());
                    } else {
                        presenter.setCardFullModel(Settings.card);
                        PersonFragment.getInstance().setTitle("Нет привязки", Settings.card.CardId);

                        Toast.makeText(app, "Не удалось загрузить данные карты с сервера", Toast.LENGTH_SHORT).show();
                    }
                }

                app.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<CardFullModel> call, Throwable t) {
                presenter.setCardFullModel(Settings.card);
                PersonFragment.getInstance().setTitle("Нет привязки", Settings.card.CardId);

                Toast.makeText(app, "Не удалось загрузить данные карты с сервера", Toast.LENGTH_SHORT).show();

                Log.e(Consts.TAG, "loadCardInformation " + t.toString());
                Crashlytics.logException(t);
                app.dismissProgressDialog();
            }
        });
    }

    void getBalance(CardFullModel card) {
        rpsApi.getBalance(ApiFactory.getBalanceModel(card), ApiFactory.getFormDataHeaders()).enqueue(new Callback<BalanceModel>() {
            @Override
            public void onResponse(Call<BalanceModel> call, Response<BalanceModel> response) {
                if (response.isSuccessful() && response.body()!=null) {
                    int balance = response.body().amount_paid - response.body().amount;
                    presenter.setBalance(String.valueOf(balance));
                } else {
                    Toast.makeText(app, "Не удалось баланс карты с сервера", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BalanceModel> call, Throwable t) {
                Toast.makeText(app, "Не удалось баланс карты с сервера", Toast.LENGTH_SHORT).show();
                Log.e(Consts.TAG, "getBalance " + t.toString());
                Crashlytics.logException(t);
            }
        });
    }

    void changeData(Map<String, String> param) {
        if (!Settings.isNFCReader) {
            changeDataOnServer(param);
        } else {
            changeDataCard(param);
        }
    }

    void changeDataOnServer(Map<String, String> param) {
        Map.Entry<String,String> entry = param.entrySet().iterator().next();
        String key= entry.getKey();
        String value=entry.getValue();

        Map<String, String> newParams = new HashMap<>();
        String newValue = value.replace("-", "");
        newParams.put(key, newValue);

        app.showProgressDialog("Запись...");
        CardReaderResponse card = Settings.card;
        CardReaderResponse newCard = card.clone(card);
        newCard.changeField(key, value);
        newCard.DateSaveCard=null;

        try {
            Settings.card = newCard;
            updateCard(newParams);
        } catch (Exception e) {
            Crashlytics.logException(e);
            Toast.makeText(app, app.getString(R.string.write_error), Toast.LENGTH_SHORT).show();
            app.dismissProgressDialog();
        }
    }

    void changeDataCard(Map<String, String> param) {
        Map.Entry<String,String> entry = param.entrySet().iterator().next();
        String key= entry.getKey();
        String value=entry.getValue();

        Map<String, String> newParams = new HashMap<>();
        String newValue = value.replace("-", "");
        newParams.put(key, newValue);

        app.showProgressDialog("Запись...");
        CardReaderResponse card = Settings.card;
        CardData data_before = CardReaderHelper.GetParam(card);

        CardReaderResponse newCard = card.clone(card);
        newCard.changeField(key, value);
        newCard.DateSaveCard=null;

        CardData data = CardReaderHelper.GetParam(newCard);
        String strData = "";
        try {
            Log.i(Consts.TAG, CardReader.ByteArrayToHexString(CardReaderHelper.dataToWBlock(data_before)));
            Log.i(Consts.TAG, CardReader.ByteArrayToHexString(CardReaderHelper.dataToWBlock(data)));

            strData = CardReader.ByteArrayToHexString(CardReaderHelper.dataToWBlock(data));
        } catch (Exception e) {
            Crashlytics.logException(e);
            Toast.makeText(app, app.getString(R.string.write_error), Toast.LENGTH_SHORT).show();
            app.dismissProgressDialog();
            return;
        }

        try {
            int result = Settings.cardReader.WriteData(strData);
            if(result == 0){
                throw new NullPointerException();
            } else {
                Settings.card = newCard;
                updateCard(newParams);
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
            Toast.makeText(app, app.getString(R.string.write_error), Toast.LENGTH_SHORT).show();
            app.dismissProgressDialog();
        }
    }

    void updateCard(Map<String, String> param) {
        rpsApi.updateCardParam(Settings.card.CardId, param, ApiFactory.getHeaders()).enqueue(new Callback<CardModel>() {
            @Override
            public void onResponse(Call<CardModel> call, Response<CardModel> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(app, "Параметр успешно изменен", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(app, "Записано на карту. Не удалось подключится к серверу.", Toast.LENGTH_SHORT).show();
                }
                app.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<CardModel> call, Throwable t) {
                Toast.makeText(app, "Записано на карту. Не удалось подключится к серверу.", Toast.LENGTH_SHORT).show();
                Log.e(Consts.TAG, "updateCardParam " + t.toString());
                Crashlytics.logException(t);
                app.dismissProgressDialog();
            }
        });
    }

    public void updateFullCard(CardReaderResponse card) {
        rpsApi.updateCardParam(Settings.card.CardId, getParam(card), ApiFactory.getHeaders()).enqueue(new Callback<CardModel>() {
            @Override
            public void onResponse(Call<CardModel> call, Response<CardModel> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(app, "Данные на сервере были обновлены в соответствии с последними данными карты", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(app, "Не удалось синхронизировать данные карты с сервером", Toast.LENGTH_SHORT).show();
                }
                app.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<CardModel> call, Throwable t) {
                Toast.makeText(app, "Не удалось синхронизировать данные карты с сервером", Toast.LENGTH_SHORT).show();
                Log.e(Consts.TAG, "updateCardParam " + t.toString());
                Crashlytics.logException(t);
                app.dismissProgressDialog();
            }
        });
    }

    Map<String, String> getParam(CardReaderResponse card) {
        Map<String, String> param = new HashMap<>();
        param.put("ParkingEnterTime", card.ParkingEnterTime.replace("-", ""));
        param.put("LastRecountTime", card.LastRecountTime.replace("-", ""));
        param.put("TVP", card.TVP.replace("-", ""));
        param.put("TKVP", card.TKVP);
        param.put("Nulltime1", card.Nulltime1.replace("-", ""));
        param.put("Nulltime2", card.Nulltime2.replace("-", ""));
        param.put("Nulltime3", card.Nulltime3.replace("-", ""));
        param.put("SumOnCard", card.SumOnCard);
        param.put("ZoneidFC", card.ZoneidFC);
        param.put("ClientGroupidFC", card.ClientGroupidFC);
        param.put("ClientTypidFC", card.ClientTypidFC);
        param.put("TSidFC", card.TSidFC);
        param.put("TPidFC", card.TPidFC);

        return param;
    }

    Map<String, String> getParam(int id, String value) {
        Map<String, String> param = new HashMap<>();
        switch (id) {
            case R.id.timeComeIn:
                param.put("ParkingEnterTime", value);
                break;
            case R.id.timeRecalculation:
                param.put("LastRecountTime", value);
                break;
            case R.id.tvp:
                param.put("TVP", value);
                break;
            case R.id.tkvp:
                param.put("TKVP", value);
                break;
            case R.id.nullVP:
                param.put("Nulltime1", value);
                break;
            case R.id.nullKVP:
                param.put("Nulltime2", value);
                break;
            case R.id.nullAbonement:
                param.put("Nulltime3", value);
                break;
            case R.id.sumCard:
                param.put("SumOnCard", value);
                break;
        }
        return param;
    }

    Map<String, String> getSpinnerParam(int id, int value) {
        Map<String, String> param = new HashMap<>();
        switch (id) {
            case R.id.zoneSpinner:
                param.put("ZoneidFC", listsUtils.getZones().get(value).IdFC.toString());
                break;
            case R.id.groupSpinner:
                param.put("ClientGroupidFC", listsUtils.getGroups().get(value).idFC.toString());
                break;
            case R.id.clientSpinner:
                param.put("ClientTypidFC", listsUtils.getClients().get(value).index.toString());
                break;
            case R.id.tariffScheduleSpinner:
                param.put("TSidFC", listsUtils.getTariffSchedules().get(value).IdFC.toString());
                break;
            case R.id.tariffSpinner:
                param.put("TPidFC", listsUtils.getTariffs().get(value).IdFC.toString());
                break;
        }
        return param;
    }
}
