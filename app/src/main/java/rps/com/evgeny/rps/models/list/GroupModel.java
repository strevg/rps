package rps.com.evgeny.rps.models.list;

import java.util.UUID;

/**
 * Created by Evgeny on 08.10.2017.
 */

public class GroupModel {
    public UUID Id;
    public String Sync;
    public Integer idFC;
    public String Name;
    public Integer _IsDeleted;
    public Integer index;

    @Override
    public String toString() {
        return Name;
    }
}
