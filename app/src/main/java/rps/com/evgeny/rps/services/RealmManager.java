package rps.com.evgeny.rps.services;

import java.util.List;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmResults;
import rps.com.evgeny.rps.models.network.AlarmZoneModel;
import rps.com.evgeny.rps.models.realm.WarningRealm;
import rps.com.evgeny.rps.utils.TimeUtils;
import rps.com.evgeny.rps.utils.ViewUtils;

import static rps.com.evgeny.rps.fragments.warning.adapters.WarningAdapter.CALLING_TYPE;
import static rps.com.evgeny.rps.fragments.warning.adapters.WarningAdapter.WARNING_TYPE;

/**
 * Created by Evgeny on 17.07.2018.
 */

public class RealmManager {
    private static RealmManager instance;
    Realm realm;

    private RealmManager() {
        realm = Realm.getDefaultInstance();
    }

    public static RealmManager getInstance() {
        if (instance==null) {
            instance = new RealmManager();
        }
        return instance;
    }

    public void removeAll() {
        final RealmResults<WarningRealm> results = realm.where(WarningRealm.class).findAll();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                results.deleteAllFromRealm();
            }
        });
    }

    public RealmResults<WarningRealm> getAllWarning() {
        return realm.where(WarningRealm.class).findAll();
    }

    public boolean isExist(String id) {
        return realm.where(WarningRealm.class).equalTo("id", id).findFirst()!=null;
    }

    public void add(final WarningRealm model) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {
                bgRealm.copyToRealmOrUpdate(model);
            }
        });
    }

    public void addList(final List<WarningRealm> list) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {
                bgRealm.copyToRealmOrUpdate(list);
            }
        });
    }

    public static WarningRealm getRealmModel(AlarmZoneModel model) {
        WarningRealm warningRealm = new WarningRealm();
        warningRealm.setId(model.getId());
        warningRealm.setCode(model.TypeId);
        warningRealm.setColor(ViewUtils.getColorByIndex(model.AlarmColorId));
        warningRealm.setText(model.Value);
        warningRealm.setTitle(model.Name);
        warningRealm.setCall(false);
        warningRealm.setType(WARNING_TYPE);
        warningRealm.setTime(TimeUtils.getCurrentDate().getTime());
        return warningRealm;
    }

    public static WarningRealm getRealmModel(String title, String text) {
        WarningRealm warningRealm = new WarningRealm();
        warningRealm.setId(UUID.randomUUID().toString());
        warningRealm.setText(text);
        warningRealm.setTitle(title);
        warningRealm.setCall(true);
        warningRealm.setType(CALLING_TYPE);
        warningRealm.setTime(TimeUtils.getCurrentDate().getTime());
        return warningRealm;
    }
}
