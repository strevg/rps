package rps.com.evgeny.rps.models.network;

import java.util.UUID;

/**
 * Created by Evgeny on 08.07.2018.
 */

public class InnerDeviceModel {
    UUID DeviceId;
    int ZoneBeforeId;
    int ZoneAfterId;
    String DispenserTypeId;
    String BarcodeTypeId;
    String BankModuleTypeId;
    int MaxBusyTimeAntennaA;
    int MaxBusyTimeAntennaB;
    int MaxBusyTimeIR;
    boolean _IsDeleted;
    String Sync;
    String IdStoyky;
    boolean SlaveExist;
    boolean ReaderOutExists;
    String OutReaderTypeId;
    boolean ReaderInExists;
    String InReaderTypeId;
    boolean DispensExists;
    boolean FeederExists;
    String FeederTypeId;
    boolean BarcodeExist;
    boolean BankModuleExist;
    boolean DisplayExists;
    int DisplayTypeId;
    int ComDisplay;
    int ComSlave;
    int ComReadOut;
    int ComReadIn;
    int ComDispens;
    int ComFeeder;
    String ComBarcode;
    int PortDiscret1;
    int PortDiscret2;
    int PortDiscret3;
    int PlateNumberUse;
    int TimeCardV;
    int TimeBarrier;
    int NumerSector;
    int TarifIdScheduleId;
    String TarifPlanId;
    String ClientGroupidToC;
    int TimeNotLoopA;
    int TimeNotIr;
    boolean LoopA;
    boolean LoopB;
    boolean IR;
    boolean PUSH;
    boolean Debug;
    boolean SlaveDebug;
    boolean ReaderOutDebug;
    boolean ReaderInDebug;
    boolean DispensDebug;
    boolean FeederDebug;
    boolean checkBoxItog;
    int logNastroy;
    String CardKey;
    String WebCameraIP;
    String WebCameraLogin;
    String WebCameraPassword;
    int TransitTypeId;
    int RackWorkingModeId;
    UUID ZoneId;
    String ServerURL;
    boolean RFIDExists;
    int ComRFID;
}
