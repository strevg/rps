package rps.com.evgeny.rps.views.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import rps.com.evgeny.rps.databinding.SessionViewHolderBinding;
import rps.com.evgeny.rps.models.person.SessionModel;
import rps.com.evgeny.rps.views.AdapterInterface;
import rps.com.evgeny.rps.views.viewholders.SessionViewHolder;

/**
 * Created by Evgeny on 23.09.2017.
 */

public class SessionAdapter extends RecyclerView.Adapter<SessionViewHolder> implements AdapterInterface<SessionModel> {
    List<SessionModel> items;

    public SessionAdapter() {
        items = new ArrayList<>();
    }

    @Override
    public SessionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        SessionViewHolderBinding binding = SessionViewHolderBinding.inflate(inflater, parent, false);
        return new SessionViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(SessionViewHolder holder, int position) {
        holder.setSession(null);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onAdd(SessionModel item) {
        int position = items.size();
        items.add(position, item);
        notifyItemInserted(position);
    }

    @Override
    public void onRemove(int pos) {
        if (pos < 0)
            return;
        items.remove(pos);
        notifyItemRemoved(pos);
    }

    @Override
    public void onRemoveAll() {
        int count = getItemCount();
        items.clear();
        notifyItemRangeRemoved(0, count);
    }

    @Override
    public void onAddAll(List<SessionModel> items) {

    }
}
