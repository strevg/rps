package rps.com.evgeny.rps.models.asterisk;

public class RegistrationRequest {
    public String username;
    public String password;

    public RegistrationRequest(String user, String pass) {
        this.username = user;
        this.password = pass;
    }
}
