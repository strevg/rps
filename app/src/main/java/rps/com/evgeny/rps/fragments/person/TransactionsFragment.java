package rps.com.evgeny.rps.fragments.person;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.crashlytics.android.Crashlytics;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rps.com.evgeny.rps.MainApplication;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.models.invitations.InvitationModel;
import rps.com.evgeny.rps.models.person.TransactionModel;
import rps.com.evgeny.rps.network.ApiFactory;
import rps.com.evgeny.rps.network.RPSApi;
import rps.com.evgeny.rps.nfc.CardReaderResponse;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.Settings;
import rps.com.evgeny.rps.views.adapters.TransactionAdapter;

/**
 * Created by Evgeny on 23.09.2017.
 */

public class TransactionsFragment extends Fragment {
    RecyclerView recyclerView;
    TransactionAdapter adapter;
    MainApplication app;
    RPSApi rpsApi;

    ProgressBar progressBar;
    CardReaderResponse card;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (MainApplication)getActivity().getApplication();
        rpsApi = ApiFactory.getRpsApi(Settings.siteName+"/");

        card = Settings.card;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.list_layout, container, false);
        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        progressBar = (ProgressBar)v.findViewById(R.id.progressBar);

        adapter = new TransactionAdapter();
        recyclerView.setAdapter(adapter);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //initTestTransaction();
        loadTransaction();
    }

    void loadTransaction() {
        showProgressBar(true);
        rpsApi.getTransactions(card.CardId, null, null, ApiFactory.getHeaders()).enqueue(new Callback<List<TransactionModel>>() {
            @Override
            public void onResponse(Call<List<TransactionModel>> call, Response<List<TransactionModel>> response) {
                if (response.isSuccessful()) {
                    for (TransactionModel model : response.body()) {
                        adapter.onAdd(model);
                    }
                }
                showProgressBar(false);
            }

            @Override
            public void onFailure(Call<List<TransactionModel>> call, Throwable t) {
                Log.e(Consts.TAG, "loadTransaction " + t.toString());
                Crashlytics.logException(t);
                showProgressBar(false);
            }
        });
    }

    void showProgressBar(final boolean show) {
        if (getActivity()!=null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        }
    }

    //-------------- test --------------
    void initTestTransaction() {
        for (int i=0; i<2; i++) {
            adapter.onAdd(getComeIn(i*4));
            adapter.onAdd(getComeOut(i*4+1));
            adapter.onAdd(getComeMoving(i*4+2));
            adapter.onAdd(getComeKassa(i*4+3));
        }
    }

    TransactionModel getCommonTransaction(int index) {
        TransactionModel model = new TransactionModel();
        model.Time ="2017-10-10 21:14:00";
        model.index = index;
        return model;
    }

    TransactionModel getComeIn(int index) {
        TransactionModel model = getCommonTransaction(index);
        model.DeviceName = "Въезд Юг";
        model.DeviceTypeID = 1;
        model.Transit = 1;
        model.Type = "Кража карты";
        model.ZoneAfterId = "Зона А";
        return model;
    }

    TransactionModel getComeOut(int index) {
        TransactionModel model = getCommonTransaction(index);
        model.DeviceName = "Выезд Юг";
        model.DeviceTypeID = 1;
        model.Transit = 2;
        model.Type = "Выезд с парковки";
        model.ParkingTime = "12 мин";
        model.ZoneBeforeId = "Зона Б";
        model.TimeEntry = "2017-10-10 22:00:00";
        return model;
    }

    TransactionModel getComeMoving(int index) {
        TransactionModel model = getCommonTransaction(index);
        model.DeviceName = "Девайс";
        model.DeviceTypeID = 1;
        model.Transit = 3;
        model.Type = "Перемещение";
        model.ZoneBeforeId = "Зона А";
        model.ZoneAfterId = "Зона B";
        return model;
    }

    TransactionModel getComeKassa(int index) {
        TransactionModel model = getCommonTransaction(index);
        model.DeviceName = "Касса 123";
        model.DeviceTypeID = 2;
        model.PaymentType = "Наличные";
        model.Paid = "200";
        model.SumOnCardBefore = "1500";
        model.SumOnCardAfter = "1300";
        return model;
    }
}
