package rps.com.evgeny.rps.models.network;

/**
 * Created by Evgeny on 12.07.2018.
 */

public class RackWorkingRequest {
    String id;
    int RackWorkingModeId;

    public RackWorkingRequest(String id, int mode) {
        this.id = id;
        this.RackWorkingModeId = mode;
    }
}
