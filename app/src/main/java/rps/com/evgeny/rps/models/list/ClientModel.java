package rps.com.evgeny.rps.models.list;

import java.util.UUID;

/**
 * Created by Evgeny on 08.10.2017.
 */

public class ClientModel {
    public Integer index;
    public UUID Id;
    public String Sync;
    public String ContactName;
    public String Phone;
    public String Email;
    public Integer _IsDeleted;

    public ClientModel(String name, int index) {
        this.index = index;
        this.ContactName = name;
    }

    @Override
    public String toString() {
        return ContactName;
    }
}
