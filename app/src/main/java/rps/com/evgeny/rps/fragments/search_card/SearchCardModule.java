package rps.com.evgeny.rps.fragments.search_card;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import rps.com.evgeny.rps.di.ActivityScoped;
import rps.com.evgeny.rps.di.FragmentScoped;

/**
 * Created by Evgeny on 15.11.2018.
 */

@Module
public abstract class SearchCardModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract SearchCardFragment searchCardFragment();

    @ActivityScoped
    @Binds
    abstract SearchCardContract.Presenter searchCardPresenter(SearchCardPresenter presenter);
}
