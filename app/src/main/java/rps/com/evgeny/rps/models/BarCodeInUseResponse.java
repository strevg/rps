package rps.com.evgeny.rps.models;

import java.util.UUID;

/**
 * Created by Evgeny on 06.09.2017.
 */

public class BarCodeInUseResponse {
    UUID Id;
    UUID IdBCM;
    UUID OperId;
    UUID CompanyId;
    int IdTP;
    int IdTS;
    String DiscountAmount;
    int GroupIdFC;
    String UsageTime;
    String Sync;
    String GuestPlateNumber;
    String GuestCarModel;
    String GuestName;
    String GuestEmail;
    String ParkingTime;
    String Comment;
    String CreatedAt;
    String EnterId;
    String ExitId;
    String IdTPForCount;
    String IdTSForCount;
    String CardId;
    String Ammount;
    int Used;
    String ActivatedAt;
    String ParkingEnterTime;
    String TPForRent;
    String TSForRent;
    String id;
    boolean editable;
    boolean editable_fields;
}
