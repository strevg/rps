package rps.com.evgeny.rps.views.viewholders;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.databinding.ViewholderActiveCallingBinding;
import rps.com.evgeny.rps.fragments.zone.adapter.CallingModel;

/**
 * Created by Evgeny on 29.05.2018.
 */

public class ActiveCallingViewHolder extends RecyclerView.ViewHolder {
    protected Context context;
    public ViewholderActiveCallingBinding binding;

    public ActiveCallingViewHolder(View view) {
        super(view);
        this.context = view.getContext();
        binding = DataBindingUtil.bind(view);
    }

    public void setData(CallingModel model) {
        setTitle(context.getResources().getString(model.isOutcoming()?R.string.title_outcomming:R.string.title_incomming));
        setText(model.getText());
        showFinishButton(model.isActive());
    }

    public void showFinishButton(boolean flag) {
        binding.answerContainer.setVisibility(flag?View.GONE:View.VISIBLE);
        binding.finishCalling.setVisibility(flag?View.VISIBLE:View.GONE);

        binding.titleCalling.setTextColor(context.getResources().getColor(flag?R.color.colorGreen:R.color.buttonYellow));
        DrawableCompat.setTint(binding.iconCalling.getDrawable(), ContextCompat.getColor(context, flag?R.color.colorGreen:R.color.buttonYellow));
    }

    public void setTitle(String title) {
        binding.titleCalling.setText(title);
    }

    public void setText(String text) {
        binding.textCalling.setText(text);
    }

    public void setOnClickListener(View.OnClickListener listener) {
        binding.answerCalling.setOnClickListener(listener);
        binding.rejectCalling.setOnClickListener(listener);
        binding.finishCalling.setOnClickListener(listener);
    }
}
