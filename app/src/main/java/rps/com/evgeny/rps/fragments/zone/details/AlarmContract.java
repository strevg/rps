package rps.com.evgeny.rps.fragments.zone.details;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import rps.com.evgeny.rps.di.BasePresenter;
import rps.com.evgeny.rps.di.BaseView;
import rps.com.evgeny.rps.models.network.DeviceZoneModel;

/**
 * Created by Evgeny on 08.06.2018.
 */

public interface AlarmContract {
    interface View extends BaseView<AlarmContract.Presenter> {
        void setAdapter(RecyclerView.Adapter adapter);
        void scrollToPosition(int pos);
        Context getContext();
    }

    interface Presenter extends BasePresenter<AlarmContract.View> {
        void setModel(DeviceZoneModel model);

        void takeView(AlarmContract.View view);

        void dropView();
    }
}
