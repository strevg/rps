package rps.com.evgeny.rps.fragments.warning.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Evgeny on 24.05.2018.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {
    List<Fragment> tabsFragmentList;
    List<String> tabsTitleList;

    public PagerAdapter(FragmentManager fm) {
        super(fm);
        this.tabsFragmentList = new ArrayList<>();
        this.tabsTitleList = new ArrayList<>();
    }

    public void onAdd(Fragment fragment, String title) {
        tabsFragmentList.add(fragment);
        tabsTitleList.add(title);
    }

    @Override
    public Fragment getItem(int position) {
        return tabsFragmentList.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabsTitleList.get(position);
    }

    @Override
    public int getCount() {
        return tabsFragmentList.size();
    }
}