package rps.com.evgeny.rps.fragments.invitations;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rps.com.evgeny.rps.MainApplication;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.databinding.CreateInvationFragmentBinding;
import rps.com.evgeny.rps.models.invitations.CreateResponse;
import rps.com.evgeny.rps.network.ApiFactory;
import rps.com.evgeny.rps.network.RPSApi;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.Settings;
import rps.com.evgeny.rps.utils.StringUtils;

/**
 * Created by Evgeny on 21.09.2017.
 */

public class CreateInvitationFragment extends Fragment implements View.OnClickListener{
    CreateInvationFragmentBinding binding;
    UUID barcodeID;
    MainApplication app;
    RPSApi rpsApi;

    CreateInvitationHelper createHelper;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (MainApplication)getActivity().getApplication();
        rpsApi = ApiFactory.getRpsApi(Settings.siteName+"/");


        createHelper = CreateInvitationHelper.getInstance();
        barcodeID = createHelper.getBarcode().Id;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.create_invation_fragment, container, false);
        View v = binding.getRoot();
        binding.createInvation.setOnClickListener(this);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (createHelper.hasInvitation()) {
            Map<String, String> invitations = createHelper.getInvitations();
            binding.firstName.setText(invitations.get("dataCreate[surname]"));
            binding.email.setText(invitations.get("dataCreate[email]"));
            binding.secondName.setText(invitations.get("dataCreate[name]"));
            binding.carNumber.setText(invitations.get("dataCreate[state_number]"));
            binding.thirdName.setText(invitations.get("dataCreate[middle_name]"));
            binding.carType.setText(invitations.get("dataCreate[brand]"));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        app.getMainActivity().setTitle(getString(R.string.title_select_create_invitation));
        app.getMainActivity().setSubtitle(getString(R.string.title_step_2));
    }

    @Override
    public void onClick(View view) {
        if (checkInvitation()) {
            createHelper.setInvitations(getInvitation());
            app.getMainActivity().replaceFragment(Consts.FragmentType.CHECK_INVITATION, null);
        }
    }

    boolean checkInvitation() {
        boolean valid = true;
        if (!StringUtils.isValidEmail(binding.email.getText().toString())) {
            setError(binding.email, "Не верно указан email");
            valid = false;
        } else {
            removeError(binding.email);
        }

        if (StringUtils.isNullOrEmpty(binding.firstName.getText().toString())) {
            setError(binding.firstName, "Фамилия не должна быть пустой");
            valid = false;
        } else {
            removeError(binding.firstName);
        }
        if (StringUtils.isNullOrEmpty(binding.secondName.getText().toString())) {
            setError(binding.secondName, "Имя не должно быть пустым");
            valid = false;
        } else {
            removeError(binding.secondName);
        }
        if (StringUtils.isNullOrEmpty(binding.thirdName.getText().toString())) {
            setError(binding.thirdName, "Отчество не должно быть пустым");
            valid = false;
        } else {
            removeError(binding.thirdName);
        }
        if (StringUtils.isNullOrEmpty(binding.carType.getText().toString())) {
            setError(binding.carType, "Марка ТС не должна быть пустой");
            valid = false;
        } else {
            removeError(binding.carType);
        }
        if (StringUtils.isNullOrEmpty(binding.carNumber.getText().toString())) {
            setError(binding.carNumber, "Номер ТС не должно быть пустым");
            valid = false;
        } else {
            removeError(binding.carNumber);
        }

        return valid;
    }

    Map<String, String> getInvitation() {
        Map<String, String> invitations = new HashMap<>();
        invitations.put("dataCreate[surname]", binding.firstName.getText().toString());
        invitations.put("dataCreate[email]", binding.email.getText().toString());
        invitations.put("dataCreate[name]", binding.secondName.getText().toString());
        invitations.put("dataCreate[state_number]", binding.carNumber.getText().toString());
        invitations.put("dataCreate[middle_name]", binding.thirdName.getText().toString());
        invitations.put("dataCreate[brand]", binding.carType.getText().toString());
        invitations.put("dataCreate[comments]", "");
        invitations.put("dataCreate[usagetime]", "7");
        invitations.put("dataCreate[usagetype]", "days");
        invitations.put("dataCreate[AnySum]", "0");
        invitations.put("dataCreate[DiscountAmount]", "");
        return invitations;
    }

    private void setError(TextView view, String text) {
        TextInputLayout parent = (TextInputLayout) view.getParentForAccessibility();
        parent.setError(text);
    }

    private void removeError(TextView view) {
        TextInputLayout parent = (TextInputLayout) view.getParentForAccessibility();
        parent.setError(null);
    }
}
