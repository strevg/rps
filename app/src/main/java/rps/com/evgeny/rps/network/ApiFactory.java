package rps.com.evgeny.rps.network;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Authenticator;
import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.Credentials;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.Route;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rps.com.evgeny.rps.BuildConfig;
import rps.com.evgeny.rps.models.BarCodeInUse;
import rps.com.evgeny.rps.models.person.CardFullModel;
import rps.com.evgeny.rps.nfc.CardReaderResponse;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.Settings;
import rps.com.evgeny.rps.utils.StringUtils;

import static okhttp3.logging.HttpLoggingInterceptor.Level.BODY;
import static okhttp3.logging.HttpLoggingInterceptor.Level.NONE;

/**
 * Created by Evgeny on 31.08.2017.
 */

public class ApiFactory {
    private static final OkHttpClient.Builder httpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient()
            .writeTimeout(30, TimeUnit.SECONDS)
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30,TimeUnit.SECONDS);

    private static Retrofit retrofit;

    @NonNull
    public static RPSApi getRpsApi() {
        return getRetrofit(Consts.BASE_URI).create(RPSApi.class);
    }

    public static RPSApi getRpsApi(String baseUrl) {
        return getRetrofit2(baseUrl).create(RPSApi.class);
    }

    @NonNull
    public static Retrofit getRetrofit2(String baseUrl) {
        if (retrofit != null && retrofit.baseUrl().toString().equals(baseUrl))
            return retrofit;

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel((BuildConfig.DEBUG)?BODY:NONE);


        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        httpClient.cookieJar(getCookieJar);
        httpClient.addInterceptor(logging);

        retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient.build())
                .build();

        return retrofit;
    }

    @NonNull
    public static Retrofit getRetrofit(String baseUrl) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(BODY);

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        httpClient.addInterceptor(logging);
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient.build())
                .build();
    }

    //----------- create cookie --------------
    static CookieJar getCookieJar = new CookieJar() {
        @Override
        public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {}
        @Override
        public List<Cookie> loadForRequest(HttpUrl url) {
            if (/*url.toString().endsWith("authUser/index") || */url.toString().endsWith("login"))
                return new ArrayList<>();
            ArrayList<Cookie> oneCookie = new ArrayList<>();
            try {
                oneCookie.add(createNonPersistentCookie("lang", "ru"));
                oneCookie.add(createNonPersistentCookie("PHPSESSID", Settings.sessionId));
                oneCookie.add(createNonPersistentCookie("UserName", Settings.userName));
                oneCookie.add(createNonPersistentCookie("RoleName", Settings.userRole));
                oneCookie.add(createNonPersistentCookie("LoginTime", Settings.loginTime));
                oneCookie.add(createNonPersistentCookie("UserId", Settings.userId));
                oneCookie.add(createNonPersistentCookie(Settings.someNameId, Settings.someValueId));
            } catch (Exception e) {
                Log.e(Consts.TAG, e.getMessage());
            }
            return oneCookie;
        }
    };

    public static Cookie createNonPersistentCookie(String name, String value) throws UnsupportedEncodingException {
        return new Cookie.Builder()
                .domain(Settings.domain)
                .path("/")
                .name(name)
                .value(URLEncoder.encode(value, "UTF-8"))
                .httpOnly()
                .secure()
                .build();
    }

    public static Map<String, String> getHeaders() {
        Map<String, String> mapHeader = new HashMap<>();
        mapHeader.put("Accept", "application/json");
        mapHeader.put("Content-Type", "application/json");
        mapHeader.put("AuthToken", Settings.token);
        return mapHeader;
    }

    public static Map<String, String> getChangeTypeFields(String id, int type) {
        LinkedHashMap<String, String> mp= new LinkedHashMap<>();
        mp.put("id", id);
        mp.put("RackWorkingModeId", String.valueOf(type));

        return mp;
    }

    public static Map<String, String> getFormDataHeaders() {
        Map<String, String> mapHeader = new HashMap<>();
        mapHeader.put("charset", "utf-8");
        mapHeader.put("AuthToken", Settings.token);
        return mapHeader;
    }

    public static Map<String, String> getUpdateObject(CardReaderResponse card, BarCodeInUse barCodeModel) {
        Map<String, String> update = new HashMap<>();
        update.put("CardId", card.CardId);
        int Sum = Integer.parseInt(card.SumOnCard) + barCodeModel.DiscountAmount;
        card.SumOnCard = String.valueOf(Sum);
        update.put("SumOnCard", String.valueOf(Sum));

        if (!StringUtils.isNullOrEmpty(String.valueOf(barCodeModel.IdTP))) {
            card.TPidFC = String.valueOf(barCodeModel.IdTP);
            update.put("TPidFC", String.valueOf(barCodeModel.IdTP));
        }

        if (!StringUtils.isNullOrEmpty(String.valueOf(barCodeModel.IdTS))) {
            card.TSidFC = String.valueOf(barCodeModel.IdTS);
            update.put("TSidFC", String.valueOf(barCodeModel.IdTS));
        }

        if (!StringUtils.isNullOrEmpty(String.valueOf(barCodeModel.GroupIdFC))) {
            card.ClientGroupidFC = String.valueOf(barCodeModel.GroupIdFC);
            update.put("ClientGroupidFC", String.valueOf(barCodeModel.GroupIdFC));
        }

        return update;
    }

    public static Map<String, String> getChangeModeReasonModel(String device, String deviceID, String reason) {
        Map<String, String> model = new HashMap<>();
        model.put("ActionNumber","1");
        model.put("ChangeTime","");
        model.put("ColumnName","Id");
        model.put("DataAfter","Пропуск автомобиля. Устройство: " + device +
                ". Причина пропуска автомобиля: " + reason);
        model.put("DataBefore","");
        model.put("ObjectGUID",deviceID);
        model.put("OperationId","52");
        model.put("ColumnName","Id");
        model.put("TableName","DeviceModel");
        model.put("Type","2");
        model.put("UserId",Settings.userId);
        model.put("_IsDeleted","0");

        return model;
    }

    public static Map<String, String> getBalanceModel(CardFullModel card) {
        Map<String, String> model = new HashMap<>();
        if (card.LastRecountTime!=null)
            model.put("LastRecountTime",card.LastRecountTime);
        if (card.ParkingEnterTime!=null)
            model.put("ParkingEnterTime",card.ParkingEnterTime);
        if (card.SumOnCard!=null)
            model.put("SumOnCard",card.SumOnCard);
        if (card.Nulltime1!=null)
            model.put("Nulltime1",card.Nulltime1);
        if (card.Nulltime2!=null)
            model.put("Nulltime2",card.Nulltime2);
        if (card.Nulltime3!=null)
            model.put("Nulltime3",card.Nulltime3);

        model.put("TVP","2000-01-01 00:00:00");
        model.put("TKVP",String.valueOf(card.TKVP));
        model.put("TPidFC",String.valueOf(card.TPidFC));
        model.put("TSidFC",String.valueOf(card.TSidFC));

        if (card.LastRecountTime!=null)
            model.put("Start",card.LastRecountTime);
        return model;
    }
}