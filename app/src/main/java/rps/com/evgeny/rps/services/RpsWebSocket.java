package rps.com.evgeny.rps.services;

import android.util.Log;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;
import rps.com.evgeny.rps.models.network.WebSocketModel;
import rps.com.evgeny.rps.network.UnsafeOkHttpClient;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.StringUtils;

/**
 * Created by Evgeny on 11.07.2018.
 */

public class RpsWebSocket extends WebSocketListener {
    private static final int NORMAL_CLOSURE_STATUS = 1000;

    private OkHttpClient client;
    private WebSocketListener listener;
    private WebSocket webSocket;
    boolean forseStart = false;

    public RpsWebSocket(WebSocketListener listener) {
        OkHttpClient.Builder httpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient()
                .writeTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60,TimeUnit.SECONDS);
        client = httpClient.build();

        this.listener = listener;
    }

    public void onStart() {
        this.forseStart = true;
        Request request = new Request.Builder()
                .url(StringUtils.getWebSocketName()).build();
        webSocket = client.newWebSocket(request, this);
    }

    public void onClose() {
        this.forseStart = false;
        this.webSocket.close(NORMAL_CLOSURE_STATUS, "close");
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response) {
        this.webSocket = webSocket;
    }
    @Override
    public void onMessage(WebSocket webSocket, String text) {
        Log.i(Consts.TAG, "WebSocket " + text);
        if (isValidMessage(text))
            listener.onMessage(new WebSocketModel(text));
    }
    @Override
    public void onMessage(WebSocket webSocket, ByteString bytes) {
        Log.i(Consts.TAG, "WebSocket " + bytes.hex());
    }
    @Override
    public void onClosing(WebSocket webSocket, int code, String reason) {
        webSocket.close(NORMAL_CLOSURE_STATUS, null);
        if (forseStart)
            listener.onRestartSocket();
    }
    @Override
    public void onFailure(WebSocket webSocket, Throwable t, Response response) {
        Log.e(Consts.TAG,"WebSocket onFailure: ");
        if (forseStart)
            listener.onRestartSocket();
    }

    private boolean isValidMessage(String text) {
        return text.contains(Consts.SINGLE_PASSAGE)
                || text.contains(Consts.ALARM)
                || text.contains(Consts.CALLING);
    }

    public interface WebSocketListener {
        void onMessage(WebSocketModel model);
        void onRestartSocket();
    }

    public interface WebSocketMessageListener {
        void onMessage(WebSocketModel model);
    }
}
