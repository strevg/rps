package rps.com.evgeny.rps.views.viewholders;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.databinding.ViewholderActiveCallingBinding;
import rps.com.evgeny.rps.databinding.ViewholderActiveEnteringBinding;

/**
 * Created by Evgeny on 29.05.2018.
 */

public class ActiveEnteringViewHolder extends RecyclerView.ViewHolder {
    protected Context context;
    public ViewholderActiveEnteringBinding binding;

    public ActiveEnteringViewHolder(View view) {
        super(view);
        this.context = view.getContext();
        binding = DataBindingUtil.bind(view);
    }

    public void setTitle(String title) {
        binding.titleCalling.setText(title);
    }

    public void setText(String text) {
        binding.textCalling.setText(text);
    }

    public void setOnClickListener(View.OnClickListener listener) {
        binding.cancelButton.setOnClickListener(listener);
    }
}
