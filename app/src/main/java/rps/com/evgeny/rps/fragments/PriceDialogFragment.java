package rps.com.evgeny.rps.fragments;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import rps.com.evgeny.rps.MainApplication;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.models.BarCodeInUse;
import rps.com.evgeny.rps.nfc.CardReader;
import rps.com.evgeny.rps.utils.Settings;
import rps.com.evgeny.rps.views.viewholders.PriceViewHolder;

/**
 * Created by Evgeny on 03.09.2017.
 */

public class PriceDialogFragment extends DialogFragment {

    private PriceViewHolder vholder;
    private BarCodeInUse bm;
    public CardReader mCardReader;
    MainApplication app;

    public static PriceDialogFragment NewInstance(Bundle bundle, RecyclerView.ViewHolder holder) {
        PriceDialogFragment fragment = new PriceDialogFragment();
        fragment.setArguments(bundle);
        fragment.vholder = (PriceViewHolder)holder;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (MainApplication) getActivity().getApplication();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        // Use this to return your custom view for this Fragment
        final View view = inflater.inflate(R.layout.edit_price, container, false);
        Button button = (Button)view.findViewById(R.id.close_price);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        Button buttonSave = (Button)view.findViewById(R.id.save_price);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                vholder.binding.changeBarcode.setText(getString(R.string.changeBarcode));
                vholder.bm.DiscountAmount = Integer.parseInt(((AppCompatEditText)view.findViewById(R.id.price_val)).getText().toString());
                vholder.binding.barcodeDiscount.setText(vholder.bm.DiscountAmount + " руб.");
                vholder.binding.barcodeName.setText("Скидка " + vholder.bm.DiscountAmount + " руб.");
                Toast.makeText(getActivity(), "Сохранено!", Toast.LENGTH_SHORT).show();
            }
        });

        ((TextView)view.findViewById(R.id.barcodeNameModal)).setText("Скидка " + vholder.bm.DiscountAmount + " руб.");
        //((AppCompatEditText)view.findViewById(R.id.price_val)).setText(vholder.bm.DiscountAmount + "");

        mCardReader = Settings.cardReader;

        return view;
    }

    public void OnCardRecieved() {
        throw new IllegalArgumentException();
    }

    public void OnCardRemoved()  {
        throw new IllegalArgumentException();
    }
}
