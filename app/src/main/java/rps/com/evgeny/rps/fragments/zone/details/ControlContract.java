package rps.com.evgeny.rps.fragments.zone.details;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import rps.com.evgeny.rps.di.BasePresenter;
import rps.com.evgeny.rps.di.BaseView;
import rps.com.evgeny.rps.fragments.zone.ZoneDevicesContract;
import rps.com.evgeny.rps.models.network.DeviceZoneModel;

/**
 * Created by Evgeny on 07.06.2018.
 */

public interface ControlContract {
    interface View extends BaseView<ControlContract.Presenter> {
        void initViewType(DeviceZoneModel device);
        void showEnterCancel();
        void showEnterNormal();
        void showCallNormal();
        void showCallNow();
        void showCallCancel();
        Context getContext();
        void showTime(int time);
        void showToast(int text);
        void showProgress(int type);
        void shadowCallingScreen(boolean show);
    }

    interface Presenter extends BasePresenter<ControlContract.View>, android.view.View.OnClickListener {
        void takeView(ControlContract.View view);
        void dropView();
    }
}
