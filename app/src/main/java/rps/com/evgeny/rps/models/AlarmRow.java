package rps.com.evgeny.rps.models;

/**
 * Created by Evgeny on 30.08.2017.
 */

public class AlarmRow {
    public String ID;
    public String subject;
    public String priorityId;
    public String categoryId;
    public String objectId;
    public String description;
    public String sourceMediaId;
    public String sender;
    public String manager;
    public String statusId;
    public String created;
    public String id;
    public String category;
    public String status;
    public String priority;
    public String serverDomain;
    public String object;
    public String createdDate;
    public String managerName;
    public String file;
    public int unread;
    public int isCloseComment;
    public String whoCreated;
}
