package rps.com.evgeny.rps.fragments.settings;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.view.View;

import javax.inject.Inject;

import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.activities.LoginActivity;
import rps.com.evgeny.rps.models.KeyWarehouse;
import rps.com.evgeny.rps.services.BackgroundControlService;
import rps.com.evgeny.rps.services.ControlManager;
import rps.com.evgeny.rps.utils.Settings;

public class MenuPresenter implements MenuContract.Presenter, View.OnClickListener{
    @Nullable
    MenuContract.View view;

    @Inject
    MenuPresenter() {
    }

    @Override
    public void takeView(MenuContract.View view) {
        this.view = view;
    }

    @Override
    public void dropView() {
        this.view = null;
    }

    @Override
    public View.OnClickListener getOnClickListener() {
        return this;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.infoButton:
                view.showInfoFragment();
                break;
            case R.id.helpButton:
                view.showHelpFragment();
                break;
            case R.id.exitButton:
                exit();
                break;
            case R.id.settingsButton:
                view.showSettingsFragment();
                break;
        }
    }

    void exit() {
        Settings.adminAccount = false;
        Settings.saveBoolean(view.getAdminMainActivity(), "adminAccount", false);

        KeyWarehouse keyStore = new KeyWarehouse(view.getAdminMainActivity());
        keyStore.delete(view.getAdminMainActivity().getPackageName());

        Intent mServiceIntent = new Intent(view.getAdminMainActivity(), BackgroundControlService.class);
        view.getAdminMainActivity().stopService(mServiceIntent);

        ControlManager.getInstance().stopSocket();

        view.getAdminMainActivity().startActivity(new Intent(view.getAdminMainActivity(), LoginActivity.class));
        view.getAdminMainActivity().finish();
    }
}
