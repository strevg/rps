package rps.com.evgeny.rps.fragments.person;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import rps.com.evgeny.rps.MainApplication;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.models.person.SessionModel;
import rps.com.evgeny.rps.network.ApiFactory;
import rps.com.evgeny.rps.network.RPSApi;
import rps.com.evgeny.rps.utils.Settings;
import rps.com.evgeny.rps.views.adapters.SessionAdapter;

/**
 * Created by Evgeny on 23.09.2017.
 */

public class SessionsFragment extends Fragment {
    RecyclerView recyclerView;
    SessionAdapter adapter;
    MainApplication app;
    RPSApi rpsApi;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (MainApplication)getActivity().getApplication();
        rpsApi = ApiFactory.getRpsApi(Settings.siteName+"/");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.list_layout, container, false);
        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        adapter = new SessionAdapter();
        recyclerView.setAdapter(adapter);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        /*final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                app.showProgressDialog("Загрузка с сервера...");
            }
        }, 50);*/
        initTestTransaction();
    }

    void initTestTransaction() {
        for (int i=0; i<5; i++) {
            adapter.onAdd(new SessionModel());
        }
    }
}
