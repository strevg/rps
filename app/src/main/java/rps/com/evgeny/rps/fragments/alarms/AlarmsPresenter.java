package rps.com.evgeny.rps.fragments.alarms;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rps.com.evgeny.rps.di.ActivityScoped;
import rps.com.evgeny.rps.fragments.warning.adapters.WarningAdapter;
import rps.com.evgeny.rps.fragments.zone.adapter.BaseType;
import rps.com.evgeny.rps.fragments.zone.adapter.CallingModel;
import rps.com.evgeny.rps.fragments.zone.adapter.ZoneBottomAdapter;
import rps.com.evgeny.rps.models.WarningAddedEventBus;
import rps.com.evgeny.rps.models.network.AlarmWebSocketModel;
import rps.com.evgeny.rps.models.network.AlarmZoneModel;
import rps.com.evgeny.rps.models.network.DeviceZoneModel;
import rps.com.evgeny.rps.models.network.WebSocketModel;
import rps.com.evgeny.rps.models.network.ZoneTableModel;
import rps.com.evgeny.rps.models.warnings.WarningComparator;
import rps.com.evgeny.rps.models.warnings.WarningModel;
import rps.com.evgeny.rps.network.ApiFactory;
import rps.com.evgeny.rps.network.RPSApi;
import rps.com.evgeny.rps.services.ControlManager;
import rps.com.evgeny.rps.services.ProximityManager;
import rps.com.evgeny.rps.services.RealmManager;
import rps.com.evgeny.rps.services.RpsWebSocket;
import rps.com.evgeny.rps.services.linphone.LinphoneManager;
import rps.com.evgeny.rps.services.linphone.callback.PhoneListener;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.Settings;
import rps.com.evgeny.rps.views.onItemClickListener;

@ActivityScoped
public class AlarmsPresenter implements AlarmsContract.Presenter, onItemClickListener<WarningModel>,
        ZoneBottomAdapter.onCallFinish, WarningAdapter.onCallingInterface, PhoneListener,
        RpsWebSocket.WebSocketMessageListener, ProximityManager.ProximityCallback {
    @Nullable
    AlarmsContract.View view;

    WarningAdapter activeAdapter;
    WarningAdapter inactiveAdapter;
    ZoneBottomAdapter bottomAdapter;

    ControlManager controlManager;
    LinphoneManager linphoneManager;
    RealmManager realmManager;
    ProximityManager proximityManager;
    RPSApi rpsApi;

    @Inject
    AlarmsPresenter() {
        rpsApi = ApiFactory.getRpsApi(Settings.siteName + "/");
    }

    @Override
    public void takeView(AlarmsContract.View view) {
        this.view = view;

        activeAdapter = new WarningAdapter();
        view.setActiveAdapter(activeAdapter);
        activeAdapter.setOnClickListener(this);
        activeAdapter.setOnCallingListener(this);

        inactiveAdapter = new WarningAdapter(true);
        view.setInactiveAdapter(inactiveAdapter);

        bottomAdapter = new ZoneBottomAdapter();
        bottomAdapter.setOnCallListener(this);
        view.setBottomAdapter(bottomAdapter);

        controlManager = ControlManager.getInstance();

        downloadZoneDevices();
        this.realmManager = RealmManager.getInstance();

        linphoneManager = LinphoneManager.getInstance(view.getContext());
        proximityManager = ProximityManager.getInstance(view.getContext());
    }

    @Override
    public void setPhoneListener() {
        linphoneManager.setPhoneListener(this);
        controlManager.setListener(this);
        checkActiveCall();
    }

    @Override
    public void dropView() {
        this.view = null;
        EventBus.getDefault().unregister(this);
        controlManager.removeListener(this);
    }

    void checkActiveCall() {
        bottomAdapter.onRemoveAll();
        if (existsActiveCall()) {
            String id = linphoneManager.getCalls()[0].toString();
            showActiveCalling(id);
            onCallConnect(id);
        }
    }

    boolean existsActiveCall() {
        return (rps.com.evgeny.rps.services.linphone.linphone.LinphoneManager.isInstanceiated()
                && linphoneManager.getCalls()!=null && linphoneManager.getCalls().length>0);
    }

    void downloadZoneDevices() {
        view.showProgressBar(true);
        rpsApi.getTableZones(ApiFactory.getHeaders()).enqueue(new Callback<List<ZoneTableModel>>() {
            @Override
            public void onResponse(Call<List<ZoneTableModel>> call, Response<List<ZoneTableModel>> response) {
                if (response.isSuccessful()) {
                    initAdapterData(response.body());
                }
                Log.i(Consts.TAG, response.body().toString());
                view.showProgressBar(false);
            }

            @Override
            public void onFailure(Call<List<ZoneTableModel>> call, Throwable t) {
                Log.e(Consts.TAG, t.getMessage());
                view.showProgressBar(false);
            }
        });
    }

    void initAdapterData(List<ZoneTableModel> data) {
        List<BaseType> activeData = getActiveData(data);
        List<BaseType> inactiveData = getInactiveData(data);

        activeAdapter.onAddAll(activeData);
        inactiveAdapter.onAddAll(inactiveData);

        view.setNumberAlarms(activeData.size());
    }

    List<BaseType> getActiveData(List<ZoneTableModel> list) {
        List<WarningModel> items = new ArrayList<>();
        for (ZoneTableModel zone : list) {
            for (DeviceZoneModel deviceZone : zone.DeviceModel) {
                for (AlarmZoneModel alarmZone : deviceZone.alarms) {
                    if (!realmManager.isExist(alarmZone.getId()))
                        items.add(new WarningModel(alarmZone));
                }
            }
        }
        Collections.sort(items, new WarningComparator());

        List<BaseType> baseList = new ArrayList<>();
        for (WarningModel model : items) {
            baseList.add(model);
        }

        return baseList;
    }

    List<BaseType> getInactiveData(List<ZoneTableModel> list) {
        List<WarningModel> items = new ArrayList<>();
        for (ZoneTableModel zone : list) {
            for (DeviceZoneModel deviceZone : zone.DeviceModel) {
                for (AlarmZoneModel alarmZone : deviceZone.alarms) {
                    if (realmManager.isExist(alarmZone.getId()))
                        items.add(new WarningModel(alarmZone));
                }
            }
        }
        Collections.sort(items, new WarningComparator());

        List<BaseType> baseList = new ArrayList<>();
        for (WarningModel model : items) {
            baseList.add(model);
        }

        return baseList;
    }

    // -------- WebSocketMessageListener ------------
    @Override
    public void onMessage(final WebSocketModel model) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (model.getMessageType() == Consts.MessageType.ALARM) {
                    if (model.getAlarm().End!=null) {
                        onCloseAlarm(model.getAlarm());
                    } else {
                        onAddAlarm(model.getAlarm());
                    }
                } else if (model.getMessageType()==Consts.MessageType.CALLING) {
                    DeviceZoneModel device = controlManager.getDevice(model.getId());
                    activeAdapter.onAdd(new CallingModel(model.getId(), device.Name));
                    view.setNumberAlarms(activeAdapter.getItemCount());
                    view.scrollToPosition(0);
                }
            }
        });
    }

    void onAddAlarm(final AlarmWebSocketModel alarm) {
        activeAdapter.onAdd(new WarningModel(alarm));
        view.scrollToPosition(0);

        view.setNumberAlarms(activeAdapter.getItemCount());
    }

    void onCloseAlarm(final AlarmWebSocketModel alarm) {
        int index = activeAdapter.getIndex(alarm.getId());
        view.scrollToPosition(index!=-1?index:0);
        activeAdapter.onRemove(alarm.getId());
        inactiveAdapter.onRemove(alarm.getId());

        view.setNumberAlarms(activeAdapter.getItemCount());
    }

    void showActiveCalling(final String id) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                activeAdapter.onAdd(new CallingModel(id, "Вызов с устройства"));
                view.setNumberAlarms(activeAdapter.getItemCount());
                view.scrollToPosition(0);
            }
        });
    }

    @Override
    public void onItemClick(View v, WarningModel item) {
        activeAdapter.onRemove(item);
        realmManager.add(RealmManager.getRealmModel(item.getModel()));

        inactiveAdapter.onAdd(item);

        view.setNumberAlarms(activeAdapter.getItemCount());
    }

    @Override
    public void onFinish(CallingModel model) {
        linphoneManager.onCancel();
    }

    @Override
    public void onAnswer(CallingModel model) {
        if (model.getId().length()==36) {
            activeAdapter.onRemove(model);
            DeviceZoneModel device = controlManager.getDevice(model.getId());
            controlManager.setDevice(device);
            controlManager.onCall(device.Host);

            view.setNumberAlarms(activeAdapter.getItemCount());
        } else {
            linphoneManager.onAnswer();
        }
    }

    @Override
    public void onCancel(CallingModel model) {
        if (model.getId().length()==36) {
            activeAdapter.onRemove(model);
            realmManager.add(RealmManager.getRealmModel(model.getId(), model.getText()));

            inactiveAdapter.onAdd(model);

            view.setNumberAlarms(activeAdapter.getItemCount());
        } else {
            linphoneManager.onCancel();
        }
    }

    void onFinishCall(String id) {
        int bottomPos = bottomAdapter.getIndex(id);
        BaseType callingMode;
        if (bottomPos!=-1) {
            callingMode = bottomAdapter.getItem(bottomPos);
            bottomAdapter.onRemove(id);
        } else {
            callingMode = activeAdapter.getItem(id);
            activeAdapter.onRemove(id);
            view.setNumberAlarms(activeAdapter.getItemCount());
        }
        realmManager.add(RealmManager.getRealmModel(id, "Вызов с устройства"));

        inactiveAdapter.onAdd(callingMode);
    }

    // ------- linphone listener -------
    @Override
    public void onCallStart(final String id) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (bottomAdapter.getIndex(id)==-1)
                    bottomAdapter.onAdd(new CallingModel(id, controlManager.getDevice().Name, true));
            }
        });
    }

    @Override
    public void onCallConnect(final String id) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                activeAdapter.onRemove(id);
                view.setNumberAlarms(activeAdapter.getItemCount());

                DeviceZoneModel device = controlManager.getDevice();
                CallingModel model = new CallingModel(id, device.Name, true);
                bottomAdapter.onAdd(model);
            }
        });
    }

    @Override
    public void onCallFinish(final String id) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                onFinishCall(id);
            }
        });
    }

    @Override
    public void onIncomingCall(String id) {
        onAnswer(new CallingModel(id));
    }

    @Override
    public void onError(String message) {
        Log.e(Consts.TAG, message);
    }

    @Override
    public void approachSensor() {
        if (existsActiveCall() && view!=null) {
            view.shadowCallingScreen(true);
        }
    }

    @Override
    public void distanceFromSensor() {
        if (existsActiveCall() && view!=null) {
            view.shadowCallingScreen(false);
        }
    }
}
