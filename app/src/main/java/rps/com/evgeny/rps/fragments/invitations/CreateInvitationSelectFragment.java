package rps.com.evgeny.rps.fragments.invitations;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rps.com.evgeny.rps.MainApplication;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.models.BarCodeInUse;
import rps.com.evgeny.rps.network.ApiFactory;
import rps.com.evgeny.rps.network.RPSApi;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.Settings;
import rps.com.evgeny.rps.views.adapters.CreateInvitationAdapter;
import rps.com.evgeny.rps.views.adapters.ReadDataAdapter;
import rps.com.evgeny.rps.views.onItemClickListener;

/**
 * Created by Evgeny on 23.09.2017.
 */

public class CreateInvitationSelectFragment extends Fragment {
    RecyclerView recyclerView;
    CreateInvitationAdapter adapter;
    MainApplication app;
    RPSApi rpsApi;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (MainApplication)getActivity().getApplication();
        rpsApi = ApiFactory.getRpsApi(Settings.siteName+"/");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.list_layout, container, false);
        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        adapter = new CreateInvitationAdapter(getActivity(), new ArrayList<BarCodeInUse>());
        adapter.setOnItemClickListener(onItemClickListener);
        recyclerView.setAdapter(adapter);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        app.getMainActivity().setTitle(getString(R.string.title_select_create_invitation));
        app.getMainActivity().setSubtitle(getString(R.string.title_step_1));
    }

    onItemClickListener<BarCodeInUse> onItemClickListener = new onItemClickListener<BarCodeInUse>() {
        @Override
        public void onItemClick(View v, BarCodeInUse item) {
            CreateInvitationHelper.getInstance().setBarcode(item);
            app.getMainActivity().replaceFragment(Consts.FragmentType.CREATE_INVITATION, null);
        }
    };

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                app.showProgressDialog("Загрузка с сервера...");
            }
        }, 50);
        loadBarcodes();
    }

    public void loadBarcodes() {
        rpsApi.getListBarCodes(ApiFactory.getHeaders()).enqueue(new Callback<List<BarCodeInUse>>() {
            @Override
            public void onResponse(Call<List<BarCodeInUse>> call, Response<List<BarCodeInUse>> response) {
                if (response.isSuccessful()) {
                    List<BarCodeInUse> data = response.body();
                    for (BarCodeInUse barCodeModel : data) {
                        adapter.onAdd(barCodeModel);
                    }
                    if (CreateInvitationHelper.getInstance().isSkipBarcode()) {
                        for (BarCodeInUse barcode : data) {
                            if (barcode.Id.equals(CreateInvitationHelper.getInstance().getBarcode().Id)) {
                                CreateInvitationHelper.getInstance().setBarcode(barcode);
                            }
                        }
                        app.getMainActivity().replaceFragment(Consts.FragmentType.CREATE_INVITATION, null);
                        CreateInvitationHelper.getInstance().setSkipBarcode(false);
                    }
                }
                app.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<List<BarCodeInUse>> call, Throwable t) {
                Log.e(Consts.TAG, "loadBarcodes " + t.toString());
                Crashlytics.logException(t);
                app.dismissProgressDialog();
            }
        });
    }

    void initTestBarcode() {
        for (int i=0; i<5; i++) {
            adapter.onAdd(new BarCodeInUse("Descriptions"));
        }
    }
}
