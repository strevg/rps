package rps.com.evgeny.rps.fragments.zone.details;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import rps.com.evgeny.rps.di.ActivityScoped;
import rps.com.evgeny.rps.di.FragmentScoped;

/**
 * Created by Evgeny on 08.06.2018.
 */
@Module
public abstract class AlarmModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract AlarmFragment alarmFragment();

    @ActivityScoped
    @Binds
    abstract AlarmContract.Presenter alarmPresenter(AlarmPresenter presenter);
}
