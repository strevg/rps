package rps.com.evgeny.rps.fragments;

import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import rps.com.evgeny.rps.MainApplication;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.databinding.MainFragmentBinding;
import rps.com.evgeny.rps.nfc.CardReader;
import rps.com.evgeny.rps.utils.Settings;
import rps.com.evgeny.rps.utils.StringUtils;

/**
 * Created by Evgeny on 30.08.2017.
 */

public class CardReaderFragment extends Fragment {
    MainFragmentBinding binding;
    public CardReader cardReader;
    MainApplication app;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (MainApplication) getActivity().getApplication();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.main_fragment, container, false);
        View v = binding.getRoot();
        if (app.getPackageManager().hasSystemFeature(PackageManager.FEATURE_NFC) ) {
            if (v != null) {
                binding.cardAccountField.setText(R.string.read_card);
                cardReader = Settings.cardReader;
            }
        }
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (StringUtils.isNullOrEmpty(Settings.userRole))
            app.getMainActivity().setTitle(getString(R.string.title_sales));
        else
            app.getMainActivity().setTitle(getString(R.string.title_work_with_card));

        app.getMainActivity().setSubtitle(null);

        if (!app.getMainActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_NFC)) {
            binding.support.setVisibility(View.VISIBLE);
            binding.cardAccountField.setText("Чтение NFC карт недоступно на данном устройстве!");
        }
    }
}
