package rps.com.evgeny.rps.fragments.warning.active;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import rps.com.evgeny.rps.di.ActivityScoped;
import rps.com.evgeny.rps.di.FragmentScoped;

/**
 * Created by Evgeny on 24.05.2018.
 */

@Module
public abstract class WarningActiveModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract WarningActiveFragment warningActiveFragment();

    @ActivityScoped
    @Binds
    abstract WarningActiveContract.Presenter warningActivePresenter(WarningActivePresenter presenter);
}
