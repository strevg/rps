package rps.com.evgeny.rps.fragments;

/**
 * Created by Evgeny on 29.09.2017.
 */

public interface onBackPressInterface {
    boolean onBackPress();
}
