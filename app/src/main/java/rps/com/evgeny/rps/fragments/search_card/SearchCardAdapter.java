package rps.com.evgeny.rps.fragments.search_card;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;
import rps.com.evgeny.rps.databinding.ViewholderSearchCardBinding;
import rps.com.evgeny.rps.models.CardModel;
import rps.com.evgeny.rps.models.SearchCard;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.views.AdapterInterface;
import rps.com.evgeny.rps.views.onItemClickListener;
import rps.com.evgeny.rps.views.viewholders.SearchCardViewholder;

public class SearchCardAdapter extends RecyclerView.Adapter<SearchCardViewholder>
        implements AdapterInterface<CardModel> {
    List<CardModel> items;
    onItemClickListener<CardModel> clickListener;

    public SearchCardAdapter() {
        items = new ArrayList<>();
    }

    public void setClickListener(onItemClickListener listener) {
        this.clickListener = listener;
    }

    @Override
    public SearchCardViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewholderSearchCardBinding binding = ViewholderSearchCardBinding.inflate(inflater, parent, false);
        return new SearchCardViewholder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(SearchCardViewholder holder, int position) {
        final CardModel model = items.get(position);
        holder.setData(model);
        holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onItemClick(v, model);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onAdd(CardModel item) {
        int position = items.size();
        items.add(position, item);
        notifyItemInserted(position);
    }

    public void onRemove(CardModel model) {
        if (model == null)
            return;
        int pos = items.indexOf(model);
        items.remove(pos);
        notifyItemRemoved(pos);
    }

    @Override
    public void onRemove(int pos) {
        if (pos < 0)
            return;
        items.remove(pos);
        notifyItemRemoved(pos);
    }

    @Override
    public void onRemoveAll() {
        int count = getItemCount();
        items.clear();
        notifyItemRangeRemoved(0, count);
    }

    @Override
    public void onAddAll(List<CardModel> items) {
        this.items = items;
        notifyDataSetChanged();
    }
}
