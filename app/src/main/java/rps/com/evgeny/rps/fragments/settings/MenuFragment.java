package rps.com.evgeny.rps.fragments.settings;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import javax.inject.Inject;
import dagger.android.support.DaggerFragment;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.activities.AdminMainActivity;
import rps.com.evgeny.rps.databinding.FragmentMenuBinding;

public class MenuFragment extends DaggerFragment implements MenuContract.View {
    FragmentMenuBinding binding;

    @Inject
    MenuContract.Presenter presenter;

    @Inject
    public MenuFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_menu, container, false);
        initViews();
        return binding.getRoot();
    }

    void initViews() {
        binding.exitButton.setOnClickListener(presenter.getOnClickListener());
        binding.helpButton.setOnClickListener(presenter.getOnClickListener());
        binding.infoButton.setOnClickListener(presenter.getOnClickListener());
        binding.settingsButton.setOnClickListener(presenter.getOnClickListener());
    }

    @Override
    public AdminMainActivity getAdminMainActivity() {
        return (AdminMainActivity) getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.takeView(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.dropView();
    }

    @Override
    public void showInfoFragment() {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.container, new InfoFragment());
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void showHelpFragment() {
        Toast.makeText(getActivity(), R.string.hint_not_help_yet, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showSettingsFragment() {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.container, new SettingsFrament());
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
