package rps.com.evgeny.rps.models.network;

import java.util.List;
import java.util.UUID;

public class AlarmWebSocketModel {
    public List<UUID> Id;
    List<Boolean> _IsDeleted;
    List<String> Sync;
    public String Host;
    public String User;
    public String Password;
    public String Advanced;
    public String DataBase;
    public String SlaveCode;
    public String action;
    public String id;
    private String DeviceId;
    public String Begin;
    public String End;
    public String Value;
    public String ValueEnd;
    public int AlarmColorId;
    public int TypeId;
    public String Name;
    public String color;
    public String state;
    public String Type;
    public String Device;
    public String Duration;

    public String getId() {
        return id;
    }
}
