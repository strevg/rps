package rps.com.evgeny.rps.fragments.calling;

import android.support.annotation.Nullable;
import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.models.event_bus.RackModeChange;
import rps.com.evgeny.rps.models.network.DeviceZoneModel;
import rps.com.evgeny.rps.models.network.WebSocketModel;
import rps.com.evgeny.rps.models.network.ZoneTableModel;
import rps.com.evgeny.rps.network.ApiFactory;
import rps.com.evgeny.rps.network.RPSApi;
import rps.com.evgeny.rps.services.ControlManager;
import rps.com.evgeny.rps.services.ProximityManager;
import rps.com.evgeny.rps.services.RpsWebSocket;
import rps.com.evgeny.rps.services.SoundVibrationManager;
import rps.com.evgeny.rps.services.TimeoutManager;
import rps.com.evgeny.rps.services.TimerManager;
import rps.com.evgeny.rps.services.linphone.LinphoneManager;
import rps.com.evgeny.rps.services.linphone.callback.PhoneListener;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.Settings;

/**
 * Created by Evgeny on 11.07.2018.
 */

public class CallingPresenter implements CallingContract.Presenter,
        RpsWebSocket.WebSocketMessageListener, PhoneListener, TimerManager.TimerTick,
        TimeoutManager.TimeoutCallback, ProximityManager.ProximityCallback,
        ControlManager.ControlManagerCallback {

    ControlManager controlManager;
    TimerManager timerManager;
    LinphoneManager linphoneManager;
    ProximityManager proximityManager;
    WebSocketModel socketModel;
    SoundVibrationManager soundVibrationManager;
    RPSApi rpsApi;

    String deviceID;

    @Nullable
    CallingContract.View view;

    @Inject
    CallingPresenter() {
    }

    @Override
    public void takeView(CallingContract.View view) {
        this.view = view;
        controlManager = ControlManager.getInstance();
        controlManager.setListener(this);
        controlManager.setTimeoutCallback(this);
        controlManager.setCallBack(this);

        rpsApi = ApiFactory.getRpsApi(Settings.siteName + "/");

        timerManager = new TimerManager(this);

        linphoneManager = LinphoneManager.getInstance(view.getCallingActivity());
        linphoneManager.setPhoneListener(this);

        proximityManager = ProximityManager.getInstance(view.getCallingActivity());
        proximityManager.register(this);

        soundVibrationManager = SoundVibrationManager.getInstance(view.getCallingActivity());

        checkZoneDevice();
        soundVibrationManager.start();
    }

    @Override
    public void dropView() {
        this.view = null;
        controlManager.removeListener(this);
        timerManager.stopTimer();
        soundVibrationManager.stop();
    }

    @Override
    public void answerCall() {
        DeviceZoneModel device = controlManager.getDevice(deviceID);
        controlManager.setDevice(device);
        controlManager.onCall(device.Host);
        soundVibrationManager.stop();
    }

    @Override
    public void cancelCall() {
        soundVibrationManager.stop();
        view.getCallingActivity().finish();
    }

    @Override
    public void endCall() {
        controlManager.onStop();
        linphoneManager.onCancel();
        soundVibrationManager.stop();
        view.getCallingActivity().finish();
    }

    @Override
    public void passSingleCar() {
        if (socketModel!=null && socketModel.getMessageType()==Consts.MessageType.IN_PROCCESS) {
            controlManager.cancelSingleCar();
        } else {
            controlManager.passSingleCar();
        }
    }

    @Override
    public void mute() {
        Log.i(Consts.TAG, "Calling Mute");
    }

    @Override
    public void sound() {
        Log.i(Consts.TAG, "Calling Sound");
    }

    @Override
    public void setDeviceID(String id) {
        this.deviceID = id;
    }

    boolean existsActiveCall() {
        return (rps.com.evgeny.rps.services.linphone.linphone.LinphoneManager.isInstanceiated()
                && linphoneManager.getCalls()!=null && linphoneManager.getCalls().length>0);
    }

    void checkZoneDevice() {
        Log.i(Consts.TAG, "checkZone device");
        if (controlManager.getZoneList()==null) {
            downloadZoneDevices();
        } else {
            Log.i(Consts.TAG, "checkZone device set");
            DeviceZoneModel device = controlManager.getDevice(deviceID);
            view.setTitle(device.Name);
        }
    }

    @Override
    public void timeIsOver(int error) {
        if (view!=null && (error == R.string.error_pass_car || error==R.string.error_cancel_car)) {
            view.showToast(error);
            view.showPassCarProgress(false);
        }
    }

    @Override
    public void onMessage(WebSocketModel model) {
        if (socketModel == null ||(model!=null && socketModel.getId().equalsIgnoreCase(model.getId()) && view!=null)) {
            handleMessage(model);
        }
    }

    void handleMessage(WebSocketModel model) {
        if (socketModel == null || socketModel.getMessageType()==Consts.MessageType.COMPLETE ||
                socketModel.getMessageType()== Consts.MessageType.CANCELED) {
            if (model.getMessageType()== Consts.MessageType.IN_PROCCESS) {
                view.showPassCarProgress(true);
            }
        } else if (socketModel.getMessageType()==Consts.MessageType.IN_PROCCESS) {
            if (model.getMessageType()==Consts.MessageType.COMPLETE
                    || model.getMessageType()==Consts.MessageType.CANCELED) {
                view.showPassCarProgress(false);
            }
        }
        this.socketModel = model;
    }

    //---------- Phone listener -----------
    @Override
    public void onCallStart(String id) {}

    @Override
    public void onCallConnect(String id) {
        view.showTalkingState();
        timerManager.startTimer();
        soundVibrationManager.stop();
    }

    @Override
    public void onCallFinish(String id) {
        timerManager.stopTimer();
    }
    @Override
    public void onIncomingCall(String id) {
        linphoneManager.onAnswer();
    }

    @Override
    public void onError(String message) {
        timerManager.stopTimer();
        Log.e(Consts.TAG, "Error calling");
    }

    @Override
    public void onTick() {
        if (view!=null)
            view.showTime(linphoneManager.getDuration());
    }

    @Override
    public void onSuccessChangeRack(RackModeChange item) {}

    @Override
    public void onSuccessPostReasonChangeMode() {}

    @Override
    public void onRequestFailed(int code) {
        if (view!=null && (code == R.string.error_pass_car || code==R.string.error_cancel_car)) {
            view.showToast(code);
            view.showPassCarProgress(false);
        }
    }

    @Override
    public void onRequestSuccess(int code) {
        if (view != null && (code == R.string.error_pass_car || code==R.string.error_cancel_car)) {
            view.showPassCarProgress(false);
        }
    }

    @Override
    public void approachSensor() {
        if (existsActiveCall() && view!=null) {
            view.lockScreenWhileTalking(true);
        }
    }

    @Override
    public void distanceFromSensor() {
        if (existsActiveCall() && view!=null) {
            view.lockScreenWhileTalking(false);
        }
    }

    void downloadZoneDevices() {
        rpsApi.getTableZones(ApiFactory.getHeaders()).enqueue(new Callback<List<ZoneTableModel>>() {
            @Override
            public void onResponse(Call<List<ZoneTableModel>> call, Response<List<ZoneTableModel>> response) {
                if (response.isSuccessful()) {
                    Log.i(Consts.TAG, "checkZone device downloadZoneDevices");
                    controlManager.setZoneList(response.body());
                    DeviceZoneModel device = controlManager.getDevice(deviceID);
                    view.setTitle(device.Name);
                }
                Log.i(Consts.TAG, response.body().toString());
            }

            @Override
            public void onFailure(Call<List<ZoneTableModel>> call, Throwable t) {
                Log.e(Consts.TAG, t.getMessage());
            }
        });
    }
}
