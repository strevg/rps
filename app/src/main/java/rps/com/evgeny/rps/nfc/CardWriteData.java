package rps.com.evgeny.rps.nfc;

/**
 * Created by Evgeny on 01.09.2017.
 */

public class CardWriteData {
    public String CardId;
    public int ParkingEnterTime;
    public int LastRecountTime;
    public byte TSidFC;
    public byte TPidFC;
    public byte ZoneidFC;
    public byte ClientGroupidFC;
    public int SumOnCard;
    public int LastPaymentTime;
    public int Nulltime1;
    public int Nulltime2;
    public int Nulltime3;
    public int TVP;
    public byte TKVP;
    public byte ClientTypidFC;
    public int DateSaveCard;
}
