package rps.com.evgeny.rps.fragments.main;

import rps.com.evgeny.rps.di.BasePresenter;
import rps.com.evgeny.rps.di.BaseView;

/**
 * Created by Evgeny on 09.06.2018.
 */

public interface MainAdminContract {
    interface View extends BaseView<Presenter> {
    }

    interface Presenter extends BasePresenter<View> {
        void takeView(MainAdminContract.View view);

        void dropView();
    }
}
