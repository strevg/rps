package rps.com.evgeny.rps.fragments.warning.active;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import rps.com.evgeny.rps.di.BasePresenter;
import rps.com.evgeny.rps.di.BaseView;

/**
 * Created by Evgeny on 24.05.2018.
 */

public interface WarningActiveContract {
    interface View extends BaseView<Presenter> {
        void setAdapter(RecyclerView.Adapter adapter);
        void setAdapterBottom(RecyclerView.Adapter adapter);
        void scrollToPosition(int pos);
        void showProgressBar(boolean show);
        void shadowCallingScreen(boolean show);
        Context getContext();
    }

    interface Presenter extends BasePresenter<View> {
        void takeView(WarningActiveContract.View view);
        void dropView();
        void setPhoneListener();
    }
}
