package rps.com.evgeny.rps.nfc;

/**
 * Created by Evgeny on 03.09.2017.
 */

public class CardReaderEventArgs {
    public CardReaderResponse card;
    public CardReader sender;

    public CardReaderEventArgs() {}

    public CardReaderEventArgs(CardReaderResponse card, CardReader sender) {
        this.card = card;
        this.sender =sender;
    }
}