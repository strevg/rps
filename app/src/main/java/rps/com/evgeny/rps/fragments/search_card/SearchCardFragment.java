package rps.com.evgeny.rps.fragments.search_card;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.databinding.FragmentSearchCardBinding;

/**
 * Created by Evgeny on 15.11.2018.
 */

public class SearchCardFragment extends DaggerFragment implements SearchCardContract.View {
    FragmentSearchCardBinding binding;
    LinearLayoutManager layoutManager;

    @Inject
    SearchCardContract.Presenter presenter;

    @Inject
    public SearchCardFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search_card, container, false);
        initViews();
        return binding.getRoot();
    }

    void initViews() {
        layoutManager = new LinearLayoutManager(getActivity());
        binding.recyclerView.setLayoutManager(layoutManager);

        presenter.takeView(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.dropView();
    }

    @Override
    public void setAdapter(RecyclerView.Adapter adapter) {
        binding.recyclerView.setAdapter(adapter);
    }

    @Override
    public void showEmptyList(final boolean show) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                binding.emptyListContainer.setVisibility(show?View.VISIBLE:View.GONE);
            }
        });
    }
}
