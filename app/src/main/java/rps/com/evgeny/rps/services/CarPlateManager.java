package rps.com.evgeny.rps.services;

import android.util.Log;

import java.util.Map;
import java.util.TreeMap;

public class CarPlateManager {
    private static CarPlateManager instance;
    private String platePattern = "(\\w){1}([0-9]{3})(\\w){2}(\\w){2,}";
    private Map<String, String> mapChars;
    private String number;
    private String region;

    private CarPlateManager() {
        initMapChars();
    }

    public static CarPlateManager getInstance() {
        if (instance==null) {
            instance = new CarPlateManager();
        }
        return instance;
    }

    private void initMapChars() {
        mapChars = new TreeMap<>();
        mapChars.put("A", "А");
        mapChars.put("B", "В");
        mapChars.put("E", "Е");
        mapChars.put("K", "К");
        mapChars.put("M", "М");
        mapChars.put("H", "Н");
        mapChars.put("O", "О");
        mapChars.put("P", "Р");
        mapChars.put("C", "С");
        mapChars.put("T", "Т");
        mapChars.put("Y", "У");
        mapChars.put("X", "Х");
    }

    public boolean isNumberPlace(String text) {
        String tmp = text.replace(" ", "").toUpperCase();
        tmp = tmp.replace("\n","");

        if (tmp.matches(platePattern)) {
            number = getCorrectedNumber(tmp.substring(0, 6));
            region = getCorrectedRegion(tmp.substring(6));
            return true;
        }
        return false;
    }

    public String getNumberPlate() {
        return number;
    }

    public String getRegionPlate() {
        return region;
    }

    String getCorrectedNumber(String number) {
        StringBuffer correctNumber = new StringBuffer();
        for (int i=0; i<number.length();i++) {
            char ch = number.charAt(i);
            String newChar;
            if (i>0 && i<4)
                newChar = correctNumber(ch);
            else
                newChar = correctChar(ch);
            correctNumber.append(newChar);
        }
        return correctNumber.toString();
    }

    String getCorrectedRegion(String region) {
        StringBuffer correctRegion = new StringBuffer();
        for (int i=0; (i<region.length()&&i<3);i++) {
            char ch = region.charAt(i);
            String newChar = correctNumber(ch);
            try {
                int num = Integer.valueOf(newChar);
                if (correctRegion.length()==0 || !(Integer.valueOf(correctRegion.toString())<10 && i==2))
                    correctRegion.append(newChar);
            } catch (Exception e) {}
        }
        return correctRegion.toString();
    }

    String correctNumber(char ch) {
        if (ch=='O') {
            ch = '0';
        } else if (ch == 'Z') {
            ch = '7';
        }
        return String.valueOf(ch);
    }

    String correctChar(char ch) {
        if (ch=='0') {
            ch = 'O';
        }
        return mapChars.get(String.valueOf(ch));
    }
}
