package rps.com.evgeny.rps.views.viewholders;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.databinding.TransactionViewHolderBinding;
import rps.com.evgeny.rps.models.person.TransactionModel;
import rps.com.evgeny.rps.utils.TimeUtils;

import static android.R.attr.value;

/**
 * Created by Evgeny on 23.09.2017.
 */

public class TransactionViewHolder extends RecyclerView.ViewHolder {
    TransactionViewHolderBinding binding;
    Context context;

    public TransactionViewHolder(View itemView) {
        super(itemView);
        binding = DataBindingUtil.bind(itemView);
        context = binding.getRoot().getContext();
    }

    public void setTransaction(TransactionModel model) {
        setTimeTransaction(model.Time);
        setIndex(model.index+"");
        setNameDevice(model.DeviceName);
        setTypeDevice(model);
        initImageType(model);
        hideAll();

        if (model.DeviceTypeID == 1) {
            if (model.Transit == 1)
                initComeIn(model);
            else if (model.Transit == 2)
                initComeOut(model);
            else if (model.Transit == 3)
                initMoving(model);
        } else {
            initKassa(model);
        }
    }

    void hideAll() {
        binding.containerEnterTime.setVisibility(View.GONE);
        binding.containerParkingTime.setVisibility(View.GONE);
        binding.containerPayment.setVisibility(View.GONE);
        binding.containerPaymentType.setVisibility(View.GONE);
        binding.containerTransactionType.setVisibility(View.GONE);
        binding.containerZoneAfter.setVisibility(View.GONE);
        binding.containerZoneBefore.setVisibility(View.GONE);
    }

    void initComeIn(TransactionModel model) {
        setTypeTransaction(model.Type);
        setZoneAfter(model.ZoneAfterId);
        binding.containerTransactionType.setVisibility(View.VISIBLE);
        binding.containerZoneAfter.setVisibility(View.VISIBLE);
    }

    void initComeOut(TransactionModel model) {
        setTypeTransaction(model.Type);
        setTimeParking(model.ParkingTime);
        setZoneBefore(model.ZoneBeforeId);
        setTimeComeIn(model.TimeEntry);
        binding.containerTransactionType.setVisibility(View.VISIBLE);
        binding.containerEnterTime.setVisibility(View.VISIBLE);
        binding.containerParkingTime.setVisibility(View.VISIBLE);
        binding.containerZoneBefore.setVisibility(View.VISIBLE);
    }

    void initMoving(TransactionModel model) {
        setTypeTransaction(model.Type);
        setZoneBefore(model.ZoneBeforeId);
        setZoneAfter(model.ZoneAfterId);
        binding.containerTransactionType.setVisibility(View.VISIBLE);
        binding.containerZoneBefore.setVisibility(View.VISIBLE);
        binding.containerZoneAfter.setVisibility(View.VISIBLE);
    }

    void initKassa(TransactionModel model) {
        setTypePayment(model.PaymentType);
        setSum(model.Paid);
        setAcceptedSum(model.SumOnCardBefore);
        setIssuedSum(model.SumOnCardAfter);

        binding.containerPayment.setVisibility(View.VISIBLE);
        binding.containerPaymentType.setVisibility(View.VISIBLE);
    }

    public void setTimeTransaction(String time) {
        time = TimeUtils.getTimeIn(time);
        binding.timeTransaction.setText(time);
    }

    public void setTypeTransaction(String type) {
        binding.typeTransaction.setText(type);
    }

    public void setTimeParking(String time) {
        time = time.equals("0")?"-":time;
        binding.timeParking.setText(time);
    }

    public void setZoneBefore(String zone) {
        binding.zoneBefore.setText(zone);
    }

    public void setZoneAfter(String zone) {
        binding.zoneAfter.setText(zone);
    }

    public void setTimeComeIn(String time) {
        binding.timeComeInZone.setText(time);
    }

    public void setSum(String sum) {
        int summa = (int)Float.parseFloat(sum);
        binding.sumCard.setText(summa+"р");
    }

    public void setIndex(String index) {
        binding.indexTransaction.setText("ID: "+index);
    }

    public void setTypeDevice(TransactionModel model) {
        String type = "";
        if (model.DeviceTypeID == 1) {
            if (model.Transit == 1)
                type = "Стойка въезда";
            else if (model.Transit == 2)
                type = "Стойка выезда";
            else if (model.Transit == 3)
                type = "Стойка переезда";
        } else {
            type = "Касса";
        }
        binding.typeDevice.setText(type);
    }

    void initImageType(TransactionModel model) {
        Drawable image = null;
        int color = color = R.color.orange;
        if (model.DeviceTypeID == 1) {
            if (model.Transit == 1) {
                image = context.getResources().getDrawable(R.drawable.ic_arrow_forward_white_24dp);
                color = R.color.green;
            } else if (model.Transit == 2) {
                image = context.getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp);
                color = R.color.red;
            } else if (model.Transit == 3) {
                image = context.getResources().getDrawable(R.drawable.ic_arrow_forward_white_24dp);
            }
        } else {
            image = context.getResources().getDrawable(R.drawable.ic_attach_money_white_24dp);
        }
        binding.imageType.setImageDrawable(image);
        binding.imageType.setColorFilter(context.getResources().getColor(color), android.graphics.PorterDuff.Mode.MULTIPLY);
    }

    public void setNameDevice(String name) {
        binding.nameDevice.setText(name);
    }

    public void setTypePayment(String type) {
        binding.typePayment.setText(type);
    }

    public void setAcceptedSum(String sum) {
        binding.sumAccepted.setText(sum+"р");
    }

    public void setIssuedSum(String sum) {
        binding.sumIssued.setText(sum+"р");
    }
}
