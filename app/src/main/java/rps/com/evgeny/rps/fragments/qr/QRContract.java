package rps.com.evgeny.rps.fragments.qr;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import rps.com.evgeny.rps.di.BasePresenter;
import rps.com.evgeny.rps.di.BaseView;

/**
 * Created by Evgeny on 07.06.2018.
 */

public interface QRContract {
    interface View extends BaseView<Presenter> {
        void setAdapter(RecyclerView.Adapter adapter);
        void scrollToPostion(int position);
        Context getContext();
    }

    interface Presenter extends BasePresenter<View> {
        void takeView(QRContract.View view);
        void dropView();
        void findItem(String query);
    }
}
