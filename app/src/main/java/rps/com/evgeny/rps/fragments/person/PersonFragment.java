package rps.com.evgeny.rps.fragments.person;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.ref.WeakReference;
import rps.com.evgeny.rps.MainApplication;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.databinding.PersonFragmentBinding;
import rps.com.evgeny.rps.network.ApiFactory;
import rps.com.evgeny.rps.network.RPSApi;
import rps.com.evgeny.rps.utils.Settings;

/**
 * Created by Evgeny on 26.09.2017.
 */

public class PersonFragment extends Fragment {
    static WeakReference<PersonFragment> instance;
    PersonFragmentBinding binding;
    MainApplication app;
    RPSApi rpsApi;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (MainApplication) getActivity().getApplication();
        rpsApi = ApiFactory.getRpsApi(Settings.siteName+"/");

        instance = new WeakReference<>(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.person_fragment, container, false);
        setupViewPager(binding.viewpager);
        binding.tablayout.setupWithViewPager(binding.viewpager);
        initToolbar();
        return binding.getRoot();
    }

    void initToolbar() {
        ((AppCompatActivity)getActivity()).setSupportActionBar(binding.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(binding.toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
    }

    public void setTitle(String title, String subTitle) {
        if (title==null)
            title = "Нет привязки";
        binding.toolbar.setTitle(title);
        binding.toolbar.setSubtitle(subTitle);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        adapter.addFragment(new InformationFragment(), getString(R.string.info_tab));
        //adapter.addFragment(new SessionsFragment(), getString(R.string.session_tab));
        adapter.addFragment(new TransactionsFragment(), getString(R.string.transaction_tab));
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);
    }

    public static PersonFragment getInstance() {
        return instance.get();
    }
}
