package rps.com.evgeny.rps.models.zones_devices;

import rps.com.evgeny.rps.models.network.DeviceZoneModel;
import rps.com.evgeny.rps.utils.ViewUtils;

/**
 * Created by Evgeny on 30.05.2018.
 */

public class ZoneChildModel {
    public DeviceZoneModel model;
    String title;
    String text;
    int color;

    public ZoneChildModel(String title, String text) {
        this.title = title;
        this.text = text;
    }

    public ZoneChildModel(DeviceZoneModel model) {
        this.model = model;
        setTitle(model.Name);
        setText(model.RackWorkingModeName);
        this.color = ViewUtils.getMaxColor(model.alarms);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getColor() {
        return color;
    }

    public DeviceZoneModel getModel(){
        return model;
    }
}
