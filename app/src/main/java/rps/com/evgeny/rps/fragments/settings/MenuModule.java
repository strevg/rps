package rps.com.evgeny.rps.fragments.settings;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import rps.com.evgeny.rps.di.ActivityScoped;
import rps.com.evgeny.rps.di.FragmentScoped;

@Module
public abstract class MenuModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract MenuFragment menuFragment();

    @ActivityScoped
    @Binds
    abstract MenuContract.Presenter menuPresenter(MenuPresenter presenter);
}
