package rps.com.evgeny.rps.models.person;

import java.util.List;
import java.util.UUID;

/**
 * Created by Evgeny on 21.10.2017.
 */

public class UserModel {
    public UUID Id;
    public String LoginName;
    public String Password;
    public String Email;
    public String FullName;
    public UUID RoleId;
    public UUID id;
    public Boolean editable;
    public Boolean editable_fields;
    public Role role;
    public Object alerts;
    public Boolean alert_loop;
}
