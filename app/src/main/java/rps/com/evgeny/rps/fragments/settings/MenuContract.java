package rps.com.evgeny.rps.fragments.settings;

import rps.com.evgeny.rps.activities.AdminMainActivity;
import rps.com.evgeny.rps.di.BasePresenter;
import rps.com.evgeny.rps.di.BaseView;

public interface MenuContract {
    interface View extends BaseView<Presenter> {
        void showInfoFragment();
        void showHelpFragment();
        void showSettingsFragment();
        AdminMainActivity getAdminMainActivity();
    }

    interface Presenter extends BasePresenter<View> {
        void takeView(MenuContract.View view);
        void dropView();
        android.view.View.OnClickListener getOnClickListener();
    }
}
