package rps.com.evgeny.rps.fragments.zone.details;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import dagger.Lazy;
import dagger.android.support.DaggerFragment;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.databinding.FragmentRecyclerBinding;
import rps.com.evgeny.rps.models.network.DeviceZoneModel;

/**
 * Created by Evgeny on 08.06.2018.
 */

public class AlarmFragment extends DaggerFragment implements AlarmContract.View {
    FragmentRecyclerBinding binding;

    @Inject
    AlarmContract.Presenter presenter;

    @Inject
    public AlarmFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_recycler, container, false);
        initViews();
        return binding.getRoot();
    }

    void initViews() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        binding.recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public void setAdapter(RecyclerView.Adapter adapter) {
        binding.recyclerView.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.takeView(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.dropView();
    }

    @Override
    public void scrollToPosition(final int pos) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                binding.recyclerView.scrollToPosition(pos);
            }
        });
    }

    public void setModel(DeviceZoneModel model) {
        presenter.setModel(model);
    }
}
