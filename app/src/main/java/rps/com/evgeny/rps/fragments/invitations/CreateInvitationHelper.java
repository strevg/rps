package rps.com.evgeny.rps.fragments.invitations;

import android.content.Context;

import java.lang.ref.WeakReference;
import java.util.Map;

import rps.com.evgeny.rps.models.BarCodeInUse;

/**
 * Created by Evgeny on 11.10.2017.
 */

public class CreateInvitationHelper {
    private static CreateInvitationHelper instance;
    private Map<String, String> invitations;
    private BarCodeInUse barcode;
    private boolean skipBarcode;

    static public CreateInvitationHelper getInstance() {
        if (instance==null) {
            instance = new CreateInvitationHelper();
        }
        return instance;
    }

    public void cleanData() {
        invitations = null;
        barcode = null;
    }

    public boolean hasBarcode() {
        return barcode!=null;
    }

    public boolean hasInvitation() {
        return invitations!=null;
    }

    public boolean isSkipBarcode() {
        return skipBarcode;
    }

    public void setSkipBarcode(boolean skipBarcode) {
        this.skipBarcode = skipBarcode;
    }

    public BarCodeInUse getBarcode() {
        return barcode;
    }

    public void setBarcode(BarCodeInUse barcode) {
        this.barcode = barcode;
    }

    public Map<String, String> getInvitations() {
        return invitations;
    }

    public void setInvitations(Map<String, String> invitations) {
        this.invitations = invitations;
    }
}
