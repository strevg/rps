package rps.com.evgeny.rps.models.person;

import java.util.List;
import java.util.UUID;

import rps.com.evgeny.rps.nfc.CardReaderResponse;
import rps.com.evgeny.rps.nfc.CardResponse;

/**
 * Created by Evgeny on 08.10.2017.
 */

public class CardFullModel {
    public String Blocked;
    public String CardId;
    public String ClientId;
    public UUID CompanyId;
    public String ParkingEnterTime;
    public String LastRecountTime;
    public String LastPaymentTime;
    public Integer TSidFC;
    public Integer TPidFC;
    public Integer ZoneidFC;
    public Integer ClientGroupidFC;
    public String SumOnCard;
    public String Nulltime1;
    public String Nulltime2;
    public String Nulltime3;
    public String TVP;
    public Integer TKVP;
    public Integer ClientTypidFC;
    public String DateSaveCard;
    public String LastPlate;
    public String _IsDeleted;
    public String Sync;
    public String Name;
    public List<TransactionModel> Transactions;

    public CardFullModel(String id) {
        this.CardId = id;
    }

    public CardReaderResponse getCard() {
        CardReaderResponse cardReaderResponse = new CardReaderResponse();
        cardReaderResponse.CardId = CardId;
        cardReaderResponse.ParkingEnterTime = ParkingEnterTime;
        cardReaderResponse.LastRecountTime = LastRecountTime;
        cardReaderResponse.TSidFC = TSidFC!=null?TSidFC.toString():null;
        cardReaderResponse.TPidFC = TPidFC!=null?TPidFC.toString():null;
        cardReaderResponse.ZoneidFC = ZoneidFC!=null?ZoneidFC.toString():null;
        cardReaderResponse.ClientGroupidFC = ClientGroupidFC!=null?ClientGroupidFC.toString():null;
        cardReaderResponse.SumOnCard = SumOnCard;
        cardReaderResponse.LastPaymentTime = LastPaymentTime;
        cardReaderResponse.Nulltime1 = Nulltime1;
        cardReaderResponse.Nulltime2 = Nulltime2;
        cardReaderResponse.Nulltime3 = Nulltime3;
        cardReaderResponse.DateSaveCard = DateSaveCard;
        cardReaderResponse.TVP = TVP;
        cardReaderResponse.TKVP = TKVP!=null?TKVP.toString():null;
        cardReaderResponse.ClientTypidFC = ClientTypidFC!=null?ClientTypidFC.toString():null;
        cardReaderResponse.LastPlate = LastPlate;
        return cardReaderResponse;
    }
}
