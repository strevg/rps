package rps.com.evgeny.rps.models;

/**
 * Created by Evgeny on 31.08.2017.
 */

public class SecurityUser {
    public String token;
    public String domain;
    public User user;

    public SecurityUser(String token, String domain, User user) {
        this.token = token;
        this.domain = domain;
        this.user = user;
    }
}
