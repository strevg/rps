package rps.com.evgeny.rps.fragments.search_card;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.view.View;

import com.crashlytics.android.Crashlytics;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rps.com.evgeny.rps.fragments.reading.NotReadingFragment;
import rps.com.evgeny.rps.fragments.reading.ReadingFragment;
import rps.com.evgeny.rps.models.CardModel;
import rps.com.evgeny.rps.models.event_bus.SearchCardModel;
import rps.com.evgeny.rps.network.ApiFactory;
import rps.com.evgeny.rps.network.RPSApi;
import rps.com.evgeny.rps.nfc.CardReaderResponse;
import rps.com.evgeny.rps.utils.Settings;
import rps.com.evgeny.rps.views.onItemClickListener;

/**
 * Created by Evgeny on 15.11.2018.
 */

public class SearchCardPresenter implements SearchCardContract.Presenter, onItemClickListener<CardModel> {
    @Nullable
    SearchCardContract.View view;
    RPSApi rpsApi;
    SearchCardAdapter adapter;
    boolean isOpening = false;

    @Inject
    SearchCardPresenter() {
        rpsApi = ApiFactory.getRpsApi(Settings.siteName + "/");
        adapter = new SearchCardAdapter();
        adapter.setClickListener(this);
    }

    @Override
    public void takeView(SearchCardContract.View view) {
        this.view = view;
        view.setAdapter(adapter);
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void dropView() {
        this.view = null;
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSearchRequest(SearchCardModel model) {
        onSearchCard(model.getQuery());
    }

    @Override
    public void onItemClick(View v, CardModel item) {
        isOpening = true;
        Settings.card = new CardReaderResponse(item);
        if (NotReadingFragment.getInstance()!=null) {
            NotReadingFragment.getInstance().showPersonFragment();
        } else {
            ReadingFragment.getInstance().showPersonFragment();
        }
        Settings.isNFCReader = false;
    }

    @Override
    public void onSearchCard(String number) {
        isOpening = false;
        rpsApi.searchCard(number, ApiFactory.getHeaders()).enqueue(new Callback<List<CardModel>>() {
            @Override
            public void onResponse(Call<List<CardModel>> call, final Response<List<CardModel>> response) {
                if (response.isSuccessful() && !isOpening) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            adapter.onAddAll(response.body());
                            view.showEmptyList(response.body().size()==0);

                            if (adapter.getItemCount()==1) {
                                onItemClick(null, response.body().get(0));
                            }
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<List<CardModel>> call, Throwable t) {
                Crashlytics.logException(t);
            }
        });
    }
}
