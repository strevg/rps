package rps.com.evgeny.rps.services;

import java.util.Timer;
import java.util.TimerTask;


public class TimerManager {
    private Timer timer;
    private TimerTick listener;

    public TimerManager(TimerTick listener) {
        this.listener = listener;
    }

    public void startTimer() {
        if (timer!=null)
            timer.cancel();
        this.timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (listener!=null)
                    listener.onTick();
            }
        }, 0, 1000);
    }

    public void stopTimer() {
        if (timer!=null)
            timer.cancel();
        timer = null;
    }

    public interface TimerTick {
        void onTick();
    }
}
