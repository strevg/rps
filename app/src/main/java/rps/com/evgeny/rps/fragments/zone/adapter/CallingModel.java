package rps.com.evgeny.rps.fragments.zone.adapter;

import rps.com.evgeny.rps.models.network.CallingSocketModel;

import static rps.com.evgeny.rps.fragments.zone.adapter.ZoneBottomAdapter.CALLING_TYPE;

/**
 * Created by Evgeny on 22.07.2018.
 */

public class CallingModel implements BaseType {
    String text;
    String id;
    boolean active;
    boolean outcoming;

    public CallingModel(String id) {
        this.id = id;
    }

    public CallingModel(String id, String text) {
        this.id = id;
        this.text = text;
    }

    public CallingModel(String id, String text, boolean active) {
        this(id, text);
        this.active = active;
        this.outcoming = true;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isOutcoming() {
        return outcoming;
    }

    @Override
    public int getType() {
        return CALLING_TYPE;
    }
}
