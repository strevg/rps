package rps.com.evgeny.rps.views;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import rps.com.evgeny.rps.R;

/**
 * Created by Evgeny on 24.05.2018.
 */

public class WarningCodeView extends RelativeLayout {
    private Context context;
    private TextView textView;

    public WarningCodeView(Context context) {
        super(context);
        initViews(context);
    }

    public WarningCodeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews(context);
    }

    public WarningCodeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews(context);
    }

    void initViews(Context context) {
        this.context = context;
        this.inflate(context, R.layout.view_warning_code, this);
        this.textView = findViewById(R.id.text);
    }

    public void setText(String text) {
        this.textView.setText(text);
    }

    public void setColor(int color) {
        GradientDrawable drawable = (GradientDrawable)textView.getBackground();
        drawable.setColor(color);
    }
}
