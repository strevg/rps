package rps.com.evgeny.rps.fragments.zone.details;

import android.support.annotation.Nullable;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import rps.com.evgeny.rps.di.ActivityScoped;
import rps.com.evgeny.rps.models.event_bus.RackModeChange;
import rps.com.evgeny.rps.models.network.RackWorkingModeModel;

/**
 * Created by Evgeny on 08.06.2018.
 */
@ActivityScoped
public class ZoneDetailsPresenter implements ZoneDetailsContract.Presenter {
    @Nullable
    ZoneDetailsContract.View view;

    @Inject
    ZoneDetailsPresenter() {
    }

    @Override
    public void takeView(ZoneDetailsContract.View view) {
        this.view = view;
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void dropView() {
        this.view = null;
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void changeRackMode(RackModeChange item) {
        view.setSubTitle(item.model.Name);
    }
}
