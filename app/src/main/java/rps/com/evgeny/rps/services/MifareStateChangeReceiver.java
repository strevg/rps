package rps.com.evgeny.rps.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.util.Log;

import rps.com.evgeny.rps.utils.Consts;

/**
 * Created by Evgeny on 30.08.2017.
 */

public class MifareStateChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (NfcAdapter.ACTION_ADAPTER_STATE_CHANGED.equals(action)) {
            handleNfcStateChanged(intent.getIntExtra(NfcAdapter.EXTRA_ADAPTER_STATE, NfcAdapter.STATE_OFF));
        }
    }

    private void handleNfcStateChanged(int newState) {
        switch (newState) {
            case NfcAdapter.STATE_OFF:
                Log.i(Consts.TAG, "NFC Power off");
                break;
            case NfcAdapter.STATE_ON:
                Log.i(Consts.TAG, "NFC Power on");
                break;
            case NfcAdapter.STATE_TURNING_ON:
                Log.i(Consts.TAG, "NFC Power turning on");
                break;
            case NfcAdapter.STATE_TURNING_OFF:
                Log.i(Consts.TAG, "NFC Power turning off");
                break;
        }
    }
}
