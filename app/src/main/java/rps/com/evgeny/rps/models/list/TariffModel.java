package rps.com.evgeny.rps.models.list;

import java.util.UUID;

/**
 * Created by Evgeny on 08.10.2017.
 */

public class TariffModel {
    public UUID Id;
    public String Name;
    public Integer ChangingTypeId;
    public Integer TypeId;
    public Integer Initial;
    public Integer InitialTimeTypeId;
    public Integer InitialAmount;
    public Integer ProtectedInterval;
    public Integer ProtectedIntervalTimeTypeId;
    public Integer FreeTime;
    public Integer FreeTimeTypeId;
    public Integer _IsDeleted;
    public String Sync;
    public Integer IdFC;
    public Integer index;

    @Override
    public String toString() {
        return Name;
    }
}
