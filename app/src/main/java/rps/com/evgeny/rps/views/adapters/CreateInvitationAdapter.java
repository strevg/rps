package rps.com.evgeny.rps.views.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import rps.com.evgeny.rps.MainApplication;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.models.BarCodeInUse;
import rps.com.evgeny.rps.views.AdapterInterface;
import rps.com.evgeny.rps.views.viewholders.BarcodeViewHolder;
import rps.com.evgeny.rps.views.onItemClickListener;

/**
 * Created by Evgeny on 23.09.2017.
 */

public class CreateInvitationAdapter extends RecyclerView.Adapter<BarcodeViewHolder>
        implements AdapterInterface<BarCodeInUse> {
    public List<BarCodeInUse> items;
    onItemClickListener<BarCodeInUse> onItemClickListener;
    MainApplication app;

    public CreateInvitationAdapter(Activity activity, List<BarCodeInUse> items) {
        this.items = items;
        app = (MainApplication) activity.getApplication();
    }

    @Override
    public BarcodeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.barcode_view_holder, null);
        return new BarcodeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final BarcodeViewHolder holder, final int position) {
        BarCodeInUse bm = items.get(position);

        holder.setTitle("Штрихкод " + (bm.index!=null ? bm.index : ""));
        holder.setDescription(bm.Description);
        holder.itemView.setTag(holder);

        holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onItemClickListener!=null) {
                    onItemClickListener.onItemClick(view, items.get(position));
                }
            }
        });
    }

    public void setOnItemClickListener(onItemClickListener listener) {
        this.onItemClickListener = listener;
    }

    @Override
    public void onAdd(BarCodeInUse item) {
        int position = items.size();
        items.add(position, item);
        notifyItemInserted(position);
    }

    @Override
    public void onRemove(int pos) {
        if (pos < 0)
            return;
        items.remove(pos);
        notifyItemRemoved(pos);
    }

    @Override
    public void onRemoveAll() {
        int count = getItemCount();
        items.clear();
        notifyItemRangeRemoved(0, count);
    }

    @Override
    public void onAddAll(List<BarCodeInUse> items) {

    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}