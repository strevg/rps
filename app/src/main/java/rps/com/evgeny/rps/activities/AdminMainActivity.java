package rps.com.evgeny.rps.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import org.greenrobot.eventbus.EventBus;
import org.linphone.core.LinphoneCall;

import dagger.android.support.DaggerAppCompatActivity;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rps.com.evgeny.rps.MainApplication;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.databinding.ActivityAdminMainBinding;
import rps.com.evgeny.rps.fragments.main.MainAdminFragment;
import rps.com.evgeny.rps.models.asterisk.RegistrationRequest;
import rps.com.evgeny.rps.models.asterisk.RegistrationResponse;
import rps.com.evgeny.rps.models.event_bus.SearchCardModel;
import rps.com.evgeny.rps.models.person.UserModel;
import rps.com.evgeny.rps.network.ApiFactory;
import rps.com.evgeny.rps.network.RPSApi;
import rps.com.evgeny.rps.services.linphone.EasyLinphone;
import rps.com.evgeny.rps.services.linphone.LinphoneManager;
import rps.com.evgeny.rps.services.linphone.callback.PhoneCallback;
import rps.com.evgeny.rps.services.linphone.callback.RegistrationCallback;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.Settings;
import rps.com.evgeny.rps.utils.StringUtils;

/**
 * Created by Evgeny on 25.05.2018.
 */

public class AdminMainActivity extends DaggerAppCompatActivity {
    public static final int PERMISSIONS_REQUEST_RECORD_AUDIO = 100;
    MainApplication app;
    ActivityAdminMainBinding binding;
    boolean inWriteMode;
    RPSApi rpsApi;
    LinphoneManager linphoneManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_admin_main);

        initViews();

        rpsApi = ApiFactory.getRpsApi(Settings.siteName + "/");
        linphoneManager = LinphoneManager.getInstance(this);

        loadUserModel();
        registrationAsterisk();
    }

    void initViews() {
        app = (MainApplication)getApplication();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentContainer, new MainAdminFragment(), "");
        transaction.commit();
    }

    void registrationAsterisk() {
        Log.i(Consts.TAG, StringUtils.getSipServer());
        if (!StringUtils.isValidLinphoneLogin()) {
            app.showProgressDialog("Авторизация в телефонии...");

            final String login = StringUtils.getAsteriskLogin();
            final String pass = StringUtils.getAsteriskPassword();
            RegistrationRequest registrationRequest = new RegistrationRequest(login,pass);
            rpsApi.registerStUser(registrationRequest, ApiFactory.getHeaders()).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        Settings.saveAsteriskLoginPassword(AdminMainActivity.this, login, pass);
                        showToast("Регистрация нового пользователя Asterisk прошла успешно!");
                        linphoneManager.login();
                    } else {
                        showToast("Не удалось зарегистрировать нового пользователя Asterisk!");
                    }
                    app.dismissProgressDialog();
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    showToast("Не удалось зарегистрировать нового пользователя Asterisk!");
                    Log.e(Consts.TAG, "OnResponse " + t.toString());
                    Crashlytics.logException(t);
                    app.dismissProgressDialog();
                }
            });
        } else {
            linphoneManager.login();
        }
    }

    public void loadUserModel() {
        rpsApi.getUser(Settings.userId, ApiFactory.getHeaders()).enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                if (response.isSuccessful()) {
                    Settings.currentUser = response.body();
                } else {
                    showToast("Не удалось загрузить данные пользователя");
                }
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
                showToast("Не удалось загрузить данные пользователя");
                Log.e(Consts.TAG, "loadUserModel " + t.toString());
                Crashlytics.logException(t);
            }
        });
    }

    public void showShadowTalking(boolean flag) {
        binding.showContainer.setVisibility(flag?View.VISIBLE:View.GONE);
    }

    public boolean isPermissionGranted() {
         return !(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && ((checkSelfPermission(Manifest.permission.RECORD_AUDIO)!= PackageManager.PERMISSION_GRANTED)));
    }

    public void requestPermission() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && ((checkSelfPermission(Manifest.permission.RECORD_AUDIO)!= PackageManager.PERMISSION_GRANTED))){
            requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO},
                    PERMISSIONS_REQUEST_RECORD_AUDIO);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_RECORD_AUDIO) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, R.string.hint_record_audio_granted, Toast.LENGTH_SHORT).show();
            } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                Toast.makeText(this, R.string.hint_record_audio_denied, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Consts.PLATE_RECOGNITION_REQUEST && resultCode==RESULT_OK) {
            String number = data.getStringExtra("plate");
            String region = data.getStringExtra("region");

            EventBus.getDefault().post(new SearchCardModel(number+region));
        }
    }

    void showToast(final String text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(AdminMainActivity.this, text, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Deprecated
    @Override
    protected void onNewIntent(Intent intent) {
        if (inWriteMode) {
            inWriteMode = false;
            Tag tag = (Tag)intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            if (tag == null) {
                return;
            }
            Settings.cardReader.onTagDiscovered(tag);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Settings.isRunningActivity = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        Settings.isRunningActivity = false;
    }
}
