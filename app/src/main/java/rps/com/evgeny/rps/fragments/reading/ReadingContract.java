package rps.com.evgeny.rps.fragments.reading;

import android.app.Activity;
import android.content.Context;

import rps.com.evgeny.rps.di.BasePresenter;
import rps.com.evgeny.rps.di.BaseView;

/**
 * Created by Evgeny on 13.06.2018.
 */

public interface ReadingContract {
    interface View extends BaseView<ReadingContract.Presenter> {
        Activity getActivity();

        void showPersonFragment();

        void showReadingFragment();
    }

    interface Presenter extends BasePresenter<ReadingContract.View> {
        void takeView(ReadingContract.View view);

        void dropView();
    }
}
