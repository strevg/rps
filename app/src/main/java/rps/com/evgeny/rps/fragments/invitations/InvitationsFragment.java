package rps.com.evgeny.rps.fragments.invitations;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rps.com.evgeny.rps.MainApplication;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.databinding.InvitationsFragmentBinding;
import rps.com.evgeny.rps.fragments.onBackPressInterface;
import rps.com.evgeny.rps.models.BarCodeInUse;
import rps.com.evgeny.rps.models.invitations.CreateResponse;
import rps.com.evgeny.rps.models.invitations.InvitationsResponseModel;
import rps.com.evgeny.rps.network.ApiFactory;
import rps.com.evgeny.rps.network.RPSApi;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.Settings;
import rps.com.evgeny.rps.utils.StringUtils;
import rps.com.evgeny.rps.utils.TimeUtils;
import rps.com.evgeny.rps.views.adapters.InvitationsAdapter;
import rps.com.evgeny.rps.models.invitations.InvitationModel;
import rps.com.evgeny.rps.views.onItemClickListener;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Evgeny on 21.09.2017.
 */

public class InvitationsFragment extends Fragment implements DatePickerDialog.OnDateSetListener, View.OnClickListener,
        onItemClickListener<InvitationModel> {
    final int VOICE_SEARCH_CODE = 3012;
    InvitationsFragmentBinding binding;
    FloatingSearchView floatingSearchView;
    InvitationsAdapter adapter;
    LinearLayoutManager layoutManager;
    MainApplication app;
    RPSApi rpsApi;

    CreateInvitationHelper createHelper;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (MainApplication)getActivity().getApplication();
        rpsApi = ApiFactory.getRpsApi(Settings.siteName+"/");
        setHasOptionsMenu(true);

        createHelper = CreateInvitationHelper.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.invitations_fragment, container, false);
        View v = binding.getRoot();
        initViews();
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                app.showProgressDialog("Загрузка с сервера...");
            }
        }, 50);
        loadInvitations();
    }

    @Override
    public void onResume() {
        super.onResume();
        app.getMainActivity().setTitle(getString(R.string.invitations));
        setSubtitle(null);
        app.getMainActivity().setOnBackPressInterface(new onBackPressInterface() {
            @Override
            public boolean onBackPress() {
                if (floatingSearchView.getVisibility()==View.VISIBLE) {
                    floatingSearchView.setVisibility(View.INVISIBLE);
                    return true;
                }
                return false;
            }
        });
        if (binding.mainInvitationContainer.findViewById(R.id.floatingSearchView)!=null) {
            binding.mainInvitationContainer.removeView(floatingSearchView);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        app.getMainActivity().setOnBackPressInterface(null);
        showSearchView(false);
    }

    void setSubtitle(String subtitle) {
        app.getMainActivity().setSubtitle(subtitle);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.date_range_menu, menu);
    }

    void initViews() {
        binding.createNewFloatButton.setOnClickListener(this);
        floatingSearchView =  binding.floatingSearchView;
        floatingSearchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSuggestionClicked(SearchSuggestion searchSuggestion) {}
            @Override
            public void onSearchAction(String currentQuery) {
                searchInvitations(currentQuery);
            }
        });
        floatingSearchView.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {
            @Override
            public void onSearchTextChanged(String oldQuery, final String newQuery) {
                //get suggestions based on newQuery
                //pass them on to the search view
                //floatingSearchView.swapSuggestions(newSuggestions);
            }
        });
        floatingSearchView.setOnMenuItemClickListener(new FloatingSearchView.OnMenuItemClickListener() {
            @Override
            public void onActionMenuItemSelected(MenuItem item) {
                if (item.getItemId() == R.id.speech_action) {
                    startVoiceRecognition();
                }
            }
        });

        layoutManager = new LinearLayoutManager(getActivity());
        binding.recycleView.setLayoutManager(layoutManager);
        adapter = new InvitationsAdapter();
        adapter.setClickListener(this);
        binding.recycleView.setAdapter(adapter);
    }

    public void startVoiceRecognition() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        this.startActivityForResult(intent, VOICE_SEARCH_CODE);
    }

    void loadInvitations() {
        rpsApi.getInvitations(ApiFactory.getHeaders()).enqueue(new Callback<List<InvitationModel>>() {
            @Override
            public void onResponse(Call<List<InvitationModel>> call, Response<List<InvitationModel>> response) {
                if (response.isSuccessful()) {
                    for (InvitationModel model : response.body()) {
                        adapter.onAdd(model);
                    }
                }
                app.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<List<InvitationModel>> call, Throwable t) {
                Log.e(Consts.TAG, "loadInvitations " + t.toString());
                Crashlytics.logException(t);
                app.dismissProgressDialog();
            }
        });
    }

    void loadInvitationsByDate(String date) {
        adapter.onRemoveAll();
        app.showProgressDialog("Загрузка с сервера...");
        rpsApi.getInvitationsByDate(date, ApiFactory.getHeaders()).enqueue(new Callback<InvitationsResponseModel>() {
            @Override
            public void onResponse(Call<InvitationsResponseModel> call, Response<InvitationsResponseModel> response) {
                if (response.isSuccessful()) {
                    for (InvitationModel model : response.body().result) {
                        adapter.onAdd(model);
                    }
                }
                app.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<InvitationsResponseModel> call, Throwable t) {
                Log.e(Consts.TAG, "loadInvitations " + t.toString());
                Crashlytics.logException(t);
                app.dismissProgressDialog();
            }
        });
    }

    void searchInvitations(String quary) {
        adapter.onRemoveAll();
        app.showProgressDialog("Загрузка с сервера...");
        rpsApi.getInvitationsByName(quary, ApiFactory.getHeaders()).enqueue(new Callback<InvitationsResponseModel>() {
            @Override
            public void onResponse(Call<InvitationsResponseModel> call, Response<InvitationsResponseModel> response) {
                if (response.isSuccessful()) {
                    for (InvitationModel model : response.body().result) {
                        adapter.onAdd(model);
                    }
                }
                app.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<InvitationsResponseModel> call, Throwable t) {
                Log.e(Consts.TAG, "searchInvitations " + t.toString());
                Crashlytics.logException(t);
                app.dismissProgressDialog();
            }
        });
    }

    Map<String, String> getInvitation(InvitationModel model) {
        Map<String, String> invitations = new HashMap<>();
        String[] name = {"", "", ""};
        if (model.GuestName!=null) {
            name = model.GuestName.split("\\s+");
        }
        if (name.length>0)
            invitations.put("dataCreate[surname]", name[0]);
        if (name.length>1)
            invitations.put("dataCreate[name]", name[1]);
        if (name.length>2)
            invitations.put("dataCreate[middle_name]", name[2]);

        invitations.put("dataCreate[email]", model.GuestEmail);
        invitations.put("dataCreate[state_number]", model.GuestPlateNumber);
        invitations.put("dataCreate[brand]", model.GuestCarModel);
        invitations.put("dataCreate[comments]", "");
        invitations.put("dataCreate[usagetime]", "7");
        invitations.put("dataCreate[usagetype]", "days");
        invitations.put("dataCreate[AnySum]", "0");
        invitations.put("dataCreate[DiscountAmount]", "");
        return invitations;
    }

    @Override
    public void onItemClick(View v, InvitationModel item) {
        createHelper.setInvitations(getInvitation(item));
        createHelper.setBarcode(new BarCodeInUse(item.IdBCM));

        showSearchView(true);
        showSearchView(false);

        app.getMainActivity().replaceFragment(Consts.FragmentType.CREATE_SELECT_INVITATION, null);
        createHelper.setSkipBarcode(true);
    }

    @ Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == VOICE_SEARCH_CODE && resultCode == RESULT_OK) {
            final ArrayList<String> matches = data
                    .getStringArrayListExtra("android.speech.extra.RESULTS");
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    floatingSearchView.setSearchText(matches.get(0));
                }
            });
            searchInvitations(matches.get(0));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        String date = "";
        switch (id) {
            case R.id.date_0:
                date = TimeUtils.getToday()+"+-+"+TimeUtils.getToday();
                setSubtitle(TimeUtils.getTodayTitle());
                break;
            case R.id.date_1:
                date = TimeUtils.getYesterday()+"+-+"+TimeUtils.getYesterday();
                setSubtitle(TimeUtils.getYesterdayTitle());
                break;
            case R.id.date_2:
                date = TimeUtils.getWeek();
                setSubtitle(TimeUtils.getDateTitle(date));
                break;
            case R.id.date_3:
                date = TimeUtils.getMonth();
                setSubtitle(TimeUtils.getDateTitle(date));
                break;
            case R.id.date_4:
                date = TimeUtils.getQuart();
                setSubtitle(TimeUtils.getDateTitle(date));
                break;
            case R.id.date_5:
                showDateDialog();
                return true;
            case R.id.search_action:
                showSearchView(true);
                return true;
        }
        if (!StringUtils.isNullOrEmpty(date)) {
            loadInvitationsByDate(date);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void showSearchView(boolean flag) {
        if (!flag) {
            app.getMainActivity().removeToolbarContainer(floatingSearchView);
        } else {
            app.getMainActivity().setToolbarContainer(floatingSearchView);
        }
        floatingSearchView.setVisibility(flag?View.VISIBLE:View.INVISIBLE);
        if (!flag)
            floatingSearchView.setSearchText("");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        Log.i(Consts.TAG, "onDateSet " + year + " " + monthOfYear + " " + dayOfMonth+
            " " + yearEnd + " " + monthOfYearEnd + " " + dayOfMonthEnd);
        StringBuffer buffer = new StringBuffer()
                .append(TimeUtils.getValidDate(dayOfMonth)).append(".").append(TimeUtils.getValidDate(monthOfYear+1)).append(".").append(year)
                .append("+-+")
                .append(TimeUtils.getValidDate(dayOfMonthEnd)).append(".").append(TimeUtils.getValidDate(monthOfYearEnd+1)).append(".").append(yearEnd);
        loadInvitationsByDate(buffer.toString());
        setSubtitle(TimeUtils.getDateTitle(buffer.toString()));
    }

    @Override
    public void onClick(View view) {
        showSearchView(true);
        showSearchView(false);

        app.getMainActivity().replaceFragment(Consts.FragmentType.CREATE_SELECT_INVITATION, null);
        createHelper.cleanData();
    }

    public void showDateDialog() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setEndTitle(getString(R.string.end_date));
        dpd.setStartTitle(getString(R.string.start_date));
        dpd.setMaxDate(Calendar.getInstance());
        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }
}
