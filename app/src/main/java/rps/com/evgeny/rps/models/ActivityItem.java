package rps.com.evgeny.rps.models;

import android.content.Intent;

/**
 * Created by Evgeny on 30.08.2017.
 */

public class ActivityItem {
    public String title;
    public Intent intent;

    @Override
    public String toString() {
        return title;
    }
}