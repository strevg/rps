package rps.com.evgeny.rps.fragments.zone.adapter;

import static rps.com.evgeny.rps.fragments.zone.adapter.ZoneBottomAdapter.ENTERING_TYPE;

/**
 * Created by Evgeny on 22.07.2018.
 */

public class EnteringModel implements BaseType {
    String text;
    String id;

    public EnteringModel(String id, String text) {
        this.id = id;
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public String getId() {
        return id;
    }

    @Override
    public int getType() {
        return ENTERING_TYPE;
    }
}
