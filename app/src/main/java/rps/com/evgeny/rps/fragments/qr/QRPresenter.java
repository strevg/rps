package rps.com.evgeny.rps.fragments.qr;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.models.invitations.InvitationModel;
import rps.com.evgeny.rps.network.ApiFactory;
import rps.com.evgeny.rps.network.RPSApi;
import rps.com.evgeny.rps.services.ControlManager;
import rps.com.evgeny.rps.services.TimeoutManager;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.Settings;
import rps.com.evgeny.rps.utils.TimeUtils;
import rps.com.evgeny.rps.views.adapters.QRAdapter;
import rps.com.evgeny.rps.views.onItemClickListener;

/**
 * Created by Evgeny on 07.06.2018.
 */

public class QRPresenter implements QRContract.Presenter, onItemClickListener<InvitationModel>,
        TimeoutManager.TimeoutCallback{
    @Nullable
    QRContract.View view;

    QRAdapter adapter;
    RPSApi rpsApi;
    ControlManager controlManager;

    @Inject
    QRPresenter() {
        rpsApi = ApiFactory.getRpsApi(Settings.siteName + "/");
    }

    @Override
    public void takeView(QRContract.View view) {
        this.view = view;
        adapter = new QRAdapter();
        adapter.setClickListener(this);
        view.setAdapter(adapter);
        controlManager = ControlManager.getInstance();
        downloadQR();
    }

    @Override
    public void dropView() {
        this.view = null;
    }

    @Override
    public void findItem(String query) {
        int position = adapter.findItem(query);
        if (position!=-1) {
            view.scrollToPostion(position);
        }
    }

    @Override
    public void timeIsOver(int error) {
        showToast(error);
    }

    void downloadQR() {
        rpsApi.getBarcodes(ApiFactory.getHeaders()).enqueue(new Callback<List<InvitationModel>>() {
            @Override
            public void onResponse(Call<List<InvitationModel>> call, Response<List<InvitationModel>> response) {
                if (response.isSuccessful()) {
                    checkExpiredAndAddAll(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<InvitationModel>> call, Throwable t) {
                Log.e(Consts.TAG, t.getMessage());
            }
        });
    }

    @Override
    public void onItemClick(View v, InvitationModel item) {
        sendBarcode(item);
    }

    void sendBarcode(final InvitationModel item) {
        TimeoutManager.getInstance().start(this, R.string.hint_send_barcode_error);
        rpsApi.sendBarcode(controlManager.getDevice().Host+"port_sign2222", item.Id.toString(), ApiFactory.getHeaders()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    adapter.onRemove(item);
                    TimeoutManager.getInstance().stop();
                    showToast(R.string.hint_send_barcode);
                } else {
                    showToast(R.string.hint_send_barcode_error);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(Consts.TAG, t.getMessage());
            }
        });
    }

    void showToast(final int text) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (view!=null && view.getContext()!=null) {
                    Toast.makeText(view.getContext(), text, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    void checkExpiredAndAddAll(List<InvitationModel> invitations) {
        List<InvitationModel> newList = new ArrayList<>();
        for (InvitationModel model : invitations) {
            if (TimeUtils.isUsageTime(model.UsageTime)) {
                newList.add(model);
            }
        }
        adapter.onAddAll(newList);
    }
}
