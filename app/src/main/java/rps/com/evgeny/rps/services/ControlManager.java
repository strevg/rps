package rps.com.evgeny.rps.services;

import android.util.Log;

import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.fragments.qr.QRFragment;
import rps.com.evgeny.rps.fragments.zone.ZoneDevicesFragment;
import rps.com.evgeny.rps.models.event_bus.RackModeChange;
import rps.com.evgeny.rps.models.network.DeviceZoneModel;
import rps.com.evgeny.rps.models.network.RackWorkingModeResponse;
import rps.com.evgeny.rps.models.network.WebSocketModel;
import rps.com.evgeny.rps.models.network.ZoneTableModel;
import rps.com.evgeny.rps.network.ApiFactory;
import rps.com.evgeny.rps.network.RPSApi;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.Settings;

/**
 * Created by Evgeny on 09.07.2018.
 */

public class ControlManager implements RpsWebSocket.WebSocketListener, TimeoutManager.TimeoutCallback{
    private static ControlManager instance;
    private RpsWebSocket webSocket;
    private RPSApi rpsApi;
    private DeviceZoneModel device;
    private WebSocketModel lastWebSocketModel;
    private List<ZoneTableModel> zoneList;
    List<RpsWebSocket.WebSocketMessageListener> listeners;
    ControlManagerCallback managerCallback;
    TimeoutManager timeoutManager;
    TimeoutManager.TimeoutCallback timeoutCallback;

    private ControlManager() {
        rpsApi = ApiFactory.getRpsApi(Settings.siteName + "/");
        webSocket = new RpsWebSocket(this);
        webSocket.onStart();

        timeoutManager = TimeoutManager.getInstance();

        listeners=new ArrayList<>();
    }

    public static ControlManager getInstance() {
        if (instance==null)
            instance = new ControlManager();
        return instance;
    }

    public void restartSocket() {
        if (webSocket!=null)
            webSocket.onClose();
        webSocket = new RpsWebSocket(this);
        webSocket.onStart();
    }

    public void stopSocket() {
        webSocket.onClose();
        webSocket = null;
    }

    @Override
    public void onMessage(WebSocketModel model) {
        if (listeners!=null && listeners.size()>0) {
            for (RpsWebSocket.WebSocketMessageListener listener : listeners)
                listener.onMessage(model);
        }
        lastWebSocketModel = model;
        Log.i(Consts.TAG, "WebSocket " + model.getMessageType() + " " + model.getId());
    }

    @Override
    public void onRestartSocket() {
        Log.i(Consts.TAG, "WebSocket onRestartSocket");
        webSocket.onStart();
    }

    @Override
    public void timeIsOver(int error) {
        if (timeoutCallback!=null) {
            timeoutCallback.timeIsOver(error);
        }
    }

    // ---- control elements -----
    public void openInvitations() {
        QRFragment qrFragment = new QRFragment();
        ZoneDevicesFragment.getInstance().addFragment(qrFragment);
    }

    public void onCall() {
        onCall(device.Host);
    }

    public void onCall(String host) {
        rpsApi.startCall(Settings.linphoneLogin, host, ApiFactory.getHeaders()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (!response.isSuccessful()) {
                    managerCallback.onRequestFailed(R.string.error_call);
                } else {
                    managerCallback.onRequestFailed(R.string.wait_call);
                }
                Log.i(Consts.TAG, "startCall " + response.isSuccessful());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                managerCallback.onRequestFailed(R.string.error_call);
                Log.e(Consts.TAG, t.getMessage());
                Crashlytics.log(t.getMessage());
            }
        });
    }

    public void onStop() {
        rpsApi.stopCall(Settings.linphoneLogin, device.Host, ApiFactory.getHeaders()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                managerCallback.onRequestFailed(R.string.error_call_stop);
                Log.i(Consts.TAG, "onStop " + response.isSuccessful());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                managerCallback.onRequestFailed(R.string.error_call_stop);
                Log.e(Consts.TAG, t.getMessage());
                Crashlytics.log(t.getMessage());
            }
        });
    }

    public void changeRackMode(int type) {
        rpsApi.changeRackMode(ApiFactory.getChangeTypeFields(device.Id.toString(), type),
                ApiFactory.getFormDataHeaders()).enqueue(new Callback<RackWorkingModeResponse>() {
            @Override
            public void onResponse(Call<RackWorkingModeResponse> call, Response<RackWorkingModeResponse> response) {
                if (response.isSuccessful()) {
                    managerCallback.onSuccessChangeRack(new RackModeChange(device.Id.toString(), response.body().RackWorkingMode));
                } else {
                    managerCallback.onRequestFailed(R.string.error_change_rack_mode);
                }
                Log.i(Consts.TAG, "changeRackMode " + response.code());
            }
            @Override
            public void onFailure(Call<RackWorkingModeResponse> call, Throwable t) {
                Log.e(Consts.TAG, t.getMessage());
            }
        });
    }

    public void postReasonChangeMode(String device, String deviceID, String text) {
        rpsApi.changeRackModeReason(ApiFactory.getChangeModeReasonModel(device, deviceID, text),
                ApiFactory.getHeaders()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    managerCallback.onSuccessPostReasonChangeMode();
                } else {
                    managerCallback.onRequestFailed(R.string.error_reason_change_mode);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(Consts.TAG, t.getMessage());
            }
        });
    }

    public void passSingleCar() {
        timeoutManager.start(this, R.string.error_pass_car);
        rpsApi.passSingleCar(device.Host + "port_sign2222", ApiFactory.getHeaders()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (!response.isSuccessful()) {
                    managerCallback.onRequestFailed(R.string.error_pass_car);
                } else {
                    managerCallback.onRequestSuccess(R.string.error_pass_car);
                    timeoutManager.stop();
                }
                Log.i(Consts.TAG, "passSingleCar " + response.code());
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(Consts.TAG, t.getMessage());
            }
        });
    }

    public void cancelSingleCar() {
        timeoutManager.start(this, R.string.error_cancel_car);
        rpsApi.cancelSingleCar(device.Host + "port_sign2222", ApiFactory.getHeaders()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (!response.isSuccessful()) {
                    managerCallback.onRequestFailed(R.string.error_cancel_car);
                } else {
                    managerCallback.onRequestSuccess(R.string.error_cancel_car);
                    timeoutManager.stop();
                }
                Log.i(Consts.TAG, "cancelSingleCar " + response.code());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(Consts.TAG, t.getMessage());
            }
        });
    }

    // ------- GET\SET
    public void setZoneList(List<ZoneTableModel> list) {
        this.zoneList = list;
    }

    public List<ZoneTableModel> getZoneList() {
        return zoneList;
    }

    public DeviceZoneModel getDevice() {
        return device;
    }

    public void setDevice(DeviceZoneModel device) {
        this.device = device;
    }

    public void setDeviceByID(String id) {
        this.device = getDevice(id);
    }

    public void setListener(RpsWebSocket.WebSocketMessageListener listener) {
        if (!listeners.contains(listener))
            this.listeners.add(listener);
    }

    public void removeListener(RpsWebSocket.WebSocketMessageListener listener) {
        this.listeners.remove(listener);
    }

    public void setCallBack(ControlManagerCallback callBack) {
        this.managerCallback = callBack;
    }

    public void setTimeoutCallback(TimeoutManager.TimeoutCallback timeoutCallback) {
        this.timeoutCallback = timeoutCallback;
    }

    public WebSocketModel getLastWebSocketModel() {
        return lastWebSocketModel;
    }

    public DeviceZoneModel getDevice(String id) {
        for (ZoneTableModel zone : zoneList) {
            for (DeviceZoneModel device : zone.DeviceModel) {
                if (device.Id.toString().equalsIgnoreCase(id)) {
                    return device;
                }
            }
        }
        return null;
    }

    public interface ControlManagerCallback {
        void onSuccessChangeRack(RackModeChange item);
        void onRequestFailed(int error);
        void onSuccessPostReasonChangeMode();
        void onRequestSuccess(int code);
    }
}
