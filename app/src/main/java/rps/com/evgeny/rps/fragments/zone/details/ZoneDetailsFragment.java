package rps.com.evgeny.rps.fragments.zone.details;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import dagger.Lazy;
import dagger.android.support.DaggerFragment;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.databinding.FragmentControlBinding;
import rps.com.evgeny.rps.databinding.FragmentTabsBinding;
import rps.com.evgeny.rps.databinding.FragmentZoneDetailsBinding;
import rps.com.evgeny.rps.fragments.warning.active.WarningActiveFragment;
import rps.com.evgeny.rps.fragments.warning.adapters.PagerAdapter;
import rps.com.evgeny.rps.fragments.warning.completed.WarningCompletedFragment;
import rps.com.evgeny.rps.models.event_bus.BackFromControlFragment;
import rps.com.evgeny.rps.models.network.DeviceZoneModel;

/**
 * Created by Evgeny on 08.06.2018.
 */

public class ZoneDetailsFragment extends DaggerFragment implements ZoneDetailsContract.View {
    FragmentZoneDetailsBinding binding;
    PagerAdapter adapter;
    DeviceZoneModel model;

    @Inject
    ZoneDetailsContract.Presenter presenter;

    @Inject
    AlarmFragment alarmFragment;
    @Inject
    ControlFragment controlFragment;

    @Inject
    public ZoneDetailsFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_zone_details, container, false);
        initViews();
        return binding.getRoot();
    }

    void initViews() {
        initTabs();

        ((AppCompatActivity)getActivity()).setSupportActionBar(binding.toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
                EventBus.getDefault().post(new BackFromControlFragment());
            }
        });

        setTitle(model.Name, model.RackWorkingModeName);
    }

    void initTabs() {
        adapter = new PagerAdapter(getChildFragmentManager());
        adapter.onAdd(controlFragment, getResources().getString(R.string.title_control));
        adapter.onAdd(alarmFragment, getResources().getString(R.string.title_alarm));
        binding.pager.setAdapter(adapter);
        binding.tabLayout.setupWithViewPager(binding.pager);
        binding.tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        alarmFragment.setModel(model);
    }

    public void setTitle(String title, String subTitle) {
        binding.toolbar.setTitle(title);
        binding.toolbar.setSubtitle(subTitle);
    }

    @Override
    public void setSubTitle(String title) {
        binding.toolbar.setSubtitle(title);
    }

    public void setModel(DeviceZoneModel model) {
        this.model = model;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.takeView(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.dropView();
    }
}
