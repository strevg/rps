package rps.com.evgeny.rps.fragments.zone.details;

import android.content.Context;

import rps.com.evgeny.rps.di.BasePresenter;
import rps.com.evgeny.rps.di.BaseView;

/**
 * Created by Evgeny on 08.06.2018.
 */

public interface ZoneDetailsContract {
    interface View extends BaseView<ZoneDetailsContract.Presenter> {
        Context getContext();
        void setSubTitle(String title);
    }

    interface Presenter extends BasePresenter<ZoneDetailsContract.View> {
        void takeView(ZoneDetailsContract.View view);

        void dropView();
    }
}
