package rps.com.evgeny.rps.models;

import java.util.UUID;

/**
 * Created by Evgeny on 30.08.2017.
 */

public class AuthUser {
    public UUID ID;
    public UUID userId;
    public UUID roleId;
    public UUID objectId;
    public String roleName;
    public String userName;
    public String objectName;
    public String sessionId;
    public String token;
    public String cardKey;
    public String sectorNumber;
}
