package rps.com.evgeny.rps.fragments.warning.active;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.activities.AdminMainActivity;
import rps.com.evgeny.rps.databinding.FragmentWarningActiveBinding;
import rps.com.evgeny.rps.services.linphone.LinphoneManager;
import rps.com.evgeny.rps.views.FadeInUpAnimator;
import rps.com.evgeny.rps.views.SlideInRightAnimator;

/**
 * Created by Evgeny on 24.05.2018.
 */

public class WarningActiveFragment extends DaggerFragment implements WarningActiveContract.View {
    FragmentWarningActiveBinding binding;

    @Inject
    WarningActiveContract.Presenter presenter;

    @Inject
    public WarningActiveFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_warning_active, container, false);
        initViews();
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.takeView(this);
    }

    void initViews() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        binding.recyclerView.setLayoutManager(layoutManager);
        binding.recyclerView.setItemAnimator(new SlideInRightAnimator());

        LinearLayoutManager layoutManager2 = new LinearLayoutManager(getActivity());
        binding.recyclerViewBottom.setLayoutManager(layoutManager2);
        binding.recyclerViewBottom.setItemAnimator(new FadeInUpAnimator());
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.setPhoneListener();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.dropView();
    }

    @Override
    public void setAdapter(RecyclerView.Adapter adapter) {
        binding.recyclerView.setAdapter(adapter);
    }

    @Override
    public void setAdapterBottom(RecyclerView.Adapter adapter) {
        binding.recyclerViewBottom.setAdapter(adapter);
    }

    @Override
    public void showProgressBar(final boolean show) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                binding.progressBar.setVisibility(show?View.VISIBLE:View.GONE);
            }
        });
    }

    @Override
    public void scrollToPosition(final int pos) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                binding.recyclerView.scrollToPosition(pos);
            }
        });
    }

    @Override
    public void shadowCallingScreen(boolean show) {
        ((AdminMainActivity)getActivity()).showShadowTalking(show);
    }
}
