package rps.com.evgeny.rps.models.event_bus;

public class SearchCardModel {
    String query;

    public SearchCardModel(String text) {
        this.query = text;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}
