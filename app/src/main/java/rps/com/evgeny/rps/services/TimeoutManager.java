package rps.com.evgeny.rps.services;

import java.util.Timer;
import java.util.TimerTask;

import rps.com.evgeny.rps.utils.Consts;

public class TimeoutManager {
    private static TimeoutManager instance;
    private Timer timer;

    public static TimeoutManager getInstance() {
        if (instance==null) {
            instance = new TimeoutManager();
        }
        return instance;
    }

    public void start(final TimeoutCallback callback, final int error) {
        if (timer!=null)
            timer.cancel();

        this.timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (timer!=null) {
                    callback.timeIsOver(error);
                }
                stop();
            }
        }, Consts.TIMEOUT_ERROR, Consts.TIMEOUT_ERROR);
    }

    public void stop() {
        if (timer!=null)
            timer.cancel();
        timer = null;
    }

    public interface TimeoutCallback {
        void timeIsOver(int error);
    }
}
