package rps.com.evgeny.rps.services;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;

import rps.com.evgeny.rps.R;

public class SoundVibrationManager {
    private Context context;
    private static SoundVibrationManager instance;
    private Uri notification;
    private MediaPlayer mediaPlayer;
    private Vibrator vibrator;

    private long[] successPattern = new long[] {0,200,50,100,50,100};

    private SoundVibrationManager(Context context) {
        this.context = context;

        notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);

        if(notification == null){
            notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            if(notification == null) {
                notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            }
        }
    }

    public static SoundVibrationManager getInstance(Context context) {
        if (instance==null) {
            instance = new SoundVibrationManager(context);
        }
        return instance;
    }

    public void start() {
        playSound();
        vibrate();
    }

    public void stop() {
        if (mediaPlayer!=null && mediaPlayer.isPlaying())
            mediaPlayer.stop();
        if (vibrator!=null && vibrator.hasVibrator())
            vibrator.cancel();
    }

    void playSound() {
        mediaPlayer = MediaPlayer.create(context, notification);
        if (mediaPlayer==null) {
            mediaPlayer = MediaPlayer.create(context, R.raw.oldphone_mono);
        }
        mediaPlayer.start();
    }

    void vibrate() {
        vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator.vibrate(VibrationEffect.createWaveform(successPattern, 1));
        } else {
            vibrator.vibrate(successPattern, 1);
        }
    }

    public void vibrateShort() {
        vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator.vibrate(VibrationEffect.createWaveform(successPattern, -1));
        } else {
            vibrator.vibrate(successPattern, -1);
        }
    }
}
