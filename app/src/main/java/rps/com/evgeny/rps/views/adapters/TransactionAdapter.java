package rps.com.evgeny.rps.views.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import rps.com.evgeny.rps.databinding.TransactionViewHolderBinding;
import rps.com.evgeny.rps.models.person.TransactionModel;
import rps.com.evgeny.rps.views.AdapterInterface;
import rps.com.evgeny.rps.views.viewholders.TransactionViewHolder;

/**
 * Created by Evgeny on 23.09.2017.
 */

public class TransactionAdapter extends RecyclerView.Adapter<TransactionViewHolder> implements AdapterInterface<TransactionModel> {
    List<TransactionModel> items;

    public TransactionAdapter() {
        items = new ArrayList<>();
    }

    @Override
    public TransactionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        TransactionViewHolderBinding binding = TransactionViewHolderBinding.inflate(inflater, parent, false);
        return new TransactionViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(TransactionViewHolder holder, int position) {
        holder.setTransaction(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onAdd(TransactionModel item) {
        int position = items.size();
        items.add(position, item);
        notifyItemInserted(position);
    }

    @Override
    public void onRemove(int pos) {
        if (pos < 0)
            return;
        items.remove(pos);
        notifyItemRemoved(pos);
    }

    @Override
    public void onRemoveAll() {
        int count = getItemCount();
        items.clear();
        notifyItemRangeRemoved(0, count);
    }

    @Override
    public void onAddAll(List<TransactionModel> items) {

    }
}
