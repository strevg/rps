package rps.com.evgeny.rps.models.list;

import java.util.UUID;

/**
 * Created by Evgeny on 08.10.2017.
 */

public class CompanyModel {
    public Integer index;
    public UUID Id;
    public String CompanyName;
    public String CompanyPhone;
    public String CompanyEmail;
    public String CompnayContactName;
    public String CompnayContactPhone;
    public String CompnayContactEmail;
    public Integer ParkingSpacesCount;
    public Integer _IsDeleted;
    public String Sync;
}
