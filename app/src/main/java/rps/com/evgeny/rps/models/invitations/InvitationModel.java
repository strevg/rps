package rps.com.evgeny.rps.models.invitations;

import java.util.UUID;

/**
 * Created by Evgeny on 03.10.2017.
 */

public class InvitationModel {
    public UUID Id;
    public UUID IdBCM;
    public UUID OperId;
    public UUID CompanyId;
    public Integer IdTP;
    public Integer IdTS;
    public Integer DiscountAmount;
    public Integer GroupIdFC;
    public String UsageTime;
    public String Sync;
    public String GuestPlateNumber;
    public String GuestCarModel;
    public String GuestName;
    public String GuestEmail;
    public String ParkingTime;
    public String Comment;
    public String CreatedAt;
    public String EnterId;
    public String ExitId;
    public UUID IdTPForCount;
    public UUID IdTSForCount;
    public String CardId;
    public String Ammount;
    public String Used;
    public String ActivatedAt;
    public String ParkingEnterTime;
    public String TPForRent;
    public String TSForRent;
    public String IsPending;
    public String EnterTime;
    public String ExitTime;

    public UUID id;
    public boolean editable;
    public boolean editable_fields;
    public String CompanyName;
    public String search;

    public InvitationModel() {}
}
