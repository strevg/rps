package rps.com.evgeny.rps.models.network;

import android.util.Log;

import java.util.UUID;

import rps.com.evgeny.rps.utils.Consts;

/**
 * Created by Evgeny on 08.07.2018.
 */

public class AlarmZoneModel {
    private String Id;
    private String DeviceId;
    public String Begin;
    public String End;
    public String Value;
    public String ValueEnd;
    public int _IsDeleted;
    public String Sync;
    public int AlarmColorId;
    public int TypeId;
    public String Name;
    public String color;
    public String state;
    public String Type;
    public String Device;
    public String Duration;

    public String getId() {
        return Id;
    }

    public AlarmZoneModel(AlarmWebSocketModel model) {
        this.Begin = model.Begin;
        this.End = model.End;
        this.Value = model.Value;
        try {
            this._IsDeleted = Boolean.valueOf(model._IsDeleted.get(0)) ? 1 : 0;
            this.Sync = model.Sync.get(0);
        } catch (Exception e) {
            Log.e(Consts.TAG, e.toString());
        }
        this.AlarmColorId = model.AlarmColorId;
        this.TypeId = model.TypeId;
        this.Name = model.Name;
        this.color = model.color;
        this.Type = model.Type;
        this.Id = model.id;
    }
}
