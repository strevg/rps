package rps.com.evgeny.rps.models;

import android.content.Context;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.util.ArrayList;
import java.util.Enumeration;

import rps.com.evgeny.rps.utils.Consts;

/**
 * Created by Evgeny on 31.08.2017.
 */

public class KeyWarehouse implements IAuth {
    Context context;
    KeyStore ks;
    KeyStore.PasswordProtection prot;
    final String fileName = "RPS.Token";
    static char[] password = null;
    Object syncObj = new Object();

    public KeyWarehouse(Context context) {
        this.context = context;
        CreateStore();
    }

    @Override
    public void CreateStore() {
        try {
            ks = KeyStore.getInstance(KeyStore.getDefaultType());
            prot = new KeyStore.PasswordProtection(password);

            synchronized (syncObj) {
                FileInputStream fileInputStream = context.openFileInput(fileName);
                ks.load(fileInputStream, password);
            }
        } catch (FileNotFoundException e) {
            loadEmptyKeyStore(password);
        } catch (Exception e) {
            Log.e(Consts.TAG, e.getMessage());
            Crashlytics.logException(e);
        }
    }

    @Override
    public ArrayList<SecurityUser> findAccountsForService(String serviceId) {
        ArrayList<SecurityUser> r = new ArrayList<>();
        String postfix = "-" + serviceId;
        try {
            Enumeration<String> aliases = ks.aliases();
            while (aliases.hasMoreElements()) {
                String alias = aliases.nextElement().toString();
                if (alias.endsWith(postfix)) {
                    KeyStore.SecretKeyEntry e = (KeyStore.SecretKeyEntry)ks.getEntry(alias, prot);
                    if (e != null) {
                        byte[] bytes = e.getSecretKey().getEncoded();
                        String raw = new String(bytes, SecretAccount.UTF8_CHARSET);
                        SecurityUser data = new Gson().fromJson(raw, SecurityUser.class);
                        r.add(data);
                    }
                }
            }
        } catch (KeyStoreException e) {
            Log.e(Consts.TAG, e.getMessage());
            Crashlytics.logException(e);
        } catch (UnrecoverableEntryException | NoSuchAlgorithmException e) {
            Log.e(Consts.TAG, e.getMessage());
            Crashlytics.logException(e);
        }
        return r;
    }

    @Override
    public void save(String token, User user, String domain, String serviceId) {
        String alias = makeAlias(serviceId);
        SecretAccount secretKey = new SecretAccount(token, user, domain);
        KeyStore.SecretKeyEntry entry = new KeyStore.SecretKeyEntry(secretKey);
        try {
            ks.setEntry(alias, entry, prot);
            save();
        } catch (KeyStoreException e) {
            Log.e(Consts.TAG, e.getMessage());
            Crashlytics.logException(e);
        }
    }

    @Override
    public void delete(String serviceId) {
        String alias = makeAlias(serviceId);
        try {
            ks.deleteEntry(alias);
            save();
        } catch (KeyStoreException e) {
            Log.e(Consts.TAG, e.getMessage());
            Crashlytics.logException(e);
        }
    }

    void save() {
        synchronized (syncObj) {
            try {
                FileOutputStream fileOutputStream = context.openFileOutput(fileName, Context.MODE_PRIVATE);
                ks.store(fileOutputStream, password);
            } catch (Exception e) {
                Log.e(Consts.TAG, e.getMessage());
                Crashlytics.logException(e);
            }
        }
    }

    static String makeAlias(String serviceId) {
        return "-" + serviceId;
    }

    void loadEmptyKeyStore(char[] password) {
        try {
            ks = KeyStore.getInstance(KeyStore.getDefaultType());
            ks.load(null, password);
        } catch (Exception e) {
            Log.e(Consts.TAG, e.getMessage());
            Crashlytics.logException(e);
        }
    }
}
