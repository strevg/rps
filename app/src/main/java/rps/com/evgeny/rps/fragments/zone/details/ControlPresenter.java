package rps.com.evgeny.rps.fragments.zone.details;

import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.di.ActivityScoped;
import rps.com.evgeny.rps.models.event_bus.RackModeChange;
import rps.com.evgeny.rps.models.network.DeviceZoneModel;
import rps.com.evgeny.rps.models.network.RackWorkingModeModel;
import rps.com.evgeny.rps.models.network.WebSocketModel;
import rps.com.evgeny.rps.services.ControlManager;
import rps.com.evgeny.rps.services.ProximityManager;
import rps.com.evgeny.rps.services.RpsWebSocket;
import rps.com.evgeny.rps.services.TimeoutManager;
import rps.com.evgeny.rps.services.TimerManager;
import rps.com.evgeny.rps.services.linphone.LinphoneManager;
import rps.com.evgeny.rps.services.linphone.callback.PhoneListener;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.views.DialogHelper;
import static rps.com.evgeny.rps.utils.Consts.CallStatus;

/**
 * Created by Evgeny on 07.06.2018.
 */
@ActivityScoped
public class ControlPresenter implements ControlContract.Presenter, View.OnClickListener,
        RpsWebSocket.WebSocketMessageListener, ControlManager.ControlManagerCallback,
        PhoneListener, TimerManager.TimerTick, TimeoutManager.TimeoutCallback, ProximityManager.ProximityCallback {
    @Nullable
    ControlContract.View view;

    ControlManager controlManager;
    WebSocketModel socketModel;
    LinphoneManager linphoneManager;
    ProximityManager proximityManager;
    CallStatus callStatus = CallStatus.NONE;
    TimerManager timerManager;

    @Inject
    ControlPresenter() {
        controlManager = ControlManager.getInstance();
    }

    @Override
    public void takeView(ControlContract.View view) {
        socketModel = new WebSocketModel(Consts.MessageType.COMPLETE, controlManager.getDevice().Id.toString());
        this.view = view;
        controlManager.setListener(this);
        controlManager.setCallBack(this);
        controlManager.setTimeoutCallback(this);

        linphoneManager = LinphoneManager.getInstance(view.getContext());
        linphoneManager.setPhoneListener(this);

        timerManager = new TimerManager(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                onMessage(controlManager.getLastWebSocketModel());
                checkActiveCall();
            }
        }, 100);

        proximityManager = ProximityManager.getInstance(view.getContext());
        proximityManager.register(this);

        view.initViewType(controlManager.getDevice());
    }

    @Override
    public void dropView() {
        this.view = null;
        controlManager.removeListener(this);
        timerManager.stopTimer();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.changeQRControl:
                controlManager.openInvitations();
                break;
            case R.id.callDeviceControl:
                handleCalling();
                break;
            case R.id.changeTypeControl:
                showReasonChangeDialog();
                break;
            case R.id.enterCarControl:
                if (socketModel.getMessageType()==Consts.MessageType.IN_PROCCESS) {
                    controlManager.cancelSingleCar();
                } else {
                    controlManager.passSingleCar();
                }
                break;
        }
    }

    void showReasonChangeDialog() {
        new DialogHelper(view.getContext()).showReasonChangeModeDialog(new DialogHelper.onReturnListener<String>() {
            @Override
            public void onReturn(String value) {
                if (value==null) {
                    view.showProgress(R.string.error_change_rack_mode);
                } else {
                    controlManager.postReasonChangeMode(controlManager.getDevice().Name, controlManager.getDevice().Id.toString(), value);
                }
            }
        });
    }

    void changeRackMode() {
        new DialogHelper(view.getContext()).showChangeRackModeDialog(controlManager.getDevice().RackWorkingModes,
                controlManager.getDevice().RackWorkingModeId,
                new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                        if (which+1!=controlManager.getDevice().RackWorkingModeId)
                            controlManager.changeRackMode(which+1);
                        return true;
                    }
                });
    }

    void checkActiveCall() {
        if (linphoneManager.getCalls()!=null && linphoneManager.getCalls().length>0) {
            onCallConnect("");
        } else {
            callStatus = CallStatus.NONE;
        }
    }

    void handleCalling() {
        if (callStatus==CallStatus.NONE || callStatus == CallStatus.STARTING) {
            callStatus = CallStatus.STARTING;
            controlManager.onCall();
        } else {
            controlManager.onStop();
        }
    }

    void updateCallView() {
        if (view==null)
            return;
        if (callStatus== CallStatus.NONE) {
            view.showCallNormal();
        } else if (callStatus==CallStatus.STARTING) {
            view.showCallNow();
        } else {
            view.showCallCancel();
        }
    }

    @Override
    public void onSuccessChangeRack(RackModeChange item) {
        try {
            controlManager.getDevice().RackWorkingModeId = item.model.Id;
            controlManager.getDevice().RackWorkingModeName = item.model.Name;
            view.showToast(R.string.success_change_rack_mode);
            EventBus.getDefault().post(item);
        } catch (Exception e) {
            view.showToast(R.string.error_change_rack_mode);
        }
        view.showProgress(R.string.error_change_rack_mode);
    }

    @Override
    public void onSuccessPostReasonChangeMode() {
        changeRackMode();
    }

    @Override
    public void onRequestFailed(int error) {
        if (view!=null) {
            view.showToast(error);
            view.showProgress(error);
        }
    }

    @Override
    public void onRequestSuccess(int code) {
        if (view!=null) {
            view.showProgress(code);
        }
    }

    @Override
    public void timeIsOver(int error) {
        if (view!=null) {
            view.showToast(error);
            view.showProgress(error);
        }
    }

    @Override
    public void onMessage(WebSocketModel model) {
        if (model!=null && socketModel.getId().equalsIgnoreCase(model.getId()) && view!=null) {
            handleMessage(model);
        }
    }

    void handleMessage(WebSocketModel model) {
        if (socketModel.getMessageType()==Consts.MessageType.COMPLETE ||
                socketModel.getMessageType()== Consts.MessageType.CANCELED) {
            if (model.getMessageType()== Consts.MessageType.IN_PROCCESS) {
                view.showEnterCancel();
            }
        } else if (socketModel.getMessageType()==Consts.MessageType.IN_PROCCESS) {
            if (model.getMessageType()==Consts.MessageType.COMPLETE
                    || model.getMessageType()==Consts.MessageType.CANCELED) {
                view.showEnterNormal();
            }
        }
        this.socketModel = model;
    }

    //---------- Phone listener -----------
    @Override
    public void onCallStart(String id) {
        callStatus = CallStatus.STARTING;
        updateCallView();
    }

    @Override
    public void onCallConnect(String id) {
        callStatus = CallStatus.TALKING;
        timerManager.startTimer();
        updateCallView();
    }

    @Override
    public void onCallFinish(String id) {
        callStatus = CallStatus.NONE;
        timerManager.stopTimer();
        updateCallView();
    }
    @Override
    public void onIncomingCall(String id) {
        linphoneManager.onAnswer();
    }

    @Override
    public void onError(String message) {
        callStatus = CallStatus.NONE;
        timerManager.stopTimer();
        updateCallView();
        Log.e(Consts.TAG, "Error calling");
    }

    @Override
    public void onTick() {
        if (view!=null)
            view.showTime(linphoneManager.getDuration());
    }

    @Override
    public void approachSensor() {
        if (callStatus==CallStatus.TALKING && view!=null) {
            view.shadowCallingScreen(true);
        }
    }

    @Override
    public void distanceFromSensor() {
        if (callStatus==CallStatus.TALKING && view!=null) {
            view.shadowCallingScreen(false);
        }
    }
}
