package rps.com.evgeny.rps.fragments.zone;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;

import rps.com.evgeny.rps.di.BasePresenter;
import rps.com.evgeny.rps.di.BaseView;
import rps.com.evgeny.rps.fragments.warning.active.WarningActiveContract;
import rps.com.evgeny.rps.models.network.DeviceZoneModel;

/**
 * Created by Evgeny on 30.05.2018.
 */

public interface ZoneDevicesContract {
    interface View extends BaseView<ZoneDevicesContract.View> {
        void setAdapter(RecyclerView.Adapter adapter);
        void setAdapterBottom(RecyclerView.Adapter adapter);
        void openMoreDetails(DeviceZoneModel model);
        void showProgressBar(boolean show);
        void shadowCallingScreen(boolean show);
        void showToast(int text);
        LayoutInflater getInflater();
        Context getContext();
    }

    interface Presenter extends BasePresenter<ZoneDevicesContract.Presenter> {
        void takeView(ZoneDevicesContract.View view);
        void dropView();
        void setPhoneListener();
    }
}
