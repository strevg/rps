package rps.com.evgeny.rps.models.zones_devices;

import java.util.ArrayList;
import java.util.List;

import rps.com.evgeny.rps.models.network.ZoneTableModel;
import rps.com.evgeny.rps.views.adapters.expandable.ParentListItem;

/**
 * Created by Evgeny on 30.05.2018.
 */

public class ZoneParentModel implements ParentListItem {
    private ZoneTableModel model;
    public List<ZoneChildModel> childrenList;
    String title;
    String text;
    int count;

    @Override
    public List<ZoneChildModel> getChildItemList() {
        return childrenList;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }

    public ZoneParentModel(List<ZoneChildModel> list, String title, String text, int count) {
        this.childrenList = list;
        this.title = title;
        this.text = text;
        this.count = count;
    }

    public ZoneParentModel(ZoneTableModel model) {
        this.model = model;

        setCount(model.FreeSpace);
        setTitle(model.Name);

        initChild();
    }

    void initChild() {
        this.childrenList = new ArrayList<>();
        for (int i=0; i<model.DeviceModel.size(); i++) {
            childrenList.add(new ZoneChildModel(model.DeviceModel.get(i)));
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getId() {
        return model.Id.toString();
    }
}
