package rps.com.evgeny.rps.models.warnings;

import dagger.Module;
import rps.com.evgeny.rps.fragments.warning.adapters.WarningAdapter;
import rps.com.evgeny.rps.fragments.zone.adapter.BaseType;
import rps.com.evgeny.rps.models.network.AlarmWebSocketModel;
import rps.com.evgeny.rps.models.network.AlarmZoneModel;
import rps.com.evgeny.rps.utils.ViewUtils;

import static rps.com.evgeny.rps.fragments.warning.adapters.WarningAdapter.WARNING_TYPE;

/**
 * Created by Evgeny on 24.05.2018.
 */

public class WarningModel implements BaseType {
    AlarmZoneModel model;

    public WarningModel() {}

    public WarningModel(AlarmZoneModel model) {
        this.model = model;
    }

    public WarningModel(AlarmWebSocketModel model) {
        this.model = new AlarmZoneModel(model);
    }

    public boolean isEmpty() {
        return model==null;
    }

    public String getCode() {
        return String.valueOf(model.TypeId);
    }

    public int getCodeInt() {
        return model.TypeId;
    }

    public int getColor() {
        return ViewUtils.getColorByIndex(model.AlarmColorId);
    }

    public String getText() {
        return model.Value;
    }

    public String getTitle() {
        return model.Name;
    }

    public int getAlarmColorId() {
        return model.AlarmColorId;
    }

    public AlarmZoneModel getModel() {
        return model;
    }

    @Override
    public int getType() {
        return WARNING_TYPE;
    }

    @Override
    public String getId() {
        return model.getId();
    }

    public String getTime() {
        return model.Begin;
    }
}
