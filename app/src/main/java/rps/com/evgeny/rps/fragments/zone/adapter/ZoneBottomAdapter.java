package rps.com.evgeny.rps.fragments.zone.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.databinding.ViewholderActiveCallingBinding;
import rps.com.evgeny.rps.databinding.ViewholderActiveEnteringBinding;
import rps.com.evgeny.rps.views.viewholders.ActiveCallingViewHolder;
import rps.com.evgeny.rps.views.viewholders.ActiveEnteringViewHolder;

/**
 * Created by Evgeny on 22.07.2018.
 */

public class ZoneBottomAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int ENTERING_TYPE = 0;
    public static final int CALLING_TYPE = 1;

    private List<BaseType> items;
    private onBottomListener listener;
    private onCallFinish callListener;

    public ZoneBottomAdapter() {
        items = new ArrayList<>();
    }

    public void setOnBottomListener(onBottomListener listener) {
        this.listener = listener;
    }

    public void setOnCallListener(onCallFinish listener) {
        this.callListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        RecyclerView.ViewHolder viewHolder;
        switch (viewType) {
            case CALLING_TYPE:
                ViewholderActiveCallingBinding calling = ViewholderActiveCallingBinding.inflate(inflater, parent, false);
                viewHolder = new ActiveCallingViewHolder(calling.getRoot());
                break;
            case ENTERING_TYPE:
                ViewholderActiveEnteringBinding entering = ViewholderActiveEnteringBinding.inflate(inflater, parent, false);
                viewHolder = new ActiveEnteringViewHolder(entering.getRoot());
                break;
            default:
                throw new IllegalStateException("unknown type");
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case CALLING_TYPE:
                ActiveCallingViewHolder calling = (ActiveCallingViewHolder)holder;
                final CallingModel model = (CallingModel)items.get(position);
                calling.setData(model);
                calling.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        switch (view.getId()) {
                            case R.id.answerCalling:
                                if (listener!=null)
                                    listener.onAnswer(model.getId());
                                break;
                            case R.id.rejectCalling:
                                if (listener!=null)
                                    listener.onCancel(model.getId());
                                break;
                            case R.id.finishCalling:
                                if (listener!=null)
                                    listener.onFinish(model.getId());
                                else if (callListener!=null)
                                    callListener.onFinish(model);
                                break;
                        }
                    }
                });
                break;

            case ENTERING_TYPE:
                ActiveEnteringViewHolder entering = (ActiveEnteringViewHolder)holder;
                final EnteringModel data = (EnteringModel)items.get(position);
                entering.setText(data.getText());
                entering.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onCancelEntering(data.getId());
                    }
                });
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void onAdd(BaseType item) {
        int position = items.size();
        this.items.add(position, item);
        notifyItemInserted(position);
    }

    public int getIndex(String id) {
        int pos = -1;
        for (BaseType item : items) {
            if (item.getId().equalsIgnoreCase(id)) {
                pos = items.indexOf(item);
                break;
            }
        }
        return pos;
    }

    public BaseType getItem(int pos) {
        if (pos<getItemCount()) {
            return items.get(pos);
        }
        return null;
    }

    public void onRemove(String id) {
        int pos = getIndex(id);
        if (pos < 0)
            return;
        this.items.remove(pos);
        notifyItemRemoved(pos);
    }

    public void onRemove(int pos) {
        if (pos < 0)
            return;
        this.items.remove(pos);
        notifyItemRemoved(pos);
    }

    public void onRemoveAll() {
        int count = getItemCount();
        this.items.clear();
        notifyItemRangeRemoved(0, count);
    }

    public void onUpdateCallingModel(String id, boolean flag) {
        int pos = -1;
        for (BaseType item : items) {
            if (item.getId().equalsIgnoreCase(id)) {
                pos = items.indexOf(item);
                break;
            }
        }
        if (pos>-1) {
            CallingModel callingModel = (CallingModel)items.get(pos);
            callingModel.setActive(flag);
            notifyItemChanged(pos);
        }
    }

    public interface onBottomListener {
        void onAnswer(String id);
        void onCancel(String id);
        void onFinish(String id);
        void onCancelEntering(String id);
    }

    public interface onCallFinish {
        void onFinish(CallingModel model);
    }
}
