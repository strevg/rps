package rps.com.evgeny.rps.fragments.alarms;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.activities.AdminMainActivity;
import rps.com.evgeny.rps.databinding.FragmentAlarmsBinding;
import rps.com.evgeny.rps.views.SlideInRightAnimator;

public class AlarmsFragment extends DaggerFragment implements AlarmsContract.View {
    FragmentAlarmsBinding binding;

    @Inject
    AlarmsContract.Presenter presenter;

    @Inject
    public AlarmsFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_alarms, container, false);
        initViews();
        return binding.getRoot();
    }

    void initViews() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        binding.activeAlarmsRecycler.setLayoutManager(layoutManager);
        binding.activeAlarmsRecycler.setItemAnimator(new SlideInRightAnimator());

        LinearLayoutManager layoutManager2 = new LinearLayoutManager(getActivity());
        binding.inactiveAlarmsRecycler.setLayoutManager(layoutManager2);
        binding.inactiveAlarmsRecycler.setItemAnimator(new SlideInRightAnimator());

        LinearLayoutManager layoutManager3 = new LinearLayoutManager(getActivity());
        binding.bottomRecycler.setLayoutManager(layoutManager3);
        binding.bottomRecycler.setItemAnimator(new SlideInRightAnimator());

        binding.activeAlarmsRecycler.setNestedScrollingEnabled(false);
        binding.inactiveAlarmsRecycler.setNestedScrollingEnabled(false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.takeView(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.setPhoneListener();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.dropView();
    }

    @Override
    public void setActiveAdapter(RecyclerView.Adapter adapter) {
        binding.activeAlarmsRecycler.setAdapter(adapter);
    }

    @Override
    public void setInactiveAdapter(RecyclerView.Adapter adapter) {
        binding.inactiveAlarmsRecycler.setAdapter(adapter);
    }

    @Override
    public void setBottomAdapter(RecyclerView.Adapter adapter) {
        binding.bottomRecycler.setAdapter(adapter);
    }

    @Override
    public void showProgressBar(final boolean show) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                binding.progressBar.setVisibility(show?View.VISIBLE:View.GONE);
            }
        });
    }

    @Override
    public void shadowCallingScreen(boolean show) {
        ((AdminMainActivity)getActivity()).showShadowTalking(show);
    }

    @Override
    public void setNumberAlarms(final int number) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String text = getString(R.string.title_new_alarms, number);
                binding.numberAlarm.setText(text);
            }
        });
    }

    @Override
    public void scrollToPosition(final int pos) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                binding.activeAlarmsRecycler.scrollToPosition(pos);
            }
        });
    }
}
