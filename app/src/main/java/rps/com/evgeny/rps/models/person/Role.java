package rps.com.evgeny.rps.models.person;

import java.util.List;
import java.util.UUID;

/**
 * Created by Evgeny on 21.10.2017.
 */

public class Role {
    public UUID Id;
    public String Name;
    public UUID ParkingId;
    public UUID id;
    public Boolean editable;
    public Boolean editable_fields;
    public List<Right> ActiveRights;

    public class Right {
        public UUID Id;
        public UUID RightId;
        public UUID RoleId;
        public Integer Active;
        public UUID id;
        public String Name;
    }
}
