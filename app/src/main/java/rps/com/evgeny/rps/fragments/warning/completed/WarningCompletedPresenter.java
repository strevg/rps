package rps.com.evgeny.rps.fragments.warning.completed;

import android.support.annotation.Nullable;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.realm.RealmResults;
import rps.com.evgeny.rps.fragments.warning.adapters.WarningAdapter;
import rps.com.evgeny.rps.fragments.zone.adapter.BaseType;
import rps.com.evgeny.rps.models.WarningAddedEventBus;
import rps.com.evgeny.rps.models.realm.WarningRealm;
import rps.com.evgeny.rps.services.RealmManager;
import rps.com.evgeny.rps.views.BaseItemAnimator;

/**
 * Created by Evgeny on 25.05.2018.
 */

public class WarningCompletedPresenter implements WarningCompletedContract.Presenter {
    @Nullable
    WarningCompletedContract.View view;

    WarningAdapter adapter;
    RealmManager realmManager;

    @Inject
    WarningCompletedPresenter() {
        adapter = new WarningAdapter(true);
    }

    @Override
    public void takeView(WarningCompletedContract.View view) {
        this.view = view;
        this.realmManager = RealmManager.getInstance();
        view.setAdapter(adapter);
        EventBus.getDefault().register(this);

        RealmResults<WarningRealm> realms = realmManager.getAllWarning();
        adapter.onAddAll(getData(realms.subList(0, realms.size())));
    }

    @Override
    public void dropView() {
        this.view = null;
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void WarningAddedEventBus(WarningAddedEventBus model) {
        RealmResults<WarningRealm> realms = realmManager.getAllWarning();
        adapter.onAddAll(getData(realms.subList(0, realms.size())));
    }

    List<BaseType> getData(List<WarningRealm> list) {
        List<BaseType> baseList = new ArrayList<>();
        for (WarningRealm model : list)
            baseList.add(model);
        return baseList;
    }
}
