package rps.com.evgeny.rps.nfc;

/**
 * Created by Evgeny on 01.09.2017.
 */

public class CardResponse {
    public final String STATUS_UNKNOWN = "Unknown";
    public final String STATUS_SUCCESS = "Success";
    public final String STATUS_TIMEOUT = "TimeOut";
    public final String STATUS_EXCEPTION = "Exception";
    public final String STATUS_CARDNOTEXISTS = "CardNotExists";
    public final String STATUS_CARDREADED = "CardReaded";
    public final String STATUS_CARDWRITED = "CardWrited";
    public final String STATUS_READERROR = "ReadError";
    public final String STATUS_AUTHERROR = "AuthError";
    public final String STATUS_WRITEERROR = "WriteError";
    public final String STATUS_SOMEONEELSESCARD = "SomeoneElsesCard";

    public String Type;
    public String Message;
    public String Code;

    public boolean IsError() {
        return Type == "E";
    }

    public String ReaderCode() {
        switch (Code) {
            case "6982":
                return STATUS_SOMEONEELSESCARD;
            case "9100":
                return STATUS_SUCCESS;
            default:
                if (IsError()) {
                    return STATUS_EXCEPTION;
                } else {
                    return STATUS_SUCCESS;
                }
        }
    }
}
