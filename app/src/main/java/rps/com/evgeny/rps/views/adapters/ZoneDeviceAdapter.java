package rps.com.evgeny.rps.views.adapters;

import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.logging.Handler;

import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.models.event_bus.RackModeChange;
import rps.com.evgeny.rps.models.network.RackWorkingModeModel;
import rps.com.evgeny.rps.models.zones_devices.ZoneChildModel;
import rps.com.evgeny.rps.models.zones_devices.ZoneParentModel;
import rps.com.evgeny.rps.utils.Settings;
import rps.com.evgeny.rps.views.adapters.expandable.ExpandableRecyclerAdapter;
import rps.com.evgeny.rps.views.adapters.expandable.ParentListItem;
import rps.com.evgeny.rps.views.adapters.expandable.ParentViewHolder;
import rps.com.evgeny.rps.views.onItemClickListener;
import rps.com.evgeny.rps.views.viewholders.ZoneChildViewHolder;
import rps.com.evgeny.rps.views.viewholders.ZoneParentViewHolder;

/**
 * Created by Evgeny on 30.05.2018.
 */

public class ZoneDeviceAdapter extends ExpandableRecyclerAdapter<ZoneParentViewHolder, ZoneChildViewHolder> {
    List<ZoneParentModel> itemList;
    private LayoutInflater mInflater;
    onItemClickListener<ZoneChildModel> moreListener;
    onItemClickListener<ZoneChildModel> listener;

    public ZoneDeviceAdapter(LayoutInflater inflater, List<ZoneParentModel> itemList) {
        super(itemList);
        this.itemList = itemList;
        this.mInflater = inflater;
    }

    public void setMoreItemClickListener(onItemClickListener listener) {
        this.moreListener = listener;
    }

    public void setItemClickListener(onItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public ZoneParentViewHolder onCreateParentViewHolder(ViewGroup viewGroup) {
        View view = mInflater.inflate(R.layout.view_holder_zone_parent, viewGroup, false);
        return new ZoneParentViewHolder(view);
    }

    @Override
    public ZoneChildViewHolder onCreateChildViewHolder(ViewGroup viewGroup) {
        View view = mInflater.inflate(R.layout.view_holder_zone_child, viewGroup, false);
        return new ZoneChildViewHolder(view);
    }

    @Override
    public void onBindParentViewHolder(final ZoneParentViewHolder parentViewHolder, int i, final ParentListItem parentListItem) {
        final ZoneParentModel data = (ZoneParentModel) parentListItem;
        parentViewHolder.setData(data);

        new android.os.Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Settings.isOpenedTab(data.getId()) && !parentViewHolder.isExpanded())
                    parentViewHolder.onClick(parentViewHolder.itemView);

                parentViewHolder.setExpandedIcon(Settings.isOpenedTab(data.getId()));
            }
        }, 500);
    }

    @Override
    public void onBindChildViewHolder(ZoneChildViewHolder childViewHolder, int i, Object childListItem) {
        final ZoneChildModel data = (ZoneChildModel)childListItem;
        childViewHolder.setOnClickMoreListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (moreListener !=null) {
                    moreListener.onItemClick(view, data);
                }
            }
        });
        childViewHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener!=null) {
                    listener.onItemClick(view, data);
                }
            }
        });
        childViewHolder.setData(data);
    }

    public void onRemoveMenuChild(int parentPos, int childPos) {
        if (childPos>=0) {
            notifyParentItemChanged(parentPos);
            itemList.get(parentPos).getChildItemList().remove(childPos);
            notifyChildItemRemoved(parentPos, childPos);
        }
    }

    public void updateRackMode(RackModeChange model) {
        int posParent = -1;
        int posChild = -1;
        boolean flagSkip = false;

        for (ZoneParentModel parent : itemList) {
            for (ZoneChildModel child : parent.childrenList) {
                if (child.model.Id.toString().equals(model.id)) {
                    posParent = itemList.indexOf(parent);
                    posChild = parent.childrenList.indexOf(child);

                    child.setText(model.model.Name);
                    child.model.RackWorkingModeId = model.model.Id;
                    child.model.RackWorkingModeName = model.model.Name;
                    flagSkip = true;
                    break;
                }
            }
            if (flagSkip)
                break;
        }

        if (posChild!=-1)
            notifyChildItemChanged(posParent, posChild);
    }

    public void onAddAll(List<ZoneParentModel> itemList) {
        this.itemList = itemList;
        onAddParentList(itemList);
        notifyDataSetChanged();
    }

    public void onAddMergeAll(List<ZoneParentModel> itemList) {
        List<ZoneParentModel> oldItemList = this.itemList;
        this.itemList = itemList;
        //onAddParentList(itemList);

        for (int i=0; i<itemList.size(); i++) {
            if (itemList.size()==oldItemList.size()) {
                ZoneParentModel parentModel = oldItemList.get(i);
                ZoneParentModel newParentModel = itemList.get(i);
                for (int j = 0; j < newParentModel.getChildItemList().size(); j++) {
                    if (parentModel.getChildItemList().size() == newParentModel.getChildItemList().size()) {
                        ZoneChildModel childModel = parentModel.getChildItemList().get(j);
                        ZoneChildModel newChildModel = newParentModel.getChildItemList().get(j);
                        if (childModel.getColor() != newChildModel.getColor()) {
                            notifyChildItemChanged(i, j);
                        }
                    }
                }
            }
        }
    }

    public void onAddMenuChild(int parentPos, int childPost, ZoneChildModel child) {
        notifyParentItemChanged(parentPos);
        itemList.get(parentPos).getChildItemList().add(child);
        notifyChildItemInserted(parentPos, childPost);
    }

    public int getCountChildOnParent(int indexParent) {
        return itemList.get(indexParent).getChildItemList().size();
    }
}
