package rps.com.evgeny.rps.nfc;

import android.app.Activity;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.MifareClassic;
import android.util.Log;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.io.IOException;
import java.math.BigInteger;

import rps.com.evgeny.rps.MainApplication;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.Settings;
import rps.com.evgeny.rps.utils.StringUtils;

/**
 * Created by Evgeny on 01.09.2017.
 */

public class CardReader implements NfcAdapter.ReaderCallback {
    private Object locker;
    private Toast infoBlock;
    CardHandler cardHandler;
    Activity activity;

    private static String SELECT_APDU_HEADER = "00A40400";
    private String keyIndex = "1";
    private MifareClassic mifare;
    private String cardKey = "FFFFFFFFFFFF";
    private int cardSector = 1;
    Thread readThread;
    String rawData = "";

    boolean IsAuthenticate = false;
    MainApplication app;

    public CardReader(CardHandler cardHandler, Object locker) {
        this();
        this.locker = locker;
        this.cardHandler = cardHandler;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
        this.app = (MainApplication) activity.getApplication();
    }

    public CardReader() {
        if (Settings.model.CardKey!=null && !Settings.model.CardKey.isEmpty())
            cardKey = Settings.model.CardKey;
        cardSector = Integer.parseInt(Settings.model.SectorNumber);

        readThread = new Thread(new Runnable() {
            @Override
            public void run() {
                ReadWork();
            }
        });
    }

    public String GetRawData() {
        return rawData;
    }

    public String GetData() {
        String data = ReadData();
        try {
            if (StringUtils.isNullOrEmpty(data)) {
                Thread.sleep(50);
                if (StringUtils.isNullOrEmpty(data)) {
                    Thread.sleep(150);
                    data = ReadData();
                    if (StringUtils.isNullOrEmpty(data)) {
                        Thread.sleep(400);
                        data = ReadData();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(Consts.TAG, e.toString());
            Crashlytics.logException(e);
        }
        rawData = data;
        return rawData;
    }

    public void StopRead() {
        try {
            if (readThread!=null && readThread.isAlive()) {
                readThread = null;
            }
            while(readThread!=null && readThread.isAlive()){}
        } catch (Exception e) {
            Log.e(Consts.TAG, e.toString());
            Crashlytics.logException(e);
        }
    }

    public void StartRead() {
        try {
            StopRead();
            if (readThread==null || !readThread.isAlive()) {
                readThread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        ReadWork();
                    }
                });
                readThread.start();
            }
        } catch (Exception e) {
            Log.e(Consts.TAG, e.toString());
            Crashlytics.logException(e);
        }
    }

    public void ReadWork() {
        boolean run = true;
        Log.i(Consts.TAG, "Working thread ...");
        FireOnCardExist();
        do {
            try {
                /*if (mifare.isConnected()) {
                    mifare.close();
                }*/
                if (!mifare.isConnected())
                    mifare.connect();
                run = mifare.isConnected();
                if (!run) {
                    Thread.sleep(100);
                    if (!mifare.isConnected())
                        mifare.connect();
                    run = mifare.isConnected();
                    /*if (mifare.isConnected()) {
                        mifare.close();
                    }
                    mifare.connect();
                    run = mifare.isConnected();*/
                }
            } catch (Exception e) {
                e.printStackTrace();
                break;
            }
            if (run) {
                try {
                    Thread.sleep(100);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        while (run);

        OnTagRemoved();
    }

    @Override
    public void onTagDiscovered(Tag tag) {
        mifare = MifareClassic.get(tag);
        Log.i(Consts.TAG, "Карта есть!");
        Log.i(Consts.TAG, ByteArrayToHexString(mifare.getTag().getId()));
        app.showProgressDialog("Чтение...");
        if (mifare != null) {
            try {
                String id = GetId();
                Log.i(Consts.TAG, "card id = " + id);
                String data = GetData();
                if (data == null) {
                    throw new Exception("Ошибка чтения!");
                } else {
                    Log.i(Consts.TAG, data);
                    StartRead();
                }
                CardData cdata = CardReaderHelper.rBlockToData(data);
                cdata.CardId = Long.parseLong(id, 16)+"";
                CardReaderResponse response = CardReaderHelper.GetResult(cdata);

                FireOnCardRead(response);
                app.dismissProgressDialog();
            } catch (Exception e) {
                Log.e(Consts.TAG, e.toString());
                Crashlytics.logException(e);
                app.dismissProgressDialog();
            }
        }
    }

    public static byte[] BuildSelectApdu(String aid) {
        return HexStringToByteArray(SELECT_APDU_HEADER + (Integer.parseInt(aid.length() / 2+"", 16)) + aid);
    }

    public static String ByteArrayToHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder(bytes.length * 2);
        for(byte b: bytes)
            sb.append(String.format("%02x", b));
        return sb.toString();
    }

    public static String toHex(byte[] data) {
        StringBuilder sb = new StringBuilder(data.length * 2);
        for (int i=0; i<data.length; i++ ) {
            sb.append(hex[(data[i] & 0xf0) >>> 4]);
            sb.append(hex[data[i] & 0x0f] );
        }
        return sb.toString();
    }

    private static final char[] hex =
            { '0' , '1' , '2' , '3' , '4' , '5' , '6' , '7' ,
                    '8' , '9' , 'a' , 'b' , 'c' , 'd' , 'e' , 'f'};

    public static byte[] HexStringToByteArray2(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    public static byte[] HexStringToByteArray(String s) {
        byte[] b = new byte[s.length() / 2];
        for (int i = 0; i < b.length; i++) {
            int index = i * 2;
            int v = Integer.parseInt(s.substring(index, index + 2), 16);
            b[i] = (byte) v;
        }
        return b;
    }

    public static byte[] HexStringToByteArray1(String s) {
        int len = s.length();
        if (len % 2 == 1) {
            throw new IllegalArgumentException();
        }
        byte[] data = new byte[len / 2]; //Allocate 1 byte per 2 hex characters
        for (int i = 0; i < len; i += 2) {
            int val, val2;
            // Convert each chatacter into an unsigned integer (base-16)
            try {
                val = Integer.valueOf(s.charAt(i) + "0", 16);
                val2 = Integer.valueOf("0" + s.charAt(i + 1), 16);
            } catch (Exception e) {
                Log.e(Consts.TAG, e.toString());
                Crashlytics.logException(e);
                continue;
            }
            data[i / 2] = (byte) (val + val2);
        }
        return data;
    }

    public String GetId() {
        return ByteArrayToHexString(mifare.getTag().getId());
    }

    public boolean Connect() {
        try {
            if (!mifare.isConnected()) {
                IsAuthenticate = false;
                mifare.connect();
            }
        } catch (Exception e) {
            Log.e(Consts.TAG, e.toString());
            Crashlytics.logException(e);
        }
        Log.i(Consts.TAG, "card is " + (mifare.isConnected() ? "" : " NOT ") + " connected!");
        return mifare.isConnected();
    }

    public boolean Authenticate() {
        try {
            IsAuthenticate = mifare.authenticateSectorWithKeyA(cardSector, HexStringToByteArray(cardKey));
            if (!IsAuthenticate) {
                mifare.close();
            }
            return IsAuthenticate;
        } catch (Exception e) {
            Log.e(Consts.TAG, e.toString());
            Crashlytics.logException(e);
            IsAuthenticate = false;
            return IsAuthenticate;
        }
    }

    public String ReadData() {
        String res = "";
        int block = 4 * cardSector;
        try {
            if (Connect() && Authenticate()) {
                Log.i(Consts.TAG, "auth success");

                byte[] readResult = mifare.readBlock(block);
                res += ByteArrayToHexString(readResult);
                Log.i(Consts.TAG, "block №" + block + " read...");

                block++;
                readResult = mifare.readBlock(block);
                res += ByteArrayToHexString(readResult);
                Log.i(Consts.TAG, "block №" + block + " read...");

                block++;
                readResult = mifare.readBlock(block);
                res += ByteArrayToHexString(readResult);
                Log.i(Consts.TAG, "block №" + block + " read...");
                return res;
            } else {
                if (mifare.isConnected() && !IsAuthenticate) {
                    Crashlytics.logException(new Exception("mifare.isConnected() && !IsAuthenticate"));
                    ShowPopup("Ошибка аутентификации карты!");
                    app.dismissProgressDialog();
                } else {
                    Crashlytics.logException(new Exception("!mifare.isConnected() && !IsAuthenticate"));
                    ShowPopup("Ошибка подключение к карте!");
                    app.dismissProgressDialog();
                }
            }
        } catch (Exception e) {
            Log.e(Consts.TAG, e.toString());
            Crashlytics.logException(e);
        } finally {
            if (mifare != null) {
                try {
                    mifare.close();
                } catch (IOException e) {
                    Log.e(Consts.TAG, e.toString());
                    Crashlytics.logException(e);
                }
            }
        }
        return null;
    }


    public int WriteData(String data) {
        Log.i(Consts.TAG, "Start writing!");
        StopRead();

        int block = 4 * cardSector;
        try  {
            if (Connect() && Authenticate()) {
                for (int i = 0; i <= 2; i++) {
                    byte[] wblock = HexStringToByteArray(data.substring(i * 32, (i+1)*32));
                    mifare.writeBlock(block+i, wblock);
                }
                String new_data = ReadData();
                if (new_data!=null && !new_data.equals(data)) {
                    throw new Exception("Not equals!");
                }
                return 1;
            } else {
                if (mifare.isConnected() && !IsAuthenticate) {
                    Crashlytics.logException(new Exception("mifare.isConnected() && !IsAuthenticate"));
                    ShowPopup("Ошибка аутентификации карты!");
                } else {
                    ShowPopup("Ошибка подключение к карте!");
                    Crashlytics.logException(new Exception("!mifare.isConnected() && !IsAuthenticate"));
                }
                app.dismissProgressDialog();
                return 0;
            }
        }
        catch (Exception e) {
            Log.e(Consts.TAG, e.toString());
            Crashlytics.logException(e);
            return 0;
        } finally {
            if (mifare != null && mifare.isConnected()) {
                try {
                    mifare.close();
                } catch (IOException e) {
                    Log.e(Consts.TAG, e.toString());
                    Crashlytics.logException(e);
                }
            }
        }
    }

    public String Send(String apdu) {
        try {
            byte[] payload = mifare.transceive(HexStringToByteArray(apdu));
            return ByteArrayToHexString(payload);
        } catch (IOException e) {
            Log.e(Consts.TAG, e.toString());
            Crashlytics.logException(e);
        }
        return null;
    }

    public void HidePopup() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                infoBlock.cancel();
            }
        });
    }

    public void ShowPopup(final String msg) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (infoBlock!=null)
                    infoBlock.cancel();
                infoBlock = Toast.makeText(activity, msg, Toast.LENGTH_SHORT);
                infoBlock.show();
            }
        });
    }

    public void OnTagRemoved() {
        IsAuthenticate = false;
        Log.i(Consts.TAG, "removed");
        FireOnCardRemove();
    }

    private void FireOnCardExist() {
        CardReaderEventArgs e = new CardReaderEventArgs();
        e.sender = this;
        OnCardExist(e);
    }

    private void FireOnCardRemove() {
        CardReaderEventArgs e = new CardReaderEventArgs();
        e.sender = this;
        OnCardRemove(e);
    }

    private void FireOnCardRead(CardReaderResponse card) {
        CardReaderEventArgs e = new CardReaderEventArgs();
        e.sender = this;
        e.card = card;
        OnCardRead(e);
    }

    public void SetActivityLocker(Object lockObj) {
        locker = lockObj;
    }

    protected void OnCardExist(CardReaderEventArgs e) {
        if (cardHandler != null) {
            cardHandler.CardExist(this, e);
        }
    }

    protected void OnCardRemove(CardReaderEventArgs e) {
        if (cardHandler != null) {
            cardHandler.CardRemove(this, e);
        }
    }

    protected void OnCardRead(CardReaderEventArgs e) {
        if (cardHandler != null) {
            cardHandler.CardRead(this, e);
        }
    }

    public interface CardHandler {
        void CardExist(Object sender, CardReaderEventArgs e);
        void CardRead(Object sender, CardReaderEventArgs e);
        void CardRemove(Object sender, CardReaderEventArgs e);
    }
}
