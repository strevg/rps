package rps.com.evgeny.rps.models.warnings;

import java.util.Comparator;

import rps.com.evgeny.rps.utils.TimeUtils;

/**
 * Created by Evgeny on 09.07.2018.
 */

public class WarningComparator implements Comparator<WarningModel> {
    @Override
    public int compare(WarningModel o1, WarningModel o2) {
        if (o1.getAlarmColorId() < o2.getAlarmColorId()) {
            return -1;
        } else if (o1.getAlarmColorId() > o2.getAlarmColorId()) {
            return 1;
        } else if (TimeUtils.isNewerTime(o1.getTime(),o2.getTime())) {
            return -1;
        } else {
            return 1;
        }
    }
}