package rps.com.evgeny.rps.fragments.zone.details;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import rps.com.evgeny.rps.di.ActivityScoped;
import rps.com.evgeny.rps.di.FragmentScoped;

/**
 * Created by Evgeny on 08.06.2018.
 */
@Module
public abstract class ZoneDetailsModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract ZoneDetailsFragment zoneDetailsFragment();

    @ActivityScoped
    @Binds
    abstract ZoneDetailsContract.Presenter zoneDetailsPresenter(ZoneDetailsPresenter presenter);
}
