package rps.com.evgeny.rps.fragments.warning.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.databinding.ViewholderCallingBinding;
import rps.com.evgeny.rps.databinding.ViewholderWarningBinding;
import rps.com.evgeny.rps.fragments.zone.adapter.BaseType;
import rps.com.evgeny.rps.fragments.zone.adapter.CallingModel;
import rps.com.evgeny.rps.models.realm.WarningRealm;
import rps.com.evgeny.rps.models.warnings.WarningComparator;
import rps.com.evgeny.rps.models.warnings.WarningModel;
import rps.com.evgeny.rps.views.onItemClickListener;
import rps.com.evgeny.rps.views.viewholders.CallingViewHolder;
import rps.com.evgeny.rps.views.viewholders.WarningViewHolder;

/**
 * Created by Evgeny on 24.05.2018.
 */

public class WarningAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int WARNING_TYPE = 0;
    public static final int CALLING_TYPE = 1;

    private List<BaseType> items;
    private boolean isCompleted;
    private onItemClickListener listener;
    private onCallingInterface callingListener;

    public WarningAdapter() {
        items = new ArrayList<>();
    }

    public WarningAdapter(boolean completed) {
        this();
        this.isCompleted = completed;
    }

    public void setOnClickListener(onItemClickListener listener) {
        this.listener = listener;
    }

    public void setOnCallingListener(onCallingInterface listener) {
        this.callingListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        RecyclerView.ViewHolder viewHolder;
        switch (viewType) {
            case CALLING_TYPE:
                ViewholderCallingBinding calling = ViewholderCallingBinding.inflate(inflater, parent, false);
                viewHolder = new CallingViewHolder(calling.getRoot());
                break;
            case WARNING_TYPE:
                ViewholderWarningBinding binding = ViewholderWarningBinding.inflate(inflater, parent, false);
                viewHolder = new WarningViewHolder(binding.getRoot());
                break;
            default:
                throw new IllegalStateException("unknown type");
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case CALLING_TYPE:
                CallingViewHolder callingHolder = (CallingViewHolder)holder;
                final BaseType data2 = items.get(position);
                if (data2 instanceof WarningRealm)
                    callingHolder.setData((WarningRealm)data2);
                else {
                    callingHolder.setData((CallingModel) data2);

                    callingHolder.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            switch (view.getId()) {
                                case R.id.answerCalling:
                                    callingListener.onAnswer((CallingModel) data2);
                                    break;
                                case R.id.rejectCalling:
                                    callingListener.onCancel((CallingModel) data2);
                                    break;
                            }
                        }
                    });
                }
                break;

            case WARNING_TYPE:
                WarningViewHolder warningHolder = (WarningViewHolder)holder;
                final BaseType data = items.get(position);
                if (data instanceof WarningRealm)
                    warningHolder.setData((WarningRealm)data);
                else
                    warningHolder.setData((WarningModel) data);
                warningHolder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (listener!=null) {
                            listener.onItemClick(view, data);
                        }
                    }
                });
                if (isCompleted)
                    warningHolder.hideButton(true);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void onAdd(BaseType item) {
        this.items.add(0, item);
        notifyItemInserted(0);
    }

    public void onAddAll(List<BaseType> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public void onRemove(int pos) {
        if (pos < 0)
            return;
        this.items.remove(pos);
        notifyItemRemoved(pos);
    }

    public int getIndex(String id) {
        int pos = -1;
        for (BaseType item : items) {
            if (item.getId().equalsIgnoreCase(id)) {
                pos = items.indexOf(item);
                break;
            }
        }
        return pos;
    }

    public BaseType getItem(String id) {
        int pos = getIndex(id);
        if (pos>-1)
            return items.get(pos);
        return null;
    }

    public void onRemove(String id) {
        int pos = getIndex(id);
        onRemove(pos);
    }

    public void onRemove(BaseType item) {
        int pos = items.indexOf(item);
        if (pos < 0)
            return;
        this.items.remove(pos);
        notifyItemRemoved(pos);
    }

    public void onRemoveAll() {
        int count = getItemCount();
        this.items.clear();
        notifyItemRangeRemoved(0, count);
    }

    public interface onCallingInterface {
        void onAnswer(CallingModel model);
        void onCancel(CallingModel model);
    }
}
