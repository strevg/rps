package rps.com.evgeny.rps.fragments.main;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.support.annotation.Nullable;
import android.widget.Toast;

import javax.inject.Inject;

import rps.com.evgeny.rps.di.ActivityScoped;
import rps.com.evgeny.rps.fragments.onBackPressInterface;
import rps.com.evgeny.rps.network.RPSApi;
import rps.com.evgeny.rps.nfc.CardReader;
import rps.com.evgeny.rps.nfc.CardReaderEventArgs;
import rps.com.evgeny.rps.nfc.CardReaderResponse;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.Settings;
import rps.com.evgeny.rps.utils.StringUtils;

/**
 * Created by Evgeny on 09.06.2018.
 */
@ActivityScoped
public class MainAdminPresenter implements MainAdminContract.Presenter {
    @Nullable
    MainAdminContract.View view;

    @Inject
    MainAdminPresenter() {
    }

    @Override
    public void takeView(MainAdminContract.View view) {
        this.view = view;
    }

    @Override
    public void dropView() {
        this.view = null;
    }
}
