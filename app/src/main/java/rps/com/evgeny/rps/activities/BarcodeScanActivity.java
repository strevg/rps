package rps.com.evgeny.rps.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.zxing.Result;

import org.greenrobot.eventbus.EventBus;

import me.dm7.barcodescanner.zxing.ZXingScannerView;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.models.SearchCard;
import rps.com.evgeny.rps.models.event_bus.SearchCardModel;
import rps.com.evgeny.rps.services.SoundVibrationManager;

public class BarcodeScanActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mScannerView;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.activity_barcode_scan);
        setupToolbar();

        ViewGroup contentFrame = (ViewGroup) findViewById(R.id.content_frame);
        mScannerView = new ZXingScannerView(this);
        contentFrame.addView(mScannerView);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    public void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if(ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void handleResult(Result rawResult) {
        EventBus.getDefault().post(new SearchCardModel(rawResult.getText()));
        Toast.makeText(this,"Найдена карта " + rawResult.getText(), Toast.LENGTH_SHORT).show();
        SoundVibrationManager.getInstance(this).vibrateShort();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                BarcodeScanActivity.this.finish();
            }
        }, 2000);
    }
}
