package rps.com.evgeny.rps.fragments.zone.details;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.activities.AdminMainActivity;
import rps.com.evgeny.rps.databinding.FragmentControlBinding;
import rps.com.evgeny.rps.models.network.DeviceZoneModel;
import rps.com.evgeny.rps.utils.StringUtils;

/**
 * Created by Evgeny on 07.06.2018.
 */

public class ControlFragment extends DaggerFragment implements ControlContract.View {
    FragmentControlBinding binding;

    @Inject
    ControlContract.Presenter presenter;

    @Inject
    public ControlFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_control, container, false);
        initViews();
        return binding.getRoot();
    }

    void initViews() {
        binding.callDeviceControl.setOnClickListener(presenter);
        binding.changeQRControl.setOnClickListener(presenter);
        binding.changeTypeControl.setOnClickListener(presenter);
        binding.enterCarControl.setOnClickListener(presenter);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.takeView(this);
        binding.callDeviceControl.hideTitle(true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.dropView();
    }

    @Override
    public void initViewType(DeviceZoneModel device) {
        if (device.Type==2) {
            binding.enterCarControl.setVisibility(View.GONE);
            binding.changeQRControl.setVisibility(View.GONE);
            binding.changeTypeControl.setVisibility(View.GONE);
        } else if (device.TransitTypeId==2) {
            binding.changeQRControl.setVisibility(View.GONE);
        }
        binding.changeQRControl.showProgressBar(false);
        binding.enterCarControl.showProgressBar(false);
        binding.changeTypeControl.showProgressBar(false);
        binding.callDeviceControl.showProgressBar(false);
    }

    @Override
    public void showProgress(final int type) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (type == R.string.error_pass_car || type==R.string.error_cancel_car) {
                    binding.enterCarControl.showProgressBar(false);
                } else if (type == R.string.error_call || type == R.string.wait_call || type==R.string.error_call_stop) {
                    binding.callDeviceControl.showProgressBar(false);
                } else if (type == R.string.error_change_rack_mode || type==R.string.error_reason_change_mode) {
                    binding.changeTypeControl.showProgressBar(false);
                }
            }
        });
    }

    @Override
    public void shadowCallingScreen(boolean show) {
        ((AdminMainActivity)getActivity()).showShadowTalking(show);
    }

    // -------- change views state ----------
    @Override
    public void showEnterCancel() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                binding.enterCarControl.setIcon(R.drawable.ic_minus_circle_outline);
                binding.enterCarControl.setTitle(getString(R.string.button_cancel));
                binding.enterCarControl.setBackColor(getResources().getColor(R.color.colorRed));
                binding.enterCarControl.setColor(getResources().getColor(R.color.colorWhite));
            }
        });
    }
    @Override
    public void showEnterNormal() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                binding.enterCarControl.setIcon(R.drawable.ic_baseline_directions_car_24px);
                binding.enterCarControl.setTitle(getString(R.string.title_control_change_type));
                binding.enterCarControl.setBackColor(getResources().getColor(R.color.colorControlBackground));
                binding.enterCarControl.setColor(getResources().getColor(R.color.buttonYellow));
            }
        });
    }

    @Override
    public void showCallNormal() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                binding.callDeviceControl.setIcon(R.drawable.ic_local_phone_24px);
                binding.callDeviceControl.setTitle(getString(R.string.title_control_call));
                binding.callDeviceControl.hideTitle(true);
                binding.callDeviceControl.setBackColor(getResources().getColor(R.color.colorControlBackground));
                binding.callDeviceControl.setColor(getResources().getColor(R.color.buttonYellow));
            }
        });
    }

    @Override
    public void showCallNow() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                binding.callDeviceControl.setIcon(R.drawable.ic_calling_phone);
                binding.callDeviceControl.setTitle(getString(R.string.button_cancel));
                binding.callDeviceControl.hideTitle(false);
                binding.callDeviceControl.setActionTitle(getString(R.string.title_call_now));
                binding.callDeviceControl.setBackColor(getResources().getColor(R.color.buttonYellow));
                binding.callDeviceControl.setColor(getResources().getColor(R.color.colorControlBackground));
            }
        });
    }

    @Override
    public void showCallCancel() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                binding.callDeviceControl.setIcon(R.drawable.ic_phone_in_talk_white);
                binding.callDeviceControl.setTitle(getString(R.string.button_end_call));
                binding.callDeviceControl.hideTitle(false);
                binding.callDeviceControl.setActionTitle(getString(R.string.title_call_connected));
                binding.callDeviceControl.setBackColor(getResources().getColor(R.color.colorRed));
                binding.callDeviceControl.setColor(getResources().getColor(R.color.colorWhite));
            }
        });
    }

    @Override
    public void showTime(final int time) {
        if (getActivity()==null)
            return;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    String text = getString(R.string.title_call_connected, StringUtils.getTimeCallConnected(time));
                    binding.callDeviceControl.setActionTitle(String.valueOf(text));
                } catch (Exception e) {}

            }
        });
    }

    @Override
    public void showToast(final int text) {
        if (getActivity()==null || text == R.string.error_call_stop)
            return;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
                } catch (Exception e) {}
            }
        });
    }
}
