package rps.com.evgeny.rps.fragments.zone;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import rps.com.evgeny.rps.di.ActivityScoped;
import rps.com.evgeny.rps.di.FragmentScoped;

/**
 * Created by Evgeny on 30.05.2018.
 */

@Module
public abstract class ZoneDevicesModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract ZoneDevicesFragment zoneDevicesFragment();

    @ActivityScoped
    @Binds
    abstract ZoneDevicesContract.Presenter zoneDevicesPresenter(ZoneDevicesPresenter presenter);
}
