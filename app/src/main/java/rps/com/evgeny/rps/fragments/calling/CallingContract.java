package rps.com.evgeny.rps.fragments.calling;

import android.content.Context;

import rps.com.evgeny.rps.activities.CallingActivity;
import rps.com.evgeny.rps.di.BasePresenter;
import rps.com.evgeny.rps.di.BaseView;

/**
 * Created by Evgeny on 11.07.2018.
 */

public interface CallingContract {
    interface View extends BaseView<CallingContract.Presenter> {
        void showIncomingState();
        void showTalkingState();
        void showToast(int text);
        void showTime(int time);
        void showPassCarProgress(boolean show);
        void lockScreenWhileTalking(boolean flag);
        void setTitle(String title);
        CallingActivity getCallingActivity();
    }

    interface Presenter extends BasePresenter<CallingContract.View> {
        void takeView(CallingContract.View view);
        void dropView();

        void setDeviceID(String id);
        void answerCall();
        void endCall();
        void cancelCall();
        void passSingleCar();
        void mute();
        void sound();
    }
}
