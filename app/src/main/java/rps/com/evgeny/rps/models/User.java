package rps.com.evgeny.rps.models;

import java.util.UUID;

/**
 * Created by Evgeny on 30.08.2017.
 */

public class User {
    public UUID Id;
    public String LoginName;
    public String Password;
    public String Email;
    public UUID UserId;
    public UUID RoleId;
    public UUID ObjectId;
    public String RoleName;
    public String UserName;
    public String ObjectName;
    public String SessionId;
    public String token;
    public String CardKey;
    public String SectorNumber;
}
