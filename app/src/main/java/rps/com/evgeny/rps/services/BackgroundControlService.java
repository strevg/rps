package rps.com.evgeny.rps.services;

import android.app.KeyguardManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;
import com.crashlytics.android.Crashlytics;
import rps.com.evgeny.rps.activities.CallingActivity;
import rps.com.evgeny.rps.models.network.WebSocketModel;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.Settings;

public class BackgroundControlService extends Service implements RpsWebSocket.WebSocketMessageListener{
    PushNotificationManager pushNotificationManager;
    ControlManager controlManager;

    public BackgroundControlService(){}

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        Settings.init(getApplicationContext());
        Settings.initSession(getApplicationContext());

        pushNotificationManager = PushNotificationManager.getInstance(this);

        controlManager = ControlManager.getInstance();
        controlManager.setListener(this);
        controlManager.restartSocket();

        super.onCreate();
        Log.i(Consts.TAG, "Service on Create");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(Consts.TAG, "Service onDestroy");

        restartService();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(Consts.TAG, "Service onStartCommand");
        return START_STICKY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.i(Consts.TAG, "Service onTaskRemoved");
        restartService();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Log.i(Consts.TAG, "Service onLowMemory");
        restartService();
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Log.i(Consts.TAG, "Service onTrimMemory");
        restartService();
    }

    void restartService() {
        Intent broadcastIntent = new Intent("RestartService");
        broadcastIntent.setClass(this, RestartService.class);
        sendBroadcast(broadcastIntent);
    }

    @Override
    public void onMessage(WebSocketModel model) {
        Log.i(Consts.TAG, "Service onMessage");
        if (model!=null && !Settings.isRunningActivity) {
            handleMessage(model);
        }
    }

    void handleMessage(WebSocketModel model) {
        if (model.getMessageType()== Consts.MessageType.ALARM && model.getAlarm().End!=null) {
            handleAlarm(model);
        } else if (model.getMessageType()==Consts.MessageType.CALLING) {
            handleCalling(model);
        }
    }

    void handleAlarm(WebSocketModel model) {
        String title = model.getAlarm().Name;
        String text = model.getAlarm().Value;
        int code = model.getAlarm().AlarmColorId;
        int number = model.getAlarm().TypeId;
        pushNotificationManager.showNotification(title, text, code, number);
    }

    void handleCalling(WebSocketModel model) {
        Intent intent = new Intent(this, CallingActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.putExtra(Consts.PREF_DEVICE_ID, model.getId());
        startActivity(intent);

        try {
            wakeUpScreen();
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }

    void wakeUpScreen() {
        PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock((PowerManager.SCREEN_BRIGHT_WAKE_LOCK |
                PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), "CallingActivity");
        if ((wakeLock != null) && (wakeLock.isHeld() == false)) {
            wakeLock.acquire();
        }

        KeyguardManager keyguardManager = (KeyguardManager) getApplicationContext().getSystemService(Context.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock keyguardLock = keyguardManager.newKeyguardLock("CallingActivity");
        keyguardLock.disableKeyguard();
    }
}
