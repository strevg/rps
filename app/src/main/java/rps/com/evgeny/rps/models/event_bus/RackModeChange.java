package rps.com.evgeny.rps.models.event_bus;

import java.security.PublicKey;

import rps.com.evgeny.rps.models.network.RackWorkingModeModel;

/**
 * Created by Evgeny on 22.07.2018.
 */

public class RackModeChange {
    public RackWorkingModeModel model;
    public String id;

    public RackModeChange(String id, RackWorkingModeModel model) {
        this.id = id;
        this.model = model;
    }
}
