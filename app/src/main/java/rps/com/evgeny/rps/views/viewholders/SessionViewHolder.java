package rps.com.evgeny.rps.views.viewholders;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.squareup.picasso.Picasso;

import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.databinding.SessionViewHolderBinding;
import rps.com.evgeny.rps.models.person.SessionModel;

/**
 * Created by Evgeny on 23.09.2017.
 */

public class SessionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    SessionViewHolderBinding binding;
    Context context;
    boolean showFullInfo = false;

    public SessionViewHolder(View itemView) {
        super(itemView);
        binding = DataBindingUtil.bind(itemView);
        binding.collapseImage.setOnClickListener(this);
        binding.collapseText.setOnClickListener(this);
        context = binding.getRoot().getContext();
    }

    public void setSession(SessionModel session) {
        setImageCarIn("http://i4.leicestermercury.co.uk/incoming/article258873.ece/ALTERNATES/s615b/Untitled.png");
        setImageCarOut("http://i4.leicestermercury.co.uk/incoming/article258873.ece/ALTERNATES/s615b/Untitled.png");
        setTimeComeIn("21.03.2017 в 13:45");
        setTimeComeOut("21.03.2017 в 13:45");
        setCarNumber("Р123АР199");
        setTimeSession("60 минут");
        setTypeClient("Разовый");
        setDevicePay("Касса 2");
        setDatePay("18.02.2017");
        setTimePay("15:18");
        setSumPay("550р.");
    }

    @Override
    public void onClick(View view) {
        showFullInfo=!showFullInfo;
        showFullInfo();
    }

    public void showFullInfo() {
        animateView(binding.fullInfoContainer);
        animateView(binding.carOutContainer);
        animateView(binding.carInContainer);
        binding.sepLine.setVisibility(showFullInfo?View.VISIBLE:View.GONE);
        binding.collapseImage.setImageResource(showFullInfo?R.drawable.ic_arrow_drop_up_white_24dp:R.drawable.ic_arrow_drop_down_white_24dp);
        binding.collapseText.setText(context.getString(showFullInfo?R.string.hide_view:R.string.show_view));
    }

    void animateView(final View view) {
        if (showFullInfo) {
            view.animate()
                    .alpha(1.0f)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                            super.onAnimationStart(animation);
                            view.setVisibility(View.VISIBLE);
                            view.setAlpha(0.0f);
                        }
                    });
        } else {
            view.animate()
                    .alpha(0.0f)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            view.setVisibility(View.GONE);
                        }
                    });
        }
    }

    public void setImageCarIn(String url) {
        Picasso.with(context).load(url).into(binding.carImageIn);
    }

    public void setImageCarOut(String url) {
        Picasso.with(context).load(url).into(binding.carImageOut);
    }

    public void setTimeComeIn(String time) {
        binding.timeComeIn.setText(time);
    }

    public void setTimeComeOut(String time) {
        binding.timeComeOut.setText(time);
    }

    public void setCarNumber(String number) {
        binding.carNumberIn.setText(number);
        binding.carNumberOut.setText(number);
    }

    public void setTimeSession(String time) {
        binding.timeSession.setText(time);
    }

    public void setTypeClient(String client) {
        binding.typeClient.setText(client);
    }

    public void setDevicePay(String device) {
        binding.devicePay.setText(device);
    }

    public void setDatePay(String date) {
        binding.datePay.setText(date);
    }

    public void setTimePay(String time) {
        binding.timePay.setText(time);
    }

    public void setSumPay(String sum) {
        binding.sumPay.setText(sum);
    }
}
