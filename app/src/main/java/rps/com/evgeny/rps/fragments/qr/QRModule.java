package rps.com.evgeny.rps.fragments.qr;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import rps.com.evgeny.rps.di.ActivityScoped;
import rps.com.evgeny.rps.di.FragmentScoped;

/**
 * Created by Evgeny on 07.06.2018.
 */

@Module
public abstract class QRModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract QRFragment qrFragment();

    @ActivityScoped
    @Binds
    abstract QRContract.Presenter qrsPresenter(QRPresenter presenter);
}