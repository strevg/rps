package rps.com.evgeny.rps.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseArray;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.TextView;


import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;

import java.io.IOException;

import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.services.CarPlateManager;
import rps.com.evgeny.rps.utils.Consts;

public class PlateRecognitionActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int requestPermissionID = 101;
    SurfaceView cameraView;
    CameraSource cameraSource;
    TextView numberTextView;
    TextView regionTextView;
    CarPlateManager carPlateManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plate_recognition);

        cameraView = findViewById(R.id.surfaceView);
        numberTextView = findViewById(R.id.numberText);
        regionTextView = findViewById(R.id.regionText);
        numberTextView.setOnClickListener(this);

        setupToolbar();

        startCameraSource();

        carPlateManager = CarPlateManager.getInstance();
    }

    public void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if(ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (!numberTextView.getText().toString().isEmpty()) {
            Intent intent = new Intent();
            intent.putExtra("plate", carPlateManager.getNumberPlate());
            intent.putExtra("region", carPlateManager.getRegionPlate());
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    private void startCameraSource() {
        final TextRecognizer textRecognizer = new TextRecognizer.Builder(getApplicationContext()).build();

        if (!textRecognizer.isOperational()) {
            Log.w(Consts.TAG, "Detector dependencies not loaded yet");
        } else {
            cameraSource = new CameraSource.Builder(getApplicationContext(), textRecognizer)
                    .setFacing(CameraSource.CAMERA_FACING_BACK)
                    .setRequestedPreviewSize(1280, 1024)
                    .setAutoFocusEnabled(true)
                    .setRequestedFps(2.0f)
                    .build();

           cameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
                @Override
                public void surfaceCreated(SurfaceHolder holder) {
                    try {

                        if (ActivityCompat.checkSelfPermission(getApplicationContext(),
                                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                            ActivityCompat.requestPermissions(PlateRecognitionActivity.this,
                                    new String[]{Manifest.permission.CAMERA},
                                    requestPermissionID);
                            return;
                        }
                        cameraSource.start(cameraView.getHolder());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                }

                @Override
                public void surfaceDestroyed(SurfaceHolder holder) {
                    cameraSource.stop();
                }
            });

            textRecognizer.setProcessor(new Detector.Processor<TextBlock>() {
                @Override
                public void release() {
                }

                @Override
                public void receiveDetections(Detector.Detections<TextBlock> detections) {
                    final SparseArray<TextBlock> items = detections.getDetectedItems();
                    if (items.size() != 0 ){

                        regionTextView.post(new Runnable() {
                            @Override
                            public void run() {
                                for(int i=0;i<items.size();i++){
                                    TextBlock item = items.valueAt(i);

                                    if (carPlateManager.isNumberPlace(item.getValue())) {
                                        numberTextView.setText(carPlateManager.getNumberPlate());
                                        regionTextView.setText(carPlateManager.getRegionPlate());
                                    }
                                }
                            }
                        });
                    }
                }
            });
        }
    }
}
