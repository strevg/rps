package rps.com.evgeny.rps.fragments.reading;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import rps.com.evgeny.rps.di.ActivityScoped;
import rps.com.evgeny.rps.di.FragmentScoped;

/**
 * Created by Evgeny on 13.06.2018.
 */
@Module
public abstract class ReadingModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract ReadingFragment readingFragment();

    @ActivityScoped
    @Binds
    abstract ReadingContract.Presenter readingPresenter(ReadingPresenter presenter);
}
