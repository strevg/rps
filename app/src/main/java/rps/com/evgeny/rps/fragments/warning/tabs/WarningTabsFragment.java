package rps.com.evgeny.rps.fragments.warning.tabs;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import dagger.Lazy;
import dagger.android.support.DaggerFragment;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.databinding.FragmentTabsBinding;
import rps.com.evgeny.rps.fragments.warning.completed.WarningCompletedFragment;
import rps.com.evgeny.rps.fragments.warning.active.WarningActiveFragment;
import rps.com.evgeny.rps.fragments.warning.adapters.PagerAdapter;

/**
 * Created by Evgeny on 24.05.2018.
 */

public class WarningTabsFragment extends DaggerFragment implements WarningTabsContract.View {
    FragmentTabsBinding binding;
    PagerAdapter adapter;
    WarningCompletedFragment completedFragment;

    @Inject
    Lazy<WarningActiveFragment> activeFragment;

    @Inject
    public WarningTabsFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_tabs, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initTabs();
    }

    void initTabs() {
        completedFragment = new WarningCompletedFragment();

        adapter = new PagerAdapter(getChildFragmentManager());
        adapter.onAdd(activeFragment.get(), getResources().getString(R.string.title_tab_active));
        adapter.onAdd(completedFragment, getResources().getString(R.string.title_tab_completed));
        binding.pager.setAdapter(adapter);
        binding.tabLayout.setupWithViewPager(binding.pager);
        binding.tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
    }
}
