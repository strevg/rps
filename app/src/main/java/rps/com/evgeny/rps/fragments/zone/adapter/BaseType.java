package rps.com.evgeny.rps.fragments.zone.adapter;

/**
 * Created by Evgeny on 22.07.2018.
 */

public interface BaseType {
    int getType();
    String getId();
}
