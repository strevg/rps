package rps.com.evgeny.rps.fragments.warning.tabs;

import rps.com.evgeny.rps.di.BasePresenter;
import rps.com.evgeny.rps.di.BaseView;
import rps.com.evgeny.rps.fragments.warning.active.WarningActiveContract;

/**
 * Created by Evgeny on 25.05.2018.
 */

public interface WarningTabsContract {
    interface View extends BaseView<WarningTabsContract.Presenter> {

    }

    interface Presenter extends BasePresenter<WarningTabsContract.View> {
        void takeView(WarningTabsContract.View view);

        void dropView();
    }
}
