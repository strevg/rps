package rps.com.evgeny.rps.fragments.calling;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import rps.com.evgeny.rps.di.ActivityScoped;
import rps.com.evgeny.rps.di.FragmentScoped;

/**
 * Created by Evgeny on 11.07.2018.
 */

@Module
public abstract class CallingModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract CallingFragment callingFragment();

    @ActivityScoped
    @Binds
    abstract CallingContract.Presenter callingPresenter(CallingPresenter presenter);
}
