package rps.com.evgeny.rps.views;

import android.view.View;

import java.util.List;

import rps.com.evgeny.rps.models.BarCodeInUse;

/**
 * Created by Evgeny on 22.09.2017.
 */

public interface AdapterInterface<T> {
    void onAdd(T item);
    void onAddAll(List<T> items);
    void onRemove(int pos);
    void onRemoveAll();
}
