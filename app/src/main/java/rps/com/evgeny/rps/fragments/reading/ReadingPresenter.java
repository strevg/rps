package rps.com.evgeny.rps.fragments.reading;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.nfc.NfcAdapter;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.widget.Toast;

import javax.inject.Inject;

import rps.com.evgeny.rps.MainApplication;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.models.ActivityItem;
import rps.com.evgeny.rps.nfc.CardReader;
import rps.com.evgeny.rps.nfc.CardReaderEventArgs;
import rps.com.evgeny.rps.nfc.CardReaderResponse;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.Settings;
import rps.com.evgeny.rps.utils.StringUtils;

/**
 * Created by Evgeny on 13.06.2018.
 */

public class ReadingPresenter implements ReadingContract.Presenter, CardReader.CardHandler {
    public int READER_FLAGS = NfcAdapter.FLAG_READER_NFC_B | NfcAdapter.FLAG_READER_NFC_F
            | NfcAdapter.FLAG_READER_NFC_A | NfcAdapter.FLAG_READER_NO_PLATFORM_SOUNDS;

    private Activity activity;
    public CardReaderResponse card;
    public CardReader cardReader;
    private Object thisLock = new Object();
    private NfcAdapter nfcAdapter;

    @Nullable
    ReadingContract.View view;
    @Inject
    Application app;

    @Inject
    ReadingPresenter() {
        cardReader = new CardReader(this, thisLock);
        Settings.cardReader = cardReader;
    }

    @Override
    public void takeView(ReadingContract.View view) {
        this.view = view;
        this.cardReader.setActivity(view.getActivity());
        this.activity = view.getActivity();

        if (activity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_NFC)) {
            enableReaderMode();
        }
    }

    @Override
    public void dropView() {
        disableReaderMode();
        this.view = null;
        this.activity = null;
    }

    @Override
    public void CardExist(Object sender, CardReaderEventArgs e) {}

    @Override
    public void CardRead(Object sender, CardReaderEventArgs e) {
        final CardReaderResponse card = e.card;

        view.showPersonFragment();
        Settings.card = card;
        cardReader.ShowPopup("Карта прочитана!");
    }

    @Override
    public void CardRemove(Object sender, CardReaderEventArgs e) {
        view.showReadingFragment();
        cardReader.ShowPopup("Карта извлечена!");
        ((MainApplication)app).dismissProgressDialog();
    }

    public NfcAdapter getNfcAdapter() {
        if (nfcAdapter == null) {
            nfcAdapter = NfcAdapter.getDefaultAdapter(activity);
        }
        return nfcAdapter;
    }

    private void enableReaderMode() {
        if (!getNfcAdapter().isEnabled()) {
            Toast.makeText(activity, "Please activate NFC and press Back to return to the application!", Toast.LENGTH_SHORT).show();
            activity.startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
        } else {
            if (getNfcAdapter() != null) {
                getNfcAdapter().enableReaderMode(activity, cardReader, READER_FLAGS, null);
            }
        }
    }

    private void disableReaderMode() {
        if (getNfcAdapter() != null) {
            getNfcAdapter().disableReaderMode(activity);
        }
    }
}
