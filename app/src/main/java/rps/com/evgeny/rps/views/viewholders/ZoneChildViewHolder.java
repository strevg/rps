package rps.com.evgeny.rps.views.viewholders;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.view.View;

import rps.com.evgeny.rps.databinding.ViewHolderZoneChildBinding;
import rps.com.evgeny.rps.models.zones_devices.ZoneChildModel;
import rps.com.evgeny.rps.utils.ViewUtils;
import rps.com.evgeny.rps.views.adapters.expandable.ChildViewHolder;

/**
 * Created by Evgeny on 30.05.2018.
 */

public class ZoneChildViewHolder extends ChildViewHolder {
    ViewHolderZoneChildBinding binding;
    Context context;

    public ZoneChildViewHolder(View view) {
        super(view);
        context = view.getContext();
        binding = DataBindingUtil.bind(view);
    }

    public void setData(ZoneChildModel model) {
        setTitle(model.getTitle());
        setText(model.getText());
        setColor(context.getResources().getColor(model.getColor()));
    }

    public void setColor(int color) {
        binding.colorImage.setImageDrawable(new ColorDrawable(color));
    }

    public void setTitle(String title) {
        binding.title.setText(title);
    }

    public void setText(String text) {
        binding.text.setVisibility(text==null?View.GONE:View.VISIBLE);
        binding.text.setText(text);
    }

    public void setOnClickMoreListener(View.OnClickListener listener) {
        binding.more.setOnClickListener(listener);
    }

    public void setOnClickListener(View.OnClickListener listener) {
        binding.getRoot().setOnClickListener(listener);
    }
}
