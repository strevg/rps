package rps.com.evgeny.rps.models.list;

import java.util.UUID;

/**
 * Created by Evgeny on 08.10.2017.
 */

public class ZoneModel {
    public UUID Id;
    public String Name;
    public Integer Capacity;
    public Integer Reserved;
    public String Sync;
    public Integer _IsDeleted;
    public Integer IdFC;
    public Integer FreeSpace;
    public Integer OccupId;
    public Integer index;

    @Override
    public String toString() {
        return Name;
    }
}
