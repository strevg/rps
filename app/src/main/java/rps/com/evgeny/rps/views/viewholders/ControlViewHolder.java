package rps.com.evgeny.rps.views.viewholders;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.databinding.ViewHolderControlBinding;

/**
 * Created by Evgeny on 07.06.2018.
 */

public class ControlViewHolder extends RelativeLayout {
    private ViewHolderControlBinding binding;
    private Context context;
    private boolean progressByClick = false;

    public ControlViewHolder(Context context) {
        super(context);
        initViews(context);
    }

    public ControlViewHolder(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews(context);
        initAttrs(attrs);
    }

    public ControlViewHolder(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews(context);
        initAttrs(attrs);
    }

    void initViews(Context context) {
        this.context = context;
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        binding = ViewHolderControlBinding.inflate(inflater, this, true);
        setBackColor(getResources().getColor(R.color.colorControlBackground));
    }

    void initAttrs(AttributeSet attributeSet) {
        TypedArray array = getContext().getTheme().obtainStyledAttributes(attributeSet,
                R.styleable.ControlViewHolder, 0, 0);
        try {
            setIcon(array.getResourceId(R.styleable.ControlViewHolder_controlImage, -1));
            setTitle(array.getString(R.styleable.ControlViewHolder_controlTitle));
            this.progressByClick = array.getBoolean(R.styleable.ControlViewHolder_controlProgressByClick, false);
        } finally {
            array.recycle();
        }
    }

    public void setTitle(String text) {
        binding.title.setText(text);
        setActionTitle("");
    }

    public void setActionTitle(String text) {
        binding.title2.setText(text);
    }

    public void setIcon(int icon) {
        binding.icon.setImageResource(icon);
    }

    public void setOnClickListener(final OnClickListener listener) {
        binding.button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (progressByClick)
                    showProgressBar(true);
                listener.onClick(ControlViewHolder.this);
            }
        });
    }

    public void setBackColor(int color) {
        binding.background.setCardBackgroundColor(color);
    }

    public void setColor(int color) {
        binding.title.setTextColor(color);
        binding.title2.setTextColor(color);
        binding.icon.setColorFilter(color);
    }

    public void hideTitle(boolean flag) {
        binding.title2.setVisibility(flag?GONE:VISIBLE);
    }

    public void showProgressBar(boolean flag) {
        binding.progressBar.setVisibility(!flag?GONE:VISIBLE);
    }
}
