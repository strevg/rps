package rps.com.evgeny.rps.models.invitations;

import java.util.ArrayList;

/**
 * Created by Evgeny on 02.10.2017.
 */

public class StatisticsModel {
    public int count_all;
    public int count_guest;
    public int count_employee;
    public int count_guest_exit;
    public int count_employee_exit;
    public int employee_on_parking_now;
    public String guest_times;
    public String employee_times;
    public int amount;
    public ArrayList<UserTransaction> statistics;
    public ArrayList<UserTransaction> guests_online;
    public ArrayList<UserTransaction> employees_online;
    public ArrayList<String> clients;
    public ArrayList<String> types;
    String dates;

    public class UserTransaction {
        String dateOrder;
        String date;
        String FullName;
        String type;
        String time;
        String debts;
    }
}
