package rps.com.evgeny.rps.fragments.zone.details;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import rps.com.evgeny.rps.di.ActivityScoped;
import rps.com.evgeny.rps.di.FragmentScoped;

/**
 * Created by Evgeny on 07.06.2018.
 */
@Module
public abstract class ControlModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract ControlFragment controlFragment();

    @ActivityScoped
    @Binds
    abstract ControlContract.Presenter controlPresenter(ControlPresenter presenter);
}
