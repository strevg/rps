package rps.com.evgeny.rps.fragments;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.lang.ref.WeakReference;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rps.com.evgeny.rps.MainApplication;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.databinding.MainFragmentBinding;
import rps.com.evgeny.rps.models.BarCodeInUse;
import rps.com.evgeny.rps.models.BarCodeInUseResponse;
import rps.com.evgeny.rps.models.CardModel;
import rps.com.evgeny.rps.network.ApiFactory;
import rps.com.evgeny.rps.network.RPSApi;
import rps.com.evgeny.rps.nfc.CardData;
import rps.com.evgeny.rps.nfc.CardReader;
import rps.com.evgeny.rps.nfc.CardReaderHelper;
import rps.com.evgeny.rps.nfc.CardReaderResponse;
import rps.com.evgeny.rps.nfc.CardWriter;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.Settings;

/**
 * Created by Evgeny on 31.08.2017.
 */

public class CardWriteFragment extends Fragment implements CardWriter.CardCallback {
    public BarCodeInUse barCodeModel;
    public CardReaderResponse cardPrev;
    MainFragmentBinding binding;
    MainApplication app;
    RPSApi rpsApi;

    public int READER_FLAGS = NfcAdapter.FLAG_READER_NFC_A | NfcAdapter.FLAG_READER_SKIP_NDEF_CHECK;
    public CardWriter mCardReader;
    private TextView mAccountField;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (MainApplication)getActivity().getApplication();
        rpsApi = ApiFactory.getRpsApi(Settings.siteName+"/");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.main_fragment, container, false);
        View v = binding.getRoot();

        binding.cardAccountField.setText(R.string.read_card);
        mCardReader = new CardWriter(new WeakReference<CardWriter.CardCallback>(this), app);
        // Disable Android Beam and register our card reader callback
        enableReaderMode();

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        app.getMainActivity().setTitle(getString(R.string.title_sales));
        app.getMainActivity().setSubtitle(null);
        enableReaderMode();
    }

    @Override
    public void onPause() {
        super.onPause();
        disableReaderMode();
    }

    private void enableReaderMode() {
        Activity activity = this.getActivity();
        NfcAdapter nfc = NfcAdapter.getDefaultAdapter(activity);
        if (nfc != null) {
            nfc.enableReaderMode(activity, mCardReader, READER_FLAGS , null);
        }
    }

    private void disableReaderMode() {
        try {
            Activity activity = this.getActivity();
            NfcAdapter nfc = NfcAdapter.getDefaultAdapter(activity);
            if (nfc != null) {
                nfc.disableReaderMode(activity);
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }

    @Override
    public void onCardReceived(CardReaderResponse card) {
        if (card.CardId != cardPrev.CardId) {
            app.getMainActivity().replaceFragment(Consts.FragmentType.CARD_READER, null);

            Toast.makeText(getActivity(), "Другая карта!", Toast.LENGTH_SHORT).show();
            app.dismissProgressDialog();
        } else {
            Map<String, String> updateObj = ApiFactory.getUpdateObject(card, barCodeModel);

            int Sum = Integer.parseInt(card.SumOnCard) + barCodeModel.DiscountAmount;
            card.SumOnCard = String.valueOf(Sum);

            CardData data = CardReaderHelper.GetParam(card);
            String strData = CardReader.ByteArrayToHexString(CardReaderHelper.dataToWBlock(data));

            try {
                mCardReader.WriteData(strData);
                updateCard(card.CardId, updateObj);

                barCodeModel.Sum = Sum;
                barCodeModel.ParkingEnterTime = card.ParkingEnterTime;
                barCodeModel.CardId = card.CardId;

                createNew(barCodeModel);

                app.getMainActivity().replaceFragment(Consts.FragmentType.READ_DATA, card);
                Toast.makeText(getActivity(), app.getString(R.string.write_success), Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                app.getMainActivity().replaceFragment(Consts.FragmentType.CARD_READER, null);
                Toast.makeText(getActivity(), app.getString(R.string.write_error), Toast.LENGTH_SHORT).show();
            } finally {
                app.dismissProgressDialog();
            }
        }
    }

    // ------------ Network ---------------
    void updateCard(String cardId, Map<String, String> updateObj) {
        rpsApi.updateCard(cardId, updateObj, ApiFactory.getHeaders()).enqueue(new Callback<CardModel>() {
            @Override
            public void onResponse(Call<CardModel> call, Response<CardModel> response) {
                if (!response.isSuccessful()) {
                    Crashlytics.logException(new Exception("updateCard !response.isSuccessful()"));
                    app.getMainActivity().replaceFragment(Consts.FragmentType.CARD_READER, null);

                    Toast.makeText(app.getMainActivity(), "Ошибка обновления карточки на сервере", Toast.LENGTH_SHORT).show();
                    return;
                }
                Log.i(Consts.TAG, "onResponse" + response.body());
            }
            @Override
            public void onFailure(Call<CardModel> call, Throwable t) {
                Log.e(Consts.TAG, t.getMessage());
                Crashlytics.logException(t);
            }
        });
    }

    void createNew(BarCodeInUse barCodeModel) {
        rpsApi.createNew(barCodeModel, ApiFactory.getHeaders()).enqueue(new Callback<BarCodeInUseResponse>() {
            @Override
            public void onResponse(Call<BarCodeInUseResponse> call, Response<BarCodeInUseResponse> response) {
                if (!response.isSuccessful()) {
                    Crashlytics.logException(new Exception("createNew !response.isSuccessful()"));
                    app.getMainActivity().replaceFragment(Consts.FragmentType.CARD_READER, null);

                    Toast.makeText(app.getMainActivity(), "Ошибка создания карточки на сервере", Toast.LENGTH_SHORT).show();
                    return;
                }
                Log.i(Consts.TAG, "onResponse" + response.body());
            }
            @Override
            public void onFailure(Call<BarCodeInUseResponse> call, Throwable t) {
                Log.e(Consts.TAG, t.getMessage());
                Crashlytics.logException(t);
            }
        });
    }

    public void setBarModel(BarCodeInUse bm) {
        cardPrev = Settings.card;
        barCodeModel = bm;
    }
}
