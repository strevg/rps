package rps.com.evgeny.rps.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.crashlytics.android.Crashlytics;

import java.util.List;

import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.models.network.AlarmZoneModel;

/**
 * Created by Evgeny on 09.06.2018.
 */

public class ViewUtils {
    public static int convertDpToPx(int dp){
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return Math.round(px);
    }

    public static int convertPxToDp(int px){
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return Math.round(dp);
    }

    public static int convertSpToPixels(float sp, Context context) {
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp,
                context.getResources().getDisplayMetrics());
        return px;
    }

    public static int getColorByIndex(int index) {
        if (index == 1)
            return R.color.colorRed;
        else if (index == 2)
            return R.color.colorOrange;
        else if (index == 3)
            return R.color.colorYellow;
        else if (index == 4)
            return R.color.colorGrey;
        return R.color.colorGreen;
    }

    public static int getDrawableByIndex(int index) {
        if (index == 1)
            return R.drawable.red_circle;
        else if (index == 2)
            return R.drawable.orange_circle;
        else if (index == 3)
            return R.drawable.yellow_cirlce;
        return R.drawable.grey_circle;
    }

    public static int getMaxColor(List<AlarmZoneModel> alarms) {
        int maxIndex = 5;
        for (int i=0; i<alarms.size(); i++) {
            if (maxIndex > alarms.get(i).AlarmColorId && alarms.get(i).End==null) {
                maxIndex = alarms.get(i).AlarmColorId;
            }
        }
        return getColorByIndex(maxIndex);
    }

    public static void hideKeyboardFrom(Context context, View view) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }
}
