package rps.com.evgeny.rps.fragments.warning.completed;

import android.support.v7.widget.RecyclerView;

import rps.com.evgeny.rps.di.BasePresenter;
import rps.com.evgeny.rps.di.BaseView;

/**
 * Created by Evgeny on 25.05.2018.
 */

public interface WarningCompletedContract {
    interface View extends BaseView<Presenter> {
        void setAdapter(RecyclerView.Adapter adapter);
    }

    interface Presenter extends BasePresenter<View> {
        void takeView(WarningCompletedContract.View view);

        void dropView();
    }
}
