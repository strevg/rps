package rps.com.evgeny.rps.models.network;

import java.util.List;
import java.util.UUID;

/**
 * Created by Evgeny on 08.07.2018.
 */

public class ZoneTableModel {
    public UUID Id;
    public String Name;
    public int Capacity;
    public int Reserved;
    public String Sync;
    public int _IsDeleted;
    public int IdFC;
    public int FreeSpace;
    public int OccupId;
    public UUID id;
    public boolean editable;
    public boolean editable_fields;
    public List<DeviceZoneModel> DeviceModel;
    public String href;
    public String image_url;
}
