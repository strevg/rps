package rps.com.evgeny.rps.fragments.person;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import fr.ganfra.materialspinner.MaterialSpinner;
import rps.com.evgeny.rps.MainApplication;
import rps.com.evgeny.rps.models.person.CardFullModel;
import rps.com.evgeny.rps.network.ApiFactory;
import rps.com.evgeny.rps.network.RPSApi;
import rps.com.evgeny.rps.nfc.CardReaderResponse;
import rps.com.evgeny.rps.utils.ListsUtils;
import rps.com.evgeny.rps.utils.RightUtils;
import rps.com.evgeny.rps.utils.Settings;
import rps.com.evgeny.rps.utils.TimeUtils;

/**
 * Created by Evgeny on 12.10.2017.
 */

public class InformationPresenter implements ListsUtils.onDownloadLists, View.OnClickListener,
        DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener,
        AdapterView.OnItemSelectedListener {
    InformationFragment fragment;
    InformationModel model;
    CardReaderResponse cardFullModel;

    ListsUtils listsUtils;
    public MainApplication app;
    public RPSApi rpsApi;

    String selectedDate, selectedTime;
    int currentViewId;
    boolean initData;

    public InformationPresenter(InformationFragment context) {
        fragment = context;
        fragment.setOnClickListener(this);

        app = (MainApplication) context.getActivity().getApplication();
        rpsApi = ApiFactory.getRpsApi(Settings.siteName+"/");

        model = new InformationModel(this);

        listsUtils = ListsUtils.getInstance(app);
        listsUtils.onAddListener(this);
        if (listsUtils.hasData()) {
            setDataSpinners();
        }
    }

    public void setCardFullModel(CardReaderResponse newCard) {
        if (TimeUtils.isNewerTime(newCard.DateSaveCard, Settings.card.DateSaveCard)) {
            Settings.card = newCard;
        } else {
            this.cardFullModel = Settings.card;
            //Если данные оказались старее, чем на карте, то обновляем данные на сервере
            model.updateFullCard(Settings.card);
        }
        this.cardFullModel = Settings.card;
        fragment.setData(Settings.card);
        if (listsUtils!=null) {
            setDataSpinners();
        }
    }

    public void setBalance(String value) {
        String balance = value+"Р.";
        fragment.setBalance(balance);
    }

    @Override
    public void onCompleted(int type) {
        if (cardFullModel==null)
            return;
        switch (type) {
            case 0: // group
                setDataSpinners(fragment.binding().groupSpinner, listsUtils.getGroups(), cardFullModel.ClientGroupidFC, type);
                break;
            case 1: // zone
                setDataSpinners(fragment.binding().zoneSpinner, listsUtils.getZones(), cardFullModel.ZoneidFC, type);
                break;
            case 2: // tariff
                setDataSpinners(fragment.binding().tariffSpinner, listsUtils.getTariffs(), cardFullModel.TPidFC, type);
                break;
            case 3: // tariff schedule
                setDataSpinners(fragment.binding().tariffScheduleSpinner, listsUtils.getTariffSchedules(), cardFullModel.TSidFC, type);
                break;
            case 4: // client
                setDataSpinners(fragment.binding().clientSpinner, listsUtils.getClients(), cardFullModel.ClientTypidFC, type);
                break;
        }
    }

    void setDataSpinners() {
        fragment.initAdapter(fragment.binding().groupSpinner, listsUtils.getGroups());
        fragment.initAdapter(fragment.binding().tariffSpinner, listsUtils.getTariffs());
        fragment.initAdapter(fragment.binding().zoneSpinner, listsUtils.getZones());
        fragment.initAdapter(fragment.binding().tariffScheduleSpinner, listsUtils.getTariffSchedules());
        fragment.initAdapter(fragment.binding().clientSpinner, listsUtils.getClients());

        if (cardFullModel!=null) {
            initData = true;
            setSelectedItem(fragment.binding().groupSpinner, 0, cardFullModel.ClientGroupidFC);
            setSelectedItem(fragment.binding().zoneSpinner, 1, cardFullModel.ZoneidFC);
            setSelectedItem(fragment.binding().tariffSpinner, 2, cardFullModel.TPidFC);
            setSelectedItem(fragment.binding().tariffScheduleSpinner, 3, cardFullModel.TSidFC);
            setSelectedItem(fragment.binding().clientSpinner, 4, cardFullModel.ClientTypidFC);

            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    initData = false;
                }
            }, 1000);
        }
    }

    void setSelectedItem(MaterialSpinner spinner, int type, String field) {
        try {
            int pos = listsUtils.getPosition(type, Integer.valueOf(field));
            fragment.setSelectedItem(spinner, pos);
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }

    void setDataSpinners(MaterialSpinner spinner, List data, String field, int type) {
        initData = true;
        fragment.initAdapter(spinner, data);
        if (cardFullModel!=null) {
            try {
                if (field!=null && !field.equals("")) {
                    int pos = listsUtils.getPosition(type, Integer.valueOf(field));
                    fragment.setSelectedItem(spinner, pos);
                }
            } catch (Exception e) {
                Crashlytics.logException(e);
            }
        }
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                initData = false;
            }
        }, 1000);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (!initData) {
            if (RightUtils.hasRights(adapterView.getId())) {
                model.changeSpinnerData(adapterView.getId(), i);
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {}

    @Override
    public void onClick(View view) {
        if (RightUtils.hasRights(view.getId())) {
            showDatePiker(view.getId());
        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        month++;
        String monthS = (month<10?"0":"") + month;
        String dayS = (day<10?"0":"") + day;
        selectedDate = year + "-" + monthS + "-" + dayS;
        showTimePicker();
    }

    @Override
    public void onTimeSet(TimePicker timePicker, int hour, int minute) {
        String hourS = (hour<10?"0":"") + hour;
        String minuteS = (minute<10?"0":"") + minute;
        selectedTime = hourS + ":" + minuteS + ":" + "00";

        setSelectDateTime();
    }

    void setSelectDateTime() {
        EditText editText = (EditText) fragment.binding().getRoot().findViewById(currentViewId);
        editText.setText(selectedDate + " в " + selectedTime);

        changeData(currentViewId, selectedDate+" "+selectedTime);
    }

    public void changeData(int id, String value) {
        model.changeData(id, value);
    }

    void showDatePiker(int id) {
        currentViewId = id;
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog =
                new DatePickerDialog(fragment.getContext(), this, calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    void showTimePicker() {
        Calendar calendar = Calendar.getInstance();
        TimePickerDialog timePickerDialog = new TimePickerDialog(fragment.getContext(), this, calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE), true);
        timePickerDialog.show();
    }

}
