package rps.com.evgeny.rps.fragments.alarms;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import rps.com.evgeny.rps.di.BasePresenter;
import rps.com.evgeny.rps.di.BaseView;

public interface AlarmsContract {
    interface View extends BaseView<AlarmsContract.Presenter> {
        void setActiveAdapter(RecyclerView.Adapter adapter);
        void setInactiveAdapter(RecyclerView.Adapter adapter);
        void setBottomAdapter(RecyclerView.Adapter adapter);
        void scrollToPosition(int pos);
        void showProgressBar(boolean show);
        void shadowCallingScreen(boolean show);
        void setNumberAlarms(int number);
        Context getContext();
    }

    interface Presenter extends BasePresenter<AlarmsContract.View> {
        void takeView(AlarmsContract.View view);
        void dropView();
        void setPhoneListener();
    }
}
