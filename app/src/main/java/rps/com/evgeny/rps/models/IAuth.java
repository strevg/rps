package rps.com.evgeny.rps.models;

/**
 * Created by Evgeny on 31.08.2017.
 */

public interface IAuth {
    void CreateStore();
    Iterable<SecurityUser> findAccountsForService(String serviceId);
    void save(String token, User user, String domain, String serviceId);
    void delete(String serviceId);
}