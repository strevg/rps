package rps.com.evgeny.rps.views.viewholders;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import rps.com.evgeny.rps.databinding.ViewHolderQrBinding;
import rps.com.evgeny.rps.databinding.ViewholderWarningBinding;
import rps.com.evgeny.rps.models.invitations.InvitationModel;

/**
 * Created by Evgeny on 07.06.2018.
 */

public class QRViewHolder extends RecyclerView.ViewHolder {
    public ViewHolderQrBinding binding;

    public QRViewHolder(View view) {
        super(view);
        binding = DataBindingUtil.bind(view);
    }

    public void setData(InvitationModel model) {
        setName(model.GuestName);
        setCompany(model.CompanyName);
        setCar(model.GuestPlateNumber, model.GuestCarModel);
    }

    public void setName(String name) {
        binding.name.setText(name);
    }

    public void setCompany(String company) {
        binding.nameCompany.setText(company);
    }

    public void setCar(String number, String model) {
        binding.carNumber.setText(number);
        binding.carType.setText(model);
    }

    public void setOnClickListener(View.OnClickListener listener) {
        binding.sendButton.setOnClickListener(listener);
    }
}
