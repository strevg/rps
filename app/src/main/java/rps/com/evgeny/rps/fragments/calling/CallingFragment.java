package rps.com.evgeny.rps.fragments.calling;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.activities.CallingActivity;
import rps.com.evgeny.rps.databinding.FragmentCallingBinding;
import rps.com.evgeny.rps.fragments.qr.QRFragment;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.StringUtils;

/**
 * Created by Evgeny on 11.07.2018.
 */

public class CallingFragment extends DaggerFragment implements CallingContract.View,
        View.OnClickListener {
    FragmentCallingBinding binding;

    @Inject
    CallingContract.Presenter presenter;

    @Inject
    public CallingFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_calling, container, false);
        initViews();
        initData();
        return binding.getRoot();
    }

    void initViews() {
        binding.buttonAccept.setOnClickListener(this);
        binding.buttonCancel.setOnClickListener(this);
        binding.buttonFinish.setOnClickListener(this);
        binding.buttonCar.setOnClickListener(this);
        binding.buttonMute.setOnClickListener(this);
        binding.buttonQR.setOnClickListener(this);
        binding.buttonSound.setOnClickListener(this);
    }

    void initData() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            String value = bundle.getString(Consts.PREF_DEVICE_ID, null);
            presenter.setDeviceID(value);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.takeView(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.dropView();
    }

    @Override
    public CallingActivity getCallingActivity() {
        return (CallingActivity) getActivity();
    }

    @Override
    public void showPassCarProgress(final boolean show) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                binding.passCarProgress.setVisibility(show?View.VISIBLE:View.GONE);
            }
        });
    }

    @Override
    public void showIncomingState() {
        binding.container1.setVisibility(View.VISIBLE);
        binding.container2.setVisibility(View.GONE);
        binding.container3.setVisibility(View.GONE);
    }

    @Override
    public void showTalkingState() {
        binding.container1.setVisibility(View.GONE);
        binding.container2.setVisibility(View.VISIBLE);
        binding.container3.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonAccept:
                presenter.answerCall();
                break;
            case R.id.buttonCancel:
                presenter.cancelCall();
                break;
            case R.id.buttonFinish:
                presenter.endCall();
                break;
            case R.id.buttonCar:
                presenter.passSingleCar();
                break;
            case R.id.buttonMute:
                presenter.mute();
                break;
            case R.id.buttonQR:
                openQRInvitations();
                break;
            case R.id.buttonSound:
                presenter.sound();
                break;
        }
    }

    public void showToast(final int text) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getActivity(), text, Toast.LENGTH_LONG).show();
            }
        });
    }

    public void lockScreenWhileTalking(final boolean flag) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                binding.showContainer.setVisibility(flag?View.VISIBLE:View.GONE);
            }
        });
    }

    @Override
    public void showTime(final int time) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    String text = getString(R.string.title_call_connected, StringUtils.getTimeCallConnected(time));
                    binding.subTitle.setText(text);
                } catch (Exception e) {}
            }
        });
    }

    @Override
    public void setTitle(String title) {
        binding.title.setText(title);
    }

    public void openQRInvitations() {
        FragmentManager manager = getChildFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.fragmentContainer, new QRFragment(), "Calling");
        transaction.addToBackStack("Calling");
        transaction.commit();
    }
}
