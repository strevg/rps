package rps.com.evgeny.rps.views.viewholders;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import rps.com.evgeny.rps.databinding.BarcodeViewHolderBinding;
/**
 * Created by Evgeny on 23.09.2017.
 */

public class BarcodeViewHolder extends RecyclerView.ViewHolder {
    public BarcodeViewHolderBinding binding;

    public BarcodeViewHolder(View view) {
        super(view);
        binding = DataBindingUtil.bind(view);
    }

    public void setOnClickListener(View.OnClickListener listener) {
        binding.selectBarcode.setOnClickListener(listener);
        binding.barcodeCard.setOnClickListener(listener);
    }

    public void setTitle(String title) {
        binding.barcodeName.setText(title);
    }

    public void setDescription(String description) {
        binding.barcodeDescription.setText(description);
    }
}
