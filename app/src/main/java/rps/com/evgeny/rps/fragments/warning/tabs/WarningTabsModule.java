package rps.com.evgeny.rps.fragments.warning.tabs;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import rps.com.evgeny.rps.di.ActivityScoped;
import rps.com.evgeny.rps.di.FragmentScoped;
import rps.com.evgeny.rps.fragments.warning.active.WarningActiveContract;
import rps.com.evgeny.rps.fragments.warning.active.WarningActiveFragment;
import rps.com.evgeny.rps.fragments.warning.active.WarningActivePresenter;

/**
 * Created by Evgeny on 25.05.2018.
 */

@Module
public abstract class WarningTabsModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract WarningTabsFragment warningTabsFragment();

    @ActivityScoped
    @Binds
    abstract WarningTabsContract.Presenter warningActivePresenter(WarningTabsPresenter presenter);
}