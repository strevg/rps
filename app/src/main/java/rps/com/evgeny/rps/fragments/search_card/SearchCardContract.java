package rps.com.evgeny.rps.fragments.search_card;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import rps.com.evgeny.rps.di.BasePresenter;
import rps.com.evgeny.rps.di.BaseView;

/**
 * Created by Evgeny on 15.11.2018.
 */

public interface SearchCardContract {
    interface View extends BaseView<Presenter> {
        void setAdapter(RecyclerView.Adapter adapter);
        void showEmptyList(boolean show);
        Context getContext();
    }

    interface Presenter extends BasePresenter<View> {
        void takeView(SearchCardContract.View view);
        void dropView();
        void onSearchCard(String number);
    }
}
