package rps.com.evgeny.rps.fragments.zone;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.di.ActivityScoped;
import rps.com.evgeny.rps.fragments.zone.adapter.CallingModel;
import rps.com.evgeny.rps.fragments.zone.adapter.EnteringModel;
import rps.com.evgeny.rps.fragments.zone.adapter.ZoneBottomAdapter;
import rps.com.evgeny.rps.models.event_bus.BackFromControlFragment;
import rps.com.evgeny.rps.models.event_bus.RackModeChange;
import rps.com.evgeny.rps.models.network.DeviceZoneModel;
import rps.com.evgeny.rps.models.network.WebSocketModel;
import rps.com.evgeny.rps.models.network.ZoneTableModel;
import rps.com.evgeny.rps.models.zones_devices.ZoneChildModel;
import rps.com.evgeny.rps.models.zones_devices.ZoneParentModel;
import rps.com.evgeny.rps.network.ApiFactory;
import rps.com.evgeny.rps.network.RPSApi;
import rps.com.evgeny.rps.services.ControlManager;
import rps.com.evgeny.rps.services.ProximityManager;
import rps.com.evgeny.rps.services.RpsWebSocket;
import rps.com.evgeny.rps.services.TimeoutManager;
import rps.com.evgeny.rps.services.linphone.LinphoneManager;
import rps.com.evgeny.rps.services.linphone.callback.PhoneListener;
import rps.com.evgeny.rps.services.linphone.linphone.LinphoneUtils;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.Settings;
import rps.com.evgeny.rps.views.DialogHelper;
import rps.com.evgeny.rps.views.adapters.ZoneDeviceAdapter;
import rps.com.evgeny.rps.views.onItemClickListener;

/**
 * Created by Evgeny on 30.05.2018.
 */

@ActivityScoped
public class ZoneDevicesPresenter implements ZoneDevicesContract.Presenter, onItemClickListener<ZoneChildModel>,
            ZoneBottomAdapter.onBottomListener, RpsWebSocket.WebSocketMessageListener,
            ControlManager.ControlManagerCallback, PhoneListener, TimeoutManager.TimeoutCallback,
            ProximityManager.ProximityCallback {
    @Nullable
    ZoneDevicesContract.View view;

    ZoneDeviceAdapter adapter;
    ZoneBottomAdapter bottomAdapter;

    ControlManager controlManager;
    LinphoneManager linphoneManager;
    List<WebSocketModel> modelList;
    ProximityManager proximityManager;
    RPSApi rpsApi;

    @Inject
    ZoneDevicesPresenter() {
    }

    @Override
    public void takeView(ZoneDevicesContract.View view) {
        this.view = view;
        adapter = new ZoneDeviceAdapter(view.getInflater(), new ArrayList<ZoneParentModel>());
        view.setAdapter(adapter);
        adapter.setMoreItemClickListener(moreListener);
        adapter.setItemClickListener(this);

        bottomAdapter = new ZoneBottomAdapter();
        bottomAdapter.setOnBottomListener(this);
        view.setAdapterBottom(bottomAdapter);

        modelList = new ArrayList<>();
        controlManager = ControlManager.getInstance();
        controlManager.setCallBack(this);
        controlManager.setTimeoutCallback(this);

        rpsApi = ApiFactory.getRpsApi(Settings.siteName + "/");

        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
        downloadZoneDevices();

        linphoneManager = LinphoneManager.getInstance(view.getContext());
        proximityManager = ProximityManager.getInstance(view.getContext());
    }

    @Override
    public void setPhoneListener() {
        linphoneManager.setPhoneListener(this);
        controlManager.setListener(this);
        controlManager.setTimeoutCallback(this);
        controlManager.setCallBack(this);
        proximityManager.register(this);
        checkActiveCall();
    }

    @Override
    public void dropView() {
        this.view = null;
        EventBus.getDefault().unregister(this);
        if (controlManager!=null)
            controlManager.removeListener(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void changeRackMode(RackModeChange item) {
        adapter.updateRackMode(item);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void backFromControlFragment(BackFromControlFragment item) {
        setPhoneListener();
    }

    void checkActiveCall() {
        bottomAdapter.onRemoveAll();
        if (existsActiveCall()) {
            String id = linphoneManager.getCalls()[0].toString();
            onCallStart(id);
            onCallConnect(id);
        }
    }

    boolean existsActiveCall() {
        return (rps.com.evgeny.rps.services.linphone.linphone.LinphoneManager.isInstanceiated()
                && linphoneManager.getCalls()!=null && linphoneManager.getCalls().length>0);
    }

    void downloadZoneDevices() {
        view.showProgressBar(true);
        rpsApi.getTableZones(ApiFactory.getHeaders()).enqueue(new Callback<List<ZoneTableModel>>() {
            @Override
            public void onResponse(Call<List<ZoneTableModel>> call, Response<List<ZoneTableModel>> response) {
                if (response.isSuccessful()) {
                    adapter.onAddAll(initData(response.body()));
                    controlManager.setZoneList(response.body());
                }
                view.showProgressBar(false);
                Log.i(Consts.TAG, response.body().toString());
            }

            @Override
            public void onFailure(Call<List<ZoneTableModel>> call, Throwable t) {
                Log.e(Consts.TAG, t.getMessage());
                view.showProgressBar(false);
            }
        });
    }

    void downloadAndMergeZoneDevices() {
        rpsApi.getTableZones(ApiFactory.getHeaders()).enqueue(new Callback<List<ZoneTableModel>>() {
            @Override
            public void onResponse(Call<List<ZoneTableModel>> call, Response<List<ZoneTableModel>> response) {
                if (view!=null) {
                    if (response.isSuccessful()) {
                        final List<ZoneTableModel> parentModels = response.body();
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                adapter.onAddMergeAll(initData(parentModels));
                                controlManager.setZoneList(parentModels);
                            }
                        });
                    }
                    view.showProgressBar(false);
                }

                Log.i(Consts.TAG, response.body().toString());
            }

            @Override
            public void onFailure(Call<List<ZoneTableModel>> call, Throwable t) {
                Log.e(Consts.TAG, t.getMessage());
                view.showProgressBar(false);
            }
        });
    }

    onItemClickListener<ZoneChildModel> moreListener = new onItemClickListener<ZoneChildModel>() {
        @Override
        public void onItemClick(View v, final ZoneChildModel data) {
            PopupMenu popupMenu = new PopupMenu(view.getContext(), v);
            controlManager.setDevice(data.getModel());
            setPhoneListener();
            initTypeMenu(popupMenu);

            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.action_change_type:
                            showReasonChangeDialog();
                            return true;
                        case R.id.action_enter_car:
                            controlManager.passSingleCar();
                            return true;
                        case R.id.action_enter_qr:
                            controlManager.openInvitations();
                            return true;
                        case R.id.action_call_device:
                            controlManager.onCall();
                            return true;
                    }
                    return false;
                }
            });
            popupMenu.show();
        }
    };

    void initTypeMenu(PopupMenu popupMenu) {
        if (controlManager.getDevice().Type==2) {
           popupMenu.inflate(R.menu.zone_menu_kassa);
        } else if (controlManager.getDevice().TransitTypeId==2) {
          popupMenu.inflate(R.menu.zone_menu_exit);
        } else {
            popupMenu.inflate(R.menu.zone_menu);
        }
    }

    @Override
    public void onItemClick(View v, ZoneChildModel item) {
        controlManager.setDevice(item.getModel());
        view.openMoreDetails(item.getModel());
    }

    void showReasonChangeDialog() {
        new DialogHelper(view.getContext()).showReasonChangeModeDialog(new DialogHelper.onReturnListener<String>() {
            @Override
            public void onReturn(String value) {
                if (value!=null) {
                    controlManager.postReasonChangeMode(controlManager.getDevice().Name, controlManager.getDevice().Id.toString(), value);
                }
            }
        });
    }

    void changeType() {
        new DialogHelper(view.getContext()).showChangeRackModeDialog(controlManager.getDevice().RackWorkingModes,
                controlManager.getDevice().RackWorkingModeId,
                new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                        controlManager.changeRackMode(which+1);
                        return true;
                    }
                });
    }

    List<ZoneParentModel> initData(List<ZoneTableModel> data) {
        List<ZoneParentModel> zoneList = new ArrayList<>();
        for (int i=0; i<data.size(); i++) {
            if (data.get(i).DeviceModel!=null && data.get(i).DeviceModel.size()>0) {
                zoneList.add(new ZoneParentModel(data.get(i)));
            }
        }
        return zoneList;
    }

    //----------- control manager ------------
    WebSocketModel getWebSocketModel(String uuid) {
        for (WebSocketModel model : modelList) {
            if (model.getId().equals(uuid))
                return model;
        }
        return null;
    }

    @Override
    public void onSuccessChangeRack(RackModeChange item) {
        try {
            controlManager.getDevice().RackWorkingModeId = item.model.Id;
            controlManager.getDevice().RackWorkingModeName = item.model.Name;
            view.showToast(R.string.success_change_rack_mode);
            EventBus.getDefault().post(item);
        } catch (Exception e) {
            view.showToast(R.string.error_change_rack_mode);
        }
    }

    @Override
    public void onSuccessPostReasonChangeMode() {
        changeType();
    }

    @Override
    public void onRequestFailed(int error) {
        if (view!=null)
            view.showToast(error);
    }

    @Override
    public void onRequestSuccess(int code) {
        if (view!=null) {
        }
    }

    @Override
    public void timeIsOver(int error) {
        if (view!=null)
            view.showToast(error);
    }

    @Override
    public void onMessage(final WebSocketModel model) {
        if (controlManager.getZoneList()==null)
            return;
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                handleSocketModel(model);
            }
        });
    }

    synchronized void handleSocketModel(WebSocketModel model) {
        final WebSocketModel socketModel = getWebSocketModel(model.getId());

        if (model.getMessageType()== Consts.MessageType.ALARM) {
            handleAlarm(model);
        } else if (socketModel == null) {
            if (model.getMessageType()==Consts.MessageType.IN_PROCCESS) {
                modelList.add(new WebSocketModel(Consts.MessageType.IN_PROCCESS, model.getId()));
                DeviceZoneModel device = controlManager.getDevice(model.getId());
                bottomAdapter.onAdd(new EnteringModel(model.getId(), device.Name));
            } else if (model.getMessageType()==Consts.MessageType.CALLING) {
                modelList.add(new WebSocketModel(Consts.MessageType.CALLING, model.getId()));
                DeviceZoneModel device = controlManager.getDevice(model.getId());
                bottomAdapter.onAdd(new CallingModel(model.getId(), device.Name));
            }
        } else {
            if (socketModel.getMessageType()==Consts.MessageType.IN_PROCCESS) {
                if (model.getMessageType()==Consts.MessageType.COMPLETE
                        || model.getMessageType()==Consts.MessageType.CANCELED) {
                    bottomAdapter.onRemove(model.getId());
                    modelList.remove(socketModel);
                }
            }
        }
    }

    void handleAlarm(WebSocketModel model) {
        downloadAndMergeZoneDevices();
    }

    //---------- Phone listener -----------
    @Override
    public void onCallStart(String id) {
        if (bottomAdapter.getIndex(id)==-1)
            bottomAdapter.onAdd(new CallingModel(id, controlManager.getDevice().Name, true));
    }

    @Override
    public void onCallConnect(String id) {
        bottomAdapter.onUpdateCallingModel(id, true);
    }

    @Override
    public void onCallFinish(String id) {
        bottomAdapter.onRemove(id);
    }

    @Override
    public void onIncomingCall(final String id) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                String name = "Устройство";
                if (controlManager.getDevice()!=null) {
                   name = controlManager.getDevice().Name;
                }
                bottomAdapter.onAdd(new CallingModel(id, name));
                onAnswer(id);
            }
        });
    }

    @Override
    public void onError(String message) {
        Log.e(Consts.TAG, message);
    }

    //----------- zone bottom adapter listener -----------
    @Override
    public void onAnswer(String id) {
        if (id.length()==36) {
            DeviceZoneModel device = controlManager.getDevice(id);
            controlManager.setDevice(device);
            controlManager.onCall(device.Host);
            bottomAdapter.onRemove(id);

            WebSocketModel socketModel = getWebSocketModel(id);
            modelList.remove(socketModel);
        } else {
            bottomAdapter.onUpdateCallingModel(id, true);
            linphoneManager.onAnswer();
        }
    }

    @Override
    public void onCancel(String id) {
        linphoneManager.onCancel();
        bottomAdapter.onRemove(id);
    }

    @Override
    public void onFinish(String id) {
        linphoneManager.onCancel();
        bottomAdapter.onRemove(id);
    }

    @Override
    public void onCancelEntering(String id) {
        controlManager.setDeviceByID(id);
        controlManager.cancelSingleCar();
    }

    @Override
    public void approachSensor() {
        if (existsActiveCall() && view!=null) {
            view.shadowCallingScreen(true);
        }
    }

    @Override
    public void distanceFromSensor() {
        if (existsActiveCall() && view!=null) {
            view.shadowCallingScreen(false);
        }
    }
}
