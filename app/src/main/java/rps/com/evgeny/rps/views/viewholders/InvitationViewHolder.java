package rps.com.evgeny.rps.views.viewholders;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import rps.com.evgeny.rps.MainApplication;
import rps.com.evgeny.rps.databinding.InvationCardviewBinding;
import rps.com.evgeny.rps.models.invitations.InvitationModel;
import rps.com.evgeny.rps.utils.StringUtils;

/**
 * Created by Evgeny on 19.09.2017.
 */

public class InvitationViewHolder extends RecyclerView.ViewHolder {
    InvationCardviewBinding binding;
    Context context;

    public InvitationViewHolder(View itemView) {
        super(itemView);

        context = itemView.getContext();
        binding = DataBindingUtil.bind(itemView);
    }

    public void setInvitation(InvitationModel model) {
        setName(model.GuestName);
        setTime(model.CreatedAt);
        setCarNumber(model.GuestPlateNumber);
        setTimeIn(model.EnterTime);
        setTimeOut(model.ExitTime);
    }

    public void setName(String name) {
        if (StringUtils.isNullOrEmpty(name)) {
            binding.driverName.setText("Имя не указано");
        } else {
            String[] names = name.split("\\s+");
            StringBuffer buffer = new StringBuffer()
                    .append(names[0]).append(" ");
            if (names.length > 1)
                buffer.append(names[1].substring(0, 1)).append(". ");
            if (names.length > 2)
                buffer.append(names[2].substring(0, 1)).append(".");
            binding.driverName.setText(buffer.toString());
        }
    }

    public void setTime(String time) {
        binding.carTime.setText(time);
    }

    public void setTimeIn(String time) {
        binding.timeComeIn.setText(time!=null?time:"Не въезжал");
    }

    public void setTimeOut(String time) {
        binding.timeComeOut.setText(time!=null?time:"Не выезжал");
    }

    public void setCarNumber(String number) {
        binding.carNumber.setText(number);
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        binding.refreshButton.setOnClickListener(onClickListener);
    }
}
