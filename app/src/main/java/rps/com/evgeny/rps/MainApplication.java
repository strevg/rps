package rps.com.evgeny.rps;

import android.app.Activity;
import android.app.Application;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDex;
import android.util.Log;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import io.realm.Realm;
import rps.com.evgeny.rps.activities.AdminMainActivity;
import rps.com.evgeny.rps.activities.MainActivity;
import rps.com.evgeny.rps.di.DaggerAppComponent;
import rps.com.evgeny.rps.services.RestartService;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.Settings;

/**
 * Created by Evgeny on 30.08.2017.
 */

public class MainApplication extends DaggerApplication implements Application.ActivityLifecycleCallbacks {
    ProgressDialog progress;
    Activity currentActivity;

    @Override
    protected AndroidInjector<? extends dagger.android.DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().application(this).build();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public void showProgressDialog(@NonNull final String text) {
        if (currentActivity==null)
            return;
        currentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progress !=null && progress.isShowing()) {
                    progress.dismiss();
                    progress=null;
                }
                progress = new ProgressDialog(currentActivity, R.style.dialog);
                progress.setCancelable(false);
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                if(text.length()>0){
                    progress.setMessage(text);
                }
                progress.show();
            }
        });
    }

    public void dismissProgressDialog() {
        if (currentActivity==null)
            return;
        currentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progress != null && progress.isShowing()) {
                    progress.dismiss();
                }
            }
        });
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(Consts.TAG, "Application onCreate");
        registerActivityLifecycleCallbacks(this);

        Realm.init(this);
        Settings.init(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        unregisterActivityLifecycleCallbacks(this);
    }

    public void setMainActivity(Activity activity) {
        this.currentActivity = (MainActivity) activity;
    }

    public MainActivity getMainActivity() {
        return (MainActivity) currentActivity;
    }

    public Activity getCurrentActivity() {
        return currentActivity;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        currentActivity = activity;
    }
    @Override
    public void onActivityStarted(Activity activity) {
        currentActivity = activity;
    }
    @Override
    public void onActivityResumed(Activity activity) {
        currentActivity = activity;
    }
    @Override
    public void onActivityPaused(Activity activity) {
        Log.i(Consts.TAG, "onPause " + activity.getClass().getSimpleName());
    }
    @Override
    public void onActivityStopped(Activity activity) {
        Log.i(Consts.TAG, "onStop " + activity.getClass().getSimpleName());
    }
    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        Log.i(Consts.TAG, "onSaveInstanceState " + activity.getClass().getSimpleName());
    }
    @Override
    public void onActivityDestroyed(Activity activity) {
        if (activity instanceof AdminMainActivity) {
            restartService();
        }
        Log.i(Consts.TAG, "onDestroy " + activity.getClass().getSimpleName());
    }

    void restartService() {
        Intent broadcastIntent = new Intent("RestartService");
        broadcastIntent.setClass(this, RestartService.class);
        sendBroadcast(broadcastIntent);
    }
}
