package rps.com.evgeny.rps.utils;

/**
 * Created by Evgeny on 30.08.2017.
 */

public class Consts {
    public static final String TAG = "rps";

    public static final String BASE_URI = "";
    public static final boolean TEST = false;

    public static final int TIMEOUT_ERROR = 3000;
    public static final int PLATE_RECOGNITION_REQUEST = 123;

    public enum FragmentType {CARD_READER, CARD_WRITER, READ_DATA, PRICE_DIALOG,
        CREATE_SELECT_INVITATION, CREATE_INVITATION, INVITATIONS, PERSON, CHECK_INVITATION, SINGLE_BARCODE};

    public enum CallStatus {NONE, STARTING, TALKING}

    public enum InformationType{GROUP, ZONE, TIME_COME_IN, TIME_CALCULATION, TARIFF, TARIFF_SCHEDULE,
        TVP, TKVP, VP, KVP, CLIENT};

    public enum MessageType {IN_PROCCESS, CANCELED, COMPLETE, ALARM, CALLING}

    public static String IN_PROCCESS = "SinglePassageInProcess";
    public static String CANCELED = "SinglePassageCanceled";
    public static String COMPLETE = "SinglePassageComplete";
    public static String ALARM = "new alarm";
    public static String SINGLE_PASSAGE = "SinglePassage";
    public static String CALLING = "device calling";

    public static final String PREF_ASTERISK_LOGIN = "prefAsteriskLogin";
    public static final String PREF_ASTERISK_PASSWORD = "prefAsteriskPassword";
    public static final String PREF_DEVICE_ID = "prefDeviceID";
}
