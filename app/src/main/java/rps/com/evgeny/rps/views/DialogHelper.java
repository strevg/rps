package rps.com.evgeny.rps.views;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;

import java.util.ArrayList;
import java.util.List;

import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.models.network.RackWorkingModeModel;
import rps.com.evgeny.rps.utils.ViewUtils;

/**
 * Created by Evgeny on 06.06.2018.
 */

public class DialogHelper {
    private Context context;
    private MaterialDialog dialog;

    public DialogHelper(Context context) {
        this.context = context;
    }

    public void showDialog(int title, int content) {
        dialog = new MaterialDialog.Builder(context)
                .theme(Theme.LIGHT)
                .title(title)
                .content(content)
                .canceledOnTouchOutside(true)
                .positiveText(android.R.string.ok)
                .show();
    }

    public void showReasonChangeModeDialog(final onReturnListener callback) {
        dialog = new MaterialDialog.Builder(context)
                .theme(Theme.DARK)
                .title(R.string.title_reason_change_mode)
                .customView(R.layout.dialog_reason_change_model, false)
                .canceledOnTouchOutside(false)
                .positiveText(R.string.button_gone)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        EditText editText = dialog.getCustomView().findViewById(R.id.edittext);
                        if (callback!=null) {
                            callback.onReturn(editText.getText().toString());
                        }
                        ViewUtils.hideKeyboardFrom(context, dialog.getView());
                    }
                })
                .negativeText(R.string.button_cancel)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        if (callback!=null) {
                            callback.onReturn(null);
                        }
                        ViewUtils.hideKeyboardFrom(context, dialog.getView());
                    }
                })
                .show();
    }

    public void showSelectionDialog(int title, int items, int def, MaterialDialog.ListCallbackSingleChoice listener) {
        dialog = new MaterialDialog.Builder(context)
                .title(title)
                .items(items)
                .itemsCallbackSingleChoice(def, listener)
                .positiveText(R.string.button_ok)
                .show();
    }

    public void showChangeRackModeDialog(List<RackWorkingModeModel> list, int pos, MaterialDialog.ListCallbackSingleChoice listener) {
        List<String> items = new ArrayList<>();
        int def=0;
        if (list==null || list.size()==0)
            return;
        for (RackWorkingModeModel modeModel : list) {
            items.add(modeModel.Name);
            if (modeModel.Id==pos) {
                def = list.indexOf(modeModel);
            }
        }

        dialog = new MaterialDialog.Builder(context)
                .theme(Theme.DARK)
                .title(R.string.title_change_type)
                .items(items)
                .itemsCallbackSingleChoice(def, listener)
                .positiveText(R.string.button_ok)
                .show();
    }

    public void dismiss(int ms) {
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if (dialog!=null)
                    dialog.dismiss();
            }
        }, ms);
    }

    public void dismiss() {
        dialog.dismiss();
    }

    public interface onReturnListener<T> {
        void onReturn(T value);
    }
}
