package rps.com.evgeny.rps.services.linphone;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import org.linphone.core.LinphoneCall;

import rps.com.evgeny.rps.services.linphone.callback.PhoneCallback;
import rps.com.evgeny.rps.services.linphone.callback.PhoneListener;
import rps.com.evgeny.rps.services.linphone.callback.RegistrationCallback;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.Settings;
import rps.com.evgeny.rps.utils.StringUtils;

/**
 * Created by Evgeny on 07.08.2018.
 */

public class LinphoneManager {
    private static final String SIP_SERVER_TEST = "sip.linphone.org";
    private static LinphoneManager instance;
    private PhoneListener phoneListener;
    private Context context;

    private LinphoneManager(Context context) {
        this.context = context;
        init();
    }

    public static LinphoneManager getInstance(Context context) {
        if (instance==null) {
            instance = new LinphoneManager(context);
        }
        return instance;
    }

    void init() {
        EasyLinphone.startService(context);
        EasyLinphone.addCallback(registrationCallback, phoneCallback);
        //loginTest();
    }

    public void setPhoneListener(PhoneListener listener) {
        this.phoneListener = listener;
    }

    public void login() {
        String user = Settings.linphoneLogin;
        String password = Settings.linphonePassword;
        String server = StringUtils.getSipServer();
        EasyLinphone.setAccount(user, password, server);
        EasyLinphone.login();
    }

    public void onCall(String user) {
        EasyLinphone.callTo(user, false);
    }

    public void onCancel() {
        EasyLinphone.hangUp();
    }

    public void onAnswer() {
        EasyLinphone.acceptCall();
    }

    public int getDuration() {
        return EasyLinphone.getDuration();
    }

    public LinphoneCall[] getCalls() {
        return EasyLinphone.getCalls();
    }

    RegistrationCallback registrationCallback = new RegistrationCallback() {
        @Override
        public void registrationOk() {
            super.registrationOk();
        }

        @Override
        public void registrationFailed() {
            super.registrationFailed();
            /*new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    if (context!=null)
                        Toast.makeText(context, "Не удалось подключиться к Linphone", Toast.LENGTH_SHORT).show();
                }
            });
            */
        }
    };

    PhoneCallback phoneCallback = new PhoneCallback() {
        @Override
        public void outgoingInit(LinphoneCall linphoneCall) {
            super.outgoingInit(linphoneCall);
            if (phoneListener!=null)
                phoneListener.onCallStart(linphoneCall.toString());
        }

        @Override
        public void callConnected(LinphoneCall linphoneCall) {
            super.callConnected(linphoneCall);
            if (phoneListener!=null)
                phoneListener.onCallConnect(linphoneCall.toString());
        }

        @Override
        public void incomingCall(LinphoneCall linphoneCall) {
            super.incomingCall(linphoneCall);
            if (phoneListener!=null)
                phoneListener.onIncomingCall(linphoneCall.toString());
        }

        @Override
        public void callEnd(LinphoneCall linphoneCall) {
            super.callEnd(linphoneCall);
            if (phoneListener!=null)
                phoneListener.onCallFinish(linphoneCall.toString());
        }

        @Override
        public void error(LinphoneCall linphoneCall) {
            super.error(linphoneCall);
            if (phoneListener!=null)
                phoneListener.onError("Error");
        }
    };

    void loginTest() {
        EasyLinphone.setAccount("strevg", "123456", SIP_SERVER_TEST);
        EasyLinphone.login();
    }
}
