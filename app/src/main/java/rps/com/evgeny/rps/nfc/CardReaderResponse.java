package rps.com.evgeny.rps.nfc;

import android.util.Log;

import java.lang.reflect.Field;

import rps.com.evgeny.rps.models.CardModel;
import rps.com.evgeny.rps.utils.Consts;

/**
 * Created by Evgeny on 01.09.2017.
 */

public class CardReaderResponse implements Cloneable{
    public String Status;
    public String CardId;
    public String ParkingEnterTime;
    public String LastRecountTime;
    public String TSidFC;
    public String TPidFC;
    public String ZoneidFC;
    public String ClientGroupidFC;
    public String SumOnCard;
    public String LastPaymentTime;
    public String Nulltime1;
    public String Nulltime2;
    public String Nulltime3;
    public String DateSaveCard;
    public String TVP;
    public String TKVP;
    public String ClientTypidFC;
    public String LastPlate;

    public CardReaderResponse(){}

    public CardReaderResponse(String cardId, String sumOnCard, String parkingEnterTime) {
        this.CardId = cardId;
        this.SumOnCard = sumOnCard;
        this.ParkingEnterTime = parkingEnterTime;
    }

    public CardReaderResponse(CardModel model) {
        CardId = model.CardId;
        ParkingEnterTime = model.ParkingEnterTime;
        LastRecountTime = model.LastRecountTime;
        TSidFC = model.TSidFC;
        TPidFC = model.TPidFC;
        ZoneidFC = model.ZoneidFC;
        ClientGroupidFC = model.ClientGroupidFC;
        SumOnCard = model.SumOnCard;
        LastPaymentTime = model.LastPaymentTime;
        Nulltime1 = model.Nulltime1;
        Nulltime2 = model.Nulltime2;
        Nulltime3 = model.Nulltime3;
        DateSaveCard = model.DateSaveCard;
        TVP = model.TVP;
        TKVP = model.TKVP;
        ClientTypidFC = model.ClientTypidFC;
    }

    public CardReaderResponse clone(CardReaderResponse card) {
        try {
            return (CardReaderResponse) card.clone();
        } catch (Exception e) {
            Log.e(Consts.TAG, "clone cardReaderResponse " + e.getMessage());
        }
        return null;
    }

    public void changeField(String field, String value) {
        Class ftClass = this.getClass();
        try {
            Field f1 = ftClass.getField(field);
            f1.set(this, value);
        } catch (Exception e) {
            Log.e(Consts.TAG, "changeField " + e.getMessage());
        }
    }
}
