package rps.com.evgeny.rps.fragments.warning.completed;

import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.databinding.FragmentWarningCompletedBinding;
import rps.com.evgeny.rps.fragments.warning.active.WarningActiveContract;
import rps.com.evgeny.rps.fragments.warning.adapters.PagerAdapter;

/**
 * Created by Evgeny on 24.05.2018.
 */

public class WarningCompletedFragment extends DaggerFragment implements WarningCompletedContract.View {
    FragmentWarningCompletedBinding binding;

    @Inject
    WarningCompletedContract.Presenter presenter;

    @Inject
    public WarningCompletedFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_warning_completed, container, false);
        initViews();
        return binding.getRoot();
    }

    void initViews() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        binding.recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.takeView(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.dropView();
    }

    @Override
    public void setAdapter(RecyclerView.Adapter adapter) {
        binding.recyclerView.setAdapter(adapter);
    }
}
