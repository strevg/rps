package rps.com.evgeny.rps.views.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import rps.com.evgeny.rps.databinding.InvationCardviewBinding;
import rps.com.evgeny.rps.models.invitations.InvitationModel;
import rps.com.evgeny.rps.utils.StringUtils;
import rps.com.evgeny.rps.views.AdapterInterface;
import rps.com.evgeny.rps.views.viewholders.InvitationViewHolder;
import rps.com.evgeny.rps.views.onItemClickListener;

/**
 * Created by Evgeny on 22.09.2017.
 */

public class InvitationsAdapter extends RecyclerView.Adapter<InvitationViewHolder> implements AdapterInterface<InvitationModel> {
    List<InvitationModel> items;
    onItemClickListener<InvitationModel> clickListener;

    public InvitationsAdapter() {
        items = new ArrayList<>();
    }

    public void setClickListener(onItemClickListener listener) {
        this.clickListener = listener;
    }

    @Override
    public InvitationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        InvationCardviewBinding binding = InvationCardviewBinding.inflate(inflater, parent, false);
        return new InvitationViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(InvitationViewHolder holder, final int position) {
        holder.setInvitation(items.get(position));
        holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onItemClick(v, items.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public int findTextIndex(String text) {
        for (InvitationModel item : items) {
            if (!StringUtils.isNullOrEmpty(item.GuestName)
                    && item.GuestName.toLowerCase().contains(text.toLowerCase()))
                return items.indexOf(item);
        }
        return 0;
    }

    @Override
    public void onAdd(InvitationModel item) {
        int position = items.size();
        items.add(position, item);
        notifyItemInserted(position);
    }

    @Override
    public void onRemove(int pos) {
        if (pos < 0)
            return;
        items.remove(pos);
        notifyItemRemoved(pos);
    }

    @Override
    public void onRemoveAll() {
        int count = getItemCount();
        items.clear();
        notifyItemRangeRemoved(0, count);
    }

    @Override
    public void onAddAll(List<InvitationModel> items) {

    }
}
