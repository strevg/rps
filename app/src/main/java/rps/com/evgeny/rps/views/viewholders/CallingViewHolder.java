package rps.com.evgeny.rps.views.viewholders;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.databinding.ViewholderCallingBinding;
import rps.com.evgeny.rps.fragments.zone.adapter.BaseType;
import rps.com.evgeny.rps.fragments.zone.adapter.CallingModel;
import rps.com.evgeny.rps.models.realm.WarningRealm;

/**
 * Created by Evgeny on 25.05.2018.
 */

public class CallingViewHolder extends RecyclerView.ViewHolder {
    protected Context context;
    public ViewholderCallingBinding binding;

    public CallingViewHolder(View view) {
        super(view);
        this.context = view.getContext();
        binding = DataBindingUtil.bind(view);
    }

    public void setData(WarningRealm data) {
        setText("Вызов завершен");
        setTitle(data.getText());
        setEndedCalling();
        hideButtons();
    }

    public void setData(CallingModel data) {
        setText(data.getText());
    }

    public void setEndedCalling() {
        binding.titleCalling.setTextColor(context.getResources().getColor(R.color.colorWhite));
        binding.iconCalling.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_local_phone_24px));
    }

    public void hideButtons() {
        binding.answerContainer.setVisibility(View.GONE);
        binding.rejectContainer.setVisibility(View.GONE);
    }

    public void setTitle(String title) {
        binding.titleCalling.setText(title);
    }

    public void setText(String text) {
        binding.textCalling.setText(text);
    }

    public void setOnClickListener(View.OnClickListener listener) {
        binding.answerCalling.setOnClickListener(listener);
        binding.rejectCalling.setOnClickListener(listener);
    }
}
