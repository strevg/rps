package rps.com.evgeny.rps.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rps.com.evgeny.rps.models.KeyWarehouse;
import rps.com.evgeny.rps.models.SecurityUser;
import rps.com.evgeny.rps.models.User;
import rps.com.evgeny.rps.models.list.GroupModel;
import rps.com.evgeny.rps.models.list.TariffModel;
import rps.com.evgeny.rps.models.list.ZoneModel;
import rps.com.evgeny.rps.models.person.UserModel;
import rps.com.evgeny.rps.network.ApiFactory;
import rps.com.evgeny.rps.nfc.CardReader;
import rps.com.evgeny.rps.nfc.CardReaderResponse;

/**
 * Created by Evgeny on 30.08.2017.
 */

public class Settings {
    public static Consts.FragmentType fragmentType;
    public static CardReaderResponse card;
    public static CardReader cardReader;
    public static boolean isHttp;
    public static String domainServer;
    public static String domain;
    public static String token;
    public static User model;
    public static String userId;
    public static String userRole;
    public static String userName;
    public static String sessionId;
    public static String objectId;
    public static String siteName;
    public static String loginTime;
    public static String someValueId;
    public static String someNameId;
    public static boolean adminAccount;
    public static boolean isRunningActivity;
    public static boolean isRunningService;
    public static boolean isAutoServiceStart = true;
    public static boolean isUserLoggedIn;
    public static List<String> openedTabs;
    public static boolean isNFCReader;
    public static boolean isEnabledPlateRecognition;

    public static String linphoneLogin;
    public static String linphonePassword;

    public static UserModel currentUser;

    public static void init(Context context) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        Settings.sessionId = sharedPrefs.getString("sessionID", null);
        Settings.userName = sharedPrefs.getString("UserName", null);
        Settings.userRole = sharedPrefs.getString("RoleName", null);
        Settings.loginTime = sharedPrefs.getString("LoginTime", null);
        Settings.userId = sharedPrefs.getString("UserId", null);
        Settings.someNameId = sharedPrefs.getString("SomeName", null);
        Settings.someValueId = sharedPrefs.getString("SomeValue", null);
        Settings.isHttp = sharedPrefs.getBoolean("isHttp", true);
        Settings.domainServer = sharedPrefs.getString("domainServer", "r-p-s.ru");
        Settings.siteName = sharedPrefs.getString("siteName", null);
        Settings.domain = sharedPrefs.getString("domain", null);
        Settings.adminAccount = sharedPrefs.getBoolean("adminAccount", false);
        Settings.isEnabledPlateRecognition = sharedPrefs.getBoolean("isEnabledPlateRecognition", false);

        linphoneLogin = sharedPrefs.getString(Consts.PREF_ASTERISK_LOGIN, null);
        linphonePassword = sharedPrefs.getString(Consts.PREF_ASTERISK_PASSWORD, null);

        Set<String> stringSet = sharedPrefs.getStringSet("openedTabs", new HashSet<String>());
        Settings.openedTabs = new ArrayList<String>();
        openedTabs.addAll(stringSet);
    }

    public static void initSession(Context context) {
        KeyWarehouse keyStore = new KeyWarehouse(context);
        ArrayList<SecurityUser> tokens = keyStore.findAccountsForService(context.getPackageName());
        if (tokens!=null && tokens.size()>0) {
            final SecurityUser secUser = tokens.get(0);
            Settings.isUserLoggedIn = true;
            Settings.siteName = Settings.getProtocole() + secUser.domain;
            Settings.domain = secUser.domain;
            Settings.userId = secUser.user.Id.toString();
            Settings.model = secUser.user;
            Settings.objectId = secUser.user.ObjectId.toString();
            Settings.token = secUser.token;
        }
    }

    public static void save(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putString("sessionID", Settings.sessionId);
        editor.putString("UserName", Settings.userName);
        editor.putString("RoleName", Settings.userRole);
        editor.putString("LoginTime", Settings.loginTime);
        editor.putString("UserId", Settings.userId);
        editor.putString("SomeName", Settings.someNameId);
        editor.putString("SomeValue", Settings.someValueId);
        editor.putBoolean("isHttp", Settings.isHttp);
        editor.putString("domainServer", Settings.domainServer);
        editor.putString("siteName", Settings.siteName);
        editor.putString("domain", Settings.domain);
        editor.putString(Consts.PREF_ASTERISK_LOGIN, Settings.linphoneLogin);
        editor.putString(Consts.PREF_ASTERISK_PASSWORD, Settings.linphonePassword);
        editor.commit();
    }

    public static void saveAsteriskLoginPassword(Context context, String user, String pass) {
        Settings.linphoneLogin = user;
        Settings.linphonePassword = pass;
        saveString(context, Consts.PREF_ASTERISK_LOGIN, user);
        saveString(context, Consts.PREF_ASTERISK_PASSWORD, pass);
    }

    public static String getProtocole() {
        return Settings.isHttp? "http://" : "https://";
    }

    public static String getDomainServer() {
        return "." + Settings.domainServer;
    }

    public static void saveBoolean(Context context, String str, boolean flag) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(str, flag);
        editor.commit();
    }

    public static void saveString(Context context, String str, String value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putString(str, value);
        editor.commit();
    }

    public static void addOpenedTab(Context context, String tabID) {
        if (!openedTabs.contains(tabID)) {
            openedTabs.add(tabID);
        }
        saveStringList(context, "openedTabs", openedTabs);
    }

    public static void removeOpenedTab(Context context, String tabID) {
        if (openedTabs.contains(tabID)) {
            openedTabs.remove(tabID);
        }
        saveStringList(context, "openedTabs", openedTabs);
    }

    public static boolean isOpenedTab(String id) {
        return openedTabs.contains(id);
    }

    public static void saveStringList(Context context, String str, List<String> value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = prefs.edit();
        Set<String> set = new HashSet<String>(value);
        editor.putStringSet(str, set);
        editor.commit();
    }
}
