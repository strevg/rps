package rps.com.evgeny.rps.fragments.zone.details;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import rps.com.evgeny.rps.databinding.InvationCardviewBinding;
import rps.com.evgeny.rps.databinding.ViewholderWarningBinding;
import rps.com.evgeny.rps.fragments.zone.adapter.BaseType;
import rps.com.evgeny.rps.models.invitations.InvitationModel;
import rps.com.evgeny.rps.models.warnings.WarningModel;
import rps.com.evgeny.rps.models.zones_devices.AlarmModel;
import rps.com.evgeny.rps.utils.StringUtils;
import rps.com.evgeny.rps.views.AdapterInterface;
import rps.com.evgeny.rps.views.onItemClickListener;
import rps.com.evgeny.rps.views.viewholders.InvitationViewHolder;
import rps.com.evgeny.rps.views.viewholders.WarningViewHolder;

/**
 * Created by Evgeny on 09.06.2018.
 */

public class AlarmAdapter extends RecyclerView.Adapter<WarningViewHolder>
        implements AdapterInterface<WarningModel> {

    private List<WarningModel> items;

    public AlarmAdapter() {
        items = new ArrayList<>();
    }

    @Override
    public WarningViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewholderWarningBinding binding = ViewholderWarningBinding.inflate(inflater, parent, false);
        return new WarningViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(WarningViewHolder holder, int position) {
        holder.hideButton(true);
        holder.setData(items.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onAdd(WarningModel item) {
        int position = items.size();
        this.items.add(position, item);
        notifyItemInserted(position);
    }

    public void onAdd(WarningModel item, int position) {
        this.items.add(position, item);
        notifyItemInserted(position);
    }

    @Override
    public void onAddAll(List<WarningModel> items) {
        int position = getItemCount();
        this.items.addAll(position, items);
        notifyItemRangeInserted(position, items.size());
    }

    @Override
    public void onRemove(int pos) {
        if (pos < 0)
            return;
        this.items.remove(pos);
        notifyItemRemoved(pos);
    }

    @Override
    public void onRemoveAll() {
        int count = getItemCount();
        this.items.clear();
        notifyItemRangeRemoved(0, count);
    }

    public int getIndex(String id) {
        int pos = -1;
        for (BaseType item : items) {
            if (item.getId().equalsIgnoreCase(id)) {
                pos = items.indexOf(item);
                break;
            }
        }
        return pos;
    }
}
