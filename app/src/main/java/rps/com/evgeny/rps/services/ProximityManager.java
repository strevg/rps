package rps.com.evgeny.rps.services;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import rps.com.evgeny.rps.utils.Consts;

import static android.content.Context.SENSOR_SERVICE;

public class ProximityManager {
    private static ProximityManager instance;
    private SensorManager sensorManager;
    private Sensor proximitySensor;
    private ProximityCallback callback;
    private Context context;

    private ProximityManager(Context context) {
        this.context = context;
        init();
    }

    public static ProximityManager getInstance(Context context) {
        if (instance==null) {
            instance = new ProximityManager(context);
        }
        return instance;
    }

    void init() {
        sensorManager = (SensorManager) context.getSystemService(SENSOR_SERVICE);
        proximitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        if(proximitySensor == null) {
            Log.e(Consts.TAG, "Proximity sensor not available.");
            return;
        }
        sensorManager.registerListener(proximitySensorListener,
                proximitySensor, 2 * 1000 * 1000);
    }

    SensorEventListener proximitySensorListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {
            if(sensorEvent.values[0] < proximitySensor.getMaximumRange()) {
                if (callback!=null)
                    callback.approachSensor();
            } else {
                if (callback!=null)
                    callback.distanceFromSensor();
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {
        }
    };

    public void register(ProximityCallback callback) {
        this.callback = callback;
    }

    public void onDestroy() {
        sensorManager.unregisterListener(proximitySensorListener);
        callback = null;
    }

    public interface ProximityCallback {
        void approachSensor();
        void distanceFromSensor();
    }
}
