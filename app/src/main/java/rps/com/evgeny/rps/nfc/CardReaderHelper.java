package rps.com.evgeny.rps.nfc;

import android.util.Log;

import com.crashlytics.android.Crashlytics;

import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import rps.com.evgeny.rps.utils.Consts;

/**
 * Created by Evgeny on 01.09.2017.
 */

public class CardReaderHelper {
    static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    static Date date0;

    public static Date getDate0() { // НУЛЕВОЕ время
        try {
            if (date0 == null) {
                date0 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse("2000-01-01 00:00:00");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date0;
    }

    //Возращает разницу между 2000-01-01 и 1970-01-01
    public static int getIntDate0() {
        Date date1 = getDate0();
        return (int) (date1.getTime()/1000);
    }

    static void setWBlock(int curPos, byte[] array, int value) {
        //boolean signedByte = false;
        for (int i = 0; i < 4; i++) {
            byte a = (byte)(((value & (255 << (i * 8))) >> (i * 8)));// + (signedByte?1:0));
            // выбор байта, начиная с младшего
            array[i + curPos] = a;
            //signedByte = (a < 0);
        }
    }

    public static byte[] dataToWBlock(CardData cardData) {
        byte[] wBlock = new byte[48]; // запись сектора карты
        int CurPos = 0;

        setWBlock(CurPos, wBlock, cardData.ParkingEnterTime);
        CurPos += 4;
        setWBlock(CurPos, wBlock, cardData.LastRecountTime);
        CurPos += 4;
        wBlock[CurPos++] = cardData.TSidFC;
        wBlock[CurPos++] = cardData.TPidFC;
        wBlock[CurPos++] = cardData.ZoneidFC;
        wBlock[CurPos++] = cardData.ClientGroupidFC;
        setWBlock(CurPos, wBlock, cardData.SumOnCard);
        CurPos += 4;
        setWBlock(CurPos, wBlock, cardData.LastPaymentTime);
        CurPos += 4;
        setWBlock(CurPos, wBlock, cardData.Nulltime1);
        CurPos += 4;
        setWBlock(CurPos, wBlock, cardData.Nulltime2);

        CurPos += 4;
        setWBlock(CurPos, wBlock, cardData.Nulltime3);

        CurPos += 4;
        setWBlock(CurPos, wBlock, cardData.TVP);

        CurPos += 4;
        wBlock[CurPos++] = cardData.TKVP;
        wBlock[CurPos++] = cardData.ClientTypidFC;
        CurPos += 2; // Параметры для новых условий
        CurPos += 4; //Время начала промежутка времени для нового условия
        setWBlock(CurPos, wBlock, cardData.DateSaveCard);

        CurPos += 4;

        return wBlock;
    }


    public static CardData rBlockToData(String inBlock) {
        byte[] rBlock = CardReader.HexStringToByteArray(inBlock);

        int CurPos = 0;

        CardData rdata = new CardData();

        rdata.ParkingEnterTime = 0;
        for (int i = 0; i < 4; i++) {
            rdata.ParkingEnterTime += (rBlock[i + CurPos] & 0xFF) << (i * 8);
        }

        CurPos += 4;
        rdata.LastRecountTime = 0;
        for (int i = 0; i < 4; i++) {
            rdata.LastRecountTime += (rBlock[i + CurPos] & 0xFF) << (i * 8);
        }

        CurPos += 4;
        rdata.TSidFC = rBlock[CurPos++];
        rdata.TPidFC = rBlock[CurPos++];
        rdata.ZoneidFC = rBlock[CurPos++];
        rdata.ClientGroupidFC = rBlock[CurPos++];
        rdata.SumOnCard = 0;
        for (int i = 0; i < 4; i++) {
            rdata.SumOnCard += (rBlock[i + CurPos] & 0xFF) << (i * 8);
        }
        CurPos += 4;
        rdata.LastPaymentTime = 0;
        for (int i = 0; i < 4; i++) {
            rdata.LastPaymentTime += (rBlock[i + CurPos] & 0xFF) << (i * 8);
        }

        CurPos += 4;
        rdata.Nulltime1 = 0;
        for (int i = 0; i < 4; i++) {
            rdata.Nulltime1 += (rBlock[i + CurPos] & 0xFF) << (i * 8);
        }

        CurPos += 4;
        rdata.Nulltime2 = 0;
        for (int i = 0; i < 4; i++) {
            rdata.Nulltime2 += (rBlock[i + CurPos] & 0xFF) << (i * 8);
        }

        CurPos += 4;
        rdata.Nulltime3 = 0;
        for (int i = 0; i < 4; i++) {
            rdata.Nulltime3 += (rBlock[i + CurPos] & 0xFF) << (i * 8);
        }

        CurPos += 4;
        rdata.TVP = 0;
        for (int i = 0; i < 4; i++) {
            rdata.TVP += (rBlock[i + CurPos] & 0xFF) << (i * 8);
        }

        CurPos += 4;
        rdata.TKVP = rBlock[CurPos++];
        rdata.ClientTypidFC = (rBlock[CurPos++]);
        CurPos += 2; //Параметры для новых условий
        CurPos += 4; //Время начала промежутка времени для нового условия
        rdata.DateSaveCard = 0;
        for (int i = 0; i < 4; i++) {//Время последней записи на карту
            rdata.DateSaveCard += (rBlock[i + CurPos] & 0xFF) << (i * 8);
        }

        CurPos += 4;

        return rdata;
    }

    public static String getFormatedDate(int seconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(getDate0());
        calendar.add(Calendar.SECOND, seconds);
        return dateFormat.format(calendar.getTime());
    }

    public static int parseFormatedDate(String date) {
        Date result = new Date();
        try {
            result = dateFormat.parse(date);
        } catch (Exception e) {
            Log.e(Consts.TAG, e.toString());
            Crashlytics.logException(e);
        }
        int date0 = getIntDate0();
        int date1 = (int)(result.getTime()/1000);
        return date1 - date0;
    }

    public static  CardReaderResponse GetResult(CardData data) {
        CardReaderResponse outCardData = new CardReaderResponse();

        outCardData.CardId = data.CardId.toLowerCase();
        CardData cardInfo = data;
        outCardData.ClientGroupidFC = byteToString(cardInfo.ClientGroupidFC);//String.valueOf(cardInfo.ClientGroupidFC); // группа клиента на карте
        outCardData.ClientTypidFC = byteToString(cardInfo.ClientTypidFC);//String.valueOf(cardInfo.ClientTypidFC);
        outCardData.LastPaymentTime = getFormatedDate(cardInfo.LastPaymentTime);
        // Время въезда в зону
        outCardData.LastRecountTime = getFormatedDate(cardInfo.LastRecountTime);
        // Время въезда в зону
        outCardData.Nulltime1 = getFormatedDate(cardInfo.Nulltime1);
        // Время въезда в зону
        outCardData.Nulltime2 = getFormatedDate(cardInfo.Nulltime2);
        // Время въезда в зону
        outCardData.Nulltime3 = getFormatedDate(cardInfo.Nulltime3);
        // Время въезда в зону
        outCardData.ParkingEnterTime = getFormatedDate(cardInfo.ParkingEnterTime);
        // Время въезда
        outCardData.DateSaveCard = getFormatedDate(cardInfo.DateSaveCard);
        // Время сохранения карты
        outCardData.SumOnCard = String.valueOf(cardInfo.SumOnCard); // сумма (баланс) на карте
        outCardData.TKVP = byteToString(cardInfo.TKVP);//String.valueOf(cardInfo.TKVP);
        outCardData.TPidFC = byteToString(cardInfo.TPidFC);//String.valueOf(cardInfo.TPidFC); // ID ТП на карте
        outCardData.TSidFC = byteToString(cardInfo.TSidFC);//String.valueOf(cardInfo.TSidFC); // ID ТС на карте
        outCardData.TVP = getFormatedDate(cardInfo.TVP);
        // Время въезда в зону
        outCardData.ZoneidFC = byteToString(cardInfo.ZoneidFC);//String.valueOf(cardInfo.ZoneidFC);

        return outCardData;
    }

    public static String byteToString(byte value) {
        int intValue = value;
        if (intValue < 0)
            intValue = 127 - intValue;
        return String.valueOf(intValue);
    }

    public static byte stringToByte(String value) {
        int intValue = Integer.valueOf(value);
        if (intValue > 127)
            intValue = 127 - intValue;
        return (byte)intValue;
    }

    public static CardData GetParam(CardReaderResponse response) {
        String T;
        int _TimeIn;
        CardData CardInfo = new CardData();

        String regex = new String("^\\d{1,3}$");

        try {
            if (response.CardId!=null && !response.CardId.isEmpty()) {
                T = response.CardId;
                CardInfo.CardId = T; // Card ID
            }

            if (response.ClientGroupidFC!=null && !response.ClientGroupidFC.isEmpty()) {
                T = response.ClientGroupidFC;

                if (T.matches(regex)) {
                    CardInfo.ClientGroupidFC = stringToByte(T);//Byte.valueOf(T);
                }
            }

            if (response.ClientTypidFC!=null && !response.ClientTypidFC.isEmpty()) {
                T = response.ClientTypidFC;

                if (T.matches(regex)) {
                    CardInfo.ClientTypidFC = stringToByte(T);//Byte.valueOf(T); // Время въезда
                }
            }
            if (response.LastPaymentTime!=null && !response.LastPaymentTime.isEmpty()) {
                T = response.LastPaymentTime;
                _TimeIn =  parseFormatedDate(T);
                CardInfo.LastPaymentTime = _TimeIn; // Время въезда
            }
            if (response.LastRecountTime!=null && !response.LastRecountTime.isEmpty()) {
                T = response.LastRecountTime;
                _TimeIn =  parseFormatedDate(T);
                CardInfo.LastRecountTime = _TimeIn; // Время въезда
            }
            if (response.Nulltime1!=null && !response.Nulltime1.isEmpty()) {
                T = response.Nulltime1;
                _TimeIn =  parseFormatedDate(T);
                CardInfo.Nulltime1 = _TimeIn; // Время въезда
            }
            if (response.Nulltime2!=null && !response.Nulltime2.isEmpty()) {
                T = response.Nulltime2;
                _TimeIn =  parseFormatedDate(T);
                CardInfo.Nulltime2 = _TimeIn; // Время въезда
            }
            if (response.Nulltime3!=null && !response.Nulltime3.isEmpty()) {
                T = response.Nulltime3;
                _TimeIn =  parseFormatedDate(T);
                CardInfo.Nulltime3 = _TimeIn; // Время въезда
            }
            if (response.ParkingEnterTime!=null && !response.ParkingEnterTime.isEmpty()) {
                T = response.ParkingEnterTime;
                _TimeIn =  parseFormatedDate(T);
                CardInfo.ParkingEnterTime = _TimeIn; // Время въезда
            }
            if (response.DateSaveCard!=null && !response.DateSaveCard.isEmpty()
                    && !response.DateSaveCard.equals("2000-01-01 00:00:00")) {
                T = response.DateSaveCard;
                _TimeIn =  parseFormatedDate(T);
                CardInfo.DateSaveCard = _TimeIn; // Время въезда
            } else {
                int timeNow = (int)(new Date().getTime()/1000);
                CardInfo.DateSaveCard = timeNow - getIntDate0();
            }
            if (response.SumOnCard!=null && !response.SumOnCard.isEmpty()) {
                T = response.SumOnCard;

                CardInfo.SumOnCard = (int) ((float)Float.valueOf(T)); // Время въезда
            }
            if (response.TKVP!=null && !response.TKVP.isEmpty()) {
                T = response.TKVP;
                if (T.matches(regex)) {
                    CardInfo.TKVP = stringToByte(T);//Byte.valueOf(T); // Время въезда
                }

            }
            if (response.TPidFC!=null && !response.TPidFC.isEmpty()) {
                T = response.TPidFC;
                if (T.matches(regex)) {
                    CardInfo.TPidFC = stringToByte(T);//Byte.valueOf(T); // Время въезда
                }

            }
            if (response.TSidFC!=null && !response.TSidFC.isEmpty()) {
                T = response.TSidFC;
                if (T.matches(regex)) {
                    CardInfo.TSidFC = stringToByte(T);//Byte.valueOf(T); // Время въезда
                }

            }
            if (response.TVP!=null && !response.TVP.isEmpty()) {
                T = response.TVP;
                _TimeIn =  parseFormatedDate(T);
                CardInfo.TVP = _TimeIn; // Время въезда
            }
            if (response.ZoneidFC!=null && !response.ZoneidFC.isEmpty()) {
                T = response.ZoneidFC;
                if (T.matches(regex)) {
                    CardInfo.ZoneidFC = stringToByte(T);//Byte.valueOf(T); // Время въезда
                }

            }
            return CardInfo;
        }
        catch (Exception ex) {
            return null;
        }
    }
}
