package rps.com.evgeny.rps.fragments.zone.details;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import rps.com.evgeny.rps.di.ActivityScoped;
import rps.com.evgeny.rps.models.WarningAddedEventBus;
import rps.com.evgeny.rps.models.network.AlarmWebSocketModel;
import rps.com.evgeny.rps.models.network.AlarmZoneModel;
import rps.com.evgeny.rps.models.network.DeviceZoneModel;
import rps.com.evgeny.rps.models.network.WebSocketModel;
import rps.com.evgeny.rps.models.warnings.WarningComparator;
import rps.com.evgeny.rps.models.warnings.WarningModel;
import rps.com.evgeny.rps.models.zones_devices.AlarmModel;
import rps.com.evgeny.rps.services.ControlManager;
import rps.com.evgeny.rps.services.RealmManager;
import rps.com.evgeny.rps.services.RpsWebSocket;
import rps.com.evgeny.rps.utils.Consts;

/**
 * Created by Evgeny on 08.06.2018.
 */
@ActivityScoped
public class AlarmPresenter implements AlarmContract.Presenter, RpsWebSocket.WebSocketMessageListener {
    @Nullable
    AlarmContract.View view;
    DeviceZoneModel model;

    AlarmAdapter adapter;
    ControlManager controlManager;
    RealmManager realmManager;

    @Inject
    AlarmPresenter() {
        adapter = new AlarmAdapter();
    }

    @Override
    public void takeView(AlarmContract.View view) {
        this.view = view;
        view.setAdapter(adapter);
        adapter.onRemoveAll();
        adapter.onAddAll(getItems());

        realmManager = RealmManager.getInstance();
        controlManager = ControlManager.getInstance();
        controlManager.setListener(this);
    }

    @Override
    public void onMessage(WebSocketModel model) {
        if (model.getMessageType() == Consts.MessageType.ALARM) {
            if (model.getAlarm().End!=null) {
                onCloseAlarm(model.getAlarm());
            } else {
                onAddAlarm(model.getAlarm());
            }
        }
    }

    void onAddAlarm(final AlarmWebSocketModel alarm) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                adapter.onAdd(new WarningModel(alarm), 0);
                if (view!=null)
                    view.scrollToPosition(0);
            }
        });
        Log.i(Consts.TAG, "onAddAlarm");
    }

    void onCloseAlarm(final AlarmWebSocketModel alarm) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                int index = adapter.getIndex(alarm.getId());
                if (view!=null)
                    view.scrollToPosition(index!=-1?index:0);
                adapter.onRemove(index);
                realmManager.add(RealmManager.getRealmModel(new AlarmZoneModel(alarm)));
            }
        });
        Log.i(Consts.TAG, "onCloseAlarm");
    }

    @Override
    public void dropView() {
        this.view = null;
    }

    public void setModel(DeviceZoneModel model) {
        this.model = model;
    }

    List<WarningModel> getItems() {
        List<WarningModel> items = new ArrayList<>();
        for (AlarmZoneModel alarm : model.alarms) {
            items.add(new WarningModel(alarm));
        }
        Collections.sort(items, new WarningComparator());
        return items;
    }
}
