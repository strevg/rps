package rps.com.evgeny.rps.models.invitations;

import java.util.List;

/**
 * Created by Evgeny on 03.10.2017.
 */

public class InvitationsResponseModel {
    public List<InvitationModel> result;
    public String date;
}
