package rps.com.evgeny.rps.utils;

import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Evgeny on 05.10.2017.
 */

public class TimeUtils {
    static DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
    static DateFormat dateParse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    static DateFormat dateFrom = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    static DateFormat dateTo = new SimpleDateFormat("yyyy-MM-dd в HH:mm");

    public static Date getCurrentDate() {
        Date today = Calendar.getInstance().getTime();
        return today;
    }

    public static boolean isNewerTime(String time1, String time2) {
        long seconds = 0;
        try {
            Date date1 = dateParse.parse(time1);
            Date date2 = dateParse.parse(time2);
            seconds = (date1.getTime()-date2.getTime())/1000;
            if (date1.compareTo(date2)>-1) {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return (seconds >-7 && seconds < 8);
    }

    public static boolean isUsageTime(String time) {
        long seconds = 0;
        try {
            Date date1 = dateParse.parse(time);
            Date date2 = getCurrentDate();
            seconds = (date1.getTime()-date2.getTime())/1000;
            if (date1.compareTo(date2)>-1) {
                return true;
            }
        } catch (Exception e) {}
        return (seconds >-7 && seconds < 8);
    }

    static String getFormatDate(Date date) {
        return dateFormat.format(date);
    }

    public static String getTimeIn(String time) {
        try {
            Date date = dateFrom.parse(time);
            return dateTo.format(date);
        } catch (Exception e) {}
        return time;
    }

    public static String getToday() {
        String reportDate = getFormatDate(getCurrentDate());
        return reportDate;
    }

    public static String getTommorow() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        return getFormatDate(calendar.getTime());
    }

    public static String getYesterday() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        return getFormatDate(calendar.getTime());
    }

    public static String getWeek() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -6);
        StringBuffer buffer = new StringBuffer()
                .append(getFormatDate(calendar.getTime()))
                .append("+-+")
                .append(getToday());
        return buffer.toString();
    }

    public static String getMonth() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -1);
        StringBuffer buffer = new StringBuffer()
                .append(getFormatDate(calendar.getTime()))
                .append("+-+")
                .append(getToday());
        return buffer.toString();
    }

    public static String getQuart() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -3);
        StringBuffer buffer = new StringBuffer()
                .append(getFormatDate(calendar.getTime()))
                .append("+-+")
                .append(getToday());
        return buffer.toString();
    }

    public static String getTodayTitle() {
        Calendar calendar = Calendar.getInstance();
        int week = calendar.get(Calendar.DAY_OF_WEEK);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        StringBuffer buffer = new StringBuffer()
                .append(getDayLocal(week)).append(", ")
                .append(day).append(" ")
                .append(getMonthLocal(month));
        return buffer.toString();
    }

    public static String getYesterdayTitle() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        int week = calendar.get(Calendar.DAY_OF_WEEK);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        StringBuffer buffer = new StringBuffer()
                .append(getDayLocal(week)).append(", ")
                .append(day).append(" ")
                .append(getMonthLocal(month));
        return buffer.toString();
    }

    public static String getDateTitle(String date) {
        return date.replace("+-+", "-");
    }

    public static String getValidDate(int day) {
        return (day<10 ? "0" : "") + day;
    }

    public static String getMonthLocal(int month) {
        return new DateFormatSymbols().getMonths()[month];
    }

    public static String getDayLocal(int day) {
        String dayStr = new DateFormatSymbols().getWeekdays()[day];
        return dayStr.substring(0, 1).toUpperCase()+dayStr.substring(1);
    }
}
