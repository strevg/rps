package rps.com.evgeny.rps.activities;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import com.crashlytics.android.Crashlytics;

import java.util.List;
import java.util.Set;

import dagger.android.support.DaggerAppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rps.com.evgeny.rps.MainApplication;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.databinding.MainBinding;
import rps.com.evgeny.rps.fragments.CardReaderFragment;
import rps.com.evgeny.rps.fragments.CardWriteFragment;
import rps.com.evgeny.rps.fragments.SingleBarcodeFragment;
import rps.com.evgeny.rps.fragments.invitations.CreateInvitationCheckFragment;
import rps.com.evgeny.rps.fragments.invitations.CreateInvitationFragment;
import rps.com.evgeny.rps.fragments.invitations.CreateInvitationSelectFragment;
import rps.com.evgeny.rps.fragments.invitations.InvitationsFragment;
import rps.com.evgeny.rps.fragments.onBackPressInterface;
import rps.com.evgeny.rps.fragments.ReadDataFragment;
import rps.com.evgeny.rps.fragments.person.PersonFragment;
import rps.com.evgeny.rps.models.BarCodeInUse;
import rps.com.evgeny.rps.models.KeyWarehouse;
import rps.com.evgeny.rps.models.list.TariffSchedule;
import rps.com.evgeny.rps.models.person.CardFullModel;
import rps.com.evgeny.rps.models.person.TransactionModel;
import rps.com.evgeny.rps.models.person.UserModel;
import rps.com.evgeny.rps.network.ApiFactory;
import rps.com.evgeny.rps.network.RPSApi;
import rps.com.evgeny.rps.nfc.CardReader;
import rps.com.evgeny.rps.nfc.CardReaderEventArgs;
import rps.com.evgeny.rps.nfc.CardReaderResponse;
import rps.com.evgeny.rps.services.MifareStateChangeReceiver;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.Settings;
import rps.com.evgeny.rps.utils.StringUtils;

/**
 * Created by Evgeny on 30.08.2017.
 */

public class MainActivity extends AppCompatActivity implements CardReader.CardHandler{
    MainBinding binding;
    MainApplication app;
    Toolbar toolbar;
    ActionBarDrawerToggle toggle;
    public CardReaderResponse card;
    public CardReader cardReader;
    private boolean mToolBarNavigationListenerIsRegistered = false;
    onBackPressInterface backPressInterface;

    private Object thisLock = new Object();
    BroadcastReceiver receiver;
    private NfcAdapter nfcAdapter;
    boolean inWriteMode;
    RPSApi rpsApi;

    public int READER_FLAGS = NfcAdapter.FLAG_READER_NFC_B | NfcAdapter.FLAG_READER_NFC_F
            | NfcAdapter.FLAG_READER_NFC_A | NfcAdapter.FLAG_READER_NO_PLATFORM_SOUNDS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        binding = DataBindingUtil.setContentView(this, R.layout.main);

        app = (MainApplication)getApplication();

        rpsApi = ApiFactory.getRpsApi(Settings.siteName+"/");

        initViews(savedInstanceState);
    }

    void initViews(Bundle savedInstanceState) {
        NavigationView navigationView = binding.navView;
        View headView = navigationView.getHeaderView(0);
        ((TextView)headView.findViewById(R.id.siteName)).setText(Settings.domain);
        if (Settings.model!=null && Settings.model.UserName!=null) {
            ((TextView) headView.findViewById(R.id.userName)).setText(Settings.model.UserName);
        }
        ((TextView)headView.findViewById(R.id.userRole)).setText(Settings.model.RoleName);

        if (!StringUtils.isNullOrEmpty(Settings.userRole)) {
            Menu menu = navigationView.getMenu();
            MenuItem navCard = menu.findItem(R.id.nav_sales);
            navCard.setTitle(getString(R.string.title_work_with_card));

            menu.removeItem(R.id.nav_invitations);
        }

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                item.setChecked(true);
                showBackButton(true);
                showBackButton(false);

                switch (item.getItemId()) {
                    case R.id.exitBtn:
                        KeyWarehouse keyStore = new KeyWarehouse(app);
                        keyStore.delete(app.getPackageName());
                        startActivity(new Intent(app, LoginActivity.class));
                        finish();
                        binding.drawerLayout.closeDrawers();
                        return true;
                    case R.id.nav_sales:
                        replaceFragment(Consts.FragmentType.CARD_READER, false, null);
                        binding.drawerLayout.closeDrawers();
                        return true;
                    case R.id.nav_invitations:
                        replaceFragment(Consts.FragmentType.INVITATIONS, false, null);
                        binding.drawerLayout.closeDrawers();
                        return true;
                }

                binding.drawerLayout.closeDrawers();
                return false;
            }
        });

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setTitle((navigationView.getMenu().findItem(R.id.nav_sales)).getTitle().toString());

        toggle = new ActionBarDrawerToggle(this, binding.drawerLayout, toolbar, R.string.open, R.string.close);
        binding.drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        cardReader = new CardReader(this, thisLock);
        cardReader.setActivity(this);
        Settings.cardReader = cardReader;

        if (savedInstanceState == null) {
            replaceFragment(Consts.FragmentType.CARD_READER, null);
        } else {
            Log.i(Consts.TAG, savedInstanceState.toString());
        }

        loadUserModel();

        //testApi();
        //test();
    }

    public void testApi() {
        rpsApi.getTransactions("4089123066", null, null, ApiFactory.getHeaders()).enqueue(new Callback<List<TransactionModel>>() {
            @Override
            public void onResponse(Call<List<TransactionModel>> call, Response<List<TransactionModel>> response) {
                Log.i(Consts.TAG, "getCardFull");
                app.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<List<TransactionModel>> call, Throwable t) {
                Log.e(Consts.TAG, "loadCardInformation " + t.toString());
                Crashlytics.logException(t);
                app.dismissProgressDialog();
            }
        });
    }

    public void loadUserModel() {
        rpsApi.getUser(Settings.userId, ApiFactory.getHeaders()).enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                if (response.isSuccessful()) {
                    Settings.currentUser = response.body();
                }
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
                Log.e(Consts.TAG, "loadUserModel " + t.toString());
                Crashlytics.logException(t);
            }
        });
    }

    void test() {
        try {
            rpsApi.listBarCode("3015839994", "2017-11-05%2020:18:13", ApiFactory.getHeaders()).enqueue(new Callback<List<BarCodeInUse>>() {
                @Override
                public void onResponse(Call<List<BarCodeInUse>> call, Response<List<BarCodeInUse>> response) {
                    if (response.isSuccessful()) {
                        List<BarCodeInUse> data = response.body();
                    }
                }

                @Override
                public void onFailure(Call<List<BarCodeInUse>> call, Throwable t) {
                    Log.e(Consts.TAG, "listBarCode " + t.toString());
                }
            });
        } catch(Exception e) {
            Toast.makeText(this, "Ошибка получения данных!", Toast.LENGTH_SHORT).show();
        }
    }

    public void showBackButton(final boolean enable) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(enable) {
                    toggle.setDrawerIndicatorEnabled(false);
                    toggle.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));
                    if(!mToolBarNavigationListenerIsRegistered) {
                        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                onBackPressed();
                            }
                        });
                        mToolBarNavigationListenerIsRegistered = true;
                    }

                } else {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);

                    toggle.setDrawerIndicatorEnabled(true);
                    toggle.setToolbarNavigationClickListener(null);
                    mToolBarNavigationListenerIsRegistered = false;
                }
            }
        });
    }

    public void setOnBackPressInterface(onBackPressInterface onBackPressInterface) {
        this.backPressInterface = onBackPressInterface;
    }

    public void setSubtitle(String text) {
        toolbar.setSubtitle(text);
    }

    public void setTitle(String text) {
        toolbar.setTitle(text);
    }

    public void replaceFragment(Consts.FragmentType type, Object data) {
        replaceFragment(type, false, data);
    }

    public void replaceFragment(Consts.FragmentType type, boolean back, Object data) {
        Settings.fragmentType = type;
        Fragment fragment = new Fragment();
        switch (type) {
            case CARD_READER:
                fragment = new CardReaderFragment();
                clearBackStack();
                break;
            case CARD_WRITER:
                fragment = new CardWriteFragment();
                break;
            case READ_DATA:
                fragment = new ReadDataFragment();
                ((ReadDataFragment)fragment).setCardReaderResponse((CardReaderResponse)data);
                break;
            case CREATE_SELECT_INVITATION:
                fragment = new CreateInvitationSelectFragment();
                back = true;
                break;
            case CREATE_INVITATION:
                fragment = new CreateInvitationFragment();
                back = true;
                break;
            case CHECK_INVITATION:
                fragment = new CreateInvitationCheckFragment();
                back = true;
                break;
            case INVITATIONS:
                fragment = new InvitationsFragment();
                clearBackStack();
                break;
            case SINGLE_BARCODE:
                fragment = new SingleBarcodeFragment();
                //clearBackStack();
                break;
            case PERSON:
                fragment = new PersonFragment();
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_fragment, fragment, type+"");
        if (back) {
            transaction.addToBackStack("");
            showBackButton(true);
        }
        try {
            transaction.commitAllowingStateLoss();
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }

    void clearBackStack() {
        try {
            FragmentManager fm = getSupportFragmentManager();
            for (int i = 0; i < fm.getBackStackEntryCount(); i++) {
                fm.popBackStack();
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }

    public void onBackStack(int count) {
        while (count>0) {
            onBackPressed();
            count--;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        disableReaderMode();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_NFC)) {
            enableReaderMode();
        }
    }

    @Override
    public void onBackPressed() {
        if (backPressInterface!=null && backPressInterface.onBackPress()) {
            return;
        }
        try {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                if (getSupportFragmentManager().getBackStackEntryCount() == 1)
                    showBackButton(false);
                getSupportFragmentManager().popBackStack();
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }

    public void setToolbarContainer(View view) {
        if (binding.toolbarContainer.findViewById(view.getId())==null)
            binding.toolbarContainer.addView(view);
    }

    public void removeToolbarContainer(View view) {
        if (binding.toolbarContainer.findViewById(view.getId())!=null)
            binding.toolbarContainer.removeView(view);
    }

    public NfcAdapter getNfcAdapter() {
        if (nfcAdapter == null) {
            nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        }
        return nfcAdapter;
    }

    @Override
    public void CardExist(Object sender, CardReaderEventArgs e) {
    }

    @Override
    public void CardRead(Object sender, CardReaderEventArgs e) {
        final CardReaderResponse card = e.card;

        if (Settings.fragmentType == Consts.FragmentType.CARD_READER) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (StringUtils.isNullOrEmpty(Settings.userRole))
                        replaceFragment(Consts.FragmentType.READ_DATA, true, card);
                    else
                        replaceFragment(Consts.FragmentType.PERSON, true, card);
                    Settings.card = card;
                }
            });
        }
    }

    @Override
    public void CardRemove(Object sender, CardReaderEventArgs e) {
        if (Settings.fragmentType == Consts.FragmentType.READ_DATA ||
                Settings.fragmentType == Consts.FragmentType.PERSON) {
            replaceFragment(Consts.FragmentType.CARD_READER, null);

            showBackButton(false);
            cardReader.ShowPopup("Карта извлечена!");
            app.dismissProgressDialog();
        }
    }

    private void enableReaderMode() {
        if (!getNfcAdapter().isEnabled()) {
            Toast.makeText(getApplicationContext(), "Please activate NFC and press Back to return to the application!", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
        } else {
            if (getNfcAdapter() != null) {
                getNfcAdapter().enableReaderMode(this, cardReader, READER_FLAGS, null);
            }
        }
    }

    private void disableReaderMode() {
        if (getNfcAdapter() != null) {
            getNfcAdapter().disableReaderMode(this);
        }
    }

    @Deprecated
    @Override
    protected void onNewIntent(Intent intent) {
        if (inWriteMode) {
            inWriteMode = false;
            Tag tag = (Tag)intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            if (tag == null) {
                return;
            }
            cardReader.onTagDiscovered(tag);
        }
    }
}
