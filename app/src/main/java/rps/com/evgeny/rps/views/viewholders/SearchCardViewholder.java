package rps.com.evgeny.rps.views.viewholders;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import rps.com.evgeny.rps.databinding.ViewholderSearchCardBinding;
import rps.com.evgeny.rps.models.CardModel;
import rps.com.evgeny.rps.models.SearchCard;

/**
 * Created by Evgeny on 15.11.2018.
 */

public class SearchCardViewholder extends RecyclerView.ViewHolder {
    public ViewholderSearchCardBinding binding;

    public SearchCardViewholder(View view) {
        super(view);
        binding = DataBindingUtil.bind(view);
    }

    public void setData(CardModel model) {
        setName(model.Name);
        setNumber(model.CardId);
    }

    public void setName(String name) {
        binding.ownerCard.setText(name);
    }

    public void setNumber(String number) {
        binding.numberCard.setText(number);
    }

    public void setOnClickListener(View.OnClickListener listener) {
        binding.getRoot().setOnClickListener(listener);
    }
}
