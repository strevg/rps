package rps.com.evgeny.rps.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.WindowManager;
import android.widget.Toast;

import dagger.android.support.DaggerAppCompatActivity;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.databinding.ActivityAdminMainBinding;
import rps.com.evgeny.rps.fragments.calling.CallingFragment;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.Settings;

public class CallingActivity extends DaggerAppCompatActivity {
    public static final int PERMISSIONS_REQUEST_RECORD_AUDIO = 100;
    ActivityAdminMainBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_admin_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        Settings.init(this);
        Settings.initSession(this);

        initViews();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.runFinalizersOnExit(true);
    }

    void initViews() {
        CallingFragment callingFragment = new CallingFragment();
        String value = getIntent().getExtras().getString(Consts.PREF_DEVICE_ID);
        Bundle bundle = new Bundle();
        bundle.putString(Consts.PREF_DEVICE_ID, value);
        callingFragment.setArguments(bundle);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentContainer, callingFragment, "");
        transaction.commit();
    }

    public boolean isPermissionGranted() {
        return !(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && ((checkSelfPermission(Manifest.permission.RECORD_AUDIO)!= PackageManager.PERMISSION_GRANTED)));
    }

    public void requestPermission() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && ((checkSelfPermission(Manifest.permission.RECORD_AUDIO)!= PackageManager.PERMISSION_GRANTED))){
            requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO},
                    PERMISSIONS_REQUEST_RECORD_AUDIO);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_RECORD_AUDIO) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, R.string.hint_record_audio_granted, Toast.LENGTH_SHORT).show();
            } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                Toast.makeText(this, R.string.hint_record_audio_denied, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
