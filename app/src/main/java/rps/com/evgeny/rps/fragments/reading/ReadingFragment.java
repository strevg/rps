package rps.com.evgeny.rps.fragments.reading;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.arlib.floatingsearchview.util.view.SearchInputView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.reflect.Field;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import rps.com.evgeny.rps.MainApplication;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.activities.BarcodeScanActivity;
import rps.com.evgeny.rps.activities.PlateRecognitionActivity;
import rps.com.evgeny.rps.databinding.FragmentReadingBinding;
import rps.com.evgeny.rps.fragments.person.PersonFragment;
import rps.com.evgeny.rps.fragments.search_card.SearchCardFragment;
import rps.com.evgeny.rps.models.event_bus.SearchCardModel;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.Settings;

/**
 * Created by Evgeny on 13.06.2018.
 */

public class ReadingFragment extends DaggerFragment implements ReadingContract.View {
    private static final int ZXING_CAMERA_PERMISSION = 1;
    private static ReadingFragment instance;
    FragmentReadingBinding binding;
    MainApplication app;
    SearchInputView searchInput;

    @Inject
    ReadingContract.Presenter presenter;

    @Inject
    public ReadingFragment() {}

    public static ReadingFragment getInstance() {
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_reading, container, false);
        instance = this;
        initViews();
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    void initViews() {
        app = (MainApplication)getActivity().getApplication();

        SearchCardFragment searchCardFragment = new SearchCardFragment();
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentContainer, searchCardFragment);
        transaction.commit();

        binding.recognition.setVisibility(Settings.isEnabledPlateRecognition?View.VISIBLE:View.GONE);

        try {
            Field mField = FloatingSearchView.class.getDeclaredField("mSearchInput");
            mField.setAccessible(true);
            searchInput = (SearchInputView)mField.get(binding.searchView);
            searchInput.setInputType(InputType.TYPE_CLASS_NUMBER);
        } catch (Exception e) {
            Log.e(Consts.TAG, e.toString());
        }

        binding.searchView.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {
            @Override
            public void onSearchTextChanged(String oldQuery, final String newQuery) {
                binding.fragmentContainer.setVisibility(newQuery.length()>0?View.VISIBLE:View.INVISIBLE);
                boolean flag = (newQuery.length()==0 && Settings.isEnabledPlateRecognition);
                binding.recognition.setVisibility(flag?View.VISIBLE:View.GONE);
                if (newQuery.length()>0)
                    EventBus.getDefault().post(new SearchCardModel(newQuery));
            }
        });
        binding.searchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSuggestionClicked(SearchSuggestion searchSuggestion) {}
            @Override
            public void onSearchAction(String currentQuery) {
            }
        });
        binding.searchView.setOnMenuItemClickListener(new FloatingSearchView.OnMenuItemClickListener() {
            @Override
            public void onActionMenuItemSelected(MenuItem item) {
                if (item.getItemId() == R.id.barcodeAction) {
                    openBarcodeReader();
                }
            }
        });
        binding.recognition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRecognition();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.takeView(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        hideKeyboard();
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.dropView();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSearchRequest(SearchCardModel model) {
        binding.searchView.setSearchText(model.getQuery());
        binding.fragmentContainer.setVisibility(View.VISIBLE);
        binding.recognition.setVisibility(View.GONE);
    }

    void openBarcodeReader() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.CAMERA}, ZXING_CAMERA_PERMISSION);
        } else {
            Intent intent = new Intent(getActivity(), BarcodeScanActivity.class);
            startActivity(intent);
        }
    }

    void openRecognition() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.CAMERA}, Consts.PLATE_RECOGNITION_REQUEST);
        } else {
            Intent intent = new Intent(getActivity(), PlateRecognitionActivity.class);
            getActivity().startActivityForResult(intent, Consts.PLATE_RECOGNITION_REQUEST);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case ZXING_CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(getActivity(), BarcodeScanActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(getActivity(), R.string.hint_permission_camera, Toast.LENGTH_SHORT).show();
                }
                break;
            case Consts.PLATE_RECOGNITION_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(getActivity(), PlateRecognitionActivity.class);
                    getActivity().startActivityForResult(intent, Consts.PLATE_RECOGNITION_REQUEST);
                } else {
                    Toast.makeText(getActivity(), R.string.hint_permission_camera_recognition, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void showPersonFragment() {
        Settings.isNFCReader = true;
        Settings.fragmentType = Consts.FragmentType.PERSON;
        PersonFragment fragment = new PersonFragment();
        replaceFragment(fragment);

        hideKeyboard();
    }

    @Override
    public void showReadingFragment() {
        if (getFragmentManager().getBackStackEntryCount() > 0)
            getFragmentManager().beginTransaction().runOnCommit(new Runnable() {
                @Override
                public void run() {
                    getFragmentManager().popBackStack();
                }
            }).commit();
    }

    void replaceFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.container, fragment, "");
        transaction.addToBackStack("");
        transaction.commit();
    }

    void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = getActivity().getCurrentFocus();
        if (view == null) {
            view = new View(getActivity());
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
