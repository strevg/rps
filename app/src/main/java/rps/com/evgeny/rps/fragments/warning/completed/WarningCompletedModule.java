package rps.com.evgeny.rps.fragments.warning.completed;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import rps.com.evgeny.rps.di.ActivityScoped;
import rps.com.evgeny.rps.di.FragmentScoped;

/**
 * Created by Evgeny on 25.05.2018.
 */

@Module
public abstract class WarningCompletedModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract WarningCompletedFragment warningCompletedFragment();

    @ActivityScoped
    @Binds
    abstract WarningCompletedContract.Presenter warningCompletedPresenter(WarningCompletedPresenter presenter);
}
