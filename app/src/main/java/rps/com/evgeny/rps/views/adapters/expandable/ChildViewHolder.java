package rps.com.evgeny.rps.views.adapters.expandable;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Evgeny on 30.05.2018.
 */

public class ChildViewHolder extends RecyclerView.ViewHolder {
    Object mChildListItem;
    ExpandableRecyclerAdapter mExpandableAdapter;

    /**
     * Default constructor.
     *
     * @param itemView The {@link View} being hosted in this ViewHolder
     */
    public ChildViewHolder(View itemView) {
        super(itemView);
    }

    /**
     * @return the childListItem associated with this view holder
     */
    public Object getChildListItem() {
        return mChildListItem;
    }

    /**
     *
     * Returns the adapter position of the Parent associated with this ChildViewHolder
     *
     * @return The adapter position of the Parent if it still exists in the adapter.
     * RecyclerView.NO_POSITION if item has been removed from the adapter,
     * RecyclerView.Adapter.notifyDataSetChanged() has been called after the last
     * layout pass or the ViewHolder has already been recycled.
     */
    public int getParentAdapterPosition() {
        int adapterPosition = getAdapterPosition();
        if (adapterPosition == RecyclerView.NO_POSITION) {
            return adapterPosition;
        }

        return mExpandableAdapter.getNearestParentPosition(adapterPosition);
    }

    /**
     *
     * Returns the adapter position of the Child associated with this ChildViewHolder
     *
     * @return The adapter position of the Child if it still exists in the adapter.
     * RecyclerView.NO_POSITION if item has been removed from the adapter,
     * RecyclerView.Adapter.notifyDataSetChanged() has been called after the last
     * layout pass or the ViewHolder has already been recycled.
     */
    public int getChildAdapterPosition() {
        int adapterPosition = getAdapterPosition();
        if (adapterPosition == RecyclerView.NO_POSITION) {
            return adapterPosition;
        }

        return mExpandableAdapter.getChildPosition(adapterPosition);
    }
}
