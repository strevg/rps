package rps.com.evgeny.rps.views.viewholders;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rps.com.evgeny.rps.MainApplication;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.activities.MainActivity;
import rps.com.evgeny.rps.databinding.PriceViewHolderBinding;
import rps.com.evgeny.rps.models.BarCodeInUse;
import rps.com.evgeny.rps.models.BarCodeInUseResponse;
import rps.com.evgeny.rps.models.CardModel;
import rps.com.evgeny.rps.network.ApiFactory;
import rps.com.evgeny.rps.network.RPSApi;
import rps.com.evgeny.rps.nfc.CardData;
import rps.com.evgeny.rps.nfc.CardReader;
import rps.com.evgeny.rps.nfc.CardReaderHelper;
import rps.com.evgeny.rps.nfc.CardReaderResponse;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.Settings;

/**
 * Created by Evgeny on 03.09.2017.
 */

public class PriceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    public BarCodeInUse bm;

    public PriceViewHolderBinding binding;
    MainActivity activity;
    MainApplication app;
    RPSApi rpsApi;
    boolean oneBarcode = false;

    public PriceViewHolder(View view) {
        super(view);
        oneBarcode = false;
        app = (MainApplication)view.getContext().getApplicationContext();
        activity = app.getMainActivity();

        binding = DataBindingUtil.bind(view);

        rpsApi = ApiFactory.getRpsApi(Settings.siteName+"/");

        binding.saveBarcode.setOnClickListener(this);
        binding.barcodeCard.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        binding.saveBarcode.setText(app.getString(R.string.saveBarcodeProgress));
        binding.changeBarcode.setText(app.getString(R.string.changeBarcode));
        app.showProgressDialog("Запись...");
        writeCard(bm);
    }

    public void hideButton(Button button) {
        button.setVisibility(View.INVISIBLE);
        button.setHeight(0);
        button.setMinHeight(0);
    }

    public void writeCard(BarCodeInUse barCodeModel) {
        CardReaderResponse card = Settings.card;
        CardData data_before = CardReaderHelper.GetParam(card);

        Map<String, String> updateObj = ApiFactory.getUpdateObject(card, barCodeModel);
        card.DateSaveCard=null;
        CardData data = CardReaderHelper.GetParam(card);

        Log.i(Consts.TAG, CardReader.ByteArrayToHexString(CardReaderHelper.dataToWBlock(data_before)));
        Log.i(Consts.TAG, CardReader.ByteArrayToHexString(CardReaderHelper.dataToWBlock(data)));

        String strData = CardReader.ByteArrayToHexString(CardReaderHelper.dataToWBlock(data));

        try {
            int result = Settings.cardReader.WriteData(strData);
            if(result == 0){
                throw new NullPointerException();
            } else {
                updateCard(barCodeModel, card.CardId, updateObj);
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
            app.getMainActivity().replaceFragment(Consts.FragmentType.CARD_READER, null);
            app.getMainActivity().showBackButton(false);

            Toast.makeText(activity, app.getString(R.string.write_error), Toast.LENGTH_SHORT).show();
            app.dismissProgressDialog();
        }
    }

    void handleAfterSuccessUpdateCard(BarCodeInUse barCodeModel) {
        barCodeModel.Sum = Integer.parseInt(Settings.card.SumOnCard) + barCodeModel.DiscountAmount;
        barCodeModel.ParkingEnterTime = Settings.card.ParkingEnterTime;
        barCodeModel.CardId = Settings.card.CardId;
        barCodeModel.IdBCM = barCodeModel.Id;

        createNew(barCodeModel);
    }

    // ------------ Network ---------------
    void updateCard(final BarCodeInUse barCodeModel, String cardId, Map<String, String> updateObj) {
        Map<String, String> header = ApiFactory.getHeaders();
        //header.put("Cookie", "DEBUG=1");
        rpsApi.updateCard(cardId, updateObj, header).enqueue(new Callback<CardModel>() {
            @Override
            public void onResponse(Call<CardModel> call, Response<CardModel> response) {
                if (!response.isSuccessful()) {
                    Crashlytics.logException(new Exception("updateCard !response.isSuccessful()"));
                    app.getMainActivity().replaceFragment(Consts.FragmentType.CARD_READER, null);
                    app.getMainActivity().showBackButton(false);

                    app.dismissProgressDialog();
                    Toast.makeText(activity, "Ошибка обновления карточки на сервере", Toast.LENGTH_SHORT).show();
                    return;
                } else
                    handleAfterSuccessUpdateCard(barCodeModel);
            }
            @Override
            public void onFailure(Call<CardModel> call, Throwable t) {
                Log.e(Consts.TAG, t.getMessage());
                Crashlytics.logException(t);
                app.dismissProgressDialog();
            }
        });
    }

    void createNew(BarCodeInUse barCodeModel) {
        rpsApi.createNew(barCodeModel, ApiFactory.getHeaders()).enqueue(new Callback<BarCodeInUseResponse>() {
            @Override
            public void onResponse(Call<BarCodeInUseResponse> call, Response<BarCodeInUseResponse> response) {
                if (!response.isSuccessful()) {
                    Crashlytics.logException(new Exception("createNew !response.isSuccessful()"));
                    app.getMainActivity().replaceFragment(Consts.FragmentType.CARD_READER, null);
                    app.getMainActivity().showBackButton(false);

                    app.dismissProgressDialog();
                    Toast.makeText(activity, "Ошибка создания карточки на сервере", Toast.LENGTH_SHORT).show();
                    return;
                }
                Log.i(Consts.TAG, "onResponse" + response.body());
                app.getMainActivity().replaceFragment(Consts.FragmentType.READ_DATA, Settings.card);

                Toast.makeText(activity, app.getString(R.string.write_success), Toast.LENGTH_SHORT).show();

                Settings.cardReader.StartRead();
                app.dismissProgressDialog();
            }
            @Override
            public void onFailure(Call<BarCodeInUseResponse> call, Throwable t) {
                t.printStackTrace();
                Log.e(Consts.TAG, t.toString());
                Crashlytics.logException(t);
                app.dismissProgressDialog();
            }
        });
    }
}
