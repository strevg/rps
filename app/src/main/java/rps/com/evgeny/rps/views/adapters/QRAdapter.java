package rps.com.evgeny.rps.views.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import rps.com.evgeny.rps.databinding.ViewHolderQrBinding;
import rps.com.evgeny.rps.models.invitations.InvitationModel;
import rps.com.evgeny.rps.utils.StringUtils;
import rps.com.evgeny.rps.views.AdapterInterface;
import rps.com.evgeny.rps.views.onItemClickListener;
import rps.com.evgeny.rps.views.viewholders.QRViewHolder;

/**
 * Created by Evgeny on 07.06.2018.
 */

public class QRAdapter extends RecyclerView.Adapter<QRViewHolder> implements AdapterInterface<InvitationModel> {
    List<InvitationModel> items;
    onItemClickListener<InvitationModel> clickListener;

    public QRAdapter() {
        items = new ArrayList<>();
    }

    public void setClickListener(onItemClickListener listener) {
        this.clickListener = listener;
    }

    @Override
    public QRViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewHolderQrBinding binding = ViewHolderQrBinding.inflate(inflater, parent, false);
        return new QRViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(QRViewHolder holder, int position) {
        final InvitationModel model = items.get(position);
        holder.setData(model);
        holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onItemClick(v, model);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onAdd(InvitationModel item) {
        int position = items.size();
        items.add(position, item);
        notifyItemInserted(position);
    }

    public void onRemove(InvitationModel model) {
        if (model == null)
            return;
        int pos = items.indexOf(model);
        items.remove(pos);
        notifyItemRemoved(pos);
    }

    @Override
    public void onRemove(int pos) {
        if (pos < 0)
            return;
        items.remove(pos);
        notifyItemRemoved(pos);
    }

    @Override
    public void onRemoveAll() {
        int count = getItemCount();
        items.clear();
        notifyItemRangeRemoved(0, count);
    }

    @Override
    public void onAddAll(List<InvitationModel> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public int findItem(String query) {
        for (int i=0; i<items.size(); i++) {
            InvitationModel item = items.get(i);
            if (StringUtils.isContains(item.GuestName, query)
                    || StringUtils.isContains(item.GuestName, query)
                    || StringUtils.isContains(item.GuestPlateNumber, query)
                    || StringUtils.isContains(item.GuestCarModel, query))
                return i;
        }
        return -1;
    }
}
