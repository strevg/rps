package rps.com.evgeny.rps.di;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import rps.com.evgeny.rps.activities.AdminMainActivity;
import rps.com.evgeny.rps.activities.CallingActivity;
import rps.com.evgeny.rps.fragments.alarms.AlarmsModule;
import rps.com.evgeny.rps.fragments.calling.CallingModule;
import rps.com.evgeny.rps.fragments.main.MainAdminModule;
import rps.com.evgeny.rps.fragments.qr.QRModule;
import rps.com.evgeny.rps.fragments.reading.ReadingModule;
import rps.com.evgeny.rps.fragments.search_card.SearchCardModule;
import rps.com.evgeny.rps.fragments.settings.MenuModule;
import rps.com.evgeny.rps.fragments.warning.active.WarningActiveModule;
import rps.com.evgeny.rps.fragments.warning.completed.WarningCompletedModule;
import rps.com.evgeny.rps.fragments.warning.tabs.WarningTabsModule;
import rps.com.evgeny.rps.fragments.zone.ZoneDevicesModule;
import rps.com.evgeny.rps.fragments.zone.details.AlarmModule;
import rps.com.evgeny.rps.fragments.zone.details.ControlModule;
import rps.com.evgeny.rps.fragments.zone.details.ZoneDetailsModule;

/**
 * Created by Evgeny on 24.05.2018.
 */

@Module
abstract class ActivityBindingModule {
    @ActivityScoped
    @ContributesAndroidInjector(modules = {WarningActiveModule.class, WarningTabsModule.class,
            WarningCompletedModule.class, ZoneDevicesModule.class, QRModule.class, ControlModule.class,
            ZoneDetailsModule.class, AlarmModule.class, MainAdminModule.class, ReadingModule.class,
            MenuModule.class, AlarmsModule.class, SearchCardModule.class})
    abstract AdminMainActivity adminMainActivity();

    @ActivityScoped
    @ContributesAndroidInjector (modules = {QRModule.class, CallingModule.class})
    abstract CallingActivity callingActivity();
}
