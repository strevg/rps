package rps.com.evgeny.rps.models.zones_devices;

import rps.com.evgeny.rps.models.network.AlarmZoneModel;
import rps.com.evgeny.rps.utils.ViewUtils;

/**
 * Created by Evgeny on 09.06.2018.
 */

public class AlarmModel {
    AlarmZoneModel model;

    public AlarmModel() {}

    public AlarmModel(AlarmZoneModel model) {
        this.model = model;
    }

    public boolean isEmpty() {
        return model==null;
    }

    public String getCode() {
        return String.valueOf(model.TypeId);
    }

    public int getColor() {
        return ViewUtils.getColorByIndex(model.AlarmColorId);
    }

    public String getText() {
        return model.Value;
    }

    public String getTitle() {
        return model.Name;
    }
}
