package rps.com.evgeny.rps.models.network;

import java.util.ArrayList;
import java.util.UUID;

import rps.com.evgeny.rps.models.list.ZoneModel;

/**
 * Created by Evgeny on 22.07.2018.
 */

public class RackWorkingModeResponse {
    UUID DeviceId;
    String ZoneBeforeId;
    int ZoneAfterId;
    String DispenserTypeId;
    String BarcodeTypeId;
    String BankModuleTypeId;
    int MaxBusyTimeAntennaA;
    int MaxBusyTimeAntennaB;
    int MaxBusyTimeIR;
    int _IsDeleted;
    String Sync;
    String IdStoyky;
    boolean SlaveExist;
    boolean ReaderOutExists;
    String OutReaderTypeId;
    boolean ReaderInExists;
    String InReaderTypeId;
    boolean DispensExists;
    boolean FeederExists;
    String FeederTypeId;
    boolean BarcodeExist;
    boolean BankModuleExist;
    boolean DisplayExists;
    int DisplayTypeId;
    int ComDisplay;
    int ComSlave;
    int ComReadOut;
    int ComReadIn;
    int ComDispens;
    int ComFeeder;
    String ComBarcode;
    int PortDiscret1;
    int PortDiscret2;
    int PortDiscret3;
    int PlateNumberUse;
    int TimeCardV;
    int TimeBarrier;
    int NumerSector;
    int TarifIdScheduleId;
    String TarifPlanId;
    String ClientGroupidToC;
    int TimeNotLoopA;
    int TimeNotIr;
    boolean LoopA;
    boolean LoopB;
    boolean IR;
    boolean PUSH;
    boolean Debug;
    boolean SlaveDebug;
    boolean ReaderOutDebug;
    boolean ReaderInDebug;
    boolean DispensDebug;
    boolean FeederDebug;
    boolean checkBoxItog;
    int logNastroy;
    String CardKey;
    String WebCameraIP;
    String WebCameraLogin;
    String WebCameraPassword;
    int TransitTypeId;
    int RackWorkingModeId;
    String ZoneId;
    String ServerURL;
    boolean RFIDExists;
    int ComRFID;
    String id;
    boolean editable;
    boolean editable_fields;
    ArrayList<Object> DisplayZones;
    public RackWorkingModeModel RackWorkingMode;
    Device Device;
    TransitType TransitType;
    ZoneModel ZoneAfter;
    ZoneModel Zone;
    ArrayList<RackWorkingModeModel> RackWorkingModes;
    String RackWorkingModeName;
    String search;
    Device DeviceModel;
    String ServerCameraIP;
    UUID Id;
    int Type;
    String Name;
    String Host;
    String User;
    String Password;
    String Advanced;
    String DataBase;
    String SlaveCode;

    public class TransitType {
        String Id;
        String Name;
        int _IsDeleted;
    }

    public class Device {
        UUID Id;
        int Type;
        String Name;
        String Host;
        String User;
        String Password;
        String Advanced;
        String _IsDeleted;
        String DataBase;
        String Sync;
        String SlaveCode;
    }
}
