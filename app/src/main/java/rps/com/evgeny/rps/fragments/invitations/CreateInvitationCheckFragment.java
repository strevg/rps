package rps.com.evgeny.rps.fragments.invitations;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rps.com.evgeny.rps.MainApplication;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.databinding.CreateInvationFragmentBinding;
import rps.com.evgeny.rps.databinding.CreateInvitationCheckFragmentBinding;
import rps.com.evgeny.rps.models.invitations.CreateResponse;
import rps.com.evgeny.rps.network.ApiFactory;
import rps.com.evgeny.rps.network.RPSApi;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.Settings;
import rps.com.evgeny.rps.utils.StringUtils;

/**
 * Created by Evgeny on 11.10.2017.
 */

public class CreateInvitationCheckFragment extends Fragment implements View.OnClickListener{
    CreateInvitationCheckFragmentBinding binding;
    MainApplication app;
    RPSApi rpsApi;

    CreateInvitationHelper createHelper;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (MainApplication)getActivity().getApplication();
        rpsApi = ApiFactory.getRpsApi(Settings.siteName+"/");

        createHelper = CreateInvitationHelper.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.create_invitation_check_fragment, container, false);
        binding.createInvitation.setOnClickListener(this);
        binding.changeBarcode.setOnClickListener(this);
        binding.changeInvitation.setOnClickListener(this);
        View v = binding.getRoot();
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setInformation();
    }

    @Override
    public void onResume() {
        super.onResume();
        app.getMainActivity().setTitle(getString(R.string.title_select_create_invitation));
        app.getMainActivity().setSubtitle(getString(R.string.title_step_3));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.changeBarcode:
                app.getMainActivity().onBackStack(2);
                break;
            case R.id.changeInvitation:
                app.getMainActivity().onBackStack(1);
                break;
            case R.id.createInvitation:
                createInvitation();
                break;
        }
    }

    void createInvitation() {
        app.showProgressDialog("Загрузка с сервера...");
        rpsApi.createInvitation(getInvitation(), ApiFactory.getFormDataHeaders()).enqueue(new Callback<CreateResponse>() {
            @Override
            public void onResponse(Call<CreateResponse> call, Response<CreateResponse> response) {
                if (response.isSuccessful()) {
                    Log.i(Consts.TAG, "Completed create invitation");
                    Toast.makeText(app.getMainActivity(), "Приглашение отправлено!", Toast.LENGTH_SHORT).show();
                    app.getMainActivity().replaceFragment(Consts.FragmentType.INVITATIONS, false, null);
                    app.getMainActivity().showBackButton(false);
                }
                app.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<CreateResponse> call, Throwable t) {
                Log.e(Consts.TAG, "createInvitation " + t.toString());
                Crashlytics.logException(t);
                app.dismissProgressDialog();
            }
        });
    }

    void setInformation() {
        Integer index = createHelper.getBarcode().index;
        binding.barcodeName.setText("Штрихкод " + (index!=null ? index : ""));
        binding.barcodeDescription.setText(createHelper.getBarcode().Description);

        Map<String, String> invitations = createHelper.getInvitations();
        String name = invitations.get("dataCreate[surname]") + " " + invitations.get("dataCreate[name]")
                + " " + invitations.get("dataCreate[middle_name]");
        binding.name.setText(name);
        binding.email.setText(invitations.get("dataCreate[email]"));
        binding.carNumber.setText(invitations.get("dataCreate[state_number]"));
        binding.carType.setText(invitations.get("dataCreate[brand]"));
    }

    Map<String, String> getInvitation() {
        Map<String, String> invitations = createHelper.getInvitations();
        invitations.put("id",  createHelper.getBarcode().Id.toString());
        return invitations;
    }
}
