package rps.com.evgeny.rps.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rps.com.evgeny.rps.MainApplication;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.activities.MainActivity;
import rps.com.evgeny.rps.models.BarCodeInUse;
import rps.com.evgeny.rps.models.BarCodeInUseResponse;
import rps.com.evgeny.rps.models.CardModel;
import rps.com.evgeny.rps.network.ApiFactory;
import rps.com.evgeny.rps.network.RPSApi;
import rps.com.evgeny.rps.nfc.CardData;
import rps.com.evgeny.rps.nfc.CardReader;
import rps.com.evgeny.rps.nfc.CardReaderHelper;
import rps.com.evgeny.rps.nfc.CardReaderResponse;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.Settings;
import rps.com.evgeny.rps.views.adapters.ReadDataAdapter;

/**
 * Created by Evgeny on 31.08.2017.
 */

public class ReadDataFragment extends Fragment {
    RecyclerView recyclerView;
    ReadDataAdapter adapter;
    RecyclerView.LayoutManager layoutManager;
    public CardReaderResponse card;
    MainApplication app;
    MainActivity activity;
    RPSApi rpsApi;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (MainApplication)getActivity().getApplication();
        activity = app.getMainActivity();
        rpsApi = ApiFactory.getRpsApi(Settings.siteName+"/");

        adapter = new ReadDataAdapter(getActivity(), new ArrayList<BarCodeInUse>());
        LoadBarcode();

        Settings.card = card;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.list_layout, container, false);
        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                app.showProgressDialog("Загрузка с сервера...");
            }
        }, 30);

    }

    @Override
    public void onResume() {
        super.onResume();
        app.getMainActivity().setTitle(getString(R.string.title_sales));
        app.getMainActivity().setSubtitle(null);
    }

    public void Write(final String txt) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.onAdd(new BarCodeInUse(txt));
            }
        });
    }

    public void LoadBarcode() {
        try {
            rpsApi.listBarCode(card.CardId, card.ParkingEnterTime, ApiFactory.getHeaders()).enqueue(new Callback<List<BarCodeInUse>>() {
                @Override
                public void onResponse(Call<List<BarCodeInUse>> call, Response<List<BarCodeInUse>> response) {
                    if (response.isSuccessful()) {
                        List<BarCodeInUse> data = response.body();
                        if (data.size()==1) {
                            try {
                                writeCard(data.get(0));
                            } catch (Exception e) {
                                Log.e(Consts.TAG, "LoadBarcode " + e.toString());
                                Crashlytics.logException(e);
                                app.dismissProgressDialog();
                            }
                        } else {
                            for (BarCodeInUse barCodeModel : data) {
                                adapter.onAdd(barCodeModel);
                            }
                            app.dismissProgressDialog();
                        }
                    }
                }

                @Override
                public void onFailure(Call<List<BarCodeInUse>> call, Throwable t) {
                    Log.e(Consts.TAG, "listBarCode " + t.toString());
                    Crashlytics.logException(t);
                    app.dismissProgressDialog();
                }
            });
        } catch(Exception e) {
            app.getMainActivity().replaceFragment(Consts.FragmentType.CARD_READER, null);
            app.dismissProgressDialog();
            Toast.makeText(getActivity(), "Ошибка получения данных!", Toast.LENGTH_SHORT).show();
        }
    }

    public void setCardReaderResponse(CardReaderResponse card) {
        this.card = card;
    }

    public void writeCard(BarCodeInUse barCodeModel) {
        app.showProgressDialog("Запись...");
        CardReaderResponse card = Settings.card;
        CardData data_before = CardReaderHelper.GetParam(card);

        Map<String, String> updateObj = ApiFactory.getUpdateObject(card, barCodeModel);
        card.DateSaveCard=null;
        CardData data = CardReaderHelper.GetParam(card);

        Log.i(Consts.TAG, CardReader.ByteArrayToHexString(CardReaderHelper.dataToWBlock(data_before)));
        Log.i(Consts.TAG, CardReader.ByteArrayToHexString(CardReaderHelper.dataToWBlock(data)));

        String strData = CardReader.ByteArrayToHexString(CardReaderHelper.dataToWBlock(data));

        try {
            int result = Settings.cardReader.WriteData(strData);
            if(result == 0){
                throw new NullPointerException();
            } else {
                updateCard(barCodeModel, card.CardId, updateObj);
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
            app.getMainActivity().replaceFragment(Consts.FragmentType.CARD_READER, null);
            app.getMainActivity().showBackButton(false);

            Toast.makeText(activity, app.getString(R.string.write_error), Toast.LENGTH_SHORT).show();
            app.dismissProgressDialog();
        }
    }

    void handleAfterSuccessUpdateCard(BarCodeInUse barCodeModel) {
        if (Settings.card==null) {
            barCodeModel.Sum = Integer.parseInt(Settings.card.SumOnCard) + barCodeModel.DiscountAmount;
            barCodeModel.ParkingEnterTime = Settings.card.ParkingEnterTime;
            barCodeModel.CardId = Settings.card.CardId;
            barCodeModel.IdBCM = barCodeModel.Id;

            createNew(barCodeModel);
        } else {
            Crashlytics.log("Setting.card = null (handleAfterSuccessUpdateCard)");
            Toast.makeText(activity, app.getString(R.string.write_error), Toast.LENGTH_SHORT).show();
        }
    }

    // ------------ Network ---------------
    void updateCard(final BarCodeInUse barCodeModel, String cardId, Map<String, String> updateObj) {
        Map<String, String> header = ApiFactory.getHeaders();
        rpsApi.updateCard(cardId, updateObj, header).enqueue(new Callback<CardModel>() {
            @Override
            public void onResponse(Call<CardModel> call, Response<CardModel> response) {
                if (!response.isSuccessful()) {
                    Crashlytics.logException(new Exception("updateCard !response.isSuccessful()"));
                    app.getMainActivity().replaceFragment(Consts.FragmentType.CARD_READER, null);
                    app.getMainActivity().showBackButton(false);

                    app.dismissProgressDialog();
                    Toast.makeText(activity, "Ошибка обновления карточки на сервере", Toast.LENGTH_SHORT).show();
                    return;
                } else
                    handleAfterSuccessUpdateCard(barCodeModel);
            }
            @Override
            public void onFailure(Call<CardModel> call, Throwable t) {
                Log.e(Consts.TAG, t.getMessage());
                Crashlytics.logException(t);
                app.dismissProgressDialog();
            }
        });
    }

    void createNew(BarCodeInUse barCodeModel) {
        rpsApi.createNew(barCodeModel, ApiFactory.getHeaders()).enqueue(new Callback<BarCodeInUseResponse>() {
            @Override
            public void onResponse(Call<BarCodeInUseResponse> call, Response<BarCodeInUseResponse> response) {
                if (!response.isSuccessful()) {
                    Crashlytics.logException(new Exception("createNew !response.isSuccessful()"));
                    app.getMainActivity().replaceFragment(Consts.FragmentType.CARD_READER, null);
                    app.getMainActivity().showBackButton(false);

                    app.dismissProgressDialog();
                    Toast.makeText(activity, "Ошибка создания карточки на сервере", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    app.getMainActivity().replaceFragment(Consts.FragmentType.SINGLE_BARCODE, null);
                }
                Log.i(Consts.TAG, "onResponse" + response.body());

                app.dismissProgressDialog();
            }
            @Override
            public void onFailure(Call<BarCodeInUseResponse> call, Throwable t) {
                t.printStackTrace();
                Log.e(Consts.TAG, t.toString());
                Crashlytics.logException(t);
                app.dismissProgressDialog();
            }
        });
    }

}
