package rps.com.evgeny.rps.network;

import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rps.com.evgeny.rps.models.BarCodeInUse;
import rps.com.evgeny.rps.models.BarCodeInUseResponse;
import rps.com.evgeny.rps.models.CardModel;
import rps.com.evgeny.rps.models.SearchCard;
import rps.com.evgeny.rps.models.asterisk.RegistrationRequest;
import rps.com.evgeny.rps.models.asterisk.RegistrationResponse;
import rps.com.evgeny.rps.models.list.GroupModel;
import rps.com.evgeny.rps.models.list.TariffModel;
import rps.com.evgeny.rps.models.list.TariffSchedule;
import rps.com.evgeny.rps.models.list.ZoneModel;
import rps.com.evgeny.rps.models.network.BalanceModel;
import rps.com.evgeny.rps.models.network.RackWorkingModeResponse;
import rps.com.evgeny.rps.models.network.ZoneTableModel;
import rps.com.evgeny.rps.models.person.CardFullModel;
import rps.com.evgeny.rps.models.person.TransactionModel;
import rps.com.evgeny.rps.models.list.ClientModel;
import rps.com.evgeny.rps.models.invitations.CreateResponse;
import rps.com.evgeny.rps.models.invitations.InvitationModel;
import rps.com.evgeny.rps.models.invitations.InvitationsResponseModel;
import rps.com.evgeny.rps.models.invitations.StatisticsModel;
import rps.com.evgeny.rps.models.User;
import rps.com.evgeny.rps.models.list.CompanyModel;
import rps.com.evgeny.rps.models.person.UserModel;

/**
 * Created by Evgeny on 31.08.2017.
 */

public interface RPSApi {
    @Headers("Accept: application/json")
    @GET("/api2/users/authUser/index")
    Call<String> autoLogin(@Header("AuthToken") String token);

    @POST("/api2/login")
    @FormUrlEncoded
    Call<User> login(@Field("LoginForm[username]") String login, @Field("LoginForm[password]") String password,
            @HeaderMap Map<String, String> headers);

    @GET("/api2/clients/UserModel/{id}")
    Call<UserModel> getUser(@Path("id") String id, @HeaderMap Map<String, String> headers);

    @PATCH("/api2/users/Cards/update")
    Call<CardModel> updateCard(@Query("id") String id, @Body Map<String, String> body, @HeaderMap Map<String, String> headers);

    @POST("/api2/users/BarCodeInUse/createNew")
    Call<BarCodeInUseResponse> createNew(@Body BarCodeInUse body, @HeaderMap Map<String, String> headers);

    @GET("/api2/users/barcode/list")
    Call<List<BarCodeInUse>> listBarCode(@Query("CardId") String cardId, @Query("ParkingEnterTime") String time,
                                         @HeaderMap Map<String, String> headers);

    @POST("/api2/users/statistics/full/")
    Call<StatisticsModel> getStatistics(@Query(value = "date", encoded = true ) String date, @HeaderMap Map<String, String> headers);

    @GET("/api2/users/barcodeinuse/index")
    Call<List<InvitationModel>> getInvitations(@HeaderMap Map<String, String> headers);

    @GET("/api2/users/barcodeinuse/show")
    Call<InvitationsResponseModel> getInvitationsByDate(@Query(value = "date", encoded = true) String date, @HeaderMap Map<String, String> headers);

    @GET("/api2/users/barcodeinuse/show")
    Call<InvitationsResponseModel> getInvitationsByName(@Query(value = "search", encoded = true) String search, @HeaderMap Map<String, String> headers);

    @FormUrlEncoded
    @POST("/api2/users/barcodeinuse/create")
    Call<CreateResponse> createInvitation(@FieldMap(encoded = true) Map<String, String> field, @HeaderMap Map<String, String> headers);

    @GET("/api2/users/barcode/index")
    Call<List<BarCodeInUse>> getListBarCodes(@HeaderMap Map<String, String> headers);

    @GET("/api2/clients/Card/Full/{id}")
    Call<CardFullModel> getCardFull(@Path("id") String path, @HeaderMap Map<String, String> headers);

    @GET("/api2/clients/Transactions")//&Time[]=2016-09-02T12:49:24.000Z&Time[]=2017-10-02T12:49:24.000Z")
    Call<List<TransactionModel>> getTransactions(@Query("Cardnumber") String cardNumber, @Query("Time[]") String timeStart,
                                 @Query("Time[]") String timeEnd, @HeaderMap Map<String, String> headers);

    @PATCH("/api2/clients/Cards/{id}")
    Call<CardModel> updateCardParam(@Path("id") String id, @Body Map<String, String> body, @HeaderMap Map<String, String> headers);

    @GET("/api2/ajax/list/Group")
    Call<List<GroupModel>> getGroups(@HeaderMap Map<String, String> headers);

    @GET("/api2/ajax/list/ZoneModel")
    Call<List<ZoneModel>> getZones(@HeaderMap Map<String, String> headers);

    @GET("/api2/ajax/list/Tariffs")
    Call<List<TariffModel>> getTariffs(@HeaderMap Map<String, String> headers);

    @GET("/api2/ajax/list/TariffScheduleModel")
    Call<List<TariffSchedule>> getTariffsSchedule(@HeaderMap Map<String, String> headers);

    @GET("/api2/clients/CompanyModel")
    Call<List<CompanyModel>> getCompany(@HeaderMap Map<String, String> headers);

    @GET("/api2/clients/ClientModel")
    Call<List<ClientModel>> getClient(@HeaderMap Map<String, String> headers);

    //new api
    @GET("/api2/rs/ZoneModel")
    Call<List<ZoneTableModel>> getTableZones(@HeaderMap Map<String, String> headers);

    @GET("/proxy/{ip_port}/SinglePassage?status=Answer")
    Call<ResponseBody> passSingleCar(@Path("ip_port") String ip, @HeaderMap Map<String, String> headers);

    @GET("/proxy/{ip_port}/SinglePassageCancel")
    Call<ResponseBody> cancelSingleCar(@Path("ip_port") String ip, @HeaderMap Map<String, String> headers);

    @FormUrlEncoded
    @POST("/api2/rs/DeviceRackModel")
    Call<RackWorkingModeResponse> changeRackMode(@FieldMap(encoded = true) Map<String, String> field, @HeaderMap Map<String, String> headers);

    @FormUrlEncoded
    @POST("/api2/rs/ActionJournal")
    Call<ResponseBody> changeRackModeReason(@FieldMap(encoded = true) Map<String, String> field, @HeaderMap Map<String, String> headers);

    @GET("/api2/rs/BarCodeInUse")
    Call<List<InvitationModel>> getBarcodes(@HeaderMap Map<String, String> headers);

    @GET("/proxy/{ip_port}/GetBarCode")
    Call<ResponseBody> sendBarcode(@Path("ip_port") String ip, @Query("Id") String id, @HeaderMap Map<String, String> headers);

    @GET("/api2/clients/Cards")
    Call<List<CardModel>> searchCard(@Query("Search") String search, @HeaderMap Map<String, String> headers);

    @FormUrlEncoded
    @POST("/api2/clients/cards/calc")
    Call<BalanceModel> getBalance(@FieldMap(encoded = true) Map<String, String> field, @HeaderMap Map<String, String> headers);

    //Asterisk
    @POST("api2/calls/asteriskCalls/registerStUser")
    Call<ResponseBody> registerStUser(@Body RegistrationRequest body, @HeaderMap Map<String, String> headers);

    @POST("api2/calls/asteriskCalls/startCall")
    Call<ResponseBody> startCall(@Query("username") String user, @Query("calle") String caller, @HeaderMap Map<String, String> headers);

    @POST("api2/calls/asteriskCalls/stopCall")
    Call<ResponseBody> stopCall(@Query("username") String user, @Query("calle") String caller, @HeaderMap Map<String, String> headers);
}
