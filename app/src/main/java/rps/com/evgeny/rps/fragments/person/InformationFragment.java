package rps.com.evgeny.rps.fragments.person;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import fr.ganfra.materialspinner.MaterialSpinner;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.databinding.InformationFragmentBinding;
import rps.com.evgeny.rps.models.person.CardFullModel;
import rps.com.evgeny.rps.nfc.CardReaderResponse;
import rps.com.evgeny.rps.utils.InputFilterMinMax;
import rps.com.evgeny.rps.utils.RightUtils;
import rps.com.evgeny.rps.utils.Settings;
import rps.com.evgeny.rps.utils.StringUtils;

/**
 * Created by Evgeny on 26.09.2017.
 */

public class InformationFragment extends Fragment {
    InformationFragmentBinding binding;
    InformationPresenter presenter;
    View.OnClickListener listener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.information_fragment, container, false);

        presenter = new InformationPresenter(this);
        //presenter.setCardFullModel(Settings.card);

        binding.timeComeIn.setOnClickListener(listener);
        binding.timeRecalculation.setOnClickListener(listener);
        binding.tvp.setOnClickListener(listener);
        binding.nullVP.setOnClickListener(listener);
        binding.nullKVP.setOnClickListener(listener);
        binding.nullAbonement.setOnClickListener(listener);

        binding.sumCard.setOnEditorActionListener(actionListener);
        binding.tkvp.setOnEditorActionListener(actionListener);

        binding.tkvp.setFilters(new InputFilter[]{ new InputFilterMinMax("0", "255")});

        return binding.getRoot();
    }

    EditText.OnEditorActionListener actionListener = new EditText.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_SEARCH ||
                    actionId == EditorInfo.IME_ACTION_DONE ||
                    event.getAction() == KeyEvent.ACTION_DOWN &&
                            event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                if (v.getId() == R.id.sumCard) {
                    presenter.changeData(binding.sumCard.getId(), binding.sumCard.getText().toString());
                } else if (v.getId() == R.id.tkvp) {
                    presenter.changeData(binding.tkvp.getId(), binding.tkvp.getText().toString());
                }
                return true;

            }
            return false;
        }
    };

    public InformationFragmentBinding binding() {
        return binding;
    }

    public void initAdapter(MaterialSpinner spinner, List list) {
        if (list!=null) {
            ArrayAdapter adapter = new ArrayAdapter<>(getActivity(), R.layout.view_spinner_item, list);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);

            spinner.setOnItemSelectedListener(presenter);

            if (!RightUtils.hasRights(spinner.getId())) {
                spinner.setEnabled(false);
                spinner.setClickable(false);
            }
        }
    }

    public void setSelectedItem(MaterialSpinner spinner, int pos) {
        if (pos!=-1 && spinner.getAdapter().getCount()>0)
            spinner.setSelection(pos+1);
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    public void setData(CardReaderResponse cardInfo) {
        setText(binding.timeComeIn, cardInfo.ParkingEnterTime);
        setText(binding.timeRecalculation, cardInfo.LastRecountTime);
        setText(binding.tvp, cardInfo.TVP);
        setText(binding.tkvp, cardInfo.TKVP);
        setText(binding.nullVP, cardInfo.Nulltime1);
        setText(binding.nullKVP, cardInfo.Nulltime2);
        setText(binding.nullAbonement, cardInfo.Nulltime3);
        setText(binding.sumCard, cardInfo.SumOnCard);
        setText(binding.numberPlateClient, cardInfo.LastPlate);
    }

    void setText(EditText editText, String value) {
        if (!StringUtils.isNullOrEmpty(value)) {
            if (editText.getId() == R.id.tvp) {
                value = value.substring(0, 19);
            }

            String time = value;
            if (editText.getId()!=R.id.numberPlateClient) {
                time = value.replace(" ", " в ");
            }
            editText.setText(time);
        }
        if (!RightUtils.hasRights(editText.getId())) {
            editText.setFocusable(false);
            editText.setClickable(false);
            editText.setEnabled(false);
        }
    }

    void setBalance(String value) {
        binding.balanceClient.setText(value);
    }
}
