package rps.com.evgeny.rps.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;

import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.activities.LoginActivity;
import rps.com.evgeny.rps.utils.ViewUtils;

public class PushNotificationManager {
    private static PushNotificationManager instance;
    private Context context;
    private int id = 0;

    private PushNotificationManager(Context context) {
        this.context = context;
    }

    public static PushNotificationManager getInstance(Context context) {
        if (instance==null) {
            instance = new PushNotificationManager(context);
        }
        return instance;
    }

    public void showNotification(String title, String text, int errorCode, int errorNumber) {
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        String channelId = "channel-01";
        String channelName = "Channel Name";
        int importance = NotificationManager.IMPORTANCE_HIGH;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId, channelName, importance);
            notificationManager.createNotificationChannel(channel);
        }

        RemoteViews contentView = new RemoteViews(context.getPackageName(), R.layout.viewholder_notification);
        contentView.setImageViewResource(R.id.errorImage, ViewUtils.getDrawableByIndex(errorCode));
        contentView.setTextViewText(R.id.title, title);
        contentView.setTextViewText(R.id.text, text);
        contentView.setTextViewText(R.id.errorText, String.valueOf(errorNumber));

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContent(contentView);

        Intent resultIntent = new Intent(context, LoginActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(LoginActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        builder.setContentIntent(resultPendingIntent);

        Notification notification = builder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notificationManager.notify(id++, notification);
    }
}
