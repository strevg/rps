package rps.com.evgeny.rps.views;

import android.view.View;

/**
 * Created by Evgeny on 22.09.2017.
 */

public interface onItemClickListener<T> {
    void onItemClick(View v, T item);
}
