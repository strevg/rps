package rps.com.evgeny.rps.utils;

import android.content.Context;
import android.text.TextUtils;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Evgeny on 03.09.2017.
 */

public class StringUtils {
    public static boolean isNullOrEmpty (String value) {
        return (value==null || value.isEmpty() || value.equals("") || value.equals("null") || value.equals(" "));
    }

    public static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static String getRandomString() {
        final String characters = "ABCEKHMOPTXY1234567890";
        int randomNum = ThreadLocalRandom.current().nextInt(0, 5 + 1);
        StringBuilder result = new StringBuilder();
        while(randomNum > 0) {
            Random rand = new Random();
            result.append(characters.charAt(rand.nextInt(characters.length())));
            randomNum--;
        }
        return result.toString();
    }

    public static String getTimeCallConnected(int sec) {
        int minutes = sec / 60;
        int seconds = sec % 60;

        StringBuffer buffer = new StringBuffer(String.format("%02d:", minutes));
        buffer.append(String.format("%02d", seconds));
        return buffer.toString();
    }

    public static String getZoneTitle(String name, int freePlace) {
        return name + " (" + freePlace+")";
    }

    public static boolean isContains(String source, String text) {
        return source.toLowerCase().contains(text.toLowerCase());
    }

    public static String getAsteriskLogin() {
        int time = (Math.abs((int)new Date().getTime())) % 1000000;
        return "sip_mobile_" + getNameDomain() + "_" + time;
    }

    public static String getAsteriskPassword() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        char tempChar;
        for (int i = 0; i < 8; i++){
            tempChar = (char) (generator.nextInt(25) + 65);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }

    public static String getNameDomain() {
        int pos = Settings.domain.indexOf(".");
        return Settings.domain.substring(0, pos);
    }

    public static boolean isValidLinphoneLogin() {
        return (Settings.linphoneLogin!=null && Settings.linphoneLogin.contains(getNameDomain()));
    }

    public static String getSipServer() {
        return "voip." + Settings.siteName.replace(Settings.getProtocole(), "") + ":6153";
    }

    public static String getWebSocketName() {
        return "wss://"+ getNameDomain() + ".r-p-s.ru/ws/socket.io/?EIO=3&transport=websocket";
    }
}
