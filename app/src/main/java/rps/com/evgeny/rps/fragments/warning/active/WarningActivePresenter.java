package rps.com.evgeny.rps.fragments.warning.active;

import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rps.com.evgeny.rps.activities.CallingActivity;
import rps.com.evgeny.rps.di.ActivityScoped;
import rps.com.evgeny.rps.fragments.warning.adapters.WarningAdapter;
import rps.com.evgeny.rps.fragments.zone.adapter.BaseType;
import rps.com.evgeny.rps.fragments.zone.adapter.CallingModel;
import rps.com.evgeny.rps.fragments.zone.adapter.ZoneBottomAdapter;
import rps.com.evgeny.rps.models.WarningAddedEventBus;
import rps.com.evgeny.rps.models.network.AlarmWebSocketModel;
import rps.com.evgeny.rps.models.network.AlarmZoneModel;
import rps.com.evgeny.rps.models.network.DeviceZoneModel;
import rps.com.evgeny.rps.models.network.WebSocketModel;
import rps.com.evgeny.rps.models.network.ZoneTableModel;
import rps.com.evgeny.rps.models.warnings.WarningComparator;
import rps.com.evgeny.rps.models.warnings.WarningModel;
import rps.com.evgeny.rps.network.ApiFactory;
import rps.com.evgeny.rps.network.RPSApi;
import rps.com.evgeny.rps.services.ControlManager;
import rps.com.evgeny.rps.services.ProximityManager;
import rps.com.evgeny.rps.services.RealmManager;
import rps.com.evgeny.rps.services.RpsWebSocket;
import rps.com.evgeny.rps.services.linphone.LinphoneManager;
import rps.com.evgeny.rps.services.linphone.callback.PhoneListener;
import rps.com.evgeny.rps.utils.Consts;
import rps.com.evgeny.rps.utils.Settings;
import rps.com.evgeny.rps.views.onItemClickListener;

/**
 * Created by Evgeny on 24.05.2018.
 */

@ActivityScoped
public class WarningActivePresenter implements WarningActiveContract.Presenter, onItemClickListener<WarningModel>,
        ZoneBottomAdapter.onCallFinish, WarningAdapter.onCallingInterface, PhoneListener,
        RpsWebSocket.WebSocketMessageListener, ProximityManager.ProximityCallback {
    @Nullable
    WarningActiveContract.View view;

    WarningAdapter adapter;
    ZoneBottomAdapter bottomAdapter;

    ControlManager controlManager;
    LinphoneManager linphoneManager;
    RealmManager realmManager;
    ProximityManager proximityManager;
    RPSApi rpsApi;

    @Inject
    WarningActivePresenter() {
        rpsApi = ApiFactory.getRpsApi(Settings.siteName + "/");
    }

    @Override
    public void takeView(WarningActiveContract.View view) {
        this.view = view;
        adapter = new WarningAdapter();
        view.setAdapter(adapter);
        adapter.setOnClickListener(this);
        adapter.setOnCallingListener(this);

        bottomAdapter = new ZoneBottomAdapter();
        bottomAdapter.setOnCallListener(this);
        view.setAdapterBottom(bottomAdapter);

        controlManager = ControlManager.getInstance();

        downloadZoneDevices();
        this.realmManager = RealmManager.getInstance();

        linphoneManager = LinphoneManager.getInstance(view.getContext());
        proximityManager = ProximityManager.getInstance(view.getContext());
    }

    @Override
    public void setPhoneListener() {
        linphoneManager.setPhoneListener(this);
        controlManager.setListener(this);
        checkActiveCall();
    }

    @Override
    public void dropView() {
        this.view = null;
        EventBus.getDefault().unregister(this);
        controlManager.removeListener(this);
    }

    void checkActiveCall() {
        bottomAdapter.onRemoveAll();
        if (existsActiveCall()) {
            String id = linphoneManager.getCalls()[0].toString();
            showActiveCalling(id);
            onCallConnect(id);
        }
    }

    boolean existsActiveCall() {
        return (rps.com.evgeny.rps.services.linphone.linphone.LinphoneManager.isInstanceiated()
                && linphoneManager.getCalls()!=null && linphoneManager.getCalls().length>0);
    }

    void downloadZoneDevices() {
        view.showProgressBar(true);
        rpsApi.getTableZones(ApiFactory.getHeaders()).enqueue(new Callback<List<ZoneTableModel>>() {
            @Override
            public void onResponse(Call<List<ZoneTableModel>> call, Response<List<ZoneTableModel>> response) {
                if (response.isSuccessful()) {
                    adapter.onAddAll(initData(response.body()));
                }
                Log.i(Consts.TAG, response.body().toString());
                view.showProgressBar(false);
            }

            @Override
            public void onFailure(Call<List<ZoneTableModel>> call, Throwable t) {
                Log.e(Consts.TAG, t.getMessage());
                view.showProgressBar(false);
            }
        });
    }

    List<BaseType> initData(List<ZoneTableModel> list) {
        List<WarningModel> items = new ArrayList<>();
        for (ZoneTableModel zone : list) {
            for (DeviceZoneModel deviceZone : zone.DeviceModel) {
                for (AlarmZoneModel alarmZone : deviceZone.alarms) {
                    if (!realmManager.isExist(alarmZone.getId()))
                        items.add(new WarningModel(alarmZone));
                }
            }
        }
        Collections.sort(items, new WarningComparator());

        List<BaseType> baseList = new ArrayList<>();
        for (WarningModel model : items) {
            baseList.add(model);
        }

        return baseList;
    }

    // -------- WebSocketMessageListener ------------
    @Override
    public void onMessage(final WebSocketModel model) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (model.getMessageType() == Consts.MessageType.ALARM) {
                    if (model.getAlarm().End!=null) {
                        onCloseAlarm(model.getAlarm());
                    } else {
                        onAddAlarm(model.getAlarm());
                    }
                } else if (model.getMessageType()==Consts.MessageType.CALLING) {
                    DeviceZoneModel device = controlManager.getDevice(model.getId());
                    adapter.onAdd(new CallingModel(model.getId(), device.Name));
                    view.scrollToPosition(0);
                }
            }
        });
    }

    void testActivity(String id) {
        Intent intent = new Intent(view.getContext(), CallingActivity.class);
        intent.putExtra(Consts.PREF_DEVICE_ID, id);
        view.getContext().startActivity(intent);
    }

    void onAddAlarm(final AlarmWebSocketModel alarm) {
        adapter.onAdd(new WarningModel(alarm));
        view.scrollToPosition(0);
        Log.i(Consts.TAG, "onAddAlarm");
    }

    void onCloseAlarm(final AlarmWebSocketModel alarm) {
        int index = adapter.getIndex(alarm.getId());
        view.scrollToPosition(index!=-1?index:0);
        adapter.onRemove(alarm.getId());
        realmManager.add(RealmManager.getRealmModel(new AlarmZoneModel(alarm)));
        EventBus.getDefault().post(new WarningAddedEventBus());
        Log.i(Consts.TAG, "onCloseAlarm");
    }

    void showActiveCalling(final String id) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                adapter.onAdd(new CallingModel(id, "Вызов с устройства"));
                view.scrollToPosition(0);
            }
        });
    }

    @Override
    public void onItemClick(View v, WarningModel item) {
        adapter.onRemove(item);
        realmManager.add(RealmManager.getRealmModel(item.getModel()));
        EventBus.getDefault().post(new WarningAddedEventBus());
    }

    @Override
    public void onFinish(CallingModel model) {
        /*bottomAdapter.onRemove(model.getId());
        realmManager.add(RealmManager.getRealmModel(model.getId(), model.getText()));
        EventBus.getDefault().post(new WarningAddedEventBus());
        */
        linphoneManager.onCancel();
    }

    @Override
    public void onAnswer(CallingModel model) {
        if (model.getId().length()==36) {
            adapter.onRemove(model);
            DeviceZoneModel device = controlManager.getDevice(model.getId());
            controlManager.setDevice(device);
            controlManager.onCall(device.Host);
        } else {
            linphoneManager.onAnswer();
        }
    }

    @Override
    public void onCancel(CallingModel model) {
        if (model.getId().length()==36) {
            adapter.onRemove(model);
            realmManager.add(RealmManager.getRealmModel(model.getId(), model.getText()));
            EventBus.getDefault().post(new WarningAddedEventBus());
        } else {
            linphoneManager.onCancel();
        }
    }

    void onFinishCall(String id) {
        int bottomPos = bottomAdapter.getIndex(id);
        if (bottomPos!=-1) {
            bottomAdapter.onRemove(id);
        } else {
            adapter.onRemove(id);
        }
        realmManager.add(RealmManager.getRealmModel(id, "Вызов с устройства"));
        EventBus.getDefault().post(new WarningAddedEventBus());
    }

    // ------- linphone listener -------
    @Override
    public void onCallStart(final String id) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (bottomAdapter.getIndex(id)==-1)
                    bottomAdapter.onAdd(new CallingModel(id, controlManager.getDevice().Name, true));
            }
        });
    }

    @Override
    public void onCallConnect(final String id) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                adapter.onRemove(id);

                DeviceZoneModel device = controlManager.getDevice();
                CallingModel model = new CallingModel(id, device.Name, true);
                bottomAdapter.onAdd(model);
            }
        });
    }

    @Override
    public void onCallFinish(final String id) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                onFinishCall(id);
            }
        });
    }

    @Override
    public void onIncomingCall(String id) {
        onAnswer(new CallingModel(id));
    }

    @Override
    public void onError(String message) {
        Log.e(Consts.TAG, message);
    }

    @Override
    public void approachSensor() {
        if (existsActiveCall() && view!=null) {
            view.shadowCallingScreen(true);
        }
    }

    @Override
    public void distanceFromSensor() {
        if (existsActiveCall() && view!=null) {
            view.shadowCallingScreen(false);
        }
    }
}
