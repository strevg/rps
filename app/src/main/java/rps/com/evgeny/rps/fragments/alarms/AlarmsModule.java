package rps.com.evgeny.rps.fragments.alarms;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import rps.com.evgeny.rps.di.ActivityScoped;
import rps.com.evgeny.rps.di.FragmentScoped;

@Module
public abstract class AlarmsModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract AlarmsFragment alarmsFragment();

    @ActivityScoped
    @Binds
    abstract AlarmsContract.Presenter alarmsPresenter(AlarmsPresenter presenter);
}
