package rps.com.evgeny.rps.views.viewholders;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import rps.com.evgeny.rps.databinding.BarcodeViewHolderBinding;
import rps.com.evgeny.rps.databinding.ViewholderWarningBinding;
import rps.com.evgeny.rps.models.realm.WarningRealm;
import rps.com.evgeny.rps.models.warnings.WarningModel;

/**
 * Created by Evgeny on 24.05.2018.
 */

public class WarningViewHolder extends RecyclerView.ViewHolder {
    public ViewholderWarningBinding binding;
    Context context;

    public WarningViewHolder(View view) {
        super(view);
        binding = DataBindingUtil.bind(view);
        this.context = view.getContext();
    }

    public void hideButton(boolean hide) {
        binding.buttonWarning.setVisibility(hide?View.INVISIBLE:View.VISIBLE);
        binding.doneImage.setVisibility(hide?View.GONE:View.VISIBLE);
        binding.doneAllImage.setVisibility(!hide?View.GONE:View.VISIBLE);
    }

    public void setData(WarningModel data) {
        if (!data.isEmpty()) {
            setTitle(data.getTitle());
            setText(data.getText());
            setCode(data.getCode(), context.getResources().getColor(data.getColor()));
        }
    }

    public void setData(WarningRealm data) {
        try {
            if (data != null) {
                setTitle(data.getTitle());
                setText(data.getText());
                setCode(String.valueOf(data.getCode()), context.getResources().getColor(data.getColor()));
            }
        } catch (Exception e) {}
    }

    public void setCode(String code, int color) {
        binding.codeWarning.setText(code);
        binding.codeWarning.setColor(color);
    }

    public void setTitle(String title) {
        binding.titleWarning.setText(title);
    }

    public void setText(String text) {
        binding.textWarning.setText(text);
    }

    public void setOnClickListener(View.OnClickListener listener) {
        binding.buttonWarning.setOnClickListener(listener);
    }
}
