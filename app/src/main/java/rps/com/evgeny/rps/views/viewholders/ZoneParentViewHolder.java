package rps.com.evgeny.rps.views.viewholders;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.databinding.ViewHolderZoneParentBinding;
import rps.com.evgeny.rps.models.zones_devices.ZoneParentModel;
import rps.com.evgeny.rps.utils.Settings;
import rps.com.evgeny.rps.views.adapters.expandable.ParentViewHolder;

/**
 * Created by Evgeny on 30.05.2018.
 */

public class ZoneParentViewHolder extends ParentViewHolder {
    public ViewHolderZoneParentBinding binding;
    Context context;
    ZoneParentModel model;

    public  ZoneParentViewHolder(View itemView) {
        super(itemView);
        context = itemView.getContext();
        binding = DataBindingUtil.bind(itemView);
        super.setIconExpandCollapseListener(new ParentListItemExpandCollapseListener() {
            @Override
            public void onParentListItemExpanded(int position) {
                setExpandedIcon(false);
                Settings.removeOpenedTab(context, model.getId());
            }

            @Override
            public void onParentListItemCollapsed(int position) {
                setExpandedIcon(true);
                Settings.addOpenedTab(context, model.getId());
            }
        });
    }

    public void setData(ZoneParentModel model) {
        this.model = model;
        setTitle(model.getTitle());
        setNumber(model.getCount()+"");
    }

    public void setExpandedIcon(boolean show) {
        binding.iconDown.setVisibility(show?View.GONE:View.VISIBLE);
        binding.iconUp.setVisibility(!show?View.GONE:View.VISIBLE);
    }

    public void setTitle(String title) {
        binding.title.setText(title);
    }

    public void setNumber(String number) {
        binding.number.setText(number);
    }


}
