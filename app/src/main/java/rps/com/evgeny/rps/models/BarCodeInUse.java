package rps.com.evgeny.rps.models;

import java.util.UUID;

/**
 * Created by Evgeny on 30.08.2017.
 */

public class BarCodeInUse {
    public UUID Id;
    public UUID BarCodeUseId;
    public UUID IdBCM;
    public UUID OperId;
    public UUID CompanyId;
    public Byte IdTP;
    public Byte IdTS;
    public Integer DiscountAmount;
    public Byte GroupIdFC;
    public String UsageTime;
    public String sync;
    public String GuestPlateNumber;
    public String GuestCarModel;
    public String GuestName;
    public String GuestEmail;
    public Integer ParkingTime;
    public String Comment;
    public String Description;
    public String CreatedAt;
    public UUID EnterId;
    public UUID ExitId;
    public UUID IdTPForCount;
    public UUID IdTSForCount;
    public String CardId;
    public Integer Ammount;
    public Boolean Used;
    public String ActivatedAt;
    public String ParkingEnterTime;
    public UUID TPForRent;
    public UUID TSForRent;
    public Boolean active;
    public Integer AnySum;
    public Integer index;
    public Integer Sum;
    public String url;

    public BarCodeInUse(String text) {
        this.index = 1;
        this.Description = text;
    }

    public BarCodeInUse(UUID id) {
        this.Id = id;
    }
}