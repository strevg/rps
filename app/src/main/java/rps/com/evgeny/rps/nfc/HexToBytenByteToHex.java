package rps.com.evgeny.rps.nfc;

/**
 * Created by Evgeny on 01.09.2017.
 */

public class HexToBytenByteToHex {
    public static int GetByteCount(String hexString) {
        int numHexChars = 0;
        // remove all none A-F, 0-9, characters
        for ( char c : hexString.toCharArray()) {
            if (IsHexDigit(c))
                numHexChars++;
        }
        // if odd number of characters, discard last character
        if (numHexChars % 2 != 0) {
            numHexChars--;
        }
        return numHexChars / 2; // 2 characters per byte
    }

    public static byte[] GetBytes(String hexString, Integer discarded) {
        discarded = 0;
        String newString = "";
        // remove all none A-F, 0-9, characters
        for ( char c : hexString.toCharArray()) {
            if (IsHexDigit(c))
                newString += c;
            else
                discarded++;
        }
        // if odd number of characters, discard last character
        if (newString.length() % 2 != 0) {
            discarded++;
            newString = newString.substring(0, newString.length() - 1);
        }

        int byteLength = newString.length() / 2;
        byte[] bytes = new byte[byteLength];
        String hex;
        int j = 0;
        for (int i = 0; i < bytes.length; i++) {
            hex = new String(new char[] { newString.toCharArray()[j], newString.toCharArray()[j + 1] });
            bytes[i] = HexToByte(hex);
            j = j + 2;
        }
        return bytes;
    }

    public static String ToString(byte[] bytes) {
        String hexString = "";
        for (int i = 0; i < bytes.length; i++) {
            hexString += String.format("%02X", bytes[i]);
        }
        return hexString;
    }

    public static String ToHex(String strvalue) {
        String hexString = "";

        for (int i = 0; i < strvalue.length(); i += 2) {
            if (i > 0) {
                hexString += "I";
            }
            hexString += strvalue.toCharArray()[i];
            hexString += strvalue.toCharArray()[i + 1];
        }
        return hexString;
    }

    public static boolean InHexFormat(String hexString) {
        boolean hexFormat = true;

        for (char digit : hexString.toCharArray()) {
            if (!IsHexDigit(digit)) {
                hexFormat = false;
                break;
            }
        }
        return hexFormat;
    }

    public static boolean IsHexDigit(char c) {
        int numChar;
        int numA ='A';
        int num1 = '0';
        c = Character.toUpperCase(c);
        numChar = c;
        if (numChar >= numA && numChar < (numA + 6))
            return true;
        if (numChar >= num1 && numChar < (num1 + 10))
            return true;
        return false;
    }

    private static byte HexToByte(String hex) {
        if (hex.length() > 2 || hex.length() <= 0)
            throw new NumberFormatException();
        byte newByte = Byte.parseByte(hex, 16);
        return newByte;
    }
}
