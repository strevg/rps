package rps.com.evgeny.rps.fragments.warning.tabs;

import android.support.annotation.Nullable;

import javax.inject.Inject;

import rps.com.evgeny.rps.di.ActivityScoped;
import rps.com.evgeny.rps.fragments.warning.active.WarningActiveContract;

/**
 * Created by Evgeny on 25.05.2018.
 */

@ActivityScoped
public class WarningTabsPresenter implements WarningTabsContract.Presenter{
    @Nullable
    WarningTabsContract.View view;

    @Inject
    WarningTabsPresenter() {
    }

    @Override
    public void takeView(WarningTabsContract.View view) {
        this.view = view;
    }

    @Override
    public void dropView() {
        this.view = null;
    }
}
