package rps.com.evgeny.rps.views.adapters;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.UUID;

import rps.com.evgeny.rps.MainApplication;
import rps.com.evgeny.rps.R;
import rps.com.evgeny.rps.fragments.PriceDialogFragment;
import rps.com.evgeny.rps.models.BarCodeInUse;
import rps.com.evgeny.rps.views.AdapterInterface;
import rps.com.evgeny.rps.views.viewholders.PriceViewHolder;
import rps.com.evgeny.rps.views.onItemClickListener;

/**
 * Created by Evgeny on 03.09.2017.
 */

public class ReadDataAdapter extends RecyclerView.Adapter<PriceViewHolder>
        implements View.OnClickListener, AdapterInterface<BarCodeInUse> {
    public List<BarCodeInUse> Items;
    MainApplication app;
    onItemClickListener<BarCodeInUse> onItemClickListener;

    public ReadDataAdapter(Activity activity, List<BarCodeInUse> items) {
        this.Items = items;
        app = (MainApplication) activity.getApplication();
    }

    public void setOnItemClickListener(onItemClickListener listener) {
        this.onItemClickListener = listener;
    }

    @Override
    public PriceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.price_view_holder, null);
        view.setOnClickListener(this);
        return new PriceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PriceViewHolder holder, int position) {
        BarCodeInUse bm = Items.get(position);
        holder.bm = bm;

        holder.binding.barcodeDescription.setText(bm.Description);
        holder.itemView.setTag(holder);

        if (bm.BarCodeUseId instanceof UUID) {
            holder.binding.barcodeCard.setCardBackgroundColor(Color.rgb(249,183,8));
            holder.binding.barcodeDiscount.setTextColor(Color.WHITE);

            holder.hideButton(holder.binding.changeBarcode);
            holder.hideButton(holder.binding.saveBarcode);

            holder.binding.textActive.setText("Активировано");
            holder.binding.textActive.setGravity(Gravity.CENTER);

            holder.binding.textActive.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
        }

        if (bm.AnySum != 1) {
            holder.hideButton(holder.binding.changeBarcode);
        } else {
            holder.binding.changeBarcode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.binding.changeBarcode.setText(app.getString(R.string.changeBarcodeProgress));
                    FragmentTransaction ft = app.getMainActivity().getFragmentManager().beginTransaction();
                    Fragment prev = app.getMainActivity().getFragmentManager().findFragmentByTag("dialog");
                    if (prev != null) {
                        ft.remove(prev);
                    }
                    ft.addToBackStack(null);
                    // Create and show the dialog.
                    PriceDialogFragment newFragment = PriceDialogFragment.NewInstance(null, holder);
                    newFragment.show(ft, "dialog");
                }
            });
        }

        if (bm.DiscountAmount == 0) {
            holder.binding.barcodeDiscount.setVisibility(View.GONE);
            holder.binding.barcodeDiscount.setMinHeight(0);
            holder.binding.barcodeDiscount.setHeight(0);
            holder.binding.barcodeName.setText("Скидка " + (bm.index!=null ? bm.index : ""));
        } else {
            holder.binding.barcodeName.setText("Скидка " + bm.DiscountAmount + " руб.");
            holder.binding.barcodeDiscount.setText(bm.DiscountAmount + " руб.");
        }
    }

    @Override
    public void onAdd(BarCodeInUse item) {
        int position = Items.size();
        Items.add(position, item);
        notifyItemInserted(position);
    }

    @Override
    public void onRemove(int pos) {
        if (pos < 0)
            return;
        Items.remove(pos);
        notifyItemRemoved(pos);
    }

    @Override
    public void onRemoveAll() {
        int count = getItemCount();
        Items.clear();
        notifyItemRangeRemoved(0, count);
    }

    @Override
    public void onAddAll(List<BarCodeInUse> items) {

    }

    @Override
    public int getItemCount() {
        return Items.size();
    }

    @Override
    public void onClick(View v) {
        if (onItemClickListener == null)
            return;
        PriceViewHolder holder = (PriceViewHolder) v.getTag();
        onItemClickListener.onItemClick(v, holder.bm);
    }
}
