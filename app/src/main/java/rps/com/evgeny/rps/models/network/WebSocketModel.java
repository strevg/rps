package rps.com.evgeny.rps.models.network;

import com.google.gson.Gson;

import rps.com.evgeny.rps.utils.Consts;

/**
 * Created by Evgeny on 18.07.2018.
 */

public class WebSocketModel {
    Consts.MessageType messageType;
    String id;
    AlarmWebSocketModel alarm;
    CallingSocketModel calling;

    public WebSocketModel(String text) {
        this.messageType = getMessageType(text);
        if (messageType == Consts.MessageType.ALARM) {
            alarm = getAlarm(text);
        } else if (messageType == Consts.MessageType.CALLING) {
            calling = getCalling(text);
            this.id = calling.getDeviceId();
        } else {
            this.id = getID(text);
        }
    }

    public WebSocketModel(Consts.MessageType messageType, String id) {
        this.id = id;
        this.messageType = messageType;
    }

    public Consts.MessageType getMessageType() {
        return messageType;
    }

    public String getId() {
        return id;
    }

    public AlarmWebSocketModel getAlarm() {
        return alarm;
    }

    public CallingSocketModel getCalling() {
        return calling;
    }

    Consts.MessageType getMessageType(String text) {
        if (text.contains(Consts.IN_PROCCESS))
            return Consts.MessageType.IN_PROCCESS;
        else if (text.contains(Consts.CANCELED))
            return Consts.MessageType.CANCELED;
        else if (text.contains(Consts.ALARM))
            return Consts.MessageType.ALARM;
        else if (text.contains(Consts.CALLING))
            return Consts.MessageType.CALLING;
        else
            return Consts.MessageType.COMPLETE;
    }

    String getID(String text) {
        int len = text.length();
        return text.substring(len-39, len-3);
    }

    AlarmWebSocketModel getAlarm(String text) {
        int start = text.indexOf("{");
        int end = text.lastIndexOf("}")+1;
        String json = text.substring(start, end);
        return  (new Gson()).fromJson(json, AlarmWebSocketModel.class);
    }

    CallingSocketModel getCalling(String text) {
        int start = text.indexOf("{");
        int end = text.lastIndexOf("}")+1;
        String json = text.substring(start, end);
        return  (new Gson()).fromJson(json, CallingSocketModel.class);
    }
}
