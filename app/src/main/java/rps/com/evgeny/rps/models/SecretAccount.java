package rps.com.evgeny.rps.models;

import com.google.gson.Gson;

import java.nio.charset.Charset;

import javax.crypto.SecretKey;

/**
 * Created by Evgeny on 31.08.2017.
 */

class  SecretAccount implements SecretKey {
    public final static Charset UTF8_CHARSET = Charset.forName("UTF-8");
    byte[] bytes;

    public SecretAccount(String token, User user, String domain) {
        SecurityUser data = new SecurityUser(token, domain, user);
        String str = new Gson().toJson(data);
        bytes = str.getBytes(UTF8_CHARSET);
    }

    @Override
    public String getAlgorithm() {
        return "RAW";
    }

    @Override
    public String getFormat() {
        return "RAW";
    }

    @Override
    public byte[] getEncoded() {
        return bytes;
    }
}